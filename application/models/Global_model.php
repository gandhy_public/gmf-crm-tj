<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Global_model extends CI_Model {

  private $m_pmorderh = 'M_PMORDERH';
  private $tj_m_pmorder = 'TJ_M_PMORDER';
  private $tj_m_pmorder_new = 'TJ_M_PMORDER_NEW';
  private $m_revision = 'M_REVISION';
  private $tj_plant = 'TJ_PLANT';
  private $tj_work_center = 'TJ_WORK_CENTER';
  private $tj_progress_status = 'TJ_PROGRESS_STATUS';

  public function get_count_new_order() {
    $work_center_list = implode("','", get_session('work_center'));
    $where_work_center = '';
    $where_work_center_dest = '';
    if ($work_center_list != '') {
      $where_work_center = "AND WORK_CENTER IN ('$work_center_list')";
      $where_work_center_dest = "AND A.WORK_CENTER_DEST IN ('$work_center_list')";
    }

    // $query = "
    // SELECT
    //   COUNT(A.AUFNR) AS COUNT
    // FROM
    //   $this->tj_m_pmorder_new A WITH ( NOLOCK )
    //   JOIN ( SELECT AUFNR, PROGRESS_STATUS FROM $this->tj_m_pmorder WITH ( NOLOCK ) GROUP BY AUFNR, PROGRESS_STATUS ) B ON A.AUFNR = B.AUFNR
    //   JOIN $this->m_pmorderh C WITH ( NOLOCK ) ON A.AUFNR = C.AUFNR
    //   LEFT JOIN $this->m_revision D WITH ( NOLOCK ) ON C.REVNR = D.REVNR 
    // WHERE
    //   B.PROGRESS_STATUS != 'Close'
    //   AND
    //   ( C.REVNR = '' OR D.TXT04 = 'REL' ) 
    //   $where_work_center";

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.AUFNR,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        IS_MOVING
      FROM
        $this->tj_m_pmorder_new A WITH ( NOLOCK )
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      WHERE
        IS_ACTIVE = 1
        $where_work_center_dest
    ),
    TMP_A AS (
      SELECT
        AUFNR,
        TOP_ORDER,
        TOP_OPERATION
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
        AND
        IS_MOVING = 1
        AND
        PROGRESS_STATUS != 'Close'
        $where_work_center
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ( B.REVNR = '' OR C.TXT04 = 'REL' )";
     
    return $this->db->query($query)->row()->COUNT;
  }

  public function get_list_plant() {
    $query = "SELECT PLANT, PLANT_DESC FROM $this->tj_plant WITH ( NOLOCK )";
    return $this->db->query($query)->result();
  }

  public function get_list_work_center() {
    $query = "SELECT WORK_CENTER, WORK_CENTER_DESC FROM $this->tj_work_center WITH ( NOLOCK )";
    return $this->db->query($query)->result();
  }

  public function get_list_progress_status() {
    $query = "SELECT PROGRESS_STATUS FROM $this->tj_progress_status WITH ( NOLOCK )";
    return $this->db->query($query)->result();
  }

}