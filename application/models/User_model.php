<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class User_model extends CI_Model {

  private $tj_user = 'TJ_USER_NEW';
  private $tj_user_role = 'TJ_USER_ROLE_NEW';
  private $tj_user_group = 'TJ_USER_GROUP_NEW';
  private $tj_work_center = 'TJ_WORK_CENTER';

  private $column_order = array(null, 'NAME', 'USERNAME', 'USER_ROLE', 'WORK_CENTER', 'IS_ACTIVE');
  private $column_search = array(null, 'NAME', 'USERNAME', 'USER_ROLE', 'WORK_CENTER', null);
  
  private $order = array('ID_USER' => 'ASC', 'IS_ACTIVE' => 'DESC');

  private function _get_datatables_params() {
    $column_order = $this->column_order;
    $column_search = $this->column_search;
    $order = $this->order;

    $i = 0;
    if($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if($item != null ) {
          if($_REQUEST['search']['value']) {  
            if($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if(count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    } 

    $j = 0;
    if($_REQUEST['columns']){
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$j];
          $like_arr[$index] = $key['search']['value'];
        }
        $j++;
      }
      $this->db->like($like_arr);
    }
      
    if(isset($_REQUEST['order'])) {
      if($_REQUEST['order']['0']['column'] == 0) {
        // $this->db->order_by(key($order), $order[key($order)]);
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      }else{
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if(isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result =  $this->db->get_compiled_select();

    $query = explode("WHERE", $result);

    if(count($query) == 2){
      $where = explode("ORDER BY", $query[1]);
      $order = $where[1];
      $where = "WHERE (".$where[0].")";
    }else{
      $where = explode("ORDER BY", $result);
      $order = $where[1];
      $where = "";
    }

    $start = $_REQUEST['start'];
    $end   = $_REQUEST['length'] + $_REQUEST['start'];

    // where_paging
    $where_paging = $_REQUEST['length'] != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : "";

    return array(
      'start'           => $start,
      'end'             => $end,
      'where'           => $where,
      'order'           => $order,
      'where_paging'    => $where_paging
    );
  }

  public function get_list_users() {
    $param = $this->_get_datatables_params('');
    
    $sql = "WITH A AS (
      SELECT
        A.ID_USER,
        A.NAME, 
        A.USERNAME,
        C.USER_ROLE,
        A.WORK_CENTER,
        A.IS_ACTIVE
      FROM
        $this->tj_user A WITH ( NOLOCK )
        JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
        JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
      WHERE
        IS_SUPERUSER != 1
    ),
    B AS (
      SELECT
        *,
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order'].") AS ROW_ID 
      FROM
        A
      ".$param['where']."
    )
    SELECT * FROM B ".$param['where_paging'];

    return $this->db->query($sql)->result();
  }

  public function get_count_list_users() {
    $param = $this->_get_datatables_params('all');
    
    $sql = "
    SELECT
      COUNT(ID_USER) AS COUNT
    FROM
      $this->tj_user WITH ( NOLOCK )
    WHERE
      IS_SUPERUSER != 1";

    $data = $this->db->query($sql)->row();

    return $data->COUNT; 
  }

  public function get_count_list_users_filtered() {
    $param = $this->_get_datatables_params('filtered');
    
    $sql = "WITH A AS (
      SELECT
        A.ID_USER,
        A.NAME, 
        A.USERNAME,
        C.USER_ROLE,
        A.WORK_CENTER,
        A.IS_ACTIVE
      FROM
        $this->tj_user A WITH ( NOLOCK )
        JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
        JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
      WHERE
        IS_SUPERUSER != 1
      )
      SELECT
        COUNT(ID_USER) AS COUNT
      FROM
        A
      ".$param['where'];

    $data = $this->db->query($sql)->row();

    return $data->COUNT; 
  }

  public function check_username($username, $except = '') {
    $data = $this->db->query("SELECT ID_USER FROM $this->tj_user WITH ( NOLOCK ) WHERE USERNAME = '$username' AND USERNAME != '$except'")->result();
    return count($data) > 0 ? true : false;
  }

  public function get_user_data($username) {
    $query = "
    SELECT 
      * 
    FROM 
      $this->tj_user A WITH ( NOLOCK )
      JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
      JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
    WHERE
      A.USERNAME = '$username' 
      AND A.IS_ACTIVE = 1";
			
    return $this->db->query($query)->row();
  }

  public function create_user($param) {
    return $this->db->insert($this->tj_user, $param);
  }

  public function update_user($param, $id_user) {
    return $this->db->set($param)->where("id_user", $id_user)->update($this->tj_user);
  }

  public function get_user_by_id($id_user) {
    $query = "
    SELECT
      *
    FROM
      $this->tj_user A WITH ( NOLOCK )
      JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
      JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
    WHERE
      A.ID_USER = $id_user";

    return $this->db->query($query)->row();
  }

  public function delete_user($id_user) {
    return $this->db->delete($this->tj_user, array('id_user' => $id_user)); 
  }
}