<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class User_role_model extends CI_Model {

  private $tj_user_role = 'TJ_USER_ROLE_NEW';

  public function get_all() {
    return $this->db->query("SELECT * FROM $this->tj_user_role WITH ( NOLOCK )")->result();
  }

}