<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Order_model extends CI_Model {

  private $m_pmorderh = 'M_PMORDERH';
  private $tj_m_pmorder = 'TJ_M_PMORDER';
  private $tj_m_pmorder_material = 'TJ_M_PMORDER_MATERIAL';
  private $tj_m_pmorder_new = 'TJ_M_PMORDER_NEW';
  private $m_revision = 'M_REVISION';
  private $tj_mrevision = 'TJ_M_REVISION';
  private $tj_order_movement = 'TJ_LOG_ORDER_MOVEMENT';
  private $tj_progress_status = 'TJ_PROGRESS_STATUS';

  private $column_order = [
    'list_order'    => [null, null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', 'TARGET_DATE', 'PMHRS', 'AMHRS', 'ARBPL', 'WORK_CENTER', 'PROGRESS_STATUS', null, null, null, null, 'SAP_STATUS'],
    'list_material' => [null, null, 'REVNR', 'AUFNR', 'MATNR', 'MATNR_ALT', 'TPLNR', 'MAKTX', 'MTART', 'BDMNG', 'QTY_TOTAL', 'MEINS', 'RESPONSIBILITY', 'MATERIAL_FULFILLMENT_STATUS', 'MATERIAL_REMARK', 'CSP_RFQ', 'PO_NUMBER', 'LEAD_TIME', 'AWB', 'INB', 'SP', 'STO', 'STORAGE_LOCATION', 'DATE_DELIVERED', 'QTY_DELIVERED', 'RECEIVER_NAME'],
    'list_daily'    => [null, null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', 'TARGET_DATE', 'PMHRS', 'AMHRS', 'WORK_CENTER', 'PROGRESS_STATUS', null],
  ];

  private $column_search = [
    'list_order'    => [null, null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', null, null, null, 'ARBPL', 'WORK_CENTER', 'PROGRESS_STATUS', 'REMARKS', 'PART_NAME', 'QTY', 'LOCATION', 'SAP_STATUS'],
    'list_material' => [null, null, 'REVNR', 'AUFNR', 'MATNR', 'MATNR_ALT', 'TPLNR', 'MAKTX', 'MTART', 'BDMNG', 'QTY_TOTAL', 'MEINS', 'RESPONSIBILITY', 'MATERIAL_FULFILLMENT_STATUS', 'MATERIAL_REMARK', 'CSP_RFQ', 'PO_NUMBER', 'LEAD_TIME', 'AWB', 'INB', 'SP', 'STO', 'STORAGE_LOCATION', null, 'QTY_DELIVERED', 'RECEIVER_NAME'],
    'list_daily'    => [null, null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', null, null, null, 'WORK_CENTER', 'PROGRESS_STATUS', 'LOCATION'],
  ];

  private $order_by = [
    'list_order'    => ['AUFNR' => 'DESC'],
    'list_material' => ['MEINS_STATUS' => 'ASC', 'REVNR' => 'DESC', 'AUFNR' => 'DESC', 'VORNR' => 'ASC', 'RSPOS' => 'ASC']
  ];
  
  private function _get_datatables_params($type) {
    if ($type != 'daily' || $type != 'material') {
      $column_order = $this->column_order['list_order'];
      $column_search = $this->column_search['list_order'];
      $order = $this->order_by['list_order'];

      if ($type == 'incoming') {
        $column_order[10] = 'ORIGIN';
        $column_search[10] = 'ORIGIN';
      }
    } else if ($type == 'daily') {
      $column_order = $this->column_order['list_daily'];
      $column_search = $this->column_search['list_daily'];
      $order = $this->order_by['list_order'];
    } else {
      $column_order = $this->column_order['list_material'];
      $column_search = $this->column_search['list_material'];
      $order = $this->order_by['list_material'];
    } 

    if ($type == 'incoming' || $type == 'log') {
      // remove first index because it's a checkbox
      array_splice($column_order, 0, 1);
      array_splice($column_search, 0, 1);
    }

    $i = 0;
    if ($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if ($item != null ) {
          if ($_REQUEST['search']['value']) {  
            if ($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if (count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    }

    $i = 0;
    if ($_REQUEST['columns']) {
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$i];
          $like_arr[$index] = $key['search']['value'];
        }
        $i++;
      }
      $this->db->like($like_arr);
    }
      
    if (isset($_REQUEST['order'])) {
      if ($_REQUEST['order']['0']['column'] == 0) {
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      } else {
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if (isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result = $this->db->get_compiled_select();

    // result
    $query = explode("WHERE", $result);

    // where_datatables
    if (count($query) == 2) {
      $where = explode('ORDER BY', $query[1]);
      $order_by = $where[1];
      $where_datatables = 'WHERE ('.$where[0].')';
    } else {
      $where = explode('ORDER BY', $result);
      $order_by = $where[1];
      $where_datatables = '';
    }

    // where_paging
    $start = $_REQUEST['start'];
    $end = $_REQUEST['length'] + $_REQUEST['start'];
    $where_paging = $_REQUEST['length'] != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : '';

    // where_work_center
    $work_center = implode("','", get_session('work_center'));
    $work_center_group = get_session('work_center_group');
    $param_work_center = $_REQUEST['work_center'];
    $work_center_list = '';
    
    $where_work_center = '';
    $where_new_order = '';
    $where_plant = '';
    if ($work_center != '' || $param_work_center != '') {
      // if droddown work center is empty
      $work_center_list = $param_work_center != '' ? $param_work_center : $work_center;

      if ($type == 'new') {
        $where_work_center = "AND WORK_CENTER IN ('$work_center_list')";
        $where_new_order = "AND WORK_CENTER_DEST IN ('$work_center_list')";
      } else if ($type == 'all') {
        if ($work_center_group == 'Hangar') {
          $where_plant = "AND C.VAWRK IN ('$work_center_list')";
        } else {
          $where_work_center = "AND ( LEFT ( ARBPL, 4 ) IN ('$work_center_list') OR WORK_CENTER_BG IN ('$work_center_list') )";
        }
      } else if ($type == 'close') {
        $where_work_center = "AND ( LEFT ( ARBPL, 4 ) IN ('$work_center_list') OR WORK_CENTER_BG IN ('$work_center_list') )";
      } else if ($type == 'incoming' || $type == 'material') {
        $where_work_center = "AND LEFT ( ARBPL, 4 ) IN ('$work_center_list')";
      } else if ($type == 'log') {
        $where_work_center = "WHERE WORK_CENTER IN ('$work_center_list')";
      } else if ($type == 'daily') {
        $where_work_center = "AND WORK_CENTER_BG IN ('$work_center_list')";
      }
    }

    // where_progress_status
    if ($type == 'close') {
      $where_progress_status = "AND ( PROGRESS_STATUS = 'Close' OR SAP_STATUS IN ('Completed', 'Closed') )";
    } else {
      // where user 'sent to other'
      if(get_session('user_role') != 'Sent to Other') {
        $where_progress_status = "AND PROGRESS_STATUS != 'Close'";
      } else {
        $where_progress_status = "AND PROGRESS_STATUS = 'Sent to Other'";
      }
    }

    // where_project_status
    $param_project_status = $_REQUEST['project_status'];
    $where_project_status = "( B.REVNR = '' OR C.TXT04 = '$param_project_status' )";

    $where_hidden_order = '';
    if($type == 'daily') {
      if(count($_REQUEST['hidden_order']) > 1) {
        $hidden_list = $_REQUEST['hidden_order'];
        unset($hidden_list[0]); // remove 'empty' value from array
        $hidden_order = implode(', ', $hidden_list);
        if ($hidden_order != '') $where_hidden_order = "AND AUFNR NOT IN ($hidden_order)";
      }
    }

    return array(
      'where_paging'              => $where_paging,
      'where_datatables'          => $where_datatables,
      'where_plant'               => $where_plant,
      'where_work_center'         => $where_work_center,
      'where_project_status'      => $where_project_status,
      'where_progress_status'     => $where_progress_status,
      'where_hidden_order'        => $where_hidden_order,
      'where_new_order'           => $where_new_order,
      'order_by'                  => $order_by
    );
  }

  public function get_list_order($type) {
    $param = $this->_get_datatables_params($type);

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        VORNR,
        AUART,
        TPLNR,
        TARGET_DATE,
        ARBEI,
        ISMNW,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        REMARKS,
        PART_NAME,
        QTY,
        LOCATION,
        IS_MOVING
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1 ".$param['where_hidden_order']."
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1  ".$param['where_progress_status']." ".$param['where_work_center']."
    ),
    TMP_B AS (
      SELECT
        A.TOP_ORDER,
        A.TOP_OPERATION,
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN A.AUART = 'GA01' THEN 'Jobcard'
          WHEN A.AUART = 'GA02' THEN 'MDR'
          WHEN A.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.KTEXT,
        A.TPLNR,
        A.TARGET_DATE,
        A.ARBPL,
        A.WORK_CENTER,
        A.WORK_CENTER_BG,
        A.PROGRESS_STATUS,
        A.SAP_STATUS,
        A.REMARKS,
        A.PART_NAME,
        A.QTY,
        A.LOCATION,
        A.IS_MOVING,
        C.VAWRK
      FROM
        TMP_A A
				JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
    ),
    TMP_C AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
        AND
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    ),
    TMP_D AS (
      SELECT
        A.*,
        CASE
          WHEN B.PMHRS IS NULL THEN 0
          ELSE B.PMHRS
        END AS PMHRS,
        CASE
          WHEN B.AMHRS IS NULL THEN 0
          ELSE B.AMHRS
        END AS AMHRS
      FROM
        TMP_B A
        JOIN TMP_C B ON A.AUFNR = B.AUFNR
    ),
    TMP_E AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_D ".$param['where_datatables']."
    )
    SELECT 
      TOP_ORDER,
      TOP_OPERATION,
      REVNR,
      AUFNR,
      AUART,
      KTEXT,
      TPLNR,
      TARGET_DATE,
      PMHRS,
      AMHRS,
      ARBPL,
      WORK_CENTER,
      WORK_CENTER_BG,
      PROGRESS_STATUS,
      SAP_STATUS,
      REMARKS,
      PART_NAME,
      QTY,
      LOCATION,
      IS_MOVING,
      VAWRK
    FROM 
      TMP_E ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1 ".$param['where_hidden_order']."
    ),
    TMP_A AS (
      SELECT
        TOP_ORDER,
        TOP_OPERATION,
        AUFNR
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1  ".$param['where_progress_status']." ".$param['where_work_center']."
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ".$param['where_project_status']." ".$param['where_plant'];

    $records_total = $this->db->query($query_all)->row()->COUNT; 

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
          AUFNR,
          VORNR,
          AUART,
          TPLNR,
          TARGET_DATE,
          ARBEI,
          ISMNW,
          ARBPL,
          WORK_CENTER,
          WORK_CENTER_BG,
          PROGRESS_STATUS,
          SAP_STATUS,
          REMARKS,
          PART_NAME,
          QTY,
          LOCATION,
          IS_MOVING
        FROM
          $this->tj_m_pmorder WITH ( NOLOCK )
        WHERE
          IS_ACTIVE = 1 ".$param['where_hidden_order']."
      ),
      TMP_A AS (
        SELECT
          *
        FROM
          TJ_M_PMORDER_TMP
        WHERE
          TOP_ORDER = 1  ".$param['where_progress_status']." ".$param['where_work_center']."
      ),
      TMP_B AS (
        SELECT
          A.TOP_ORDER,
          A.TOP_OPERATION,
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          CASE
            WHEN A.AUART = 'GA01' THEN 'Jobcard'
            WHEN A.AUART = 'GA02' THEN 'MDR'
            WHEN A.AUART = 'GA05' THEN 'PD Sheet'
            ELSE 'Other'
          END AS AUART,
          B.KTEXT,
          A.TPLNR,
          A.TARGET_DATE,
          A.ARBPL,
          A.WORK_CENTER,
          A.WORK_CENTER_BG,
          A.PROGRESS_STATUS,
          A.SAP_STATUS,
          A.REMARKS,
          A.PART_NAME,
          A.QTY,
          A.LOCATION,
          A.IS_MOVING,
          C.VAWRK
        FROM
          TMP_A A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
      ),
      TMP_C AS (
        SELECT
          A.AUFNR,
          SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
          SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
          AND
          A.TOP_OPERATION = 1
        GROUP BY
          A.AUFNR 
      ),
      TMP_D AS (
        SELECT
          A.*,
          CASE
            WHEN B.PMHRS IS NULL THEN 0
            ELSE B.PMHRS
          END AS PMHRS,
          CASE
            WHEN B.AMHRS IS NULL THEN 0
            ELSE B.AMHRS
          END AS AMHRS
        FROM
          TMP_B A
          JOIN TMP_C B ON A.AUFNR = B.AUFNR
      )
      SELECT COUNT(TOP_OPERATION) AS COUNT FROM TMP_D ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function get_list_new_order() {
    $param = $this->_get_datatables_params('new');

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.AUFNR,
        VORNR,
        AUART,
        TPLNR,
        TARGET_DATE,
        ARBEI,
        ISMNW,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        REMARKS,
        PART_NAME,
        QTY,
        LOCATION,
        IS_MOVING
      FROM
        $this->tj_m_pmorder_new A WITH ( NOLOCK )
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      WHERE
        IS_ACTIVE = 1 ".$param['where_new_order']."
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1 
        AND
        IS_MOVING = 1 
        ".$param['where_progress_status']." ".$param['where_work_center']."
    ),
    TMP_B AS (
      SELECT
        A.TOP_ORDER,
        A.TOP_OPERATION,
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN A.AUART = 'GA01' THEN 'Jobcard'
          WHEN A.AUART = 'GA02' THEN 'MDR'
          WHEN A.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.KTEXT,
        A.TPLNR,
        A.TARGET_DATE,
        A.ARBPL,
        A.WORK_CENTER,
        A.WORK_CENTER_BG,
        A.PROGRESS_STATUS,
        A.SAP_STATUS,
        A.REMARKS,
        A.PART_NAME,
        A.QTY,
        A.LOCATION,
        A.IS_MOVING,
        C.VAWRK
      FROM
        TMP_A A
				JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
    ),
    TMP_C AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
        AND
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    ),
    TMP_D AS (
      SELECT
        A.*,
        CASE
          WHEN B.PMHRS IS NULL THEN 0
          ELSE B.PMHRS
        END AS PMHRS,
        CASE
          WHEN B.AMHRS IS NULL THEN 0
          ELSE B.AMHRS
        END AS AMHRS
      FROM
        TMP_B A
        JOIN TMP_C B ON A.AUFNR = B.AUFNR
    ),
    TMP_E AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_D ".$param['where_datatables']."
    )
    SELECT 
      TOP_ORDER,
      TOP_OPERATION,
      REVNR,
      AUFNR,
      AUART,
      KTEXT,
      TPLNR,
      TARGET_DATE,
      PMHRS,
      AMHRS,
      ARBPL,
      WORK_CENTER,
      WORK_CENTER_BG,
      PROGRESS_STATUS,
      SAP_STATUS,
      REMARKS,
      PART_NAME,
      QTY,
      LOCATION,
      IS_MOVING,
      VAWRK
    FROM 
      TMP_E ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.AUFNR,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        IS_MOVING
      FROM
        $this->tj_m_pmorder_new A WITH ( NOLOCK )
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      WHERE
        IS_ACTIVE = 1 ".$param['where_new_order']."
    ),
    TMP_A AS (
      SELECT
        AUFNR,
        TOP_ORDER,
        TOP_OPERATION
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1 
        AND
        IS_MOVING = 1 
        ".$param['where_progress_status']." ".$param['where_work_center']."
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ".$param['where_project_status']." ".$param['where_plant'];

    $records_total = $this->db->query($query_all)->row()->COUNT; 

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
          ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
          A.AUFNR,
          VORNR,
          AUART,
          TPLNR,
          TARGET_DATE,
          ARBEI,
          ISMNW,
          ARBPL,
          WORK_CENTER,
          WORK_CENTER_BG,
          PROGRESS_STATUS,
          SAP_STATUS,
          REMARKS,
          PART_NAME,
          QTY,
          LOCATION,
          IS_MOVING
        FROM
          $this->tj_m_pmorder_new A WITH ( NOLOCK )
          JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        WHERE
          IS_ACTIVE = 1 ".$param['where_new_order']."
      ),
      TMP_A AS (
        SELECT
          *
        FROM
          TJ_M_PMORDER_TMP
        WHERE
          TOP_ORDER = 1
          AND
          IS_MOVING = 1 
          ".$param['where_progress_status']." ".$param['where_work_center']."
      ),
      TMP_B AS (
        SELECT
          A.TOP_ORDER,
          A.TOP_OPERATION,
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          CASE
            WHEN A.AUART = 'GA01' THEN 'Jobcard'
            WHEN A.AUART = 'GA02' THEN 'MDR'
            WHEN A.AUART = 'GA05' THEN 'PD Sheet'
            ELSE 'Other'
          END AS AUART,
          B.KTEXT,
          A.TPLNR,
          A.TARGET_DATE,
          A.ARBPL,
          A.WORK_CENTER,
          A.WORK_CENTER_BG,
          A.PROGRESS_STATUS,
          A.SAP_STATUS,
          A.REMARKS,
          A.PART_NAME,
          A.QTY,
          A.LOCATION,
          A.IS_MOVING,
          C.VAWRK
        FROM
          TMP_A A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
      ),
      TMP_C AS (
        SELECT
          A.AUFNR,
          SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
          SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
          AND
          A.TOP_OPERATION = 1
        GROUP BY
          A.AUFNR 
      ),
      TMP_D AS (
        SELECT
          A.*,
          CASE
            WHEN B.PMHRS IS NULL THEN 0
            ELSE B.PMHRS
          END AS PMHRS,
          CASE
            WHEN B.AMHRS IS NULL THEN 0
            ELSE B.AMHRS
          END AS AMHRS
        FROM
          TMP_B A
          JOIN TMP_C B ON A.AUFNR = B.AUFNR
      )
      SELECT COUNT(TOP_OPERATION) AS COUNT FROM TMP_D ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function get_list_log_order() {
    $param = $this->_get_datatables_params('log');

    $query = "
    WITH TJ_LOG_TMP AS (
      SELECT
        DISTINCT
        AUFNR
      FROM
        $this->tj_order_movement WITH ( NOLOCK ) ".$param['where_work_center']."
    ),
    TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.AUFNR,
        VORNR,
        AUART,
        TPLNR,
        TARGET_DATE,
        ARBEI,
        ISMNW,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        REMARKS,
        PART_NAME,
        QTY,
        LOCATION,
        IS_MOVING
      FROM
        TJ_LOG_TMP A WITH ( NOLOCK ) 
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR 
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
    ),
    TMP_B AS (
      SELECT
        A.TOP_ORDER,
        A.TOP_OPERATION,
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN A.AUART = 'GA01' THEN 'Jobcard'
          WHEN A.AUART = 'GA02' THEN 'MDR'
          WHEN A.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.KTEXT,
        A.TPLNR,
        A.TARGET_DATE,
        A.ARBPL,
        A.WORK_CENTER,
        A.WORK_CENTER_BG,
        A.PROGRESS_STATUS,
        A.SAP_STATUS,
        A.REMARKS,
        A.PART_NAME,
        A.QTY,
        A.LOCATION,
        A.IS_MOVING,
        C.VAWRK
      FROM
        TMP_A A
				JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    ),
    TMP_C AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    ),
    TMP_D AS (
      SELECT
        A.*,
        CASE
          WHEN B.PMHRS IS NULL THEN 0
          ELSE B.PMHRS
        END AS PMHRS,
        CASE
          WHEN B.AMHRS IS NULL THEN 0
          ELSE B.AMHRS
        END AS AMHRS
      FROM
        TMP_B A
        JOIN TMP_C B ON A.AUFNR = B.AUFNR
    ),
    TMP_E AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_D ".$param['where_datatables']."
    )
    SELECT 
      TOP_ORDER,
      TOP_OPERATION,
      REVNR,
      AUFNR,
      AUART,
      KTEXT,
      TPLNR,
      TARGET_DATE,
      PMHRS,
      AMHRS,
      ARBPL,
      WORK_CENTER,
      WORK_CENTER_BG,
      PROGRESS_STATUS,
      SAP_STATUS,
      REMARKS,
      PART_NAME,
      QTY,
      LOCATION,
      IS_MOVING,
      VAWRK
    FROM 
      TMP_E ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_LOG_TMP AS (
      SELECT
        DISTINCT
        AUFNR
      FROM
        $this->tj_order_movement WITH ( NOLOCK ) ".$param['where_work_center']."
    ),
    TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.AUFNR,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        TJ_LOG_TMP A WITH ( NOLOCK ) 
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR 
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        AUFNR,
        TOP_ORDER,
        TOP_OPERATION
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR";

    $records_total = $this->db->query($query_all)->row()->COUNT; 

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TJ_LOG_TMP AS (
        SELECT
          DISTINCT
          AUFNR
        FROM
          $this->tj_order_movement WITH ( NOLOCK ) ".$param['where_work_center']."
      ),
      TJ_M_PMORDER_TMP AS (
        SELECT
          ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
          ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
          A.AUFNR,
          VORNR,
          AUART,
          TPLNR,
          TARGET_DATE,
          ARBEI,
          ISMNW,
          ARBPL,
          WORK_CENTER,
          WORK_CENTER_BG,
          PROGRESS_STATUS,
          SAP_STATUS,
          REMARKS,
          PART_NAME,
          QTY,
          LOCATION,
          IS_MOVING
        FROM
          TJ_LOG_TMP A WITH ( NOLOCK ) 
          JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR 
        WHERE
          IS_ACTIVE = 1
      ),
      TMP_A AS (
        SELECT
          *
        FROM
          TJ_M_PMORDER_TMP
        WHERE
          TOP_ORDER = 1 
      ),
      TMP_B AS (
        SELECT
          A.TOP_ORDER,
          A.TOP_OPERATION,
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          CASE
            WHEN A.AUART = 'GA01' THEN 'Jobcard'
            WHEN A.AUART = 'GA02' THEN 'MDR'
            WHEN A.AUART = 'GA05' THEN 'PD Sheet'
            ELSE 'Other'
          END AS AUART,
          B.KTEXT,
          A.TPLNR,
          A.TARGET_DATE,
          A.ARBPL,
          A.WORK_CENTER,
          A.WORK_CENTER_BG,
          A.PROGRESS_STATUS,
          A.SAP_STATUS,
          A.REMARKS,
          A.PART_NAME,
          A.QTY,
          A.LOCATION,
          A.IS_MOVING,
          C.VAWRK
        FROM
          TMP_A A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      ),
      TMP_C AS (
        SELECT
          A.AUFNR,
          SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
          SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
          AND
          A.TOP_OPERATION = 1
        GROUP BY
          A.AUFNR 
      ),
      TMP_D AS (
        SELECT
          A.*,
          CASE
            WHEN B.PMHRS IS NULL THEN 0
            ELSE B.PMHRS
          END AS PMHRS,
          CASE
            WHEN B.AMHRS IS NULL THEN 0
            ELSE B.AMHRS
          END AS AMHRS
        FROM
          TMP_B A
          JOIN TMP_C B ON A.AUFNR = B.AUFNR
      )
      SELECT COUNT(TOP_OPERATION) AS COUNT FROM TMP_D ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function get_list_incoming_order() {
    $param = $this->_get_datatables_params('incoming');

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, ARBPL ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        VORNR,
        AUART,
        TPLNR,
        TARGET_DATE,
        ARBEI,
        ISMNW,
        ARBPL,
        ORIGIN,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        REMARKS,
        PART_NAME,
        QTY,
        LOCATION,
        IS_MOVING
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1 
        AND
        IS_MOVING = 0
        AND
        MOVED = 0
        AND
        ARBPL != ORIGIN
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
        ".$param['where_progress_status']." ".$param['where_work_center']."
    ),
    TMP_B AS (
      SELECT
        A.TOP_ORDER,
        A.TOP_OPERATION,
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN A.AUART = 'GA01' THEN 'Jobcard'
          WHEN A.AUART = 'GA02' THEN 'MDR'
          WHEN A.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.KTEXT,
        A.TPLNR,
        A.TARGET_DATE,
        A.ORIGIN,
        A.WORK_CENTER,
        A.WORK_CENTER_BG,
        A.PROGRESS_STATUS,
        A.SAP_STATUS,
        A.REMARKS,
        A.PART_NAME,
        A.QTY,
        A.LOCATION,
        A.IS_MOVING,
        C.VAWRK
      FROM
        TMP_A A
				JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
    ),
    TMP_C AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_plant']."
        AND
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    ),
    TMP_D AS (
      SELECT
        A.*,
        CASE
          WHEN B.PMHRS IS NULL THEN 0
          ELSE B.PMHRS
        END AS PMHRS,
        CASE
          WHEN B.AMHRS IS NULL THEN 0
          ELSE B.AMHRS
        END AS AMHRS
      FROM
        TMP_B A
        JOIN TMP_C B ON A.AUFNR = B.AUFNR
    ),
    TMP_E AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_D ".$param['where_datatables']."
    )
    SELECT 
      TOP_ORDER,
      TOP_OPERATION,
      REVNR,
      AUFNR,
      AUART,
      KTEXT,
      TPLNR,
      TARGET_DATE,
      PMHRS,
      AMHRS,
      ORIGIN,
      WORK_CENTER,
      WORK_CENTER_BG,
      PROGRESS_STATUS,
      SAP_STATUS,
      REMARKS,
      PART_NAME,
      QTY,
      LOCATION,
      IS_MOVING,
      VAWRK
    FROM 
      TMP_E ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, ARBPL ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        ARBPL,
        ORIGIN,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1 
        AND
        IS_MOVING = 0
        AND
        MOVED = 0
        AND
        ARBPL != ORIGIN
    ),
    TMP_A AS (
      SELECT
        TOP_ORDER,
        TOP_OPERATION,
        AUFNR
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
        ".$param['where_progress_status']." ".$param['where_work_center']."
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ".$param['where_project_status']." ".$param['where_plant'];

    $records_total = $this->db->query($query_all)->row()->COUNT; 

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, ARBPL ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
          AUFNR,
          VORNR,
          AUART,
          TPLNR,
          TARGET_DATE,
          ARBEI,
          ISMNW,
          ARBPL,
          ORIGIN,
          WORK_CENTER,
          WORK_CENTER_BG,
          PROGRESS_STATUS,
          SAP_STATUS,
          REMARKS,
          PART_NAME,
          QTY,
          LOCATION,
          IS_MOVING
        FROM
          $this->tj_m_pmorder WITH ( NOLOCK )
        WHERE
          IS_ACTIVE = 1 
          AND
          IS_MOVING = 0
          AND
          MOVED = 0
          AND
          ARBPL != ORIGIN
      ),
      TMP_A AS (
        SELECT
          *
        FROM
          TJ_M_PMORDER_TMP
        WHERE
          TOP_ORDER = 1
          ".$param['where_progress_status']." ".$param['where_work_center']."
      ),
      TMP_B AS (
        SELECT
          A.TOP_ORDER,
          A.TOP_OPERATION,
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          CASE
            WHEN A.AUART = 'GA01' THEN 'Jobcard'
            WHEN A.AUART = 'GA02' THEN 'MDR'
            WHEN A.AUART = 'GA05' THEN 'PD Sheet'
            ELSE 'Other'
          END AS AUART,
          B.KTEXT,
          A.TPLNR,
          A.TARGET_DATE,
          A.ORIGIN,
          A.WORK_CENTER,
          A.WORK_CENTER_BG,
          A.PROGRESS_STATUS,
          A.SAP_STATUS,
          A.REMARKS,
          A.PART_NAME,
          A.QTY,
          A.LOCATION,
          A.IS_MOVING,
          C.VAWRK
        FROM
          TMP_A A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
      ),
      TMP_C AS (
        SELECT
          A.AUFNR,
          SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
          SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_plant']."
          AND
          A.TOP_OPERATION = 1
        GROUP BY
          A.AUFNR 
      ),
      TMP_D AS (
        SELECT
          A.*,
          CASE
            WHEN B.PMHRS IS NULL THEN 0
            ELSE B.PMHRS
          END AS PMHRS,
          CASE
            WHEN B.AMHRS IS NULL THEN 0
            ELSE B.AMHRS
          END AS AMHRS
        FROM
          TMP_B A
          JOIN TMP_C B ON A.AUFNR = B.AUFNR
      )
      SELECT COUNT(TOP_OPERATION) AS COUNT FROM TMP_D ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function get_list_material() {
    $param = $this->_get_datatables_params('material');

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        VORNR,
        RSPOS,
        TPLNR,
        MATNR,
        MAKTX,
        MTART,
        MEINS,
        BDMNG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
        AND MATNR != '' ".$param['where_work_center']."
    ),
    TMP_A AS (
      SELECT
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        A.VORNR,
        A.RSPOS,
        A.TPLNR,
        A.MATNR,
        A.MAKTX,
        A.MTART,
        A.MEINS,
        CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT) AS BDMNG,
        D.MATNR_ALT,
        D.RESPONSIBILITY,
        D.MATERIAL_FULFILLMENT_STATUS,
        D.MATERIAL_REMARK,
        D.CSP_RFQ,
        D.PO_NUMBER,
        D.LEAD_TIME,
        D.AWB,
        D.INB,
        D.SP,
        D.STO,
        D.STORAGE_LOCATION,
        D.DATE_DELIVERED,
        D.QTY_DELIVERED,
        D.RECEIVER_NAME
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
      WHERE
        ".$param['where_project_status']." ".$param['where_progress_status']."
    ),
    TMP_B AS (
      SELECT
       A.MATNR,
       SUM ( CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT ) ) AS QTY_TOTAL,
       (
        SELECT 
          CASE 
            WHEN COUNT(*) = 1 THEN 'TRUE'
            WHEN COUNT(*) > 1 THEN 'FALSE'
          END
        FROM (
          SELECT
            DISTINCT D.MEINS
          FROM 
            TJ_M_PMORDER_TMP D
          WHERE 
            D.MATNR = A.MATNR
        ) AS MEINS_TABLE
       ) AS MEINS_STATUS
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_project_status']." ".$param['where_progress_status']."
      GROUP BY
        A.MATNR
    ),
    TMP_C AS (
      SELECT
        A.*,
        B.QTY_TOTAL,
        B.MEINS_STATUS
      FROM
        TMP_A A
        JOIN TMP_B B ON A.MATNR = B.MATNR
    ),
    TMP_D AS ( 
      SELECT 
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM 
        TMP_C ".$param['where_datatables']."
    )
    SELECT
      REVNR,
      AUFNR,
      VORNR,
      RSPOS,
      MATNR,
      MATNR_ALT,
      TPLNR,
      MAKTX,
      MTART,
      BDMNG,
      QTY_TOTAL,
      MEINS,
      MEINS_STATUS,
      RESPONSIBILITY,
      MATERIAL_FULFILLMENT_STATUS,
      MATERIAL_REMARK,
      CSP_RFQ,
      PO_NUMBER,
      LEAD_TIME,
      AWB,
      INB,
      SP,
      STO,
      STORAGE_LOCATION,
      DATE_DELIVERED,
      QTY_DELIVERED,
      RECEIVER_NAME
    FROM 
      TMP_D ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        VORNR,
        RSPOS,
        MATNR,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
        AND MATNR != '' ".$param['where_work_center']."
    )
    SELECT
      COUNT(A.MATNR) AS COUNT
    FROM
      TJ_M_PMORDER_TMP A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ".$param['where_project_status']." ".$param['where_progress_status'];

    $records_total = $this->db->query($query_all)->row()->COUNT;

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          AUFNR,
          VORNR,
          RSPOS,
          TPLNR,
          MATNR,
          MAKTX,
          MTART,
          MEINS,
          BDMNG,
          PROGRESS_STATUS,
          SAP_STATUS
        FROM
          $this->tj_m_pmorder WITH ( NOLOCK )
        WHERE
          IS_ACTIVE = 1
          AND MATNR != '' ".$param['where_work_center']."
      ),
      TMP_A AS (
        SELECT
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          A.VORNR,
          A.RSPOS,
          A.TPLNR,
          A.MATNR,
          A.MAKTX,
          A.MTART,
          A.MEINS,
          CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT) AS BDMNG,
          D.MATNR_ALT,
          D.RESPONSIBILITY,
          D.MATERIAL_FULFILLMENT_STATUS,
          D.MATERIAL_REMARK,
          D.CSP_RFQ,
          D.PO_NUMBER,
          D.LEAD_TIME,
          D.AWB,
          D.INB,
          D.SP,
          D.STO,
          D.STORAGE_LOCATION,
          D.DATE_DELIVERED,
          D.QTY_DELIVERED,
          D.RECEIVER_NAME
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
          LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
        WHERE
          ".$param['where_project_status']." ".$param['where_progress_status']."
      ),
      TMP_B AS (
        SELECT
        A.MATNR,
        SUM ( CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT ) ) AS QTY_TOTAL,
        (
          SELECT 
            CASE 
              WHEN COUNT(*) = 1 THEN 'TRUE'
              WHEN COUNT(*) > 1 THEN 'FALSE'
            END
          FROM (
            SELECT
              DISTINCT D.MEINS
            FROM 
              TJ_M_PMORDER_TMP D
            WHERE 
              D.MATNR = A.MATNR
          ) AS MEINS_TABLE
        ) AS MEINS_STATUS
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_project_status']." ".$param['where_progress_status']."
        GROUP BY
          A.MATNR
      ),
      TMP_C AS (
        SELECT
          A.*,
          B.QTY_TOTAL,
          B.MEINS_STATUS
        FROM
          TMP_A A
          JOIN TMP_B B ON A.MATNR = B.MATNR
      )
      SELECT COUNT(MATNR) AS COUNT FROM TMP_C ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

}