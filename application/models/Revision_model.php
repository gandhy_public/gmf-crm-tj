<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Revision_model extends CI_Model {

  private $m_revision = 'M_REVISION';
  private $tj_m_revision = 'TJ_M_REVISION';
  private $tj_log_project_planning = 'TJ_LOG_PROJECT_PLANNING';
  private $tj_plant = 'TJ_PLANT';

  private $column_order_all = array(null, 'REVNR', null, 'UPDATE_DATE', 'TPLNR', 'VAWRK', 'REVTX', null, null, null, 'REVBD', 'REVED');
  private $column_search_all = array(null, 'REVNR', null, 'UPDATE_DATE', 'TPLNR', 'VAWRK', 'REVTX', null, null, null, null, null);

  private $column_order = array(null, 'REVNR', 'TPLNR', 'VAWRK', 'REVTX', null, null, null, 'REVBD', 'REVED');
  private $column_search = array(null, 'REVNR', 'TPLNR', 'VAWRK', 'REVTX', null, null, null, null, null);

  private $order = array('TOP_PROJECT' => 'DESC', 'REVNR' => 'DESC');

  private function _get_datatables_params($type_data) {
    if (is_has_feature('prod_planning_act')) {
      $column_order = $this->column_order_all;
      $column_search = $this->column_search_all;
    } else {
      $column_order = $this->column_order;
      $column_search = $this->column_search;
    }
    
    $order = $this->order;
    
    $i = 0;
    if($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if($item != null ) {
          if($_REQUEST['search']['value']) {  
            if($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if(count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    } 

    $j = 0;
    if($_REQUEST['columns']){
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$j];
          $like_arr[$index] = $key['search']['value'];
        }
        $j++;
      }
      $this->db->like($like_arr);
    }

    if(isset($_REQUEST['order'])) {
      if($_REQUEST['order']['0']['column'] == 0) {
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      }else{
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if(isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result =  $this->db->get_compiled_select();

    $start = $_REQUEST['start'];
    $end   = $_REQUEST['length'] + $_REQUEST['start'];

    $where_work_center = "";
    $work_center = implode("','", get_session('work_center'));
    $work_center_group = get_session('work_center_group');
    $param_work_center = $_REQUEST['location'];
    if (($work_center != "" || $param_work_center != "") && $work_center_group == 'Hangar') {
      $work_center_list = $param_work_center != '' ? $param_work_center : $work_center;
      $where_work_center = "VAWRK IN ('$work_center_list')";
    } else if ($param_work_center != "" && $work_center_group != 'Hangar') {
      $where_work_center = "VAWRK IN = '$param_work_center'";
    } else {
      $where_work_center = "VAWRK IN ( 'GAH1', 'GAH3', 'GAH4' )";
    }

    $query = explode("WHERE", $result);

    if(count($query) == 2){
      $where    = explode("ORDER BY", $query[1]);
      $order_by = $where[1];
      $where    = "WHERE (".$where[0].")";
    }else{
      $where    = explode("ORDER BY", $result);
      $order_by = $where[1];
      $where    = "";
    }

    if($type_data == 'all') $where = "";

    // where_paging
    $where_paging = $end != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : "";

    // where_project_status
    $where_project_status = "TXT04 = '".$_REQUEST['project_status']."'";

    return array(
      'start'                 => $start,
      'end'                   => $end,
      'where_work_center'     => $where_work_center,
      'where_project_status'  => $where_project_status,
      'where_paging'          => $where_paging,
      'where'                 => $where,
      'order_by'              => $order_by
    );
  }

  public function get_list_projects() {
    $param = $this->_get_datatables_params('');
    
    $sql = "
    WITH TMP_A AS (
      SELECT
        A.REVNR,
        A.TPLNR,
        A.VAWRK,
        A.REVTX,
        A.REVBD,
        A.REVED,
        B.STATUS_PLN,
        CASE 
          WHEN B.TOP_PROJECT IS NULL THEN 0
          ELSE B.TOP_PROJECT
        END AS TOP_PROJECT,
        ( SELECT TOP 1 O.UPDATE_DATE FROM M_PMORDERH O WHERE O.REVNR = A.REVNR ORDER BY O.UPDATE_DATE DESC ) AS UPDATE_DATE
      FROM
        $this->m_revision A WITH ( NOLOCK )
        LEFT JOIN $this->tj_m_revision B WITH ( NOLOCK ) ON A.REVNR = B.REVNR
      WHERE
        A.".$param['where_work_center']."
        AND
        A.REVBD != ''
        AND
        A.REVED != ''
        AND
        A.".$param['where_project_status']."
      ),
      TMP_B AS ( 
        SELECT ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID, 
        * 
      FROM 
        TMP_A ".$param['where']."
      )
      SELECT * FROM TMP_B ".$param['where_paging'];

    return $this->db->query($sql)->result();
  }

  public function get_count_list_projects() {
    $param = $this->_get_datatables_params('all');
    
    $sql = "
    SELECT
      COUNT(REVNR) AS COUNT
    FROM
      $this->m_revision WITH ( NOLOCK )
    WHERE
      ".$param['where_work_center']."
      AND
      REVBD != ''
      AND
      REVED != ''
      AND
      ".$param['where_project_status'];

    $data = $this->db->query($sql)->row();

    return $data->COUNT;
  }

  public function get_count_list_projects_filtered() {
    $param = $this->_get_datatables_params('filtered');
    
    $sql = "
    WITH TMP_A AS (
      SELECT
        A.REVNR,
        A.TPLNR,
        A.VAWRK,
        A.REVTX,
        A.REVBD,
        A.REVED,
        B.STATUS_PLN,
        CASE 
          WHEN B.TOP_PROJECT IS NULL THEN 0
          ELSE B.TOP_PROJECT
        END AS TOP_PROJECT,
        ( SELECT TOP 1 O.UPDATE_DATE FROM M_PMORDERH O WHERE O.REVNR = A.REVNR ORDER BY O.UPDATE_DATE DESC ) AS UPDATE_DATE
      FROM
        $this->m_revision A WITH ( NOLOCK )
        LEFT JOIN $this->tj_m_revision B WITH ( NOLOCK ) ON A.REVNR = B.REVNR 
      WHERE
        A.".$param['where_work_center']."
        AND
        A.REVBD != ''
        AND
        A.REVED != ''
        AND
        A.".$param['where_project_status']."
      )
      SELECT COUNT(REVNR) AS COUNT FROM TMP_A ".$param['where'];

    $data = $this->db->query($sql)->row();

    return $data->COUNT;
  }

  public function get_projects($param) {
    $where = "";

    if($param['status'] == 'ALL') {
      $where .= "AND ( TXT04 = 'CRTD' OR TXT04 = 'REL' )";
    } else {
      $where .= "AND TXT04 = '".$param['status']."'";
    }

    if($param['location'] != '') {
      $where .= "AND VAWRK = '".$param['location']."'";
    }

    if ($param['search'] != '') {
      $text = $param['search'];
      $where .= "AND ( REVNR LIKE '%$text%' OR REVTX LIKE '%$text%' OR VAWRK LIKE '%$text%' )";
    }

    $get_projects = "
      SELECT
        REVNR,
        REVTX,
        REVBD,
        REVED,
        VAWRK,
        TXT04
      FROM
        $this->m_revision WITH ( NOLOCK )
      WHERE
        VAWRK IN ( SELECT PLANT FROM $this->tj_plant WITH ( NOLOCK ) )
        AND
        REVBD != ''
        AND
        REVED != ''
        AND
        ( SUBSTRING(AEDAT, 1, 4) = '".this_year()."' OR SUBSTRING(REVED, 1, 4) = '".this_year()."' )
        $where";

    $get_hangar = "WITH TMP AS ($get_projects) SELECT VAWRK FROM TMP GROUP BY VAWRK";

    $projects = $this->db->query($get_projects)->result();
    $hangar = $this->db->query($get_hangar)->result();

    return ['projects' => $projects, 'hangar' => $hangar];
  }

  public function get_top_project($total = '') {
    $work_center = get_session("work_center");
    $work_center_group = get_session('work_center_group');
    if(count($work_center) > 0 && $work_center_group == 'Hangar') {
      $work_center_list = count($work_center) > 0 ? implode("','", $work_center) : $_REQUEST['location'];
      $where_work_center = "AND VAWRK IN ('$work_center_list')";
    } else {
      $where_work_center = "AND VAWRK IN ( 'GAH1', 'GAH3', 'GAH4' )";
    }

    $sql = "
    WITH TEMP AS (
      SELECT
        A.REVNR,
        A.REVTX,
        A.VAWRK,
        A.TPLNR,
        CASE 
          WHEN B.TOP_PROJECT IS NULL THEN 0
          ELSE B.TOP_PROJECT
        END AS TOP_PROJECT
      FROM
        $this->m_revision A WITH ( NOLOCK )
        LEFT JOIN $this->tj_m_revision B WITH ( NOLOCK ) ON A.REVNR = B.REVNR
      WHERE
        A.TXT04 = 'REL' $where_work_center
    )
    SELECT * FROM TEMP WHERE TOP_PROJECT = 1";

    $data = $this->db->query($sql)->result();

    return $total == '' ? $data : count($data);
  }

  public function get_detail_project($revnr) {
    $sql = "
    SELECT
      COMPANY_NAME,
      REVNR,
      REVTX,
      VAWRK,
      TPLNR,
      REVBD,
      REVED
    FROM
      $this->m_revision A WITH ( NOLOCK )
      LEFT JOIN V_Customer B WITH ( NOLOCK ) ON A.PARNR_TPLNR = B.ID_CUSTOMER
    WHERE
      REVNR = '$revnr'";
    return $this->db->query($sql)->row();
  }

  public function get_day_maintenance($revision, $multiple = FALSE){
    $sql = "
    SELECT
      REVNR,
      DATEDIFF( DAY, REVBD, CONVERT ( VARCHAR ( 10 ), GETDATE(), 112 )) AS DAY_MAINTENANCE,
      DATEDIFF( DAY, REVBD, REVED ) AS DAY_PLAN
    FROM
      $this->m_revision WITH ( NOLOCK )
    WHERE
      REVNR IN ('$revision')";

    $data = $this->db->query($sql)->result();

    $result = [];
    foreach ($data as $key => $val) {
      $result[$val->REVNR]['actual'] = intval($val->DAY_MAINTENANCE) + 1;
      $result[$val->REVNR]['plan'] = intval($val->DAY_PLAN) + 1;
    }

    return $multiple == FALSE ? $result[$revision] : $result;
  }

  public function update_top_project($revision, $status) {
    $sql = "
      IF EXISTS ( SELECT REVNR FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR = '$revision' ) 
        BEGIN
          UPDATE $this->tj_m_revision SET TOP_PROJECT = $status WHERE REVNR = '$revision'
        END
      ELSE
        BEGIN
          INSERT $this->tj_m_revision ( REVNR, STATUS_PLANNING ) VALUES ( '$revision', $status )
        END";

    $this->db->query($sql);
    return true;
  }

  public function set_status_planning($revision, $type) {
    try {
      $user = get_session('name');
      $user_role = get_session('user_role');

      $status = $type == 'create' ? 'NEED REVIEW' : 'REVIEWED';

      if ($type == 'create' || ($type == 'edit' && $user_role == 'Admin Hangar')) {
        $status = 'NEED REVIEW';
      } else if ($type == 'edit' && $user_role == 'Controller Hangar') {
        $status = 'UPDATED';
      } else if ($type == 'confirm') {
        $status = 'REVIEWED';
      }

      $query_update_status = "
      IF EXISTS ( SELECT REVNR FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR = '$revision' ) 
        BEGIN
          UPDATE $this->tj_m_revision SET STATUS_PLN = '$status' WHERE REVNR = '$revision'
        END
      ELSE
        BEGIN
          INSERT $this->tj_m_revision ( REVNR, STATUS_PLN ) VALUES ( '$revision', '$status' )
        END";

      $this->db->query($query_update_status);

      $type = strtoupper($type);

      $query_update_log = "
        INSERT $this->tj_log_project_planning ( REVNR, ACTION, USER_NAME, USER_ROLE, INSERT_DATE )
        VALUES 
        ( '$revision', '$type', '$user','$user_role', GETDATE() )
      ";

      $this->db->query($query_update_log);

      return true;
    } catch(Exception $e) {
      return false;
    }
  }

  public function submit_hilite($revision, $hilite) {
    try {
      $query = "
      IF EXISTS (SELECT REVNR FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR = '".$revision."') 
        BEGIN
          UPDATE $this->tj_m_revision SET HILITE = '".nl2br($hilite)."' WHERE REVNR = '$revision'
        END
      ELSE
        BEGIN
          INSERT INTO $this->tj_m_revision (REVNR, HILITE) VALUES ('".$revision."', '".nl2br($hilite)."')
        END";

      $this->db->query($query);

      return true;
    } catch(Exception $e) {
      return false;
    }
  }

  public function sync_hilite($data) {
    try {
      foreach ($data as $val) {
        $query = "
        IF EXISTS (SELECT REVNR FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR = '".$val[0]."') 
          BEGIN
            UPDATE $this->tj_m_revision SET HILITE = '".nl2br($val[1])."' WHERE REVNR = '".$val[0]."'
          END
        ELSE
          BEGIN
            INSERT INTO $this->tj_m_revision (REVNR, HILITE) VALUES ('".$val[0]."', '".nl2br($val[1])."')
          END";

        $this->db->query($query);
      }
      return true;
    } catch(Exception $e) {
      return false;
    }
  }

  public function get_hilite($revision, $multiple = FALSE) {
    $sql = "
    SELECT
      REVNR,
      HILITE
    FROM
      $this->tj_m_revision WITH ( NOLOCK )
    WHERE
      REVNR IN ('$revision')";

    $data = $this->db->query($sql)->result();

    $result = [];
    foreach ($data as $val) {
      $result[$val->REVNR] = str_replace("<br />", "", $val->HILITE);
    }

    if (count($result) > 0) {
      return $multiple == FALSE ? $result[$revision] : $result;
    } else {
      return $multiple == FALSE ? "" : $result;
    }    
  }
}