<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class RFClog_model extends CI_Model {

  private $rfclog = "TB_RFC_LOG";

  private $column_order = ['CRM', 'REVISION', 'DTIME', 'EXEC_BY', 'JOBSTATUS', 'RFCSTATUS'];
  private $column_search = ['CRM', 'REVISION', 'DTIME', 'EXEC_BY', 'JOBSTATUS', 'RFCSTATUS'];
  private $order_by = ['DTIME' => 'DESC'];

  private function _get_datatables_params() {
    $column_order = $this->column_order;
    $column_search = $this->column_search;
    $order = $this->order_by;

    $i = 0;
    if ($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if ($item != null ) {
          if ($_REQUEST['search']['value']) {  
            if ($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if (count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    }

    $i = 0;
    if ($_REQUEST['columns']) {
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$i];
          $like_arr[$index] = $key['search']['value'];
        }
        $i++;
      }
      $this->db->like($like_arr);
    }
      
    if (isset($_REQUEST['order'])) {
      if ($_REQUEST['order']['0']['column'] == 0) {
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      } else {
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if (isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result = $this->db->get_compiled_select();

    // result
    $query = explode("WHERE", $result);

    // where_datatables
    if (count($query) == 2) {
      $where = explode('ORDER BY', $query[1]);
      $order_by = $where[1];
      $where_datatables = 'WHERE ('.$where[0].')';
    } else {
      $where = explode('ORDER BY', $result);
      $order_by = $where[1];
      $where_datatables = '';
    }

    // where_paging
    $start = $_REQUEST['start'];
    $end = $_REQUEST['length'] + $_REQUEST['start'];
    $where_paging = $_REQUEST['length'] != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : '';

    return array(
      'where_paging'              => $where_paging,
      'where_datatables'          => $where_datatables,
      'order_by'                  => $order_by
    );
  }

  public function ajax_list_sync() {
    $param = $this->_get_datatables_params();

    $query = "
    WITH TMP_A AS (
      SELECT
        CASE
          WHEN VARIANT LIKE '%TB%' OR VARIANT LIKE '%MATERIAL%' THEN 'TB'
          WHEN VARIANT LIKE '%TJ%' THEN 'TJ'
          ELSE 'Other'
        END AS CRM,
        JOBNAME,
        VARIANT,
        DTIME,
        CASE
          WHEN JOBSTATUS = 'F' THEN 'FINISH'
          WHEN JOBSTATUS = 'A' THEN 'ERROR'
          ELSE 'RUNNING'
        END AS JOBSTATUS,
        RFCSTATUS,
        EXEC_BY,
        REVISION
      FROM
        $this->rfclog
    ),
    TMP_B AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_A ".$param['where_datatables']."
    )
    SELECT * FROM TMP_B ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "SELECT COUNT(JOBNAME) AS COUNT FROM $this->rfclog";

    $records_total = $this->db->query($query_all)->row()->COUNT; 

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TMP_A AS (
        SELECT
          CASE
            WHEN VARIANT LIKE '%TB%' OR VARIANT LIKE '%MATERIAL%' THEN 'TB'
            WHEN VARIANT LIKE '%TJ%' THEN 'TJ'
            ELSE 'Other'
          END AS CRM,
          JOBNAME,
          VARIANT,
          DTIME,
          CASE
            WHEN JOBSTATUS = 'F' THEN 'FINISH'
            WHEN JOBSTATUS = 'A' THEN 'ERROR'
            ELSE 'RUNNING'
          END AS JOBSTATUS,
          RFCSTATUS,
          EXEC_BY,
          REVISION
        FROM
          $this->rfclog
      ),
      TMP_B AS (
        SELECT
          ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
          *
        FROM
          TMP_A ".$param['where_datatables']."
      )
      SELECT COUNT(JOBNAME) AS COUNT FROM TMP_B";

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function check_already_created($n, $v) {
    $sql = "SELECT COUNT(ID) AS COUNT FROM $this->rfclog WITH ( NOLOCK ) WHERE JOBNAME = '$n' AND VARIANT = '$v' AND RFCSTATUS = 'CREATED'";
    $data = $this->db->query($sql)->row();
    return $data->COUNT;
  }

  public function check_already_running() {
    return $this->db->query("SELECT * FROM $this->rfclog WITH ( NOLOCK ) WHERE RFCSTATUS = 'CREATED'")->result();
  }

  public function create_log($param) {
    return $this->db->insert($this->rfclog, $param);
  }

  public function update_log($id, $param) {
    return $this->db->set($param)->where("ID", $id)->update($this->rfclog);
  }

  public function get_log_by_ids($ids) {
    $array = implode("','", $ids);
    return $this->db->query("SELECT * FROM $this->rfclog WITH ( NOLOCK ) WHERE ID IN ('".$array."')")->result();
  }

}