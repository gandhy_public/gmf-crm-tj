<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class User_group_model extends CI_Model {

  private $tj_user_group = 'TJ_USER_GROUP_NEW';
  private $tj_user_role = 'TJ_USER_ROLE_NEW';

  private $column_order = array(null, 'USER_GROUP', 'IS_ACTIVE');
  private $column_search = array(null, 'USER_GROUP', null);

  private $order = array('ID_USER_GROUP' => 'ASC', 'IS_ACTIVE' => 'DESC');

  public function get_all() {
    $query = "
    SELECT 
      *
    FROM 
      $this->tj_user_group A WITH ( NOLOCK )
      JOIN $this->tj_user_role B WITH ( NOLOCK ) ON A.USER_ROLE_ID = B.ID_USER_ROLE
    WHERE 
      A.IS_ACTIVE = 1";
    return $this->db->query($query)->result();
  }

  private function _get_datatables_params($type_data) {
    $column_order = $this->column_order;
    $column_search = $this->column_search;
    $order = $this->order;

    $i = 0;
    if($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if($item != null ) {
          if($_REQUEST['search']['value']) {  
            if($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if(count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    } 

    $j = 0;
    if($_REQUEST['columns']){
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$j];
          $like_arr[$index] = $key['search']['value'];
        }
        $j++;
      }
      $this->db->like($like_arr);
    }
      
    if(isset($_REQUEST['order'])) {
      if($_REQUEST['order']['0']['column'] == 0) {
        $this->db->order_by(key($order), $order[key($order)]);
      }else{
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if(isset($order)) {
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $result =  $this->db->get_compiled_select();

    $query = explode("WHERE", $result);

    if(count($query) == 2){
      $where = explode("ORDER BY", $query[1]);
      $order = $where[1];
      $where = "WHERE (".$where[0].")";
    }else{
      $where = explode("ORDER BY", $result);
      $order = $where[1];
      $where = "";
    }

    $start = $_REQUEST['start'];
    $end   = $_REQUEST['length'] + $_REQUEST['start'];

    // where_paging
    $where_paging = $_REQUEST['length'] != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : "";

    if($type_data == 'all') $where = "";

    return array(
      'start'           => $start,
      'end'             => $end,
      'where'           => $where,
      'order'           => $order,
      'where_paging'    => $where_paging
    );
  }

  public function get_list_groups() {
    $param = $this->_get_datatables_params('');
    
    $sql = "WITH A AS (
      SELECT
        *,
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order'].") AS ROW_ID 
      FROM
        $this->tj_user_group WITH ( NOLOCK )
        ".$param['where']."
      )
      SELECT * FROM A ".$param['where_paging'];

    return $this->db->query($sql)->result();
  }

  public function get_count_list_groups() {
    $param = $this->_get_datatables_params('all');
    
    $sql = "SELECT COUNT(ID_USER_GROUP) AS COUNT FROM $this->tj_user_group WITH ( NOLOCK )";

    $data = $this->db->query($sql)->row();

    return $data->COUNT; 
  }

  public function get_count_list_groups_filtered() {
    $param = $this->_get_datatables_params('filtered');
    
    $sql = "SELECT COUNT(ID_USER_GROUP) AS COUNT FROM $this->tj_user_group WITH ( NOLOCK ) ".$param['where'];

    $data = $this->db->query($sql)->row();

    return $data->COUNT; 
  }

  public function create_group($param) {
    $this->db->insert($this->tj_user_group, $param);
    return $this->db->query("SELECT ID_USER_GROUP FROM $this->tj_user_group WITH ( NOLOCK ) ORDER BY ID_USER_GROUP DESC")->row()->ID_USER_GROUP;
  }

  public function update_group($param, $id_user_group) {
		$this->db->set($param);
		$this->db->where('id_user_group', $id_user_group);
		return $this->db->update($this->tj_user_group);
  }

}