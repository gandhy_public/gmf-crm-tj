<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Export_model extends CI_Model {

  public function get_data($query) {
    $query = explode('WHERE ROW_ID', $query)[0];
    return $this->db->query($query)->result();
  }

}