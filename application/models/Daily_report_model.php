<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Daily_report_model extends CI_Model {

  private $m_pmorderh = 'M_PMORDERH';
  private $tj_m_pmorder = 'TJ_M_PMORDER';
  private $tj_daily_report = 'TJ_DAILY_REPORT_NEW';
  private $tj_user = 'TJ_USER_NEW';

  private $column_order_history = array(null, 'DOCNO', 'ASSIGNMENT_DATE', 'CREATED_BY', 'CREATED_AT');
  private $column_search_history = array(null, 'DOCNO', null, 'CREATED_BY', null);

  private $order = array('ID_DAILY_REPORT' => 'DESC');

  private function _get_datatables_params($type, $type_data) {
    if($type == 'detail') {
      $column_order = $this->column_order_detail;
      $column_search = $this->column_search_detail;
    } else {
      $column_order = $this->column_order_history;
      $column_search = $this->column_search_history;
    }

    $order = $this->order;
    
    $i = 0;
    if($_REQUEST['search']['value']) {
      foreach ($column_search as $item){
        if($_REQUEST['search']['value']) { 
          if($i === 0) {
            $this->db->group_start();
            $this->db->like($item, $_REQUEST['search']['value']);
          } else {
            $this->db->or_like($item, $_REQUEST['search']['value']);
          }
          if(count($column_search) - 1 == $i) $this->db->group_end();
        }
        $i++;
      }
    }
    
    $j = 0;
    if($_REQUEST['columns']) {
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$j];
          $like_arr[$index] = $key['search']['value'];
        }
        $j++;
      }
      $this->db->like($like_arr);
    }

    if(isset($_REQUEST['order'])) {
      if($_REQUEST['order']['0']['column'] == 0) {
        // $this->db->order_by(key($order), $order[key($order)]);
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      }else{
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if(isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result = $this->db->get_compiled_select();

    $start = $_REQUEST['start'];
    $end   = $_REQUEST['length'] + $_REQUEST['start'];

    $query = explode('WHERE', $result);

    if(count($query) == 2){
      $where = explode('ORDER BY', $query[1]);
      $order = $where[1];
      $where = 'AND (".$where[0].")';
    }else{
      $where = explode('ORDER BY', $result);
      $order = $where[1];
      $where = '';
    }

    if($type_data == 'all') $where = '';

    // where_paging
    $where_paging = $end != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : '';

    return array(
      'start'           => $start,
      'end'             => $end,
      'where_paging'    => $where_paging,
      'where'           => $where,
      'order'           => $order
    );
  }

  public function get_list_daily_report_history() {
    $param = $this->_get_datatables_params('history', '');
    $user_id = get_session('id_user');
    
    $where_user_id = (get_session('user_role') != "Administrator" ) ? "AND A.USER_ID = $user_id" : "";

    $sql = "
    WITH TEMP AS (
      SELECT
        A.ID_DAILY_REPORT,
        A.DOCNO,
        A.AUFNR,
        A.CREATED_AT,
        A.USER_ID,
        B.NAME AS CREATED_BY,
        A.ASSIGNMENT_DATE,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.DOCNO ORDER BY A.DOCNO DESC ) AS ROW_NUM
      FROM
        $this->tj_daily_report A WITH ( NOLOCK )
        JOIN $this->tj_user B WITH ( NOLOCK ) ON A.USER_ID = B.ID_USER
      WHERE
        A.IS_DELETED = 0 $where_user_id
    ), A AS ( 
      SELECT
        *, 
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order']." ) AS ROW_ID
      FROM
        TEMP 
      WHERE
        ROW_NUM = 1 ".$param['where']."
    ) 
    SELECT
      *
    FROM
      A ".$param['where_paging'];

    $is_filtered = $param['where'] != "" ? true : false;

    return array(
      'result'      => $this->db->query($sql)->result(),
      'is_filtered' => $is_filtered
    );
  }

  public function get_count_list_daily_report_history() {
    $param = $this->_get_datatables_params('history', 'all');
    $user_id = get_session('id_user');
    $sql = "
    WITH TEMP AS (
      SELECT
        A.DOCNO,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.DOCNO ORDER BY A.DOCNO DESC ) AS ROW_NUM
      FROM
        $this->tj_daily_report A WITH ( NOLOCK )
        JOIN ( SELECT ID_USER FROM $this->tj_user WITH ( NOLOCK ) ) AS B ON A.USER_ID = B.ID_USER
      WHERE
        A.USER_ID = $user_id
        AND
        A.IS_DELETED = 0
    )
    SELECT
      COUNT(DOCNO) AS COUNT
    FROM
      TEMP 
    WHERE
      ROW_NUM = 1";
      
    $data = $this->db->query($sql)->row();

    return $data->COUNT;
  }

  public function get_count_list_daily_report_history_filtered() {
    $param = $this->_get_datatables_params('history', 'filtered');
    $user_id = get_session('id_user');
    $sql = "WITH TEMP AS (
      SELECT
        A.DOCNO,
        A.AUFNR,
        A.CREATED_AT,
        B.NAME AS CREATED_BY,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.DOCNO ORDER BY A.DOCNO DESC ) AS ROW_NUM
      FROM
        $this->tj_daily_report A WITH ( NOLOCK )
        JOIN ( SELECT ID_USER, NAME FROM $this->tj_user WITH ( NOLOCK ) ) AS B ON A.USER_ID = B.ID_USER
      WHERE
        A.USER_ID = $user_id
        AND
        A.IS_DELETED = 0
    )
    SELECT
      COUNT(DOCNO) AS COUNT
    FROM
      TEMP 
    WHERE
      ROW_NUM = 1 ".$param['where'];
      
    $data = $this->db->query($sql)->row();

    return $data->COUNT;
  }

  public function get_list_daily_report_by_docno($docno) {
    if($docno != 'latest') {
      $where_daily = "DOCNO = '$docno'";
    } else {
      $sql = "SELECT CREATED_AT FROM $this->tj_daily_report WITH ( NOLOCK ) ORDER BY ID_DAILY_REPORT DESC";
      $new_entry = $this->db->query($sql)->row()->CREATED_AT;
      $where_daily = "CREATED_AT = '$new_entry'";
    }

    $sql = "
    WITH TJ_M_PMORDER_TMP AS ( 
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR ORDER BY A.AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY A.AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        A.ID_DAILY_REPORT,
        CASE
          WHEN C.REVNR = '' THEN 'Non Project'
          ELSE C.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN B.AUART = 'GA01' THEN 'Jobcard'
          WHEN B.AUART = 'GA02' THEN 'MDR'
          WHEN B.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.TPLNR,
        C.KTEXT,
        B.ARBEI,
        B.ISMNW,
        A.PLAN_TARGET,
        A.HIGHLIGHT,
        A.STATUS,
        A.ASSIGNMENT_DATE,
        B.TARGET_DATE
      FROM
        $this->tj_daily_report A WITH ( NOLOCK )
        JOIN $this->tj_m_pmorder B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        JOIN $this->m_pmorderh C WITH ( NOLOCK ) ON A.AUFNR = C.AUFNR
      WHERE
        $where_daily
        AND
        IS_DELETED = 0
        AND 
        A.AUFNR IS NOT NULL
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1
    ),
    TMP_B AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      WHERE
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    )
    SELECT
      A.*,
      CASE
        WHEN B.PMHRS IS NULL THEN 0
        ELSE B.PMHRS
      END AS PMHRS,
      CASE
        WHEN B.AMHRS IS NULL THEN 0
        ELSE B.AMHRS
      END AS AMHRS
    FROM
      TMP_A A
      JOIN TMP_B B ON A.AUFNR = B.AUFNR";

    return array(
      'query' => $sql,
      'result' => $this->db->query($sql)->result()
    );
  }

  public function submit_daily_report($param) {
    $user_id = get_session('id_user');

    if ($param['type'] == 'create') {
      $docno = $this->generate_docno();
      $created_at = date('Y-m-d H:i:s');
    } else {
      $docno = $param['docno'];
      $sql = "SELECT CREATED_AT FROM $this->tj_daily_report WITH ( NOLOCK ) WHERE DOCNO = '$docno'";
      $created_at = $this->db->query($sql)->row()->CREATED_AT;
    }

    $insert = $update = [];
    foreach ($param['id_daily_report'] as $key => $id) {
      if ($id == 0 || $id == '0') {
        $insert[] = [
          'DOCNO'           => $docno,
          'AUFNR'           => $param['aufnr'][$key],
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['type'] == 'create' ? '' : $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'CREATED_AT'      => $created_at,
          'UPDATED_AT'      => date('Y-m-d H:i:s'),
          'USER_ID'         => $user_id,
          'IS_DELETED'      => 0
        ];
      } else {
        $update[] = array(
          'ID_DAILY_REPORT' => $id,
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'UPDATED_AT'      => date('Y-m-d H:i:s')
        );
      }
    }

    if (count($insert) > 0) $this->db->insert_batch($this->tj_daily_report, $insert); 
    if (count($update) > 0) $this->db->update_batch($this->tj_daily_report, $update, 'ID_DAILY_REPORT');

    return $docno;
  }

  public function insert_daily_report($param) {
    $docno = $this->generate_docno();
    $user_id = get_session('id_user');
    $created_at = date('Y-m-d H:i:s');

    $submit = [];
    foreach ($param['id_daily_report'] as $key => $id) {
      $submit[] = [
        'DOCNO'           => $docno,
        'AUFNR'           => $param['aufnr'][$key],
        'PLAN_TARGET'     => $param['plan_target'][$key],
        'HIGHLIGHT'       => $param['highlight'][$key],
        'ASSIGNMENT_DATE' => $param['assignment_date'],
        'CREATED_AT'      => $created_at,
        'UPDATED_AT'      => $created_at,
        'USER_ID'         => $user_id,
        'IS_DELETED'      => 0
      ];
    }

    $this->db->insert_batch($this->tj_daily_report, $submit); 

    $insert = [];
    foreach ($param['id_daily_report'] as $key => $id) {
      if ($id == 0 || $id == '0') {
        $insert[] = [
          'DOCNO'           => $docno,
          'AUFNR'           => $param['aufnr'][$key],
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'CREATED_AT'      => $created_at,
          'UPDATED_AT'      => $updated_at,
          'USER_ID'         => $user_id,
          'IS_DELETED'      => 0
        ];
      } else {
        $data = array(
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'UPDATED_AT'      => $updated_at
        );

        $this->db->where('ID_DAILY_REPORT', $id);
        $this->db->update($this->tj_daily_report, $data);
      }

      if (count($insert) > 0) $this->db->insert_batch($this->tj_daily_report, $insert); 
    }

    return $docno;
  }

  public function update_daily_report($param) {
    $user_id = get_session('id_user');
    $docno = $param['docno'];

    $sql = "SELECT CREATED_AT FROM $this->tj_daily_report WITH ( NOLOCK ) WHERE DOCNO = '$docno'";
    $created_at = $this->db->query($sql)->row()->CREATED_AT;
    $updated_at = date('Y-m-d H:i:s');

    $insert = [];
    foreach ($param['id_daily_report'] as $key => $id) {
      if ($id == 0 || $id == '0') {
        $insert[] = [
          'DOCNO'           => $docno,
          'AUFNR'           => $param['aufnr'][$key],
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'CREATED_AT'      => $created_at,
          'UPDATED_AT'      => $updated_at,
          'USER_ID'         => $user_id,
          'IS_DELETED'      => 0
        ];
      } else {
        $data = array(
          'PLAN_TARGET'     => $param['plan_target'][$key],
          'HIGHLIGHT'       => $param['highlight'][$key],
          'STATUS'          => $param['status'][$key],
          'ASSIGNMENT_DATE' => $param['assignment_date'],
          'UPDATED_AT'      => $updated_at
        );

        $this->db->where('ID_DAILY_REPORT', $id);
        $this->db->update($this->tj_daily_report, $data);
      }

      if (count($insert) > 0) $this->db->insert_batch($this->tj_daily_report, $insert); 
    }

    return $docno;
  }

  public function delete_daily_report_by_id($id) {
    $this->db->where_in('ID_DAILY_REPORT', $id)->update($this->tj_daily_report, ['IS_DELETED' => 1]);
  }

  public function get_time_created($docno) {
    $sql = "SELECT CREATED_AT FROM $this->tj_daily_report WITH ( NOLOCK ) WHERE DOCNO = '$docno'";
    return $this->db->query($sql)->row()->CREATED_AT;
  }

  public function get_order_by_docno($docno) {
    $sql = "SELECT AUFNR, CREATED_AT FROM $this->tj_daily_report WITH ( NOLOCK ) WHERE DOCNO = '$docno'";
    $data = $this->db->query($sql)->result();

    $result = array();
    foreach ($data as $val) {
      $result[] = $val->AUFNR;
    }

    return $result;
  }

  public function update_daily_report_per_column($param) {
    $sql = "UPDATE $this->tj_daily_report SET ".$param['column']." = '".$param['value']."' WHERE DOCNO = '".$param['docno']."'";

    $this->db->query($sql);
    return true;
  }

  public function get_daily_menu_docno_today() {
    $user_id = get_session('id_user');
    $query = "
    SELECT
      TOP 1
      DOCNO,
      ASSIGNMENT_DATE
    FROM
      $this->tj_daily_report WITH ( NOLOCK )
    WHERE
      ( ASSIGNMENT_DATE >= CONVERT ( CHAR ( 8 ), GETDATE ( ), 112 ) AND ASSIGNMENT_DATE < CONVERT ( CHAR ( 8 ), Dateadd ( DAY, 1, GETDATE ( ) ), 112 ) )
      AND
      USER_ID = $user_id
    ORDER BY
      UPDATED_AT DESC, ASSIGNMENT_DATE DESC";

    return $this->db->query($query)->row();
  }

  private function generate_docno() {
    $query = "SELECT DOCNO FROM $this->tj_daily_report WITH ( NOLOCK ) GROUP BY DOCNO ORDER BY DOCNO DESC";
    $data = $this->db->query($query)->row();

    if (count($data) > 0) {
      $before = $this->removeLeadingZero($data->DOCNO);
      $docno = $this->addLeadingZero($before + 1);
    } else {
      $docno = $this->addLeadingZero(1);
    }

    return $docno;
  }

  private function addLeadingZero($value, $threshold = 8) {
    return sprintf('%0' . $threshold . 's', $value);
  }

  private function removeLeadingZero($value) {
    return (int)ltrim($value, '0');
  }

}