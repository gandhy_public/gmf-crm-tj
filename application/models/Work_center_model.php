<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Work_center_model extends CI_Model {

  private $tj_work_center = 'TJ_WORK_CENTER';

  public function get_all() {
    return $this->db->query("SELECT * FROM $this->tj_work_center WITH ( NOLOCK )")->result();
  }

  public function get_desc($work_center) {
    return $this->db->query("SELECT * FROM $this->tj_work_center WITH ( NOLOCK ) WHERE WORK_CENTER = '$work_center'")->row();
  }

  public function get_grouping() {
    $data = $this->db->query("SELECT WORK_CENTER FROM $this->tj_work_center WITH ( NOLOCK )")->result();
    $result = [];
    $inserted = false;
    foreach ($data as $key => $val) {
      $val = strtolower($val->WORK_CENTER);
      if (strpos($val, 'gah') !== false) {
        if (!$inserted) {
          $result['hangar'] = 0;
          $inserted = true;
        }
      } else {
        $result[$val] = 0;
      }
    }
    return $result;
  }

  public function get_grouping_select() {
    $data = $this->db->query("SELECT WORK_CENTER FROM $this->tj_work_center WITH ( NOLOCK )")->result();
    $result = [];
    $inserted = false;
    foreach ($data as $key => $val) {
      $val = $val->WORK_CENTER;
      if (strpos($val, 'GAH') !== false) {
        if (!$inserted) {
          $result[] = 'Hangar';
          $inserted = true;
        }
      } else {
        $result[] = $val;
      }
    }
    return $result;
  }

  public function get_grouping_by_area() {
    $result['hangar'] = [];
    $result['shop'] = [];

    $data = $this->db->query("SELECT WORK_CENTER FROM $this->tj_work_center WITH ( NOLOCK )")->result();

    foreach ($data as $key => $val) {
      $work_center = $val->WORK_CENTER;
      if (strpos($work_center, 'GAH') !== false) {
        $result['hangar'][] = $work_center;
      } else {
        $result['shop'][] = $work_center;
      }
    }

    return $result;
  }
}