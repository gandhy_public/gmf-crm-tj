<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Plant_model extends CI_Model {

  private $tj_plant = "TJ_PLANT";

  public function get_all() {
    $query = "SELECT PLANT FROM $this->tj_plant WITH ( NOLOCK )";
    $data = $this->db->query($query)->result();

    $result = array();
    foreach ($data as $val) {
      $result[] = $val->PLANT;
    }

    return $result;
  }

  public function get_only($column) {
    $query = "SELECT $column FROM $this->tj_plant WITH ( NOLOCK )";
    $data = $this->db->query($query)->result();

    $result = [];
    foreach($data as $val) {
      $result[] = $val->{$column};
    }
    
    return $result;
  }

}