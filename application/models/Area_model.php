<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Area_model extends CI_Model {

  private $tj_area = 'TJ_AREA';

  public function get_only($column, $area = '') {
    $where_area = $area == '' ? '' : "WHERE AREA = '$area'";
    $query = "SELECT $column FROM $this->tj_area WITH ( NOLOCK ) $where_area GROUP BY $column";
    $data = $this->db->query($query)->result();

    $result = [];
    foreach($data as $val) {
      $result[] = $val->{$column};
    }
    
    return $result;
  }

}