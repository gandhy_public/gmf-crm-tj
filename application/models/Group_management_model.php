<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Group_management_model extends CI_Model {

  private $tj_user = 'TJ_USER_NEW';
  private $tj_user_group = 'TJ_USER_GROUP_NEW';
  private $tj_user_role = 'TJ_USER_ROLE_NEW';
  private $tj_modul = 'TJ_MODUL';
  private $tj_menu = 'TJ_MENU_NEW';
  private $tj_group_management  = 'TJ_GROUP_MANAGEMENT';

  public function get_menu($group_id) {
    $query = "
    SELECT
      MODUL,
      MENU,
      URL,
      ICON
    FROM
      $this->tj_menu C WITH ( NOLOCK )
      JOIN $this->tj_modul D WITH ( NOLOCK ) ON C.MODUL_ID = D.ID_MODUL 
    WHERE
      ID_MENU IN (
        SELECT 
          MENU_ID
        FROM 
          TJ_GROUP_MANAGEMENT 
        WHERE 
          USER_GROUP_ID = $group_id
      )
    ORDER BY
      ID_MODUL ASC";

    $data = $this->db->query($query)->result();

    $menu = [];
    foreach ($data as $item) {
      $menu[$item->MODUL . '|' . $item->ICON][] = [
        'menu'  => $item->MENU,
        'url'   => $item->URL
      ];
    }

    return $menu;
  }

  public function get_menu_all() {
    $query = "
    SELECT 
      * 
    FROM 
      $this->tj_menu A WITH ( NOLOCK )
      JOIN $this->tj_modul B WITH ( NOLOCK ) ON A.MODUL_ID = B.ID_MODUL
    WHERE
      A.IS_ACTIVE = 1";
    
    return $this->db->query($query)->result();
  }

  public function create_menu_management($param) {
    $this->db->insert_batch($this->tj_group_management , $param);
  }

  public function update_menu_management($param, $user_group_id) {
    $this->db->where('user_group_id', $user_group_id);
		$this->db->delete($this->tj_group_management );
    $this->db->insert_batch($this->tj_group_management , $param);
  }

  public function get_menu_by_group_id($user_group_id) {
    $query = "
    SELECT 
      * 
    FROM 
      $this->tj_group_management  A WITH ( NOLOCK )
      JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
      JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
      JOIN $this->tj_menu D WITH ( NOLOCK ) ON A.MENU_ID = D.ID_MENU
      JOIN $this->tj_modul E WITH ( NOLOCK ) ON D.MODUL_ID = E.ID_MODUL
    WHERE
      A.USER_GROUP_ID = '$user_group_id'";
    
    return $this->db->query($query)->result();
  }

  public function check_group_used($user_group_id) {
    $query = "SELECT * FROM $this->tj_user WHERE USER_GROUP_ID = '$user_group_id'";
    $data = $this->db->query($query)->result();
    return count($data) > 0 ? true : false;
  }

}