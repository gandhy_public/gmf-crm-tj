<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Order_view_model extends CI_Model {

  private $m_pmorderh = 'M_PMORDERH';
  private $tj_m_pmorder = 'TJ_M_PMORDER';
  private $tj_m_pmorder_material = 'TJ_M_PMORDER_MATERIAL';
  private $tj_m_pmorder_new = 'TJ_M_PMORDER_NEW';
  private $m_revision = 'M_REVISION';
  private $tj_mrevision = 'TJ_M_REVISION';
  private $tj_order_movement = 'TJ_LOG_ORDER_MOVEMENT';
  private $tj_progress_status = 'TJ_PROGRESS_STATUS';

  private $column_order = [
    'list_order'    => [null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', 'TARGET_DATE', 'PMHRS', 'AMHRS', 'ARBPL', 'WORK_CENTER', 'PROGRESS_STATUS', null, null, null, null, 'SAP_STATUS'],
    'list_material' => [null, 'REVNR', 'AUFNR', 'MATNR', 'MATNR_ALT', 'TPLNR', 'MAKTX', 'MTART', 'BDMNG', 'QTY_TOTAL', 'MEINS', 'RESPONSIBILITY', 'MATERIAL_FULFILLMENT_STATUS', 'MATERIAL_REMARK', 'CSP_RFQ', 'PO_NUMBER', 'LEAD_TIME', 'AWB', 'INB', 'SP', 'STO', 'STORAGE_LOCATION', 'DATE_DELIVERED', 'QTY_DELIVERED', 'RECEIVER_NAME']
  ];

  private $column_search = [
    'list_order'    => [null, 'REVNR', 'AUFNR', 'AUART', 'KTEXT', 'TPLNR', null, null, null, 'ARBPL', 'WORK_CENTER', 'PROGRESS_STATUS', null, null, null, null, 'SAP_STATUS'],
    'list_material' => [null, 'REVNR', 'AUFNR', 'MATNR', 'MATNR_ALT', 'TPLNR', 'MAKTX', 'MTART', 'BDMNG', 'QTY_TOTAL', 'MEINS', 'RESPONSIBILITY', 'MATERIAL_FULFILLMENT_STATUS', 'MATERIAL_REMARK', 'CSP_RFQ', 'PO_NUMBER', 'LEAD_TIME', 'AWB', 'INB', 'SP', 'STO', 'STORAGE_LOCATION', null, 'QTY_DELIVERED', 'RECEIVER_NAME']
  ];

  private $order_by = [
    'list_order'    => ['AUFNR' => 'DESC'],
    'list_material' => ['MEINS_STATUS' => 'ASC', 'REVNR' => 'DESC', 'AUFNR' => 'DESC', 'VORNR' => 'ASC', 'RSPOS' => 'ASC']
  ];

  private function _get_datatables_params($type) {
    if ($type == 'order') {
      $column_order = $this->column_order['list_order'];
      $column_search = $this->column_search['list_order'];
      $order = $this->order_by['list_order'];
    } else {
      $column_order = $this->column_order['list_material'];
      $column_search = $this->column_search['list_material'];
      $order = $this->order_by['list_material'];
    } 
    
    $i = 0;
    if ($_REQUEST['search']['value']) {
      foreach ($column_search as $item) {
        if ($item != null ) {
          if ($_REQUEST['search']['value']) {  
            if ($i === 0) {
              $this->db->group_start();
              $this->db->like($item, $_REQUEST['search']['value']);
            } else {
              $this->db->or_like($item, $_REQUEST['search']['value']);
            }
            if (count(array_filter($column_search)) - 1 == $i) $this->db->group_end();
          }
          $i++;
        }
      }
    }

    $i = 0;
    if ($_REQUEST['columns']) {
      $like_arr = [];
      foreach ($_REQUEST['columns'] as $key) {
        if($key['search']['value']) {
          $index = $column_search[$i];
          $like_arr[$index] = $key['search']['value'];
        }
        $i++;
      }
      $this->db->like($like_arr);
    }
      
    if (isset($_REQUEST['order'])) {
      if ($_REQUEST['order']['0']['column'] == 0) {
        foreach ($order as $key => $val) {
          $this->db->order_by($key, $val);
        }
      } else {
        $this->db->order_by($column_order[$_REQUEST['order']['0']['column']], strtoupper($_REQUEST['order']['0']['dir']));
      }
    } else if (isset($order)) {
      foreach ($order as $key => $val) {
        $this->db->order_by($key, $val);
      }
    }

    $result = $this->db->get_compiled_select();

    // result
    $query = explode("WHERE", $result);

    // where_datatables
    if(count($query) == 2){
      $where = explode('ORDER BY', $query[1]);
      $order_by = $where[1];
      $where_datatables = 'WHERE ('.$where[0].')';
    } else {
      $where = explode('ORDER BY', $result);
      $order_by = $where[1];
      $where_datatables = '';
    }

    // where_paging
    $start = $_REQUEST['start'];
    $end   = $_REQUEST['length'] + $_REQUEST['start'];
    $where_paging = $_REQUEST['length'] != '-1' ? "WHERE ROW_ID > $start AND ROW_ID <= $end" : '';

    // where_revision
    $revision = $_REQUEST['revision'];
    $where_revision = '';
    if ($revision != '') {
      if ($type == 'order') {
        $where_revision = "B.REVNR = '$revision'";
      } else if ($type == 'material') {
        $where_revision = "B.REVNR = '$revision'";
      }
    }

    $where_work_center = '';
    $where_order_type = '';
    $where_progress_status = '';
    $where_material_status = '';

    if ($type == 'order') {
      // where_work_center
      $work_center = $_REQUEST['work_center'];
      if ($work_center == 'Hangar') {
        $param_work_center = "GAH1', 'GAH3', 'GAH4";
        $where_work_center = "AND WORK_CENTER_BG IN ('$param_work_center')";
      } else {
        $where_work_center = "AND WORK_CENTER_BG = '$work_center'";
      }

      // where_order_type
      $order_type = $_REQUEST['order_type'];
      $where_order_type = "AND AUART = '$order_type'";

      // where_order_status
      $progress_status = $_REQUEST['progress_status'];
      $progress_status = $progress_status == 'Progress' ? $progress_status.' in '.$work_center : $progress_status;
      $where_progress_status = "AND PROGRESS_STATUS = '$progress_status'";
    } else if ($type == 'material') {
      // where_material_status
      $material_status = $_REQUEST['material_status'];
      $where_material_status = "AND D.MATERIAL_FULFILLMENT_STATUS = '$material_status'";
    }

    return array(
      'where_paging'          => $where_paging,
      'where_datatables'      => $where_datatables,
      'where_revision'        => $where_revision,
      'where_work_center'     => $where_work_center,
      'where_progress_status' => $where_progress_status,
      'where_order_type'      => $where_order_type,
      'where_material_status' => $where_material_status,
      'order_by'              => $order_by
    );
  }

  public function get_list_order() {
    $param = $this->_get_datatables_params('order');

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        VORNR,
        AUART,
        TPLNR,
        TARGET_DATE,
        ARBEI,
        ISMNW,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS,
        REMARKS,
        PART_NAME,
        QTY,
        LOCATION,
        IS_MOVING
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        *
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1 ".$param['where_order_type']." ".$param['where_work_center']." ".$param['where_progress_status']."
    ),
    TMP_B AS (
      SELECT
        A.TOP_ORDER,
        A.TOP_OPERATION,
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        CASE
          WHEN A.AUART = 'GA01' THEN 'Jobcard'
          WHEN A.AUART = 'GA02' THEN 'MDR'
          WHEN A.AUART = 'GA05' THEN 'PD Sheet'
          ELSE 'Other'
        END AS AUART,
        B.KTEXT,
        A.TPLNR,
        A.TARGET_DATE,
        A.ARBPL,
        A.WORK_CENTER,
        A.WORK_CENTER_BG,
        A.PROGRESS_STATUS,
        A.SAP_STATUS,
        A.REMARKS,
        A.PART_NAME,
        A.QTY,
        A.LOCATION,
        A.IS_MOVING
      FROM
        TMP_A A
				JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_revision']."
    ),
    TMP_C AS (
      SELECT
        A.AUFNR,
        SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
        SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      WHERE
        ".$param['where_revision']."
        AND
        A.TOP_OPERATION = 1
      GROUP BY
        A.AUFNR 
    ),
    TMP_D AS (
      SELECT
        A.*,
        CASE
          WHEN B.PMHRS IS NULL THEN 0
          ELSE B.PMHRS
        END AS PMHRS,
        CASE
          WHEN B.AMHRS IS NULL THEN 0
          ELSE B.AMHRS
        END AS AMHRS
      FROM
        TMP_B A
        JOIN TMP_C B ON A.AUFNR = B.AUFNR
    ),
    TMP_E AS (
      SELECT
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM
        TMP_D ".$param['where_datatables']."
    )
    SELECT 
      TOP_ORDER,
      TOP_OPERATION,
      REVNR,
      AUFNR,
      AUART,
      KTEXT,
      TPLNR,
      TARGET_DATE,
      PMHRS,
      AMHRS,
      ARBPL,
      WORK_CENTER,
      WORK_CENTER_BG,
      PROGRESS_STATUS,
      SAP_STATUS,
      REMARKS,
      PART_NAME,
      QTY,
      LOCATION,
      IS_MOVING
    FROM 
      TMP_E ".$param['where_paging'];

    $result = $this->db->query($query)->result();
    
    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
        AUFNR,
        AUART,
        ARBPL,
        WORK_CENTER,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        TOP_ORDER,
        TOP_OPERATION,
        AUFNR
      FROM
        TJ_M_PMORDER_TMP
      WHERE
        TOP_ORDER = 1 ".$param['where_order_type']." ".$param['where_work_center']." ".$param['where_progress_status']."
    )
    SELECT
      COUNT(A.TOP_OPERATION) AS COUNT
    FROM
      TMP_A A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
    WHERE
      ".$param['where_revision'];

    $records_total = $this->db->query($query_all)->row()->COUNT;

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query_filtered = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR ASC, VORNR ASC ) AS TOP_ORDER,
          ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION,
          AUFNR,
          AUART,
          TPLNR,
          TARGET_DATE,
          ARBEI,
          ISMNW,
          ARBPL,
          WORK_CENTER,
          WORK_CENTER_BG,
          PROGRESS_STATUS,
          SAP_STATUS,
          REMARKS,
          PART_NAME,
          QTY,
          LOCATION,
          IS_MOVING
        FROM
          $this->tj_m_pmorder WITH ( NOLOCK )
        WHERE
          IS_ACTIVE = 1
      ),
      TMP_A AS (
        SELECT
          *
        FROM
          TJ_M_PMORDER_TMP
        WHERE
          TOP_ORDER = 1 ".$param['where_order_type']." ".$param['where_work_center']." ".$param['where_progress_status']."
      ),
      TMP_B AS (
        SELECT
          A.TOP_ORDER,
          A.TOP_OPERATION,
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          CASE
            WHEN A.AUART = 'GA01' THEN 'Jobcard'
            WHEN A.AUART = 'GA02' THEN 'MDR'
            WHEN A.AUART = 'GA05' THEN 'PD Sheet'
            ELSE 'Other'
          END AS AUART,
          B.KTEXT,
          A.TPLNR,
          A.TARGET_DATE,
          A.ARBPL,
          A.WORK_CENTER,
          A.WORK_CENTER_BG,
          A.PROGRESS_STATUS,
          A.SAP_STATUS,
          A.REMARKS,
          A.PART_NAME,
          A.QTY,
          A.LOCATION,
          A.IS_MOVING
        FROM
          TMP_A A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_revision']."
      ),
      TMP_C AS (
        SELECT
          A.AUFNR,
          SUM ( CAST ( REPLACE( A.ARBEI, ',', '' ) AS FLOAT ) ) AS 'PMHRS',
          SUM ( CAST ( REPLACE( A.ISMNW, ',', '' ) AS FLOAT ) ) AS 'AMHRS' 
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        WHERE
          ".$param['where_revision']."
          AND
          A.TOP_OPERATION = 1
        GROUP BY
          A.AUFNR 
      ),
      TMP_D AS (
        SELECT
          A.*,
          CASE
            WHEN B.PMHRS IS NULL THEN 0
            ELSE B.PMHRS
          END AS PMHRS,
          CASE
            WHEN B.AMHRS IS NULL THEN 0
            ELSE B.AMHRS
          END AS AMHRS
        FROM
          TMP_B A
          JOIN TMP_C B ON A.AUFNR = B.AUFNR
      )
      SELECT COUNT(TOP_OPERATION) AS COUNT FROM TMP_D ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }

  public function get_list_material() {
    $param = $this->_get_datatables_params('material');

    $query = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        VORNR,
        RSPOS,
        TPLNR,
        MATNR,
        MAKTX,
        MTART,
        MEINS,
        BDMNG,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
        AND MATNR != ''
    ),
    TMP_A AS (
      SELECT
        CASE
          WHEN B.REVNR = '' THEN 'Non Project'
          ELSE B.REVNR
        END AS REVNR,
        A.AUFNR,
        A.VORNR,
        A.RSPOS,
        A.TPLNR,
        A.MATNR,
        A.MAKTX,
        A.MTART,
        A.MEINS,
        CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT) AS BDMNG,
        D.MATNR_ALT,
        D.RESPONSIBILITY,
        D.MATERIAL_FULFILLMENT_STATUS,
        D.MATERIAL_REMARK,
        D.CSP_RFQ,
        D.PO_NUMBER,
        D.LEAD_TIME,
        D.AWB,
        D.INB,
        D.SP,
        D.STO,
        D.STORAGE_LOCATION,
        D.DATE_DELIVERED,
        D.QTY_DELIVERED,
        D.RECEIVER_NAME
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
      WHERE
        ".$param['where_revision']." ".$param['where_material_status']."
    ),
    TMP_B AS (
      SELECT
       A.MATNR,
       SUM ( CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT ) ) AS QTY_TOTAL,
       (
        SELECT 
          CASE 
            WHEN COUNT(*) = 1 THEN 'TRUE'
            WHEN COUNT(*) > 1 THEN 'FALSE'
          END
        FROM (
          SELECT
            DISTINCT D.MEINS
          FROM 
            TJ_M_PMORDER_TMP D
          WHERE 
            D.MATNR = A.MATNR
        ) AS MEINS_TABLE
       ) AS MEINS_STATUS
      FROM
        TJ_M_PMORDER_TMP A
        JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
        LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
        LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
      WHERE
        ".$param['where_revision']." ".$param['where_material_status']."
      GROUP BY
        A.MATNR
    ),
    TMP_C AS (
      SELECT
        A.*,
        B.QTY_TOTAL,
        B.MEINS_STATUS
      FROM
        TMP_A A
        JOIN TMP_B B ON A.MATNR = B.MATNR
    ),
    TMP_D AS ( 
      SELECT 
        ROW_NUMBER ( ) OVER ( ORDER BY ".$param['order_by']." ) AS ROW_ID,
        *
      FROM 
        TMP_C ".$param['where_datatables']."
    )
    SELECT
      REVNR,
      AUFNR,
      VORNR,
      RSPOS,
      MATNR,
      MATNR_ALT,
      TPLNR,
      MAKTX,
      MTART,
      BDMNG,
      QTY_TOTAL,
      MEINS,
      MEINS_STATUS,
      RESPONSIBILITY,
      MATERIAL_FULFILLMENT_STATUS,
      MATERIAL_REMARK,
      CSP_RFQ,
      PO_NUMBER,
      LEAD_TIME,
      AWB,
      INB,
      SP,
      STO,
      STORAGE_LOCATION,
      DATE_DELIVERED,
      QTY_DELIVERED,
      RECEIVER_NAME
    FROM 
      TMP_D ".$param['where_paging'];

    $result = $this->db->query($query)->result();

    $query_all = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        VORNR,
        RSPOS,
        MATNR,
        PROGRESS_STATUS,
        SAP_STATUS
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
        AND MATNR != ''
    )
    SELECT
      COUNT(A.MATNR) AS COUNT
      FROM
      TJ_M_PMORDER_TMP A
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
      LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
      LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
    WHERE
      ".$param['where_revision']." ".$param['where_material_status'];

    $records_total = $this->db->query($query_all)->row()->COUNT;

    $records_filtered = $records_total;
    if ($param['where_datatables'] != '') {
      $query = "
      WITH TJ_M_PMORDER_TMP AS (
        SELECT
          AUFNR,
          VORNR,
          RSPOS,
          TPLNR,
          MATNR,
          MAKTX,
          MTART,
          MEINS,
          BDMNG,
          PROGRESS_STATUS,
          SAP_STATUS
        FROM
          $this->tj_m_pmorder WITH ( NOLOCK )
        WHERE
          IS_ACTIVE = 1
          AND MATNR != ''
      ),
      TMP_A AS (
        SELECT
          CASE
            WHEN B.REVNR = '' THEN 'Non Project'
            ELSE B.REVNR
          END AS REVNR,
          A.AUFNR,
          A.VORNR,
          A.RSPOS,
          A.TPLNR,
          A.MATNR,
          A.MAKTX,
          A.MTART,
          A.MEINS,
          CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT) AS BDMNG,
          D.MATNR_ALT,
          D.RESPONSIBILITY,
          D.MATERIAL_FULFILLMENT_STATUS,
          D.MATERIAL_REMARK,
          D.CSP_RFQ,
          D.PO_NUMBER,
          D.LEAD_TIME,
          D.AWB,
          D.INB,
          D.SP,
          D.STO,
          D.STORAGE_LOCATION,
          D.DATE_DELIVERED,
          D.QTY_DELIVERED,
          D.RECEIVER_NAME
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
          LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
        WHERE
          ".$param['where_revision']." ".$param['where_material_status']."
      ),
      TMP_B AS (
        SELECT
        A.MATNR,
        SUM ( CAST ( REPLACE( A.BDMNG, ',', '' ) AS FLOAT ) ) AS QTY_TOTAL,
        (
          SELECT 
            CASE 
              WHEN COUNT(*) = 1 THEN 'TRUE'
              WHEN COUNT(*) > 1 THEN 'FALSE'
            END
          FROM (
            SELECT
              DISTINCT D.MEINS
            FROM 
              TJ_M_PMORDER_TMP D
            WHERE 
              D.MATNR = A.MATNR
          ) AS MEINS_TABLE
        ) AS MEINS_STATUS
        FROM
          TJ_M_PMORDER_TMP A
          JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
          LEFT JOIN $this->m_revision C WITH ( NOLOCK ) ON B.REVNR = C.REVNR
          LEFT JOIN $this->tj_m_pmorder_material D WITH ( NOLOCK ) ON ( A.AUFNR = D.AUFNR AND A.VORNR = D.VORNR AND A.RSPOS = D.RSPOS )
        WHERE
        ".$param['where_revision']." ".$param['where_material_status']."
        GROUP BY
          A.MATNR
      ),
      TMP_C AS (
        SELECT
          A.*,
          B.QTY_TOTAL,
          B.MEINS_STATUS
        FROM
          TMP_A A
          JOIN TMP_B B ON A.MATNR = B.MATNR
      )
      SELECT COUNT(MATNR) AS COUNT FROM TMP_C ".$param['where_datatables'];

      $records_filtered = $this->db->query($query_filtered)->row()->COUNT;
    }

    return array(
      'query'             => $query,
      'result'            => $result,
      'records_total'     => $records_total,
      'records_filtered'  => $records_filtered
    );
  }
}