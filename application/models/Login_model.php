<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Login_model extends CI_Model {

	private $tj_user = "TJ_USER_NEW";
	private $tj_user_role = "TJ_USER_ROLE_NEW";
	private $tj_user_group = "TJ_USER_GROUP_NEW";

	private function checkLdap($username, $password) {
		$dn = "DC=gmf-aeroasia,DC=co,DC=id";
		$ip_ldap = [
			'0' => "192.168.240.57",
			'1' => "172.16.100.46"
		];
		for($a=0;$a<count($ip_ldap);$a++){
			$ldapconn = ldap_connect($ip_ldap[$a]);
			if($ldapconn){
				break;
			} else {
				continue;
			}
		}

		//$ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
		if ($ldapconn) {
			ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
			$ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
			@$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
			@$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
			@$info = ldap_get_entries($ldapconn, @$sr);
			@$infomail = ldap_get_entries($ldapconn, @$srmail);
			@$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
			@$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
			/*print("<pre>".print_r(@$info,true)."</pre>");
			print("<pre>".print_r(@$infomail,true)."</pre>");
			print("<pre>".print_r(@$usermail,true)."</pre>");
			print("<pre>".print_r(@$bind,true)."</pre>");
			die();*/
			if(@$info['count']){
				return (@$info[0]["samaccountname"][0] == $username AND $bind) ? true : false;
			} else {
				return false;
			}
		} else {
			echo "LDAP Connection trouble, please try again 2/3 times";
		}
	}

	public function auth($username, $password) {
		try {
			$query = "SELECT PASSWORD FROM $this->tj_user WITH ( NOLOCK ) WHERE USERNAME = '$username' AND IS_ACTIVE = 1";
			$cekUserUsingLdap = $this->db->query($query)->result_array();

			if(count($cekUserUsingLdap) > 0 ) {
				if($cekUserUsingLdap[0]['PASSWORD'] == 'using ldap'){
					$cek_ldap = $this->checkLdap($username, $password);
					if(!$cek_ldap) throw new Exception('Data user LDAP not found!', 1);
					$password = $cekUserUsingLdap[0]['PASSWORD'];
				} else {
					$password = md5($password);
				}
			} else {
				throw new Exception('Data user not found!', 1);
			}

			$query = "	
				SELECT 
					* 
				FROM 
					$this->tj_user A WITH ( NOLOCK )
					JOIN $this->tj_user_group B WITH ( NOLOCK ) ON A.USER_GROUP_ID = B.ID_USER_GROUP
					JOIN $this->tj_user_role C WITH ( NOLOCK ) ON B.USER_ROLE_ID = C.ID_USER_ROLE
				WHERE
					A.USERNAME = '$username' 
					AND A.PASSWORD = '$password'
					AND A.IS_ACTIVE = 1
					AND B.IS_ACTIVE = 1";
			
			$check_user = $this->db->query($query)->row();

			if(count($check_user) < 1) throw new Exception('Data user not found!', 1);

			return [
				'code_status' => 's',
				'message'     => 'success',
				'data'        => $check_user
			];
		} catch (Exception $e) {
			return [
				'code_status' => 'e',
				'message'     => $e->getMessage(),
				'data'        => []
			];
		}
	}

	public function update_last_login($id_user) {
		date_default_timezone_set('Asia/Jakarta');
		$datetime = date("Y-m-d h:i:s");
		
		return $this->db->set(['last_login' => $datetime])->where('ID_USER', $id_user)->update($this->tj_user);
	}
}