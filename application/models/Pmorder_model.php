<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Pmorder_model extends CI_Model {

  private $m_pmorderh = 'M_PMORDERH';
  private $tj_m_pmorder = 'TJ_M_PMORDER';
  private $tj_m_pmorder_material = 'TJ_M_PMORDER_MATERIAL';
  private $tj_m_pmorder_new = 'TJ_M_PMORDER_NEW';
  private $tj_m_revision = 'TJ_M_REVISION';
  private $tj_order_movement = 'TJ_LOG_ORDER_MOVEMENT';
  private $tj_progress_status = 'TJ_PROGRESS_STATUS';

  public function get_total_progress_status($revision, $work_center = '') {
    $where_work_center = ($work_center != '') ? "AND B.WORK_CENTER_BG IN ('$work_center')" : '';

    $sql = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        AUART,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR DESC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    )
    SELECT
      B.AUART,
      B.PROGRESS_STATUS,
      B.TOP_ORDER,
      B.TOP_OPERATION
    FROM 
      $this->m_pmorderh A WITH ( NOLOCK )
      JOIN TJ_M_PMORDER_TMP B ON A.AUFNR = B.AUFNR
    WHERE 
      A.REVNR = '$revision'
       $where_work_center
      AND
      B.TOP_ORDER = 1";

    $data = $this->db->query($sql)->result();

    $order_type = ['GA01', 'GA02', 'GA05'];
    $progress_status = $this->db->query("SELECT PROGRESS_STATUS FROM $this->tj_progress_status")->result();
 
    $result = array();
    foreach ($order_type as $auart) {
      $auart = strtolower($auart);
      foreach ($progress_status as $status) {
        $result[strtolower($auart)][strtolower($status->PROGRESS_STATUS)] = 0;
      }
      $result[$auart]['progress'] = 0;
      $result[$auart]['total'] = 0;
    }

    foreach ($data as $item) {
      $status = strtolower($item->PROGRESS_STATUS);
      $auart = strtolower($item->AUART);
      
      // $result[$auart][$status]++;
      $result[$auart]['total']++;

      if ($status == 'open' || $status == 'waiting ro' || $status == 'waiting material') {
        $result[$auart]['open']++;
      } else if (strpos($status, 'progress') !== false || $status == 'sent to other') {
        $result[$auart]['progress']++;
      } else { // status close
        $result[$auart][$status]++;
      }
    }

    return $result;
  }

  public function get_total_progress_status_monitoring($revision, $work_center, $progress_status) {
    $where_work_center = ($work_center != '') ? "AND B.WORK_CENTER_BG IN ('$work_center')" : '';

    $sql = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        AUART,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR DESC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    )
    SELECT
      A.REVNR,
      B.AUART,
      B.PROGRESS_STATUS,
      B.TOP_ORDER,
      B.TOP_OPERATION
    FROM 
      $this->m_pmorderh A WITH ( NOLOCK )
      JOIN TJ_M_PMORDER_TMP B ON A.AUFNR = B.AUFNR
    WHERE 
      A.REVNR IN ('$revision') $where_work_center
      AND
      B.TOP_ORDER = 1";

    $data = $this->db->query($sql)->result();

    $revision_list = explode("','", $revision);
    $order_type = ['GA01', 'GA02', 'GA05'];

    $result = array();
    foreach ($revision_list as $revnr) {
      foreach ($order_type as $auart) {
        foreach ($progress_status as $status) {
          $result[$revnr][$auart][$status] = 0;
        }
      }
    }

    foreach ($data as $val) {
      $result[$val->REVNR][$val->AUART][$val->PROGRESS_STATUS]++;
    }

    return $result;
  }

  public function get_progress_status_by_revision($revision, $work_center = '') {
    $where_work_center = ($work_center != '') ? "AND B.WORK_CENTER_BG IN ('$work_center')" : '';

    $sql = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        AUART,
        WORK_CENTER_BG,
        PROGRESS_STATUS,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR DESC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        B.WORK_CENTER_BG AS WORK_CENTER,
        B.AUART,
        B.PROGRESS_STATUS,
        B.TOP_ORDER,
        B.TOP_OPERATION
      FROM 
        $this->m_pmorderh A WITH ( NOLOCK )
        JOIN TJ_M_PMORDER_TMP B ON A.AUFNR = B.AUFNR 
      WHERE 
        A.REVNR = '$revision' $where_work_center
        AND
        B.TOP_ORDER = 1
    )
    SELECT 
      WORK_CENTER, AUART, PROGRESS_STATUS, TOP_ORDER, TOP_OPERATION, COUNT(*) AS TOTAL
    FROM 
      TMP_A
    GROUP BY 
      WORK_CENTER, AUART, PROGRESS_STATUS, TOP_ORDER, TOP_OPERATION";

    return $this->db->query($sql)->result_array();
  }

  public function get_order_close_percentage($revision, $work_center, $multiple = FALSE) {
    $where_work_center = ($work_center != '') ? "AND B.WORK_CENTER IN ('$work_center')" : '';

    $sql = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        AUART,
        WORK_CENTER,
        PROGRESS_STATUS,
        SAP_STATUS,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR DESC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    ),
    TMP_A AS (
      SELECT
        A.REVNR,
        B.AUART,
        B.PROGRESS_STATUS,
        B.SAP_STATUS,
        B.TOP_ORDER,
        B.TOP_OPERATION
      FROM 
        $this->m_pmorderh A WITH ( NOLOCK )
        JOIN TJ_M_PMORDER_TMP B ON A.AUFNR = B.AUFNR 
      WHERE 
        A.REVNR IN ('$revision') $where_work_center
        AND
        B.TOP_ORDER = 1
    ),
    TMP_TOTAL AS (
      SELECT 
        REVNR,
        AUART,
        COUNT(REVNR) AS TOTAL
      FROM
        TMP_A
      GROUP BY
        REVNR, AUART
    ),
    TMP_CLOSE AS (
      SELECT 
        REVNR,
        AUART,
        COUNT(REVNR) AS TOTAL_CLOSE
      FROM
        TMP_A
      WHERE
        PROGRESS_STATUS = 'Close'
        OR
        SAP_STATUS IN ( 'Completed', 'Closed' )
      GROUP BY
        REVNR, AUART
    )
    SELECT
      A.REVNR,
      A.AUART,
      CASE
        WHEN B.TOTAL_CLOSE IS NULL THEN 0
        ELSE B.TOTAL_CLOSE
      END AS TOTAL_CLOSE,
      A.TOTAL
    FROM
      TMP_TOTAL A
      LEFT JOIN TMP_CLOSE B ON ( A.REVNR = B.REVNR AND A.AUART = B.AUART )";

    $data = $this->db->query($sql)->result();

    $revision_list = explode("','", $revision);
    $order_type = ['GA01', 'GA02', 'GA05'];

    $result = [];

    foreach ($revision_list as $revnr) {
      foreach ($order_type as $auart) {
        $result[$revnr][$auart] = -1;
      }
    }

    foreach ($data as $val) {
      foreach ($revision_list as $revnr) {
        foreach ($order_type as $auart) {
          if ($revnr == $val->REVNR && $auart == $val->AUART) {
            $result[$revnr][$auart] = @round((intval($val->TOTAL_CLOSE) / intval($val->TOTAL)) * 100, 2);
          }   
        }
      }
    }

    return $multiple == FALSE ? $result[$revision] : $result;
  }

  public function get_total_manhours($revnr) { 
    $sql = "
    WITH TJ_M_PMORDER_TMP AS (
      SELECT
        AUFNR,
        ARBPL_BG,
        ARBEI,
        ISMNW,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR ORDER BY AUFNR DESC, VORNR ASC ) AS TOP_ORDER,
        ROW_NUMBER ( ) OVER ( PARTITION BY AUFNR, VORNR ORDER BY VORNR ASC, RSPOS DESC, ARBEI DESC, ISMNW DESC ) AS TOP_OPERATION
      FROM
        $this->tj_m_pmorder WITH ( NOLOCK )
      WHERE
        IS_ACTIVE = 1
    )
    SELECT
      A.AUFNR,
      B.ARBPL_BG,
      CAST ( REPLACE ( B.ARBEI, ',', '' ) AS FLOAT ) AS PMHRS,
      CAST ( REPLACE ( B.ISMNW, ',', '' ) AS FLOAT ) AS AMHRS
    FROM
      $this->m_pmorderh A WITH ( NOLOCK )
      JOIN TJ_M_PMORDER_TMP B ON A.AUFNR = B.AUFNR 
    WHERE
      A.REVNR = '$revnr'
      AND
      B.TOP_OPERATION = 1";

    return $this->db->query($sql)->result();
  }

  public function update_progress_status($aufnr, $status) {
    for ($i=0; $i < count($aufnr); $i++) { 
      $sql = "UPDATE $this->tj_m_pmorder SET PROGRESS_STATUS = '$status' WHERE AUFNR = '".$aufnr[$i]."'";
      $this->db->query($sql);
    }
          
    return true;
  }

  public function approve_order($param) {
    $aufnr = $param['aufnr'];
    $work_center_origin = $param['work_center_origin'];
    $work_center_moving = $param['work_center_moving'];

    $approval_date = date('Y-m-d h:i:s');
    
    $submit = [];
    for ($i=0; $i < count($aufnr); $i++) { 
      $sql = "IF EXISTS (SELECT AUFNR FROM $this->tj_order_movement WITH ( NOLOCK ) WHERE AUFNR LIKE '".$aufnr[$i]."') 
        BEGIN
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) VALUES ('".$aufnr[$i]."', '".$work_center_moving[$i]."', GETDATE());
          UPDATE $this->tj_m_pmorder SET MOVED = 1 WHERE AUFNR = '".$aufnr[$i]."' AND ARBPL = '".$work_center_moving[$i]."'
        END
      ELSE
        BEGIN
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) VALUES ('".$aufnr[$i]."', '".$work_center_origin[$i]."', GETDATE());
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) VALUES ('".$aufnr[$i]."', '".$work_center_moving[$i]."', GETDATE());
          UPDATE $this->tj_m_pmorder SET MOVED = 1 WHERE AUFNR = '".$aufnr[$i]."' AND ARBPL IN ('".$work_center_origin[$i]."', '".$work_center_moving[$i]."')
        END";

      $this->db->query($sql);

      $submit[] = [
        'AUFNR'           => $aufnr[$i],
        'WORK_CENTER_BG'  => $work_center_moving[$i],
        'PROGRESS_STATUS' => 'Open',
        'IS_WC_CHANGED'   => 1,
        'IS_MOVING'       => 0
      ];
    }

    // delete order from TJ_M_PMORDER_NEW
    $this->db->where_in('AUFNR', $aufnr)->delete($this->tj_m_pmorder_new);

    // update TJ_M_PMORDER
    $this->db->update_batch($this->tj_m_pmorder, $submit, 'AUFNR');

    return true;
  }

  public function move_order($aufnr, $work_center_curr, $work_center_dest) {
    $aufnr_list = implode("','", $aufnr);
    
    $this->db->query("UPDATE $this->tj_m_pmorder SET WORK_CENTER = '$work_center_dest', IS_MOVING = 1, UPDATE_DATE = GETDATE() WHERE AUFNR IN ('".$aufnr_list."')");

    $submit = [];
    for ($i=0; $i < count($aufnr); $i++) { 
      $submit[] = [
        'AUFNR'             => $aufnr[$i],
        'WORK_CENTER_SRC'   => $work_center_curr[$i],
        'WORK_CENTER_DEST'  => $work_center_dest,
        'INSERT_DATE'       => date('Y-m-d H:i:s')
      ];
    }

    $this->db->insert_batch($this->tj_m_pmorder_new, $submit); 

    return true;
  }

  public function cancel_move_order($aufnr) {
    $aufnr_list = implode("','", $aufnr);

    // back to before changed and check if workcenter has been changed before
    $query = "
    UPDATE
      $this->tj_m_pmorder
    SET
      WORK_CENTER = WORK_CENTER_BG,
      IS_WC_CHANGED = CASE  
                        WHEN IS_WC_CHANGED = 1 THEN 1  
                        ELSE 0
                      END,
      IS_MOVING = 0,
      UPDATE_DATE = GETDATE()
    WHERE
      AUFNR IN ('".$aufnr_list."')";
    $this->db->query($query);
    
    $this->db->where('AUFNR', $aufnr_list)->delete($this->tj_m_pmorder_new);

    return true;
  }

  public function update_responsibility_material($param) {
    $sql = "
    IF EXISTS ( SELECT AUFNR FROM $this->tj_m_pmorder_material WITH ( NOLOCK ) WHERE AUFNR = '".$param['aufnr']."' AND VORNR = '".$param['vornr']."' AND RSPOS = '".$param['rspos']."' ) BEGIN
      UPDATE
        $this->tj_m_pmorder_material
      SET 
        RESPONSIBILITY = '".$param['responsibility']."' 
      WHERE
        AUFNR = '".$param['aufnr']."' 
        AND VORNR = '".$param['vornr']."' 
        AND RSPOS = '".$param['rspos']."' 
    END ELSE BEGIN
      INSERT INTO $this->tj_m_pmorder_material( AUFNR, VORNR, RSPOS, RESPONSIBILITY )
      VALUES
      ('".$param['aufnr']."', '".$param['vornr']."', '".$param['rspos']."', '".$param['responsibility']."' ) 
    END";

    $this->db->query($sql);
    return true;
  }

  public function update_order_per_column($param) {
    $sql = "UPDATE $this->tj_m_pmorder SET ".$param['column']." = '".$param['value']."' WHERE AUFNR = '".$param['aufnr']."'";

    $this->db->query($sql);
    return true;
  }

  public function update_material_per_column($param) {
    $sql = "
    IF EXISTS ( SELECT AUFNR FROM $this->tj_m_pmorder_material WITH ( NOLOCK ) WHERE AUFNR = '".$param['aufnr']."' AND VORNR = '".$param['vornr']."' AND RSPOS = '".$param['rspos']."' ) BEGIN
      UPDATE
        $this->tj_m_pmorder_material
      SET 
        ".$param['column']." = '".$param['value']."' 
      WHERE
        AUFNR = '".$param['aufnr']."' 
        AND VORNR = '".$param['vornr']."' 
        AND RSPOS = '".$param['rspos']."' 
    END ELSE BEGIN
      INSERT INTO $this->tj_m_pmorder_material( AUFNR, VORNR, RSPOS, ".$param['column']." )
      VALUES
      ('".$param['aufnr']."', '".$param['vornr']."', '".$param['rspos']."', '".$param['value']."' ) 
    END";

    $this->db->query($sql);
    return true;
  }

  public function update_material_multiple($param) {
    $target = $param['target'];
    $column = $param['column'];
    $value = $param['value'];
  
    for ($i = 0; $i < count($target); $i++) { 
      $split = explode('|', $target[$i]);
      $aufnr = $split[1];
      $vornr = $split[2];
      $rspos = $split[3];

      $sql = "
      IF EXISTS ( SELECT AUFNR FROM $this->tj_m_pmorder_material WITH ( NOLOCK ) WHERE AUFNR = '$aufnr' AND VORNR = '$vornr' AND RSPOS = '$rspos' ) 
        BEGIN
          UPDATE
            $this->tj_m_pmorder_material
          SET 
            $column = '$value' 
          WHERE
            AUFNR = '$aufnr' 
            AND VORNR = '$vornr' 
            AND RSPOS = '$rspos' 
        END 
      ELSE 
        BEGIN
          INSERT INTO $this->tj_m_pmorder_material( AUFNR, VORNR, RSPOS, $column )
          VALUES
          ('$aufnr', '$vornr', '$rspos', '$value' ) 
        END";
  
      $this->db->query($sql);
    }
          
    return true;
  }
  
  public function get_materials_value($revision, $work_center, $multiple = FALSE) {
    $where_work_center = ($work_center != '') ? "AND B.WORK_CENTER IN ('$work_center')" : '';

    $sql = "
    WITH TMP AS (
      SELECT
        C.REVNR,
        A.AUFNR,
        A.MATERIAL_FULFILLMENT_STATUS,
        A.CSP_RFQ
      FROM
        $this->tj_m_pmorder_material A WITH ( NOLOCK )
        LEFT JOIN ( SELECT AUFNR, VORNR, RSPOS, MATNR, WORK_CENTER FROM $this->tj_m_pmorder WITH ( NOLOCK ) ) AS B ON ( A.AUFNR = B.AUFNR AND A.VORNR = B.VORNR AND A.RSPOS = B.RSPOS )
        LEFT JOIN ( SELECT REVNR, AUFNR FROM $this->m_pmorderh WITH ( NOLOCK ) ) AS C ON A.AUFNR = C.AUFNR
      WHERE
        C.REVNR IN ('$revision')
        AND
        B.MATNR != ''
    ),
    TMP_ACTUAL AS (
      SELECT
        REVNR,
        MATERIAL_FULFILLMENT_STATUS, 
        COUNT(CSP_RFQ) AS TOTAL,
        CSP_RFQ
      FROM
        TMP
      WHERE
        MATERIAL_FULFILLMENT_STATUS = 'Actual Nil Stock'
        AND
        CSP_RFQ != ''
      GROUP BY 
        REVNR,
        MATERIAL_FULFILLMENT_STATUS,
        CSP_RFQ
    ),
    TMP_ALL AS (
      SELECT
        REVNR,
        MATERIAL_FULFILLMENT_STATUS, 
        COUNT(MATERIAL_FULFILLMENT_STATUS) AS TOTAL
      FROM
        TMP
      WHERE
        MATERIAL_FULFILLMENT_STATUS != 'Actual Nil Stock'
      GROUP BY 
        REVNR,
        MATERIAL_FULFILLMENT_STATUS
    )
    SELECT * FROM TMP_ACTUAL
    UNION
    SELECT *, NULL AS CSP_RFQ FROM TMP_ALL";
    
    $data = $this->db->query($sql)->result();

    $material_status = ['Actual Nil Stock', 'Ordered By Purchasing', 'Shipment/Custom Process', 'Provision in Store', 'Preloaded in Hangar/Shop Store', 'Delivered to Production'];

    $result = [];
    $revision_list = explode("','", $revision);
    foreach ($revision_list as $revnr) {
      foreach ($material_status as $val) {
        if ($val == 'Actual Nil Stock') {
          $result[$revnr][$val] = [
            'csp' => 0,
            'rfq' => 0
          ];
        } else {
          $result[$revnr][$val] = 0;
        }
      }
    }

    foreach ($data as $key => $val) {
      if ($val->CSP_RFQ != "") {
        $result[$val->REVNR][$val->MATERIAL_FULFILLMENT_STATUS][strtolower($val->CSP_RFQ)] = $val->TOTAL;
      } else {
        $result[$val->REVNR][$val->MATERIAL_FULFILLMENT_STATUS] = $val->TOTAL;
      }
    }

    return $multiple == FALSE ? $result[$revision] : $result;
  }

  public function get_material_value_single($revision, $column, $value) {
    $sql = "
    SELECT
      A.MATERIAL_FULFILLMENT_STATUS
    FROM
      $this->tj_m_pmorder_material A WITH ( NOLOCK )
      JOIN $this->m_pmorderh B WITH ( NOLOCK ) ON A.AUFNR = B.AUFNR
    WHERE
      B.REVNR = '$revision' AND ".$column." = '$value'";
    return count($this->db->query($sql)->result());
  }

  public function sync_order($data) {
    $aufnr = [];
    $work_center = [];
    foreach ($data as $val) {
      $aufnr[] = $val[0];
      $work_center[] = $val[2];
    }

    for ($i=0; $i < count($aufnr); $i++) { 
      $sql = "
      IF EXISTS (SELECT AUFNR FROM $this->tj_order_movement WITH ( NOLOCK ) WHERE AUFNR = '".$aufnr[$i]."') 
        BEGIN
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) VALUES ('".$aufnr[$i]."', '".$work_center[$i]."', GET_DATE());
        END
      ELSE
        BEGIN
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) 
          SELECT TOP 1
            AUFNR,
            ARBPL_BG,
            GET_DATE()
          FROM
            $this->m_pmorder
          WHERE
            AUFNR = '".$aufnr[$i]."';
          INSERT INTO $this->tj_order_movement (AUFNR, WORK_CENTER, INSERT_DATE) VALUES ('".$aufnr[$i]."', '".$work_center[$i]."', GET_DATE())
        END";

      $this->db->query($sql);
    }

    $value = "";
    $i = 1;
    foreach ($data as $val) {
      $rslt = implode("','", $val);
      $value .= "('$rslt')";
      if ($i < count($data)) $value .= ",";
      $i++;
    }

    $sql = "
    UPDATE
      TJ_M_PMORDER
    SET
      PROGRESS_STATUS = S.PROGRESS_STATUS,
      WORK_CENTER = S.WORK_CENTER,
      WORK_CENTER_BG = S.WORK_CENTER,
      UPDATE_DATE = GETDATE()
    FROM
      $this->tj_m_pmorder T WITH ( NOLOCK )
      JOIN (
        VALUES
        $value
      ) AS S ( AUFNR, PROGRESS_STATUS, WORK_CENTER ) ON T.AUFNR = S.AUFNR";
    
    $this->db->query($sql);

    $this->db->query("UPDATE TJ_CHECK SET SYNC_ORDER = 1");

    return TRUE;
  }

  public function is_top_project($revision) {
    $revision = implode("','", $revision);
    $data = $this->db->query("SELECT REVNR, TOP_PROJECT FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR IN ('$revision')")->result();

    $result = [];
    foreach ($data as $val) {
      if ($val->TOP_PROJECT == 1 || $val->TOP_PROJECT == "1") {
        $result[] = $val->REVNR;
      }
    }

    return $result;
  }

  public function get_status_check($identity) {
    $sql = "SELECT * FROM TJ_CHECK WITH ( NOLOCK )";
    $data = $this->db->query($sql)->result_array();

    $value = '';
    foreach ($data as $val) {
      if ($val['ID'] == $identity) {
        $value = $val['VALUE'];
      }
    }

    return $value;
  }
}
