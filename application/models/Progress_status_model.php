<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Progress_status_model extends CI_Model {

  private $tj_progress_status = 'TJ_PROGRESS_STATUS';

  public function get() {
    $data = $this->db->query("SELECT PROGRESS_STATUS FROM $this->tj_progress_status WITH ( NOLOCK )")->result();

    $result = array();
    foreach ($data as $val) {
      $result[] = $val->PROGRESS_STATUS;
    }

    return $result;
  }

  public function get_without_progress() {
    $data = $this->db->query("SELECT PROGRESS_STATUS FROM $this->tj_progress_status WITH ( NOLOCK ) WHERE PROGRESS_STATUS NOT LIKE 'Progress%'")->result();

    $result = array();
    foreach ($data as $val) {
      $result[] = $val->PROGRESS_STATUS;
    }

    return $result;
  }

  public function get_label() {
    $progress_status = $this->db->query("SELECT PROGRESS_STATUS FROM $this->tj_progress_status WITH ( NOLOCK )")->result();
 
    $result = array();

    if (get_session('work_center_group') == 'Shop') {
      $work_center = get_session('work_center');

      $result_init = ['Open', 'Waiting RO', 'Waiting Material', 'Sent to Other', 'Close'];

      foreach ($progress_status as $value) {
        $status = $value->PROGRESS_STATUS;
        if (in_array($status, $result_init) == false) {
          // remove 'progress in'
          $new_status = explode(" ", $status); 
          $new_status = $new_status[2];
          if (in_array($new_status, $work_center)) {
            $result[] = $status;
          }
        } else {
          $result[] = $status;
        }
      }
    } else {
      foreach ($progress_status as $value) {
        $result[] = $value->PROGRESS_STATUS;
      }
    }

    return $result;
  }
}