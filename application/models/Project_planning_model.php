<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Project_planning_model extends CI_Model {

  private $m_revision = 'M_REVISION';
  private $tj_m_revision = 'TJ_M_REVISION';
  private $tj_project_planning = 'TJ_PROJECT_PLANNING';
  private $tj_log_project_planning = 'TJ_LOG_PROJECT_PLANNING';

  public function insert_project_planning($data) {
    $this->db->insert_batch($this->tj_project_planning, $data);
  }

  public function update_project_planning($data) {
    $this->db->update_batch($this->tj_project_planning, $data, 'ID_PROJECT_PLANNING');
  }

  public function delete_project_planning($id_planning) {
    $this->db->where_in('ID_PROJECT_PLANNING', $id_planning)->delete($this->tj_project_planning);
  }

  public function check_project_planning($revision) {
    $data = $this->db->query("SELECT REVNR FROM $this->tj_project_planning WITH ( NOLOCK ) WHERE REVNR = '$revision'")->result();
    return count($data) > 0 ? true : false;
  }

  public function get_project_planning_info($revision) {
    $query = "WITH CRT AS (
      SELECT 
        TOP 1
        REVNR,
        USER_NAME 
      FROM 
        TJ_LOG_PROJECT_PLANNING
      WHERE 
        REVNR = '$revision'
        AND
        ACTION = 'CREATE'
      ORDER BY
        INSERT_DATE ASC
    ),
    CFRM AS (
      SELECT 
        TOP 1 
        REVNR,
        USER_NAME 
      FROM 
        TJ_LOG_PROJECT_PLANNING
      WHERE 
        REVNR = '$revision'
        AND
        ACTION = 'CONFIRM'
      ORDER BY
        INSERT_DATE DESC
    ),
    EDT AS (
      SELECT 
        TOP 1
        REVNR,
        USER_NAME 
      FROM 
        TJ_LOG_PROJECT_PLANNING
      WHERE 
        REVNR = '$revision'
        AND
        ACTION = 'EDIT'
      ORDER BY
        INSERT_DATE DESC
    )
    SELECT
      A.USER_NAME AS CREATED_BY,
      B.USER_NAME AS REVIEWED_BY,
      C.USER_NAME AS EDITED_BY
    FROM
      CRT A
      LEFT JOIN CFRM B ON A.REVNR = B.REVNR
      LEFT JOIN EDT C ON A.REVNR = C.REVNR";

    return $this->db->query($query)->row();
  }

  public function get_project_planning($revision, $type = 'display') {
    $where_status = ($type == 'display') ? "AND B.STATUS_PLN != 'NEED REVIEW'" : '';

    $query = "
    SELECT
      A.ID_PROJECT_PLANNING,
      A.REVNR,
      A.AREA,
      A.PHASE,
      A.START_DATE,
      A.END_DATE,
      A.REMARKS,
      B.STATUS_PLN
    FROM
      $this->tj_project_planning A WITH ( NOLOCK ) 
      JOIN $this->tj_m_revision B WITH ( NOLOCK ) ON A.REVNR = B.REVNR
    WHERE
      A.REVNR = '$revision' $where_status";

    return $this->db->query($query)->result();
  }

  public function get_project_planning_all($param) {
    $where = "";

    $work_center = implode("','", get_session('work_center'));
    $work_center_group = get_session('work_center_group');
    if ($work_center_group == 'Hangar') {
      $where .= "AND C.VAWRK IN ('$work_center')";
    } else {
      $where .= "AND C.VAWRK IN ( 'GAH1', 'GAH3', 'GAH4' )";
    }

    if($param['search'] != '') {
      $search = $param['search'];
      $where .= "AND ( A.REVNR LIKE '%$search%' OR C.TPLNR LIKE '%$search%' )";
    } 

    if($param['area'] != '') {
      $where .= "AND A.AREA = '".$param['area']."'";
    }

    $query = "
    SELECT
      A.REVNR,
      C.TPLNR,
      A.AREA,
      A.PHASE,
      A.START_DATE,
      A.END_DATE,
      A.REMARKS
    FROM
      $this->tj_project_planning A WITH ( NOLOCK ) 
      JOIN $this->tj_m_revision B WITH ( NOLOCK ) ON A.REVNR = B.REVNR
      JOIN $this->m_revision C WITH ( NOLOCK ) ON A.REVNR = C.REVNR
    WHERE
      B.STATUS_PLN != 'NEED REVIEW'
      AND
      C.TXT04 = 'REL'
      $where";

    return $this->db->query($query)->result();
  }

  public function sync_project_planning($data) {
    try {
      $user = get_session('name');
      $user_role = get_session('user_role');

      $revision = [];
      foreach ($data as $key => $val) {
        if (!in_array($val['REVNR'], $revision)) {
          $query = "
          IF EXISTS (SELECT REVNR FROM $this->tj_project_planning WITH ( NOLOCK ) WHERE REVNR = '".$val['REVNR']."') 
            BEGIN
              DELETE FROM $this->tj_project_planning WHERE REVNR = '".$val['REVNR']."';
            END
          
          IF EXISTS (SELECT REVNR FROM $this->tj_m_revision WITH ( NOLOCK ) WHERE REVNR = '".$val['REVNR']."')
            BEGIN
              UPDATE $this->tj_m_revision SET STATUS_PLN = 'NEED REVIEW' WHERE REVNR = '".$val['REVNR']."'
            END
          ELSE
            BEGIN
              INSERT $this->tj_m_revision (REVNR, STATUS_PLN) VALUES ('".$val['REVNR']."', 'NEED REVIEW')
            END";

          $this->db->query($query);

          $query_update_log = "
            INSERT $this->tj_log_project_planning ( REVNR, ACTION, USER_NAME, USER_ROLE, INSERT_DATE )
            VALUES 
            ( '".$val['REVNR']."', 'CREATE', '$user','$user_role', GETDATE() )
          ";

          $this->db->query($query_update_log);

          $revision[] = $val['REVNR'];
        }
      }

      $this->db->insert_batch($this->tj_project_planning, $data); 
      return true;
    } catch(Exception $e) {
      return false;
    }
  }

  public function get_log_planning($revision) {
    $sql = "
    SELECT
      ACTION,
      USER_NAME,
      INSERT_DATE
    FROM
      $this->tj_log_project_planning WITH ( NOLOCK )
    WHERE
      REVNR = '$revision'
    ORDER BY
      INSERT_DATE DESC";

    return $this->db->query($sql)->result();
  }
}