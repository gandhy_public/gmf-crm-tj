<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require FCPATH . '/vendor/autoload.php';

class MY_Controller extends CI_Controller {
  
  function __construct() {
    parent::__construct();
    $this->load->model('global_model');
    $this->load->model('user_model');
    $this->load->model('group_management_model');

    // check if the user has session
    is_has_session($this->session->userdata('crm_sess_logged_in'));

    // check if the user set remember when login
    $remember = $this->input->cookie('crm_tj_remember');
    if (isset($remember) || $remember != '' || $remember != null) {
      // set session
      $data = $this->user_model->get_user_data($remember);
      $this->set_session($data);
    }

    $url = strtolower($this->uri->segment(1)) . '/' . strtolower($this->uri->segment(2));

    if ($url != 'production_control/monitoring') setcookie('crm_tj_monitoring_prod', false, 2147485657, "/");
    if ($url != 'daily/monitoring') setcookie('crm_tj_monitoring_daily', false, 2147485657, "/");

    $this->plant = $this->global_model->get_list_plant();
    $this->work_center = $this->global_model->get_list_work_center();
    $this->progress_status = $this->global_model->get_list_progress_status();

    // default options for pusher
    $options = array(
      'cluster' => 'ap1',
      'useTLS' => true
    );

    $this->pusher = new Pusher\Pusher(
      'c586033a29a9db975254',
      '4dcf238586fada560a56',
      '640900',
      $options
    );

  }

	function view($content, $data = NULL) {
    $data['header'] = $this->load->view('template/header', $data, TRUE);
    $data['sidebar'] = $this->load->view('template/sidebar', $data, TRUE);
    $data['content'] = $this->load->view($content, $data, TRUE);

    $this->load->view('template/index', $data); 
  }

  private function set_session($data) {
    $work_center_group = '';
    if ($data->USER_ROLE == 'Admin Hangar' || $data->USER_ROLE == 'Controller Hangar') {
      $work_center_group = 'Hangar';
    } else if ($data->USER_ROLE == 'Controller Shop') {
      $work_center_group = 'Shop';
    }

    $work_center = $data->WORK_CENTER;

    $sess = array(
      'id_user'           => $data->ID_USER,    
      'name'              => $data->NAME,
      'username'          => $data->USERNAME,
      'user_group'        => $data->USER_GROUP,
      'user_role'         => $data->USER_ROLE,
      'work_center'       => $work_center == "" ? [] : explode(",", $work_center),
      'work_center_group' => $work_center_group
    );

    // get user menu
    $get_menu = $this->group_management_model->get_menu($data->USER_GROUP_ID);

    $homepage_url = '';
    foreach ($get_menu as $key => $val) {
      $homepage_url = $val[0]['url']; break;
    }

    $sess['homepage_url'] = $homepage_url;

    $menu = [];
    foreach ($get_menu as $sub_menu) {
      foreach ($sub_menu as $val) {
        $menu[] = strtolower($val['menu']);
      }
    }
    // use this session for check if the user has access
    $sess['menu'] = $menu;

    // features
    $sess['features'] = get_features($data->USER_ROLE);

    $this->session->set_userdata('crm_sess_logged_in', $sess);
  }
}