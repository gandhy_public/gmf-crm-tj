<div class="is-row">
  <div class="is-col">
    <div class="project-info display-flex align-items-center">
      <div class="main-info">
        <span class="title-big"><?= rtrim($detail_project->TPLNR); ?></span>
        <span class="subtitle-big">REVISION <?= rtrim($detail_project->REVNR); ?></span>
      </div>
      <ul class="display-flex align-items-center">
        <li>
          <span class="title">Description</span>
          <span class="value"><?= rtrim($detail_project->REVTX); ?></span>
        </li>
        <li>
          <span class="title">Location</span>
          <span class="value"><?= convert_location($detail_project->VAWRK); ?></span>
        </li>
        <li>
          <span class="title">Customer</span>
          <span class="value"><?= $detail_project->COMPANY_NAME; ?></span>
        </li>
      </ul>
    </div>
  </div>
</div>
<div class="is-row">
  <div class="is-col">
    <div class="card order-type-status jc display-flex align-items-center">
      <div class="chart">
        <div class="inner">
          <canvas id="jc-chart"></canvas>
        </div>
        <div class="inner-info">
          <span class="total">0</span>
          <span class="text">Total</span>
        </div>
      </div>
      <div class="info">
        <h3 class="title">Jobcard</h3>
        <ul class="detail display-flex">
          <li>
            <span class="title">Open</span>
            <span class="value opn">0</span>
          </li>
          <li>
            <span class="title">Progress</span>
            <span class="value prgs">0</span>
          </li>
          <li>
            <span class="title">Close</span>
            <span class="value cls">0</span>
          </li>
        </ul>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
  <div class="is-col">
    <div class="card order-type-status mdr display-flex align-items-center">
      <div class="chart">
        <div class="inner">
          <canvas id="mdr-chart"></canvas>
        </div>
        <div class="inner-info">
          <span class="total">0</span>
          <span class="text">Total</span>
        </div>
      </div>
      <div class="info">
        <h3 class="title">MDR</h3>
        <ul class="detail display-flex">
          <li>
            <span class="title">Open</span>
            <span class="value opn">0</span>
          </li>
          <li>
            <span class="title">Progress</span>
            <span class="value prgs">0</span>
          </li>
          <li>
            <span class="title">Close</span>
            <span class="value cls">0</span>
          </li>
        </ul>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
  <div class="is-col">
    <div class="card order-type-status pds display-flex align-items-center">
      <div class="chart">
        <div class="inner">
          <canvas id="pds-chart"></canvas>
        </div>
        <div class="inner-info">
          <span class="total">0</span>
          <span class="text">Total</span>
        </div>
      </div>
      <div class="info">
        <h3 class="title">PD Sheet</h3>
        <ul class="detail display-flex">
          <li>
            <span class="title">Open</span>
            <span class="value opn">0</span>
          </li>
          <li>
            <span class="title">Progress</span>
            <span class="value prgs">0</span>
          </li>
          <li>
            <span class="title">Close</span>
            <span class="value cls">0</span>
          </li>
        </ul>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
</div>
<div class="is-row">
  <div class="is-col">
    <div class="card no-padding">
      <div class="content-inner display-flex status-close-maintenance">
        <div class="left">
          <div class="progress-status">
            <div class="card-header no-border">
              <div class="title-area">
                <h3 class="title">Progress Status</h3>
              </div>
            </div>
            <div class="card-content">
              <div id="progress-status-chart" style="height:300px; width:100%"></div>
              <ul class="legend-area legend-center" style="margin-top: -10px;">
                <li class="item open">
                  <span>Open</span>
                </li>
                <li class="item pgrs">
                  <span>Progress</span>
                </li>
                <li class="item cls">
                  <span>Close</span>
                </li>
                <li class="item wro">
                  <span>Waiting RO</span>
                </li>
                <li class="item wmtr">
                  <span>Waiting Material</span>
                </li>
                <li class="item sto">
                  <span>Sent to Other</span>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="right display-flex">
          <div class="item close-area">
            <div class="inner">
              <h3 class="title">Order Close</h3>
              <div class="display-flex">
                <div class="inner-item jc">
                  <div class="value">0%</div>
                  <h4 class="title-type">Jobcard</h4>
                </div>
                <div class="inner-item mdr">
                  <div class="value">0%</div>
                  <h4 class="title-type">MDR</h4>
                </div>
                <div class="inner-item pds">
                  <div class="value">0%</div>
                  <h4 class="title-type">PD Sheet</h4>
                </div>
              </div>
            </div>
          </div>
          <div class="item maintenance">
            <div class="inner">
              <h3 class="title">Maintenance</h3>
              <div class="value">0 of 0</div>
              <div class="desc">Day of Maintenance</div>
            </div>
          </div>
        </div>
        <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
      </div>
    </div>
  </div>
</div>
<div class="is-row">
  <div class="is-col">
    <div class="card material_status" style="min-height:260px">
      <div class="card-header bb">
        <div class="title-area">
          <h3 class="title small">Material Status</h3>
        </div>
      </div>
      <div class="card-content outer">
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
</div>
<div class="is-row">
  <div class="is-col">
    <div class="card manhours">
      <div class="card-header no-border">
        <div class="title-area">
          <h3 class="title">Plan & Actual Manhours</h3>
        </div>
        <ul class="legend-area">
          <li class="item plan">
            <span>Plan</span>
          </li>
          <li class="item actual">
            <span>Actual</span>
          </li>
        </ul>
      </div>
      <div class="card-content" style="height: 350px">
        <canvas id="manhours-chart"></canvas>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
</div>
<div class="is-row">
  <div class="is-col">
    <div class="card project-planning">
      <div class="card-header no-border">
        <div class="title-area">
          <h3 class="title">Project Planning</h3>
        </div>
        <div class="option-box">
          <div class="option-item">
            <span>Timeframe</span>
            <select class="by_timeframe">
              <option value="daily" selected>Daily</option>
              <option value="weekly">Weekly</option>
              <option value="monthly">Monthly</option>
            </select>
          </div>
        </div>
      </div>
      <div class="card-content" style="min-height: 200px">
        <div id="timeline"></div>
        <ul class="legend-area legend-center" style="margin-top:15px;display:none">
          <li class="item removal">
            <span>Removal</span>
          </li>
          <li class="item inspection">
            <span>Inspection</span>
          </li>
          <li class="item rectification">
            <span>Rectification</span>
          </li>
          <li class="item installation">
            <span>Installation</span>
          </li>
          <li class="item weighing">
            <span>Weighing</span>
          </li>
          <li class="item painting">
            <span>Painting</span>
          </li>
          <li class="item rts">
            <span>RTS</span>
          </li>
        </ul>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </div>
  </div>
</div>