<div class="content-header"></div>
<div class="content-body">
  <div class="is-row">
    <div class="is-col">
      <div class="card"> 
        <div class="card-header no-border">
          <div class="title-area">
            <h3 class="title">Projects Timeline</h3>
            <span class="subtitle total-project">Loading data...</span>
          </div>
          <div class="option-box">
            <div class="option-item">
              <span>Timeframe</span>
              <select class="by_timeframe">
                <option value="daily">Daily</option>
                <option value="weekly" selected>Weekly</option>
                <option value="monthly">Monthly</option>
              </select>
            </div>
            <div class="option-item">
              <span>Status</span>
              <select class='by_project_status'>
                <option value="ALL" selected>Created & Release</option>
                <option value="CRTD">Created</option>
                <option value="REL">Release</option>
              </select>
            </div>
            <div class="option-item">
              <span>Location</span>
              <select class="by_location">
                <option value="">All Location</option>
                <?php foreach ($this->plant as $val): ?>
                  <option value="<?= $val->PLANT ?>"><?= $val->PLANT_DESC ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="option-item">
              <span>Filter</span>
              <input class="by_search" type="search" name="search" placeholder="Type and enter">
            </div>
          </div>
        </div>
        <div class="card-content project-timeline" style="min-height:200px;position:relative">
          <div id="timeline"></div>
          <ul class="legend-area legend-center" style="margin-top:15px">
            <li class="item crtd">
              <span>Created</span>
            </li>
            <li class="item rel">
              <span>Release</span>
            </li>
          </ul>
          <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
        </div>
      </div>
    </div>
  </div>
  <div class="is-row" style="margin-left:0">
    <div class="monitoring dark dashboard">
      <div class="container">
        <div class="monitoring-board">
          <div class="monitoring-header display-flex space-between align-items-center"style="margin: 0 -6px;">
            <i class="material-icons nav-prev owl-nav-trigger" nav-trigger="prev">navigate_before</i>
            <h1 class="title">PRODUCTION CONTROL</h1>
            <i class="material-icons nav-next owl-nav-trigger" nav-trigger="next">navigate_next</i>
          </div>
          <ul class="list-item owl-carousel">
            <?php foreach($top_project as $item): ?>
            <li class="item revision-<?= trim($item->REVNR); ?>">
              <div class="card project-summary" revision="<?= trim($item->REVNR); ?>">
                <i class="material-icons">arrow_forward</i>
                <div class="heading">
                  <h3 class="aircraft-reg"><?= trim($item->TPLNR); ?></h3>
                  <span class="description"><?= trim($item->REVTX); ?></span>
                  <span class="day-maintenance">Day 0 of 0</span>
                </div>
                <div class="close-summary display-flex align-items-center">
                  <div class="close-summary-item">
                    <span class="value jc">0%</span>
                    <div class="order-type">Jobcard</div>
                  </div>
                  <div class="close-summary-item">
                    <span class="value mdr">0%</span>
                    <div class="order-type">MDR</div>
                  </div>
                  <div class="close-summary-item">
                    <span class="value pds">0%</span>
                    <div class="order-type">PD Sheet</div>
                  </div>
                  <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
                </div>
              </div>
              <div class="card detail-summary hilite">
                <div class="heading">
                  <h4 class="title">HILITE</h4>
                </div>
                <div class="summary-content">
                  <marquee class="hilite-text" direction="up" scrollamount="1.5" onmouseout="this.start();" onmouseover="this.stop();"></marquee>
                  <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
                </div>
              </div>
              <div class="card detail-summary material">
                <div class="heading">
                  <h4 class="title">MATERIAL STATUS</h4>
                </div>
                <div class="summary-content">
                  <ul class="list-material">
                    <li class="list-material-item">
                      <div class="color-identity"></div>
                      <div class="status">Status</div>
                      <div class="value">0</div>
                    </li>
                  </ul>
                  <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
                </div>
              </div>
              <div class="card detail-summary progress-status">
                <div class="heading">
                  <h4 class="title">PROGRESS STATUS</h4>
                </div>
                <div class="summary-content hangar">
                  <div class="chart" style="width:100%">
                    <canvas class="progress-status-chart-<?= trim($item->REVNR); ?>" width="100%"></canvas>
                    <ul class="legend-area legend-center small" style="margin-bottom:10px">
                      <li class="item jc"><span>JC</span></li>
                      <li class="item mdr"><span>MDR</span></li>
                      <li class="item pds"><span>PDS</span></li>
                    </ul>
                  </div>
                </div>
                <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
              </div>
            </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>