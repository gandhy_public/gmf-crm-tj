<script src="<?= base_url('assets/vendor/chart.js/chart.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/chart.js/roundedBarCharts.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/visjs/vis.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  document.addEventListener('DOMContentLoaded', function(event) {
    var timelineTarget = document.getElementById('timeline');
    
    var timeline, items, groups;

    // Load project timeline    
    loadProjectTimeline();

    // Filter by timeframe
    $('.by_timeframe').change(function(){
      var scale = $(this).val();
      changeViewScaleTimeline(timeline, scale);
    });

    // Filter by project status and location
    $('.by_project_status, .by_location').change(function(){
      loadProjectTimeline();
    });

    // Filter by user input
    $('.by_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') loadProjectTimeline();
    });

    // scroll horizontal on timeline by pressing left or right
    $(document).on('keydown', function (e) {
      if (e.keyCode == 37) {
        scrollTimeline(timeline, 0.2);
      } else if (e.keyCode == 39)  {
        scrollTimeline(timeline, -0.2);
      }
    });

    function loadProjectTimeline() {
      var timeframe = $('.by_timeframe').val();
      var status = $('.by_project_status').val();
      var location = $('.by_location').val();
      var search = $('.by_search').val();
      
      $.ajax({
        url: '<?= base_url("index.php/api/dashboard/get_project_timeline"); ?>',
        type: 'POST',
        data: {
          status: status,
          location: location,
          search: search
        },
        beforeSend: function() {
          $('.project-timeline').append(getLoaderOverlay());
          $('.total-project').text('Loading data...');
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == 'success') {
            var data = response.data; 

            // project timeline options
            var options = {
              width: "100%",
              maxHeight: "400px",
              horizontalScroll: true, 
              verticalScroll: true, 
              zoomable: false,
            };

            if (data.items.length == 0) {
              options.start = getCurrentDate();
            }

            if(timeline !== undefined) {
              items = new vis.DataSet(data.items);
              groups = new vis.DataSet(data.groups);

              // destroy if action 'redraw'
              timeline.destroy();

              timeline = new vis.Timeline(timelineTarget, items, groups, options);
              changeViewScaleTimeline(timeline, timeframe);
            } else {
              items = new vis.DataSet(data.items);
              groups = new vis.DataSet(data.groups);

              timeline = new vis.Timeline(timelineTarget, items, groups, options);
              changeViewScaleTimeline(timeline, timeframe);
            }

            timeline.on('select', function(properties) {
              var projectLink = items._data[properties.items[0]].link
              window.open(projectLink, '_blank');
            });  

            $('.total-project').text('Showing ' + data.items.length + ' results');
          } 
        },
        error: function(request, status, error) {
          alert('Something error when loading project timeline!');
          $('.project-timeline').hide();
          console.log(error);
        },
        complete: function() {
          $('.project-timeline .ovrly').remove();
        }
      });
    }
  });
</script>

<script>
  var i = 0;
  
  var topProject = $.parseJSON('<?= json_encode($top_project); ?>').length;
  var items = topProject > 3 ? 3 : topProject;

  // init carousel
  var carousel = $('.owl-carousel').owlCarousel({
    items: items,
    nav: true,
    dots: false,
    margin: 20,
    slideBy: 1,
    loop: true,
    navSpeed: 1000,
    autoplay: false,
    autoplayTimeout: 10000,
    autoplaySpeed: 1000,
    autoplayHoverPause: false,
  });

  var workCenterGroup = '<?= get_session("work_center_group"); ?>' || '';
  if (workCenterGroup == 'Shop') {
    $('.monitoring-header .title').text('PRODUCTION CONTROL SHOP');
  } else {
    $('.monitoring-header .title').text('PRODUCTION CONTROL HANGAR');
  }

  $(document).ready(function() {
    $('.monitoring-header .owl-nav-trigger').click(function(){
      $('.owl-nav .owl-' + $(this).attr('nav-trigger')).trigger('click');
    });
    
    var pusher = new Pusher('c586033a29a9db975254', {
      cluster: 'ap1',
      forceTLS: true
    });

    // pusher top project
    var channel = pusher.subscribe('CRM-TJ-development');
    channel.bind('update_top_project', function(data) {
      location.reload();
    });

    // pusher hilite
    channel.bind('update_hilite', function(data) {
      var data = $.parseJSON(data);
      $.each(data, function(revision, hilite) {
        setHilite(revision, hilite);
      });
    });

    // pusher material status
    channel.bind('update_material_status', function(data) {
      var data = $.parseJSON(data);      

      $.each(data, function(revision, valRevision) {
        $.each(data[revision], function(item, val) {
          if (val.name == 'Actual Nil Stock') {
            value = val.value.csp + ' <span>(CSP)</span> ' + val.value.rfq + ' <span>(RFQ)</span>';
          } else {
            value = val.value;
          }

          setMaterialStatusSingle(revision, val.name, value);
        });
      });
    });

    var chartStyle = {
      fontColor: '#adb4c3',
      gridLineColor: 'rgba(25, 40, 64, 1)'
    };

    var topProject = $.parseJSON('<?= json_encode($top_project); ?>');
    var progressStatusChart = [];

    var revisionList = [];

    $.each(topProject, function(key, val) {
      var revision = val.REVNR;
      revisionList.push(revision);

      var initValue = [];
      progressStatusChart[revision] = setProgressStatusChart(revision, initValue, initValue, initValue);
    });

    // get selected project
    var progressStatus = $.parseJSON('<?= json_encode($progress_status_label); ?>');
    $.ajax({
      url: '<?= base_url("index.php/api/production_control/get_data_top_project"); ?>',
      type: 'POST',
      data: {
        revision: revisionList,
        progress_status: progressStatus
      },
      success: function(response) {
        var response = $.parseJSON(response);

        if (response.status == 'success') {
          var data = response.data;
          $.each(data, function(key, val) {
            var revision = key;
            
            // day of maintenance
            $('.revision-' + revision + ' .project-summary .day-maintenance').text('Day ' + val.maintenance.actual + ' of ' + val.maintenance.plan);

            var day = val.maintenance.actual;
            var totalDay = val.maintenance.plan;
            var maintenancePercentage = (get_percentage(day, totalDay, false) / 100);

            if (maintenancePercentage <= 0.5) {
              $('.revision-' + revision + ' .project-summary .day-maintenance').addClass('normal');
            } else if (maintenancePercentage > 0.5 && maintenancePercentage <= 0.8) {
              $('.revision-' + revision + ' .project-summary .day-maintenance').addClass('warning');
            } else {
              $('.revision-' + revision + ' .project-summary .day-maintenance').addClass('oot');
            }

            // order close percentage
            var orderClosePercentage = val.order_close_percentage;

            var jcPercentageValue = orderClosePercentage.GA01 == '-1' ? 'N/A' : orderClosePercentage.GA01 + '%';
            $('.revision-' + revision + ' .project-summary .close-summary .value.jc').text(jcPercentageValue);
 
            var jcClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA01);
            $('.revision-' + revision + ' .project-summary .close-summary .value.jc').addClass(jcClass);

            var mdrPercentageValue = orderClosePercentage.GA02 == '-1' ? 'N/A' : orderClosePercentage.GA02 + '%';
            $('.revision-' + revision + ' .project-summary .close-summary .value.mdr').text(mdrPercentageValue);

            var mdrClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA02);
            $('.revision-' + revision + ' .project-summary .close-summary .value.mdr').addClass(mdrClass);

            var pdsPercentageValue = orderClosePercentage.GA05 == '-1' ? 'N/A' : orderClosePercentage.GA05 + '%';
            $('.revision-' + revision + ' .project-summary .close-summary .value.pds').text(pdsPercentageValue);

            var pdsClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA05);
            $('.revision-' + revision + ' .project-summary .close-summary .value.pds').addClass(pdsClass);

            $('.revision-' + revision + ' .project-summary .close-summary .ovrly').remove();

            // hilite
            setHilite(revision, val.hilite);

            // material status
            var materialStatus = val.material_status;
            var listMaterialStatus = "";

            $.each(materialStatus, function(key, material) {
              var value = material.name == 'Actual Nil Stock' ? material.value.csp + ' <span>(CSP)</span> ' + material.value.rfq + ' <span>(RFQ)</span>' : material.value;
              listMaterialStatus += '<li class="list-material-item" status="' + material.name + '">' +
                '<div class="color-identity" style="background:' + material.color + '"></div>' +
                '<div class="status">' + material.name + '</div>' +
                '<div class="value" style="color:' + material.color + '">' + value + '</div>' +
              '</li>';
            });

            $('.revision-' + revision + ' .material .list-material').html(listMaterialStatus);
            $('.revision-' + revision + ' .material .ovrly').remove();
            
            var listMaterialStatusLength = $('.revision-' + revision + ' .material .list-material-item').length;
            if (materialStatus.length === listMaterialStatusLength) $('.revision-' + revision + ' .material .ovrly').remove();

            var jc_status = [];
            $.each(val.progress_status.GA01, function(key, value) {
              jc_status.push(value);
            });

            var mdr_status = [];
            $.each(val.progress_status.GA02, function(key, value) {
              mdr_status.push(value);
            });

            var pds_status = [];
            $.each(val.progress_status.GA05, function(key, value) {
              pds_status.push(value);
            });
            
            // progress status chart
            for (let i = 0; i < progressStatusChart[revision].length; i++) {
              progressStatusChart[revision][i].data.datasets[0].data = jc_status;
              progressStatusChart[revision][i].data.datasets[1].data = mdr_status;
              progressStatusChart[revision][i].data.datasets[2].data = pds_status;
              progressStatusChart[revision][i].update();
            }

            $('.revision-' + revision + ' .progress-status .ovrly').remove();
          });

          // play carousel
          carousel.trigger('play.owl.autoplay');
        }
      },
      error: function(request, status, error) {
        alert('Something error when loading projects!');
        $('.monitoring .list-item').html('');
        console.log(error);
      }
    });
 
    runMaterialTransition();

    // set hilite
    function setHilite(revision, hilite) {
      $('.revision-' + revision + ' .hilite .hilite-text').text(hilite);
      $('.revision-' + revision + ' .hilite .ovrly').remove();
    }

    // set material status
    function setMaterialStatusSingle(revision, status, value) {
      $('.revision-' + revision + ' .material .list-material [status="' + status + '"] .value').text(value);
    }

    // progress status chart template
    function setProgressStatusChart(revision, jc, mdr, pds) {
      var progressStatusChartLabel = [];
      var progressStatus = '<?= json_encode($progress_status_label); ?>';
      $.each($.parseJSON(progressStatus), function(key, val){
        var status = '';
        switch (val) {
          case 'Open': status = 'Open'; break;
          case 'Waiting RO': status = 'Waiting RO'; break;
          case 'Waiting Material': status = 'Waiting Material'; break;
          case 'Progress in Hangar': status = 'Hangar'; break;
          case 'Progress in W101': status = 'W101'; break;
          case 'Progress in W102': status = 'W102'; break;
          case 'Progress in W103': status = 'W103'; break;
          case 'Progress in W401': status = 'W401'; break;
          case 'Progress in W402': status = 'W402'; break;
          case 'Progress in W501': status = 'W501'; break;
          case 'Progress in W502': status = 'W502'; break;
          case 'Sent to Other': status = 'Sent to Other'; break;
          case 'Close': status = 'Close'; break;
        }
        progressStatusChartLabel.push(status);
      });

      var length = $('.progress-status-chart-' + revision).length;
      var progressStatusChart = [];
      for (let i = 0; i < length; i++) {
        var progressStatusChartTarget = document.getElementsByClassName('progress-status-chart-' + revision)[i];
        
        var progressStatusChartData = {
          labels: progressStatusChartLabel,
          datasets: [
            {
              label: 'Jobcard',
              backgroundColor: '#13ce66',
              data: jc,
            }, 
            {
              label: 'MDR',
              backgroundColor: '#ffab00',
              data: mdr,
            },
            {
              label: 'PD Sheet',
              backgroundColor: '#0065FF',
              data: pds,
            }
          ]
        }

        progressStatusChart[i] = new Chart(progressStatusChartTarget, {
          type: 'bar',
          data: progressStatusChartData,
          options: {
            // cornerRadius: 4,
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [{
                ticks: {
                  fontColor: chartStyle.fontColor,
                  fontSize: 10,
                  beginAtZero: true,
                  userCallback: function(label, index, labels) {
                    if (Math.floor(label) === label) return label;
                  },
                },
                gridLines: {
                  zeroLineColor: chartStyle.gridLineColor,
                  color: chartStyle.gridLineColor,
                }
              }],
              xAxes: [{
                ticks: {
                  fontColor: chartStyle.fontColor,
                  fontSize: 10
                },
                barPercentage: 0.6,
                // categoryPercentage: 0.6,
                gridLines: {
                  zeroLineColor: chartStyle.gridLineColor,
                  color: chartStyle.gridLineColor,
                }
              }]
            },
            legend: {
              display: false
            },
          }
        });
      }

      return progressStatusChart;
    }

    // update progress status chart style
    function updateProgressStatusChartStyle(revision, style) {
      for (let i = 0; i < progressStatusChart[revision].length; i++) {
        progressStatusChart[revision][i].options.scales.yAxes[0].gridLines.zeroLineColor = style;
        progressStatusChart[revision][i].options.scales.yAxes[0].gridLines.color = style;
        progressStatusChart[revision][i].options.scales.xAxes[0].gridLines.zeroLineColor = style;
        progressStatusChart[revision][i].options.scales.xAxes[0].gridLines.color = style;
        progressStatusChart[revision][i].update();
      }
    }

    // run material status transition
    function runMaterialTransition() {
      var listMaterialContainer = $('.material .list-material');
      var itemHeight = $('.material .list-material').height();

      setInterval(function() {
        listMaterialContainer.stop().animate({scrollTop: itemHeight}, 1000, 'linear', function() {
          var el = $('.list-material-item:nth-child(1), .list-material-item:nth-child(2), .list-material-item:nth-child(3)', this);
          $(this).scrollTop(0).find('.list-material-item:last').after(el);
        });
      }, 5000);
    }  

    $('.project-summary').click(function(){
      revisionLink = '<?= base_url('index.php/dashboard/detail_project/'); ?>' + $(this).attr('revision');
      window.open(revisionLink, '_blank');
    });
  });
</script>