<script src="<?= base_url('assets/vendor/chart.js/chart.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/chart.js/roundedBarCharts.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/highcharts/highcharts.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/highcharts/grouped-categories.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/visjs/vis.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    // Jobcard and MDR Status
    // ==================================================================
    var jobcardChartTarget = document.getElementById("jc-chart");
    var mdrChartTarget = document.getElementById("mdr-chart");
    var pdsChartTarget = document.getElementById("pds-chart");

    var doughnutChartOption = {
      legend: {
        display: false
      },
      cutoutPercentage: 82,
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        enabled: false
      }
    }

    var jobcardChart = new Chart(jobcardChartTarget, {
      type: 'doughnut',
      data: {
        labels: ['Total'],
        datasets: [{
          data: [1],
          backgroundColor: ['#ddd'],
        }]
      },
      options: doughnutChartOption
    });

    var mdrChart = new Chart(mdrChartTarget, {
      type: 'doughnut',
      data: {
        labels: ['Total'],
        datasets: [{
          data: [1],
          backgroundColor: ['#ddd'],
        }]
      },
      options: doughnutChartOption
    });

    var pdsChart = new Chart(pdsChartTarget, {
      type: 'doughnut',
      data: {
        labels: ['Total'],
        datasets: [{
          data: [1],
          backgroundColor: ['#ddd'],
        }]
      },
      options: doughnutChartOption
    });

    function addDataDoughnutChart(chart, data) {
      chart.data.labels = data.labels;
      chart.data.datasets[0].data = data.data;
      chart.data.datasets[0].backgroundColor = data.backgroundColor;
      chart.options.tooltips.enabled = true;
      chart.update();
    }

    // Progress Status
    // ==================================================================

    var progressStatusChartTarget = document.getElementById("progress-status-chart");

    var progressStatusChart = new Highcharts.Chart({
      chart: {
        renderTo: progressStatusChartTarget,
        type: "column",
        style: {
          fontFamily: 'Open Sans'
        }
      },
      title: {
        text: ''
      },
      xAxis: {
        categories: [
          {
            name: 'Hangar',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W101',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W102',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W103',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W401',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W402',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W501',
            categories: ['JC', 'MDR', 'PDS']
          },
          {
            name: 'W502',
            categories: ['JC', 'MDR', 'PDS']
          }
        ],
        labels: {
          enabled: true,
          style: {
            fontSize: '10px',
            width: 5
          },
          groupedOptions: [{
            rotation: 0, 
            align: 'center',
            tickColor: '#eaeaea',
          }],
          rotation: 0,
          tickColor: '#eaeaea',
        },
      },
      yAxis: {
        title: {
          text: ''
        },
        tickColor: '#ddd'
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      plotOptions: {
        column: {
          stacking: 'normal'
        }
      },
      series: [
      ]
    });

    $.ajax({
      url: '<?= base_url("index.php/api/dashboard/get_status_close_maintenance/"); ?>',
      type: 'POST',
      data: {
        revision: '<?= $detail_project->REVNR; ?>'
      },
      success: function(response) {
        var response = $.parseJSON(response);

        if (response.status == "success") {
          var data = response.data;

          // Order Type Status
          // ==================================================================
          // show or hide overlay
          if(data.order_type_status.ga01.total > 0) {
            $('.order-type-status.jc .ovrly').remove();
            
            // update doughnut chart
            var data_jc = {
              labels: ['Open', 'Progress', 'Close'],
              data: [data.order_type_status.ga01.open, data.order_type_status.ga01.progress, data.order_type_status.ga01.close],
              backgroundColor: ['#FF4949', '#0065FF', '#13CE66'],
            };
            addDataDoughnutChart(jobcardChart, data_jc);

            // update jc value
            $('.order-type-status.jc .total').text(data.order_type_status.ga01.total);
            $('.order-type-status.jc .opn').text(data.order_type_status.ga01.open);
            $('.order-type-status.jc .prgs').text(data.order_type_status.ga01.progress);
            $('.order-type-status.jc .cls').text(data.order_type_status.ga01.close);
          } else {
            $('.order-type-status.jc .total').text('N/A').addClass('not-available');
            $('.order-type-status.jc .ovrly').remove();
          }
          
          if(data.order_type_status.ga02.total > 0) {
            $('.order-type-status.mdr .ovrly').remove();

            // update doughnut chart
            var data_mdr = {
              labels: ['Open', 'Progress', 'Close'],
              data: [data.order_type_status.ga02.open, data.order_type_status.ga02.progress, data.order_type_status.ga02.close],
              backgroundColor: ['#FF4949', '#0065FF', '#13CE66'],
            };
            addDataDoughnutChart(mdrChart, data_mdr);

            // update mdr value
            $('.order-type-status.mdr .total').text(data.order_type_status.ga02.total);
            $('.order-type-status.mdr .opn').text(data.order_type_status.ga02.open);
            $('.order-type-status.mdr .prgs').text(data.order_type_status.ga02.progress);
            $('.order-type-status.mdr .cls').text(data.order_type_status.ga02.close);
          } else {
            $('.order-type-status.mdr .total').text('N/A').addClass('not-available');
            $('.order-type-status.mdr .ovrly').remove();
          }

          if(data.order_type_status.ga05.total > 0) {
            $('.order-type-status.pds .ovrly').remove();
            
            // update doughnut chart
            var data_jc = {
              labels: ['Open', 'Progress', 'Close'],
              data: [data.order_type_status.ga05.open, data.order_type_status.ga05.progress, data.order_type_status.ga05.close],
              backgroundColor: ['#FF4949', '#0065FF', '#13CE66'],
            };
            addDataDoughnutChart(pdsheetChart, data_pdsheet);

            // update jc value
            $('.order-type-status.pds .total').text(data.order_type_status.ga05.total);
            $('.order-type-status.pds .opn').text(data.order_type_status.ga05.open);
            $('.order-type-status.pds .prgs').text(data.order_type_status.ga05.progress);
            $('.order-type-status.pds .cls').text(data.order_type_status.ga05.close);
          } else {
            $('.order-type-status.pds .total').text('N/A').addClass('not-available');
            $('.order-type-status.pds .ovrly').remove();
          }

          // Progress Status
          // ==================================================================
          $('.status-close-maintenance .ovrly').remove();
          var jc_value = [];
          $.each(data.order_type_status.ga01, function(key, value) {
            if(key != 'progress' && key != 'total') {
              jc_value.push(value);
            }
          });

          var mdr_value = [];
          $.each(data.order_type_status.ga02, function(key, value) {
            if(key != 'progress' && key != 'total') {
              mdr_value.push(value);
            }
          });
          
          $.each(data.progress_status_chart.reverse(), function(index, value) {
            progressStatusChart.addSeries({
              name: value.name,
              type: 'column',
              color: value.color,
              data: value.data,
              events: {
                click: function (event) {
                  var revision = '<?= rtrim($detail_project->REVNR); ?>';
                  var workCenter = event.point.category.parent.name;
                  var orderType = event.point.category.name;
                  var status = event.point.series.name;

                  if (orderType == 'JC') {
                    orderType = 'GA01';
                  } else if (orderType == 'MDR') {
                    orderType = 'GA02';
                  } else {
                    orderType = 'GA05';
                  }
                  
                  var sessWorkCenter = $.parseJSON('<?= json_encode(get_session('work_center')); ?>');
                  
                  if (sessWorkCenter.length == 0 || sessWorkCenter.includes(workCenter) == true || (workCenter == 'Hangar' && (sessWorkCenter.includes('GAH1') == true || sessWorkCenter.includes('GAH3') == true || sessWorkCenter.includes('GAH4') == true))) {
                    window.open('<?= base_url("index.php/production_control/view_order?"); ?>' + 'revision=' + revision + '&work_center=' + workCenter + '&type=' + orderType + '&status=' + status, '_blank');
                  }              
                }
              }
            });
          });

          // Maintenance
          // ==================================================================
          var day = data.maintenance.actual;
          var totalDay = data.maintenance.plan;
          var maintenancePercentage = (get_percentage(day, totalDay, false) / 100);
          $('.status-close-maintenance .maintenance .value').text(day + ' of ' + totalDay);

          if (maintenancePercentage <= 0.5) {
            $('.status-close-maintenance .maintenance .value').addClass('normal');
          } else if (maintenancePercentage > 0.5 && maintenancePercentage <= 0.8) {
            $('.status-close-maintenance .maintenance .value').addClass('warning');
          } else {
            $('.status-close-maintenance .maintenance .value').addClass('oot');
          }

          // Close Area
          // ==================================================================
          var total_close_jc_mdr = data.order_type_status.ga01.close + data.order_type_status.ga02.close;
          var total_jc_mdr = data.order_type_status.ga01.total + data.order_type_status.ga02.total;
        
          if(data.order_type_status.ga01.total > 0) {
            $('.status-close-maintenance .close-area .inner-item.jc .value').text(get_percentage(data.order_type_status.ga01.close, data.order_type_status.ga01.total));
            var jcClass = getOrderClosePercentageClass(maintenancePercentage, get_percentage(data.order_type_status.ga01.close, data.order_type_status.ga01.total, false));
            $('.status-close-maintenance .close-area .inner-item.jc .value').addClass(jcClass);
          } else {
            $('.status-close-maintenance .close-area .inner-item.jc .value').text('N/A').addClass('na');
          }
          
          if(data.order_type_status.ga02.total > 0) {
            $('.status-close-maintenance .close-area .inner-item.mdr .value').text(get_percentage(data.order_type_status.ga02.close, data.order_type_status.ga02.total));
            var mdrClass = getOrderClosePercentageClass(maintenancePercentage, get_percentage(data.order_type_status.ga02.close, data.order_type_status.ga02.total, false));
            $('.status-close-maintenance .close-area .inner-item.mdr .value').addClass(mdrClass);
          } else {
            $('.status-close-maintenance .close-area .inner-item.mdr .value').text('N/A').addClass('na');
          }
          
          if(data.order_type_status.ga05.total > 0) {
            $('.status-close-maintenance .close-area .inner-item.pds .value').text(get_percentage(data.order_type_status.ga05.close, data.order_type_status.ga05.total));
            var pdsClass = getOrderClosePercentageClass(maintenancePercentage, get_percentage(data.order_type_status.ga05.close, data.order_type_status.ga05.total, false));
            $('.status-close-maintenance .close-area .inner-item.pds .value').addClass(pdsClass);
          } else {
            $('.status-close-maintenance .close-area .inner-item.pds .value').text('N/A').addClass('na');
          }
        }
      },
      error: function(request, status, error) {
        alert('Something error when loading data!');
        console.log(error);
      }
    });
  });
</script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    $.ajax({
      url: '<?= base_url("index.php/api/dashboard/get_material_status/"); ?>',
      type: 'POST',
      data: {
        revision: '<?= $detail_project->REVNR; ?>'
      },
      success: function(response) {
        var response = $.parseJSON(response);

        if (response.status == "success") {
          var data = response.data;

          var material_status = "";
          $.each(data, function(index, value) {
            var count = '';
            if(value.name == 'Actual Nil Stock') {
              count = value.value.csp + '<span>(CSP)</span>' + value.value.rfq + '<span>(RFQ)</span>';
            }else{
              count = value.value;
            }
            material_status = '<div class="inner-box">' +
              '<div class="summary-material" status="'+value.name+'" style="background: '+value.color+'">' +
                '<div class="detail">' +
                  '<h3 class="count">'+count+'</h3>' +
                  '<span class="title">'+value.name+'</span>' +
                '</div>' +
                '<div class="icon"><i class="material-icons">'+value.icon+'</i></div>' +
              '</div>' +
            '</div>';
            $('.material_status .outer').append(material_status);
            $('.material_status .ovrly').remove();
          });
        }
      },
      error: function(request, status, error) {
        alert('Something error when getting material status!');
        console.log(error);
      }
    });

    $('.material_status').on('click', '.summary-material', function() {
      var revision = '<?= rtrim($detail_project->REVNR); ?>';
      var status = $(this).attr('status');
      window.open('<?= base_url("index.php/production_control/view_material?"); ?>' + 'revision=' + revision + '&material_status=' + status, '_blank');
    });
  });
</script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var manhoursChartTarget = document.getElementById("manhours-chart");
    var manhoursChartData = {
      labels: ["Hangar", "W101", "W102", "W103", "W401", "W402", "W501", "W502"],
      datasets: [
        {
          label: "Plan Manhours",
          backgroundColor: "#0065FF",
          data: [0, 0, 0, 0, 0, 0, 0, 0],
        }, 
        {
          label: "Actual Manhours",
          backgroundColor: "#13ce66",
          data: [0, 0, 0, 0, 0, 0, 0, 0],
        }
      ]
    }
    var manhoursChart = new Chart(manhoursChartTarget, {
      type: 'bar',
      data: manhoursChartData,
      options: {
        cornerRadius: 4,
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                if (Math.floor(label) === label) return label;
              },
            },
            gridLines: {
              zeroLineColor: '#ddd',
              color: 'rgba(234, 234, 234, .5)'
            }
          }],
          xAxes: [{
            barPercentage: 0.4,
            categoryPercentage: 0.6,
            gridLines: {
              zeroLineColor: '#eaeaea',
              color: 'rgba(234, 234, 234, .5)'
            }
          }]
        },
        legend: {
          display: false
        },
      }
    });

    $.ajax({
      url: '<?= base_url("index.php/api/dashboard/get_manhours/"); ?>',
      type: 'POST',
      data: {
        revision: '<?= $detail_project->REVNR; ?>'
      },
      success: function(response) {
        var response = $.parseJSON(response);

        if (response.status == "success") {
          var data = response.data;

          $('.manhours .ovrly').remove();

          var plan_value = [];
          $.each(data.plan, function(index, value){
            plan_value.push(value);
          });

          var actual_value = [];
          $.each(data.actual, function(index, value){
            actual_value.push(value);
          });

          manhoursChart.data.datasets[0].data = plan_value;
          manhoursChart.data.datasets[1].data = actual_value;
          manhoursChart.update();
        }
      },
      error: function(request, status, error) {
        alert('Something error when getting manhours!');
        console.log(error);
      }
    });
  });
</script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var timelineTarget = document.getElementById('timeline');
    var timeline, items, groups;
    
    loadProjectPlanning();

    $('.by_timeframe').change(function(){
      var scale = $(this).val();
      changeViewScaleTimeline(timeline, scale);
    });

    $(document).on('keydown', function (e) {
      if (e.keyCode == 37) {
        scrollTimeline(timeline, 0.2);
      } else if (e.keyCode == 39)  {
        scrollTimeline(timeline, -0.2);
      }
    });

    function loadProjectPlanning() {
      var timeframe = $(".by_timeframe").val();

      $.ajax({
        url: '<?= base_url("index.php/api/dashboard/get_project_planning_detail/"); ?>',
        type: 'POST',
        data: {
          revision: '<?= rtrim($detail_project->REVNR); ?>'
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;

            $('.project-planning .ovrly').remove();
          
            var options = {
              width: "100%",
              horizontalScroll: true, 
              verticalScroll: true, 
              zoomable: false,
              // zoomKey: 'ctrlKey',
            };

            if (data.items.length == 0) {
              options.start = getCurrentDate();
            }

            items = new vis.DataSet(data.items);
            groups = new vis.DataSet(data.groups);

            timeline = new vis.Timeline(timelineTarget, items, groups, options);
            changeViewScaleTimeline(timeline, timeframe);
            $('.vis-timeline').css('visibility', 'visible !important');

            // container.addEventListener('mouseover', function(event){
            //   var properties = timeline.getEventProperties(event);
            //   console.log('mouseover properties:', properties);
            // });
          }
        },
        error: function(request, status, error) {
          alert('Something error when getting project planning data!');
          console.log(error);
        }
      });
    }
  });
</script>