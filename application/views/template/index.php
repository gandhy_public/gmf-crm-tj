<?= $header; ?>
<div class="wrapper">
	<?= $sidebar; ?>
	<div class="content">
		<header class="main-header">
			<div class="display-flex">
				<a href="javascript:void(0);" class="toggle-sidebar"><i class="material-icons">menu</i></a>
				<div class="headline">
					<h2 class="title"><?= $page ?></h2>
					<span class="subtitle">Furnishing & Upholstery Services</span>
				</div>
			</div>
			<ul class="main-navbar">
				<!-- <li>
					<div class="notif-wrapper">

					</div>
					<div class="notifications">
						<a class="inner" href="">
							<div class="count">3</div>
							<i class="material-icons">notifications</i>
						</a>
					</div>
				</li> -->
				<li class="user-panel" style="position: relative">
					<div class="user-info">
						<div class="user">
							<span class="name"><?= get_session("name"); ?></span>
							<span class="role"><?= get_session("user_role"); ?></span>
						</div>
						<img class="image" src="<?= base_url('assets/img/avatar.png'); ?>" alt="user">
					</div>
					<ul class="user-dropdown">
						<li>
							<a href="">Settings</a>
						</li>
						<li>
							<a href="">Log Out</a>
						</li>
					</ul>
				</li>
			</ul>
		</header>
		<div class="main-content">
			<?= $content; ?>
		</div>
	</div>
</div>
<script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/metismenu/js/metismenu.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/bootoast/bootoast.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/tippy/tippy.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/sidebar.js'); ?>"></script>

<script>
	$('.has-ovrly').append(getLoaderOverlay());

	function getLoaderOverlay() {
		return '<div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>';
	}
	
	function handleAjaxError(request, status, error) {
		alert('Something error, please check the console!');
		console.log(error);
	}

	function get_percentage(value, total, percentage = true) {
    if(total > 0) {
      if (percentage) {
        return Number(((value / total) * 100).toFixed(2)) + '%';
      } else {
        return Number(((value / total) * 100).toFixed(2));
      }
    } else {
      if (percentage) {
        return '0%';
      } else {
        return 0;
      }
    }
	}
	
	function getOrderClosePercentageClass(day_maintenance_percentage, task_percentage) {
		var task = task_percentage / 100;

		if (task < 0) {
			return 'na';
		} else if (task == 0 || (day_maintenance_percentage - task) > 0.3) {
			return 'oot';
		} else if (task == 1 || (day_maintenance_percentage - task) <= 0) {
			return 'normal';
		} else if ((day_maintenance_percentage - task) > 0 && (day_maintenance_percentage - task) <= 0.3 ) { 
			return 'warning';
		}
	}

	function add(a, b) {
    return a + b;
	}
</script>

<?php if ($script) echo $this->load->view($script, NULL, TRUE); ?>

<script>
	var pusher = new Pusher('c586033a29a9db975254', {
		cluster: 'ap1',
		forceTLS: true
	});

	// pusher new order
	var channel = pusher.subscribe('CRM-TJ-development');
	channel.bind('update_new_order', function(data) {
		var work_center = $.parseJSON('<?= json_encode(get_session("work_center")); ?>');
		var data = $.parseJSON(data);

		$.each(data.work_center, function(key, val){
			if (work_center.includes(val)) {
				var notif_icon = $('.main-sidebar .notif-icon');
				var notif_count = $('.main-sidebar .notif-count');

				var total = parseInt(notif_count.text()) || 0;
				if (data.action == 'approve') {
					total -= data.total;
				} else if (data.action == 'move') {
					total += data.total;
					display_toast('success', 'You have new order waiting for approval!');
				} else if (data.action == 'cancel') {
					total -= data.total;
					display_toast('warning', data.total + ' order has been canceled!');
				}

				notif_count.text(total);

				if(total > 0) {
					notif_icon.show();
					notif_count.show();
				} else {
					notif_icon.hide();
					notif_count.hide();
				}
			}
		});
	});


	$('#metismenu').metisMenu();

	display_tooltip('.excel', 'Download .xlsx file', 'left');
	display_tooltip('.excel-right', 'Download .xlsx file', 'right');
	display_tooltip('.single-button.print', 'Print', 'right');
	display_tooltip('.single-button.synchronize', 'Synchronize Order', 'left');
	display_tooltip('.single-button.upload-project', 'Upload Production Planning & Hilite', 'left');

	function getCurrentDate() {
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd < 10) {
			dd = '0'+dd
		} 

		if(mm < 10) {
			mm = '0'+mm
		} 

		today = yyyy + '-' + mm + '-' + dd;
		return today;
	}

	function display_toast(type, msg) {
		bootoast.toast({
			message: msg,
			type: type,
			position: "right-bottom",
			icon: "",
			animationDuration: "300",
			dismissable: false
		});
	}

	function display_tooltip(target, text, placement = 'top') {
		tippy(target, {
			content: text,
			arrow: true,
			arrowType: 'sharp',
			size: 'small',
			animation: 'shift-away',
			placement: placement,
		});
	}

	function update_order_per_column(param) {
		$.ajax({
			url: '<?= base_url("index.php/api/production_control/update_order_per_column"); ?>',
			type: 'POST',
			data: {
				aufnr: param.aufnr,
				column: param.column,
				value: param.value
			},
			success: function(response) {
				var response = $.parseJSON(response);
			
				if (response.status == "success") {
					display_toast('info', 'Successfully update ' + param.alias.toLowerCase() + ' for order '+ param.aufnr);
				}
			},
			error: function(request, status, error) {
				alert('Something error when submit ' + param.alias.toLowerCase() + '!');
				console.log(error);
			}
		});
	}

	function update_material_per_column(param) {
		var target = param.target.split('|');
		var part_number = target[4];

		$.ajax({
      url: param.url,
			type: param.type,
			data: {
				revnr: target[0],
				aufnr: target[1],
				vornr: target[2],
				rspos: target[3],
				column: param.column,
				value: param.value
			},
			success: function(response) {
				var response = $.parseJSON(response);
            
				if (response.status == "success") {
					display_toast('info', param.alias + ' for part number ' + part_number + ' successfully updated');
				}
			},
			error: function(request, status, error) {
				alert('Something error when submit ' + param.alias.toLowerCase() + '!');
				console.log(error);
			}
		});
	}

	function changeViewScaleTimeline(timeline, scale) {
		var start, end;
		var startDate = new Date();
		var endDate = new Date();

		setTimeout(function () {
			if(scale === 'daily') {
				start = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() - 7);
				end = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + 7);
			} else if (scale === 'weekly') {
				start = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate() - (7 * 4));
				end = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate() + (7 * 4));
			} else if (scale === 'monthly') {
				start = new Date(startDate.setMonth(startDate.getMonth() - 6));
				end = new Date(endDate.setMonth(endDate.getMonth() + 6));
			}
			
			timeline.setWindow(start, end);

			setTimeout(() => {
				var currentDate = new Date();
				timeline.moveTo(currentDate);
				// if(move) timeline.moveTo(currentDate);
			}, 1000);
		}, 1000);
	}

	function scrollTimeline(timeline, percentage) {
		var range = timeline.getWindow();
		var interval = range.end - range.start;

		timeline.setWindow({
			start: range.start.valueOf() - interval * percentage,
			end:   range.end.valueOf()   - interval * percentage
		});
	}

	function dateDiffInDays(a, b) {
		var _MS_PER_DAY = 1000 * 60 * 60 * 24;

		// Discard the time and time-zone information.
		var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
		var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

		return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}
</script>
</body>
</html>