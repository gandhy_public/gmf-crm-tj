<aside class="main-sidebar">
  <div class="main-logo">
    <img src="<?= base_url('assets/img/logo-gmf-white.png'); ?>" alt="logo">
    <div class="text"><h1>Furnishing & Upholstery Services</h1><span>GMF AeroAsia</span></div>
  </div>
  <nav class="main-menu">
    <div class="slimscroll">
      <h3 class="title">Menu</h3>
      <ul class="menu" class="metismenu" id="metismenu">
      <?php
        $data_menu = json_decode($_COOKIE['crm_tj_menu'],true);
        foreach ($data_menu as $key_modul => $val_modul) {
          $key = explode('|', $key_modul);

          $modul_name = $key[0];
          $icon = $key[1];

          $menu = $submenu = '';

          $li_class = '';
          if ($modul_name != 'Monitoring') {
            $li_class = (strpos($val_modul[0]['url'], $this->uri->segment(1)) !== false) ? 'active' : '';
          } 

          if (count($val_modul) == 1) {
            $menu = "<a href='".base_url('index.php/'.$val_modul[0]['url'])."'><i class='material-icons'>$icon</i><span class='text'>$modul_name</span></a>";
          } else {
            $li_class .= ' has-child';

            $menu .= "<a href='javascript: void(0);'>";
            
            if ($modul_name == 'Production Control' && in_array('new', get_session('menu'))) {
              $menu .= "<span class='notif-icon' style='display:none'></span>"; 
            }

            $menu .= "<i class='material-icons'>$icon</i><span class='text'>$modul_name</span><span class='icon-expand'></span></a>";

            $submenu .= "<ul class='nav-second-level' aria-expanded='false'>";
            foreach ($val_modul as $key_menu => $val_menu) {
              $notif = ($val_menu['menu'] == 'New') ? "<span class='notif-count' style='display:none'>0</span>" : '';
              $submenu .= "<li><a href='".base_url('index.php/'.$val_menu['url'])."'>".$val_menu['menu']." $notif</a></li>";
            }
            $submenu .= "</ul>";
          }

          $content = $menu . $submenu;

          echo "<li class='$li_class'>$content</li>";
        }
        ?>
        <li>
          <a href="<?= base_url('index.php/logout'); ?>">
            <i class="material-icons">reply</i><span class="text">Logout</span>
          </a>
        </li>
      </ul>
    </div>
  </nav>
</aside>