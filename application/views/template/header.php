<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="PT. Sinergi Informatika Semen Indonesia">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<!-- title -->
  <title>GMF AeroAsia - Furnishing & Upholstery Services</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">

	<!-- icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- css -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/dropdown.bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/visjs/vis-timeline-graph2d.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/metismenu/css/metismenu.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/datepicker/css/datepicker.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/jquery-dropdown/css/jquery.dropdown.min.css'); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/bootoast/bootoast.min.css'); ?>">
	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.css"> -->
	<link rel="stylesheet" href="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.css'); ?>" />
	<link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">

	<!-- fonts -->
	<link rel="stylesheet" href="<?= base_url('assets/fonts/cerebri-sans.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/fonts/skripsweet.css'); ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<script src="https://js.pusher.com/4.3/pusher.min.js"></script>
</head>
<body class="sidebar-collapse">