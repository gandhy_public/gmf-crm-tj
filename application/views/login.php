<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="PT. Sinergi Informatika Semen Indonesia">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<!-- title -->
  <title>GMF AeroAsia - Furnishing & Upholstery Services</title>

  <!-- favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">

  <!-- fonts & icon -->
  <link rel="stylesheet" href="<?= base_url('assets/fonts/cerebri-sans.css'); ?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" href="<?= base_url('assets/fonts/skripsweet.css'); ?>">

  <!-- css -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/bootoast/bootoast.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('assets/css/login.css'); ?>">
</head>
<body>
  <div class="login-box">
    <div class="login-logo">
      <img src="<?= base_url('assets/img/logo-gmf.png'); ?>"/>
      <h3 class="title">Furnishing & Upholstery Services</h3>
    </div>
    <div class="login-card">
      <div class="heading">
        <h4 class="title">Sign in to CRM</h4>
        <span class="subtitle">Please enter your credentials</span>
      </div>
      <form id="form-login" action="<?= base_url('index.php/login/auth'); ?>">
        <div class="form-item">
          <label>Username</label>
          <input type="text" name="username" placeholder="Enter your username" required>
        </div>
        <div class="form-item with-checkbox">
          <label>Password</label>
          <input type="password" name="password" placeholder="Enter your password" required>
          <label class="is-checkbox">
            <div class="checkbox"><label><input type="checkbox" name="remember"/><span>Remember me</span></label></div>
          </label>
        </div>
        <div class="form-item" style="text-align:right">
          <button class="button button-login" type="submit">Sign in</button>
        </div>
      </form>
    </div>
    <div class="login-footer">
      <span>Copyright © <?= date('Y'); ?> GMF AeroAsia</span>
    </div>
  </div>
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/bootoast/bootoast.min.js'); ?>"></script>
  <script>
    $(document).ready(function () {
      $('#form-login').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
          url : $(this).attr('action'),
          type: "POST",
          data: $(this).serialize(),
          beforeSend: function() {
            $('.button-login').prop('disabled', true);
            $('.button-login').text("Processing...");
          },
          success: function (response) {
            var data = $.parseJSON(response);
              
            bootoast.toast({
              message: data.message,
              type: data.type,
              position: "right-top",
              animationDuration: "300",
              dismissable: true
            });

            if(data.status){
              $('.button-login').text("Redirecting...");
              window.location.replace(data.url);  
            }else{
              $('.button-login').prop('disabled', false);
              $('.button-login').text("Sign in");
            }
          }
        });
      });
    });
  </script>
</body>
</html>