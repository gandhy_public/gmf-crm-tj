<div class="content-header"></div>
<div class="content-body">
  <div class="card">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">List Order</h3>
      </div>
      <div class="option-box">
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length' data-target='list-orders'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content multi">
      <div class="toolbar">
        <div class="toolbar-full">
          <input class="toolbar-item full" id="order_search" type="search" placeholder="Fill and enter to search">
          <div class="toolbar-item">
            <div class="single-button export_to_excel excel disabled" data-target="list-orders"><img class="icon" src="<?= base_url('assets/img/excel.svg'); ?>"/></div>
          </div>
        </div>
      </div>
      <table id="list-orders" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
            <th>SAP Status</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
            <th>SAP Status</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  <form id="export_excel_form" action="<?= base_url('index.php/api/export/excel'); ?>" method="POST" style="display:none;">
    <input type="hidden" name="page">
    <textarea type="hidden" name="query"></textarea>
  </form>
</div>
