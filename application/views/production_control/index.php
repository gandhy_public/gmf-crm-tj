  <div class="content-header"></div>
  <div class="content-body" style="padding-top:40px;">
    <ul class="tab-menu">
      <li class="tab-link active" data-tab="tab-list-order">List Order</li>
      <li class="tab-link" data-tab="tab-list-material">List Material</li>
    </ul>
    <div class="card tab-content active" id="tab-list-order" style="border-radius: 0px 8px 8px 8px;">
      <div class="card-header">
        <div class="title-area">
          <h3 class="title">List Order <span class="all">ALL ORDER</span></h3>
        </div>
        <div class="option-box">
          <?php
          $work_center = get_session('work_center');
          if (empty($work_center) || count($work_center) > 1):?>
          <div class="option-item">
            <span>Work Center</span>
            <select class='filter_by_work_center' data-target='list-order'>
              <option value="">All</option>
              <?php foreach ($this->work_center as $item): 
                if (!empty($work_center)) {
                  if (in_array($item->WORK_CENTER, $work_center)) {
                    echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                  }
                } else {
                  echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                }
              endforeach; ?>
            </select>
          </div>
          <?php endif; ?>
          <div class="option-item">
            <span>Revision Status</span>
            <select class='filter_by_project_status' data-target='list-order'>
              <option value="CRTD">Created</option>
              <option value="REL" selected>Release</option>
              <option value="CLSD">Close</option>
            </select>
          </div>
          <div class="option-item length-data">
            <span>Show</span>
            <select class='page-length' data-target='list-order'>
              <option value="10">10</option>
              <option value="50">50</option>
              <option value="100">100</option>
              <option value="-1">All</option>
            </select>
          </div>
        </div>
      </div>
      <div class="card-content multi">
        <div class="toolbar">
          <?php 
          $class = 'full';
          if (is_has_feature('move_order')):
          ?>
            <div class="toolbar-left">
              <div class="toolbar-item toolbar-dropdown" style="position: relative">
                <select name="work_center">
                  <option value="">Update Work Center</option>
                  <?php foreach ($this->work_center as $item): ?>
                    <option value="<?= $item->WORK_CENTER; ?>"><?= $item->WORK_CENTER; ?></option>
                  <?php endforeach; ?>
                </select>
                <button class="button is-success" type="button" id="move_order" disabled>Move</button>
              </div>
              <div class="toolbar-item toolbar-dropdown">
                <select name="update_status">
                  <option value="">Update Status</option>
                  <?php foreach ($progress_status as $val): ?>
                    <option value="<?= $val; ?>"><?= $val; ?></option>
                  <?php endforeach; ?>
                </select>
                <button class="button is-success" type="button" id="update_status" disabled>Update</button>
              </div>
            </div>
          <?php 
          $class = 'right';
          endif; 
          ?>
          <div class="toolbar-<?= $class; ?>">
            <input class="toolbar-item full" id="order_search" type="search" placeholder="Fill and enter to search">
            <?php if (is_has_feature('sync_order') && $status_sync_order == 0):?>
              <div class="toolbar-item">
                <div class="single-button synchronize display-flex align-items-center justify-center disabled" style="background:#0065FF; margin-right: -8px"><i class="material-icons warning sync_order" data-type="modal" modal-target="syncModal">sync</i></div>
              </div>
            <?php endif; ?>
            <div class="toolbar-item">
              <div class="single-button export_to_excel excel disabled" data-target="list-order"><img class="icon" src="<?= base_url('assets/img/excel.svg'); ?>"/></div>
            </div>
          </div>
        </div>
        <table id="list-order" class="display is-striped is-bordered" style="width:100%">
          <thead>
            <tr>
              <th>
                <div class="checkbox">
                  <label><input id="select_all" class="check" type="checkbox"><span></span></label>
                </div>
              </th>
              <th>No</th>
              <th>Project</th>
              <th>Order</th>
              <th>Type</th>
              <th>Description</th>
              <th>A/C REG</th>
              <th>Target Date</th>
              <th>Pmhrs (hours)</th>
              <th>Amhrs (hours)</th>
              <th>Work Center Origin</th>
              <th>Work Center Moving</th>
              <th>Progress Status</th>
              <th>Remarks</th>
              <th>Part Name</th>
              <th>Qty</th>
              <th>Location</th>
              <th>SAP Status</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>#</th>
              <th>No</th>
              <th>Project</th>
              <th>Order</th>
              <th>Type</th>
              <th>Description</th>
              <th>A/C REG</th>
              <th>Target Date</th>
              <th>Pmhrs (hours)</th>
              <th>Amhrs (hours)</th>
              <th>Work Center Origin</th>
              <th>Work Center Moving</th>
              <th>Progress Status</th>
              <th>Remarks</th>
              <th>Part Name</th>
              <th>Qty</th>
              <th>Location</th>
              <th>SAP Status</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <div class="card tab-content" id="tab-list-material">
      <div class="card-header">
        <div class="title-area">
          <h3 class="title">List Material</h3>
        </div>
        <div class="option-box">
          <?php
          $work_center = get_session('work_center');
          if (empty($work_center) || count($work_center) > 1):?>
          <div class="option-item">
            <span>Work Center</span>
            <select class='filter_by_work_center' data-target='list-material'>
              <option value="">All</option>
              <?php foreach ($this->work_center as $item): 
                if (!empty($work_center)) {
                  if (in_array($item->WORK_CENTER, $work_center)) {
                    echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                  }
                } else {
                  echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                }
              endforeach; ?>
            </select>
          </div>
          <?php endif; ?>
          <div class="option-item">
            <span>Revision Status</span>
            <select class='filter_by_project_status' data-target='list-material'>
              <option value="CRTD">Created</option>
              <option value="REL" selected>Release</option>
              <option value="CLSD">Close</option>
            </select>
          </div>
          <div class="option-item length-data">
            <span>Show</span>
            <select class='page-length' data-target='list-material'>
              <option value="10">10</option>
              <option value="50">50</option>
              <option value="100">100</option>
              <option value="-1">All</option>
            </select>
          </div>
        </div>
      </div>
      <div class="card-content">
        <div class="toolbar">
          <?php 
          $class = 'full';
          if (is_has_feature('update_material_form')):
          ?>
          <div class="toolbar-left">
            <div class="toolbar-item toolbar-dropdown">
              <select name="responsibility">
                <option value="">Responsibility</option>
                <?php foreach ($responsibility as $val): ?>
                  <option value="<?= $val; ?>"><?= $val; ?></option>
                <?php endforeach; ?>
              </select>
              <button class="button is-success" type="button" id="responsibility" data-column="RESPONSIBILITY" disabled>Change</button>
            </div>
            <div class="toolbar-item toolbar-dropdown">
              <select name="material_status">
                <option value="">Material Status</option>
                <?php foreach ($material_status as $val): ?>
                  <option value="<?= $val; ?>"><?= $val; ?></option>
                <?php endforeach; ?>
              </select>
              <button class="button is-success" type="button" id="material_status" data-column="MATERIAL_FULFILLMENT_STATUS" disabled>Change</button>
            </div>
          </div>
          <?php
          $class = 'right';
          endif;
          ?>
          <div class="toolbar-<?= $class; ?>">
            <input class="toolbar-item full" id="order_search" type="search" placeholder="Fill and enter to search">
            <div class="toolbar-item">
            <div class="single-button export_to_excel excel disabled" data-target="list-material"><img class="icon" src="<?= base_url('assets/img/excel.svg'); ?>"/></div>
            </div>
          </div>
        </div>
        <table id="list-material" class="display is-striped is-bordered" style="width:100%">
          <thead>
            <tr>
              <th rowspan="2">
                <div class="checkbox">
                  <label><input id="select_all_material" class="check" type="checkbox"><span></span></label>
                </div>
              </th>
              <th rowspan="2">No</th>
              <th rowspan="2">Project</th>
              <th rowspan="2">Order</th>
              <th rowspan="2">Part Number</th>
              <th rowspan="2">Alternate Part Number</th>
              <th rowspan="2">Type</th>
              <th rowspan="2">Material Desc.</th>
              <th rowspan="2">Material Type</th>
              <th rowspan="2">Qty on the task</th>
              <th rowspan="2">Qty on all task</th>
              <th rowspan="2">UoM</th>
              <th rowspan="2">Responsibility</th>
              <th rowspan="2">Material Fulfillment Status</th>
              <th rowspan="2" style="min-width: 150px;">Remarks</th>
              <th class="small">Actual Nil Stock</th>
              <th class="small" colspan="2">Ordered By Purchasing</th>
              <th class="small" colspan="3">Shipment/Custom Process</th>
              <th class="small">Provision in Store</th>
              <th class="small">Preloaded in Hangar/Shop Store</th>
              <th class="small" colspan="3">Delivered to Production</th>
            </tr>
            <tr>
              <th class="small">CSP/RFQ</th>
              <th class="small">PO Number</th>
              <th class="small">Lead Time</th>
              <th class="small">AWB</th>
              <th class="small">INB</th>
              <th class="small">SP</th>
              <th class="small">STO</th>
              <th class="small">Storage Location</th>
              <th class="small">Date Delivered</th>
              <th class="small">Qty Delivered</th>
              <th class="small">Receiver Name</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>#</th>
              <th>No</th>
              <th>Project</th>
              <th>Order</th>
              <th>Part Number</th>
              <th>Alternate Part Number</th>
              <th>Type</th>
              <th>Material Desc.</th>
              <th>Material Type</th>
              <th>Qty on the task</th>
              <th>Qty on all task</th>
              <th>UoM</th>
              <th>Responsibility</th>
              <th>Material Fulfillment Status</th>
              <th>Remarks</th>
              <th>CSP/RFQ</th>
              <th>PO Number</th>
              <th>Lead Time</th>
              <th>AWB</th>
              <th>INB</th>
              <th>SP</th>
              <th>STO</th>
              <th>Storage Location</th>
              <th>Date Delivered</th>
              <th>Qty Delivered</th>
              <th>Receiver Name</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <form id="export_excel_form" action="<?= base_url('index.php/api/export/excel'); ?>" method="POST" style="display:none;">
      <input type="hidden" name="page">
      <textarea type="hidden" name="query"></textarea>
    </form>
  </div>

  <?php require_once('order_modal.php'); ?>
  <?php require_once('sync_modal.php'); ?>