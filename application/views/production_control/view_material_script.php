<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/datatables.buttons.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/jszip.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/buttons.html5.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/select2/select2.full.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var queryMaterial = "";

  function get_param_revision() {
    return getUrlParameter('revision') === undefined ? '' : getUrlParameter('revision');
  }

  function get_param_material_status() {
    return getUrlParameter('material_status') === undefined ? '' : getUrlParameter('material_status');
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_part_number_table = $('#list-part-number').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/production_control/ajax_view_list_material'); ?>",
        type: "POST",
        data: function(d) {
          d.revision = get_param_revision();
          d.material_status = get_param_material_status();
        },
        complete: function (json, type) { //type return "success" or "parsererror"
          queryMaterial = json.responseJSON.query;
          $('.export_to_excel[data-target="list-part-number"]').removeClass('disabled');
        }
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'revnr', name: 'revnr'},
        {data: 'order', name: 'order'},
        {data: 'part_number', name: 'part_number'},
        {data: 'part_number_alt', name: 'part_number_alt'},
        {data: 'reg', name: 'reg'},
        {data: 'material_desc', name: 'material_desc'},
        {data: 'material_type', name: 'material_type'},
        {data: 'qty', name: 'qty'},
        {data: 'qty_total', name: 'qty_total'},
        {data: 'uom', name: 'uom'},
        {data: 'responsibility', name: 'responsibility'},
        {data: 'material_fulfillment', name: 'material_fulfillment'},
        {data: 'remarks', name: 'remarks'},
        {data: 'csp_rfq', name: 'csp_rfq'},
        {data: 'po_number', name: 'po_number'},
        {data: 'lead_time', name: 'lead_time'},
        {data: 'awb', name: 'awb'},
        {data: 'inb', name: 'inb'},
        {data: 'sp', name: 'sp'},
        {data: 'sto', name: 'sto'},
        {data: 'storage_location', name: 'storage_location'},
        {data: 'date_delivered', name: 'date_delivered'},
        {data: 'qty_delivered', name: 'qty_delivered'},
        {data: 'receiver_name', name: 'receiver_name'},
      ],
      fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
        if ( aData['uom_status'] == "FALSE" ) {
          $('td', nRow).eq(10).css({'background-color':'#ffab00', 'color': '#fff'});
          $('td', nRow).eq(11).css({'background-color':'#ffab00', 'color': '#fff'});
        }
      },
      order: [[ 0, '' ]],
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_part_number_table.page.len($(this).val()).draw();
    });
    
    $('table tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Part Number', 'Alternate Part Number', 'Type', 'Material Desc.', 'Material Type', 'Qty on the task', 'Qty on all tas', 'UoM', 'Responsibility', 'Material Fulfillment Status', 'Remarks', 'CSP/RFQ', 'PO Number', 'Lead Time', 'AWB', 'INB', 'SP', 'STO', 'Storage Location', 'Qty Delivered', 'Receiver Name'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_part_number_table.columns().eq(0).each(function(colIdx) {
      $('input', list_part_number_table.column(colIdx).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_part_number_table.column(colIdx).search(this.value).draw();
        }
      });
    });

    $('#material_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_part_number_table.search(this.value).draw();
      }
    });

    // Export to excel
    $(".export_to_excel").on("click", function() {
      if ($(this).hasClass('disabled') != true) {
        $('#export_excel_form [name=page]').val('List Material');
        $('#export_excel_form [name=query]').val(queryMaterial);

        $('#export_excel_form').submit();
      }
    });
  });
</script>