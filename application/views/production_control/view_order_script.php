<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var queryOrder = "";

  function get_param_revision() {
    return getUrlParameter('revision') === undefined ? '' : getUrlParameter('revision');
  }

  function get_param_work_center() {
    return getUrlParameter('work_center') === undefined ? '' : getUrlParameter('work_center');
  }

  function get_param_order_type() {
    return getUrlParameter('type') === undefined ? '' : getUrlParameter('type');
  }

  function get_param_progress_status() {
    return getUrlParameter('status') === undefined ? '' : getUrlParameter('status');
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_order_table = $('#list-orders').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/production_control/ajax_view_list_order"); ?>',
        type: "POST",
        data: function(d) {
          d.revision = get_param_revision();
          d.work_center = get_param_work_center();
          d.order_type = get_param_order_type();
          d.progress_status = get_param_progress_status();
        },
        complete: function(json, type) {
          queryOrder = json.responseJSON.query;
          $('.export_to_excel[data-target="list-orders"]').removeClass('disabled');
        }
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},
        {data: 'project', name: 'project'},
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
        {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
        {data: 'origin', name: 'origin'},
        {data: 'moving', name: 'moving'},
        {data: 'status', name: 'status'},
        {data: 'remarks', name: 'remarks', orderable: false},
        {data: 'part_name', name: 'part_name', orderable: false},
        {data: 'qty', name: 'qty', orderable: false},
        {data: 'location', name: 'location', orderable: false},
        {data: 'sap_status', name: 'sap_status'},
      ],
      order: [[ 0, '' ]]
    });

    $('.page-length').on('change', function(){
      list_order_table.page.len($(this).val()).draw();
    });

    $('table tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center Origin', 'Work Center Moving', 'Progress Status'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_order_table.columns().eq(0).each(function(colId) {
      $('input', list_order_table.column(colId).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_order_table.column(colId).search(this.value).draw();
        }
      });
    });

    $('#order_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_order_table.search(this.value).draw();
      }
    });

    // Export to excel
    $(".export_to_excel").on("click", function() {
      if ($(this).hasClass('disabled') != true) {
        $('#export_excel_form [name=page]').val('List Order');
        $('#export_excel_form [name=query]').val(queryOrder);

        $('#export_excel_form').submit();
      }
    });
  });
</script>