<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/datatables.buttons.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/jszip.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/buttons.html5.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/select2/select2.full.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var queryOrder = "";
  var queryMaterial = "";

  var show = getUrlParameter('show');

  if (show == 'list_order' || show == 'list_material') {
    $('.content-body').css('padding-top', '0');
    $('.tab-menu').hide();
    $('.card.tab-content').css('border-radius', '8px');

    $('.filter_by_work_center').parents('.option-item').hide();
    $('.filter_by_project_status').parents('.option-item').hide();
  } else {
    $('.tab-menu').show();
  }
    
  if (show === undefined || show == 'list_order') {
    $('[data-tab="tab-list-order"]').addClass('active');
    $('#tab-list-order').addClass('active');
  } else {
    $('[data-tab="tab-list-part-number"]').addClass('active');
    $('#tab-list-part-number').addClass('active');
  }

  function get_project_status(table) {
    return $(".filter_by_project_status[data-target='"+table+"']").val();
  }

  function get_work_center(table) {
    return $(".filter_by_work_center[data-target='"+table+"']").val();
  }

  function get_param_revision() {
    return getUrlParameter('revision') === undefined ? '' : getUrlParameter('revision');
  }

  function get_param_work_center() {
    return getUrlParameter('work_center') === undefined ? '' : getUrlParameter('work_center');
  }

  function get_param_order_type() {
    return getUrlParameter('type') === undefined ? '' : getUrlParameter('type');
  }

  function get_param_order_status() {
    return getUrlParameter('status') === undefined ? '' : getUrlParameter('status');
  }

  function get_param_material_status() {
    return getUrlParameter('material_status') === undefined ? '' : getUrlParameter('material_status');
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_order_table = $('#list-orders').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/production_control/ajax_list_all_order"); ?>',
        type: "POST",
        data: function(d) {
          d.project_status = get_project_status('list-orders');
          d.work_center = get_work_center('list-orders');
          d.param_revision = get_param_revision();
          d.param_work_center = get_param_work_center();
          d.param_order_type = get_param_order_type();
          d.param_order_status = get_param_order_status();
        },
        complete: function(json, type) {
          $('#list-orders [data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'yyyy-mm-dd',
          });
          queryOrder = json.responseJSON.query;
          $('.export_to_excel[data-target="list-orders"]').removeClass('disabled');
          $('.single-button.synchronize').removeClass('disabled');
        }
      },
      columns: [            
        {data: 'check', name: 'check', orderable: false},
        {data: 'no', name: 'no', orderable: false},
        {data: 'project', name: 'project'},            
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
        {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
        {data: 'origin', name: 'origin'},
        {data: 'moving', name: 'moving'},
        {data: 'status', name: 'status'},
        {data: 'remarks', name: 'remarks', orderable: false},
        {data: 'location', name: 'location', orderable: false},
        {data: 'part_name', name: 'part_name', orderable: false},
        {data: 'qty', name: 'qty', orderable: false},
        {data: 'sap_status', name: 'sap_status'},
      ],
      order: [[ 0, '' ]],
      drawCallback: function() {
        $('.select2_progress_status').select2({
          dropdownAutoWidth : true,
          width: 'element',
          minimumResultsForSearch: -1
        });
        $('.select2_location').select2({
          placeholder: 'Location',
          dropdownAutoWidth : true,
          width: 'element',
          minimumResultsForSearch: -1
        });
      }
    });

    $('#tab-list-order tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center Origin', 'Work Center Moving', 'Progress Status'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_order_table.columns().eq(0).each(function(colId) {
      $('input', list_order_table.column(colId).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_order_table.column(colId).search(this.value).draw();
        }
      });
    });

    // update progress status single
    $('#list-orders').on("select2:select", '.select2_progress_status', function(e) { 
      var data = e.params.data;
      var aufnr = [];
      aufnr.push($(this).data('identity'));
      var status = data.text;

      if(data.text == 'Sent to Other') {
        $('#list-orders .remarks-' + $(this).data('identity')).prop('disabled', false);
        $('#list-orders .remarks-' + $(this).data('identity')).focus();
      }

      $.ajax({
        url: '<?= base_url("index.php/api/production_control/update_progress_status"); ?>',
        type: 'POST',
        data: {
          status: status,
          aufnr: aufnr
        },
        success: function(response) {
          var response = $.parseJSON(response);
          if (response.status == "success") {
            if(data.text == 'Close') {
              display_toast('success', 'Order ' + aufnr[0] + ' has been set to Close');
              list_order_table.ajax.reload();
            } else {
              display_toast('success', 'Order ' + aufnr[0] + ' has been set to ' + data.text);
            }
          }
        },
        error: function(request, status, error) {
          alert('Something error when update progress status!');
          console.log(error);
        }
      });
    });

    $('#order_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_order_table.search(this.value).draw();
      }
    });

    // update single column
    $('#list-orders').on('change', '.input-date', function(e) {
      var aufnr = $(this).data('aufnr');
      var column = $(this).data('column');
      var alias = $(this).data('alias');
      var value = $(this).val();

      var inputTarget = $('#list-orders .target_date[data-aufnr=' + aufnr + ']');

      var today = new Date();
      today = Date.parse(today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate());
      var targetDate = Date.parse($(this).val());

      if(targetDate >= today || value == '') {
        if (value != '') {
          var target_date_bg = '';
          var diff = dateDiffInDays(new Date(), new Date(value));
          if (diff <= 0) {
            inputTarget.css({'background':'#ff4949', 'color':'#ffffff'});
          } else if(diff > 0 && diff <= 3) {
            inputTarget.css({'background':'#FFAB00', 'color':'#ffffff'});
          } else {
            inputTarget.css({'background':'#fff', 'color':'#465368'});
          }
        } else {
          inputTarget.css({'background':'#fff', 'color':'#465368'});
        }
        
        $.ajax({
          url: '<?= base_url("index.php/api/production_control/update_order_per_column"); ?>',
          type: 'POST',
          data: {
            aufnr: aufnr,
            column: column,
            value: value
          },
          success: function(response) {
            var response = $.parseJSON(response);
            
            if (response.status == "success") {
              display_toast('info', 'Successfully update target date for order '+ aufnr);
            }
          },
          error: function(request, status, error) {
            alert('Something error when update target date!');
            console.log(error);
          }
        });
      } else {
        inputTarget.val('');
        swal({
          title: "Whoops!",
          text: "Can't insert previous date!",
          icon: "error",
          button: true
        });
      }
    });

    $('#list-orders').on('change', '.select-editable', function(e) {
      if($(this).val() != '') {
        e.preventDefault(); 
        update_order_per_column({
          url: '<?= base_url("index.php/api/production_control/update_order_per_column"); ?>',
          type: 'POST',
          aufnr: $(this).data('aufnr'),
          column: $(this).data('column'),
          alias: $(this).data('alias'),
          value: $(this).val()
        });
      }
    });

    $('#list-orders').on('keydown', '.input-editable', function(e) {
      var keyCode = e.keyCode || e.which; 
      if(keyCode == 9 || keyCode == 13) {
        // if($(this).val() != '') {
          e.preventDefault(); 
          $(this).blur();
          
          update_order_per_column({
            url: '<?= base_url("index.php/api/production_control/update_order_per_column"); ?>',
            type: 'POST',
            aufnr: $(this).data('aufnr'),
            column: $(this).data('column'),
            alias: $(this).data('alias'),
            value: $(this).val()
          });

          // move to the next input
          var index = $('.input-editable').index(this) + 1;
          $('.input-editable').eq(index).focus();
        // }
      }
    });

    $('#select_all').click(function(event) {
      if (this.checked) {
        var i = 0;
        $('#list-orders .check.item:not(:disabled)').each(function() {
          this.checked = true;
          i++;
        });
        if (i > 0) {
          $('#move_order').prop("disabled", false);
          $('#update_status').prop("disabled", false);
        }
      } else {
        $('#list-orders :checkbox').each(function() {
          this.checked = false;
          $('#move_order').prop("disabled", true);
          $('#update_status').prop("disabled", true);
        });
      }
    });

    $('#move_order').prop("disabled", true);
    $('#update_status').prop("disabled", true);

    $('#list-orders').on('click', '.check', function() {
      if ($(this).is(':checked')) {
        $('#move_order').prop("disabled", false);
        $('#update_status').prop("disabled", false);
      } else {
        if ($('.check').filter(':checked').length < 1) {
          $('#move_order').attr('disabled', true);
          $('#update_status').prop("disabled", true);
        }
      }
    });

    $('#move_order').on('click', function() {
      if($('select[name=work_center]').val() !== '') {
        var work_center_curr = [];
        var work_center_dest = $('select[name=work_center]').val();
        var aufnr = [];
        $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
          aufnr.push($(this).val());
          work_center_curr.push($(this).attr('work_center'));
        });

        $('#move_order').prop('disabled', true);
        $('#move_order').text('Processing...');

        setTimeout(() => {
          $.ajax({
            url: '<?= base_url("index.php/api/production_control/move_order"); ?>',
            type: 'POST',
            data: { 
              aufnr: aufnr,
              work_center_curr: work_center_curr,
              work_center_dest: work_center_dest,
            },
            success: function(response) {
              var response = $.parseJSON(response);
              
              if (response.status == 'success') {
                swal({
                  title: "Work Center Updated",
                  text: response.message,
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function() {
                    list_order_table.ajax.reload(null, false);
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error when update work area!');
              console.log(error);
            },
            complete: function() {
              $('#move_order').prop('disabled', false);
              $('#move_order').text('Move');
              $('select[name=work_center]').val('');
              $('select[name=update_status]').val('');
              $('#move_order').attr('disabled', true);
              $('#update_status').attr('disabled', true);
              $('#select_all').prop('checked', false);
            }
          });
        }, 2000);
      } else {
        swal({
          title: "Whoops!",
          text: "Please choose work center berofe click move",
          icon: "error",
          button: true
        });
      }
    });

    $('#update_status').on('click', function() {
      if($('select[name=update_status]').val() !== '') {
        var aufnr = [];
        var status = $('select[name=update_status]').val();

        $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
          aufnr.push($(this).val());
        });

        $('#update_status').prop('disabled', true);
        $('#update_status').text('Processing...');

        setTimeout(() => {
          $.ajax({
            url: '<?= base_url("index.php/api/production_control/update_progress_status"); ?>',
            type: 'POST',
            data: { 
              aufnr: aufnr,
              status: status
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == "success") {
                swal({
                  title: "Progress Status Updated",
                  text: "Successfully update Progress Status!",
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function() {
                    list_order_table.ajax.reload(null, false);
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error when update progress status!');
              console.log(error);
            },
            complete: function() {
              $('#update_status').prop('disabled', false);
              $('#update_status').text('Update');
              $('select[name=work_center]').val('');
              $('select[name=update_status]').val('');
              $('#move_order').attr('disabled', true);
              $('#update_status').attr('disabled', true);
              $('#select_all').prop('checked', false);
            }
          });
        }, 2000);
      } else {
        swal({
          title: "Whoops!",
          text: "Please choose status berofe click update",
          icon: "error",
          button: true
        });
      }
    });

    $('select[name=update_status]').on('click', function() {
      var status = $(this).val();
      var work_center = [];
      $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
        work_center.push($(this).attr('work_center'));
      });

      $('select[name=update_status] option').show();

      try {
          for(var i = 1; i < work_center.length; i++) {
            if(work_center[i] !== work_center[0]) throw 'Please select order that have same Work Center';
          }

          var data = '<?= json_encode($work_center_group_select); ?>';
          var array = $.parseJSON(data);
          var work_center_first = work_center[0].includes('GAH') == true ? 'Hangar' : work_center[0];
          var index = array.indexOf(work_center_first);
          array.splice(index, 1);

          for (let i = 0; i < array.length; i++) {
            $('select[name=update_status] option[value="Progress in ' + array[i] + '"]').hide();
          }
      } catch (error) {
        var data = '<?= json_encode($work_center_group_select); ?>';
        var array = $.parseJSON(data);
        for (let i = 0; i < array.length; i++) {
          $('select[name=update_status] option[value="Progress in ' + array[i] + '"]').hide();
        }
      }
    });

    $('#list-orders, #list-part-number').on('click', '.order-number', function(event) {
      event.preventDefault();
      var aufnr = $(this).attr('aufnr');
      $('#orderModal .modal-body .ovrly').show();
      
      $('#orderModal .modal-header .title').text('ORDER ' + aufnr);

      $('#orderModal .modal-body iframe').attr('src', 'http://gmftjapps.com/sp/spviewcrm.php?ord=' + aufnr);
    
      $('#orderModal .modal-body iframe').on('load', function () {
        $('#orderModal .modal-body .ovrly').hide();
      });
    });

    $('#list-orders').on('click', '.cancel-move', function(event) {
      var aufnr = [$(this).attr('aufnr')];
      var work_center_dest = [$(this).attr('work_center')];

      var text = (aufnr.length > 1) ? 'You will cancel ' + aufnr.length + ' order!' : 'You will cancel order ' + aufnr[0] + '!';

      swal({
        title: "Are you sure?",
        text: text,
        icon: "warning",
        buttons: [
          'No',
          'Yes'
        ],
        dangerMode: true,
      }).then(function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '<?= base_url("index.php/api/production_control/cancel_move_order"); ?>',
            type: 'POST',
            data: { 
              aufnr: aufnr,
              work_center_dest: work_center_dest,
              act: 'cancel'
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == 'success') {
                swal({
                  title: "Success",
                  text: response.message,
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function() {
                    list_order_table.ajax.reload(null, false);
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error when cancel move order!');
              console.log(error);
            }
          });
        }
      });
    });

    var list_part_number_table = $('#list-part-number').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/production_control/ajax_list_material'); ?>",
        type: "POST",
        data: function(d) {
          d.project_status = get_project_status('list-part-number');
          d.work_center = get_work_center('list-part-number');
          d.param_revision = get_param_revision();
          d.param_material_status = get_param_material_status();
        },
        complete: function (json, type) { //type return "success" or "parsererror"
          $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'yyyy-mm-dd',
          });
          queryMaterial = json.responseJSON.query;
          $('.export_to_excel[data-target="list-part-number"]').removeClass('disabled');
        }
      },
      columns: [            
        {data: 'check', name: 'check', orderable: false}, 
        {data: 'no', name: 'no', orderable: false},            
        {data: 'revnr', name: 'revnr'},
        {data: 'order', name: 'order'},
        {data: 'part_number', name: 'part_number'},
        {data: 'part_number_alt', name: 'part_number_alt'},
        {data: 'reg', name: 'reg'},
        {data: 'material_desc', name: 'material_desc'},
        {data: 'material_type', name: 'material_type'},
        {data: 'qty', name: 'qty'},
        {data: 'qty_total', name: 'qty_total'},
        {data: 'uom', name: 'uom'},
        {data: 'responsibility', name: 'responsibility'},
        {data: 'material_fulfillment', name: 'material_fulfillment'},
        {data: 'remarks', name: 'remarks'},
        {data: 'csp_rfq', name: 'csp_rfq'},
        {data: 'po_number', name: 'po_number'},
        {data: 'lead_time', name: 'lead_time'},
        {data: 'awb', name: 'awb'},
        {data: 'inb', name: 'inb'},
        {data: 'sp', name: 'sp'},
        {data: 'sto', name: 'sto'},
        {data: 'storage_location', name: 'storage_location'},
        {data: 'date_delivered', name: 'date_delivered'},
        {data: 'qty_delivered', name: 'qty_delivered'},
        {data: 'receiver_name', name: 'receiver_name'},
      ],
      fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
        if ( aData['uom_status'] == "FALSE" ) {
          $('td', nRow).eq(10).css({'background-color':'#ffab00', 'color': '#fff'});
          $('td', nRow).eq(11).css({'background-color':'#ffab00', 'color': '#fff'});
        }
      },
      order: [[ 0, '' ]],
      drawCallback: function() {
        $('.select2_responsibility').select2({
          placeholder: 'Responsibility',
          dropdownAutoWidth : true,
          width: 'element',
          minimumResultsForSearch: -1
        });
        $('.select2_material_fulfillment').select2({
          placeholder: 'Material Fullfilment Status',
          dropdownAutoWidth : true,
          width: 'element',
          minimumResultsForSearch: -1
        });
        $('.select2_csp_rfq').select2({
          placeholder: 'CSP/RFQ',
          dropdownAutoWidth : true,
          width: 'element',
          minimumResultsForSearch: -1
        });
      },
    });

    // Custome page length
    $('.page-length').on('change', function(){
      if($(this).data('target') == 'list-orders') {
        list_order_table.page.len($(this).val()).draw();
      } else {
        list_part_number_table.page.len($(this).val()).draw();
      }
    });
    
    // Export to excel
    $(".export_to_excel").on("click", function() {
      if ($(this).hasClass('disabled') != true) {
        if($(this).data('target') == 'list-orders' && queryOrder !== '') {
          var page = 'List Order';
          var query = queryOrder;
        } else if ($(this).data('target') == 'list-part-number' && queryMaterial !== '') {
          var page = 'List Material';
          var query = queryMaterial;
        }

        $('#export_excel_form [name=page]').val(page);
        $('#export_excel_form [name=query]').val(query);

        $('#export_excel_form').submit();
      }
    });
  
    // Custom filter
    $('.filter_by_project_status, .filter_by_work_center').on('change', function(){
      if($(this).data('target') == 'list-orders') {
        list_order_table.ajax.reload(null, false);
      } else {
        list_part_number_table.ajax.reload(null, false);
      }
    });

    $('#tab-list-part-number tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Part Number', 'Alternate Part Number', 'Type', 'Material Desc.', 'Material Type', 'Qty on the task', 'Qty on all tas', 'UoM', 'Responsibility', 'Material Fulfillment Status', 'Remarks', 'CSP/RFQ', 'PO Number', 'Lead Time', 'AWB', 'INB', 'SP', 'STO', 'Storage Location', 'Qty Delivered', 'Receiver Name'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_part_number_table.columns().eq(0).each(function(colIdx) {
      $('input', list_part_number_table.column(colIdx).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_part_number_table.column(colIdx).search(this.value).draw();
        }
      });
    });

    $('#material_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_part_number_table.search(this.value).draw();
      }
    });

    // update responsibility
    $('#list-part-number').on("select2:select", '.select2_responsibility', function(e) { 
      var data = e.params.data;

      update_material_per_column({
        url: '<?= base_url("index.php/api/production_control/update_material_per_column"); ?>',
        type: 'POST',
        target: $('#list-part-number .identity-'+ $(this).data('id')).val(),
        column: 'RESPONSIBILITY',
        alias: 'Responsibility',
        value: data.text
      });

      var index = $('.input-editable').index(this) + 1;
      if($('.input-editable').eq(index).hasClass('select2')) {
        $('.input-editable').eq(index).focus();
        $('.input-editable').eq(index).select2('open');
      }
    });

    $('#list-part-number').on("select2:select", '.select2_material_fulfillment', function(e) { 
      var data = e.params.data;
      var id = $(this).data('id');
      var inputs = [];

      update_material_per_column({
        url: '<?= base_url("index.php/api/production_control/update_material_per_column"); ?>',
        type: 'POST',
        target: $('#list-part-number .identity-'+ $(this).data('id')).val(),
        column: 'MATERIAL_FULFILLMENT_STATUS',
        alias: 'Material Fulfillment Status',
        value: data.text
      });

      switch (data.text) {
        case 'Actual Nil Stock':
          inputs = ['csp_rfq'];
          break;
        case 'Ordered By Purchasing':
          inputs = ['po_number', 'lead_time'];
          break;
        case 'Shipment/Custom Process':
          inputs = ['awb', 'inb', 'sp'];
          break;
        case 'Provision in Store':
          inputs = ['sto'];
          break;
        case 'Preloaded in Hangar/Shop Store':
          inputs = ['storage_location'];
          break;
        case 'Delivered to Production':
          inputs = ['date_delivered', 'qty_delivered', 'receiver_name'];
          break;
        default:
          break;
      }

      $('.input-editable.mfs').prop("disabled", true);
      var indexs = [];
      var a = 0;
      $.each(inputs, function( index, value ) {
        var target = '.input-editable.mfs.' + value + '[data-id='+id+']';
        $(target).prop("disabled", false);
        indexs[index] = $('.input-editable').index($(target));
      });

      $('.input-editable').eq(indexs[0]).focus();
      if($('.input-editable').eq(indexs[0]).hasClass('select2')) {
        $('.input-editable').eq(indexs[0]).select2('open');
      }
    });

    // update single column
    $('#list-part-number').on('keydown', '.input-editable', function(e) {
      var keyCode = e.keyCode || e.which; 
      if(keyCode == 9 || keyCode == 13) {
        // if($(this).val() != '') {
          e.preventDefault(); 
          update_material_per_column({
            url: '<?= base_url("index.php/api/production_control/update_material_per_column"); ?>',
            type: 'POST',
            target: $('#list-part-number .identity-'+ $(this).data('id')).val(),
            column: $(this).data('column'),
            alias: $(this).data('alias'),
            value: $(this).val()
          });

          // move to the next input
          var index = $('.input-editable').index(this) + 1;
          var ctn = false;
          while(ctn == false) {
            if($('.input-editable').eq(index).hasClass('select2')) {
              $('.input-editable').eq(index).focus();
              $('.input-editable').eq(index).select2('open');
              ctn = true;
            } else if ($('.input-editable').eq(index).prop('disabled') == true) {
              index++;
            } else {
              $('.input-editable').eq(index).focus();
              ctn = true;
            }
          }
        // }
      }
    });

    $('#list-part-number').on('change', '.select-editable', function(e) {
      if($(this).val() != '') {
        e.preventDefault(); 
        update_material_per_column({
          url: '<?= base_url("index.php/api/production_control/update_material_per_column"); ?>',
          type: 'POST',
          target: $('#list-part-number .identity-'+ $(this).data('id')).val(),
          column: $(this).data('column'),
          alias: $(this).data('alias'),
          value: $(this).val()
        });
      }
    });

    $('[data-tab="tab-list-order"]').click(function(){
      // list_order_table.ajax.reload(null, false);
    });

    $('[data-tab="tab-list-part-number"]').click(function(){
      // list_part_number_table.ajax.reload(null, false);
    });

    $('.modal-box').on('click', '.cancel, .close', function(event) {
      event.preventDefault();
      var modal = $(this).attr('modal-target');
      hideModal(modal);
      $('#syncModal #form-sync [name=file]').val('');
      $('#syncModal #form-sync .file-return').text('').hide();
    });

    $('#select_all_material').click(function(event) {
      if (this.checked) {
        var i = 0;
        $('#list-part-number .check.item:not(:disabled)').each(function() {
          this.checked = true;
          i++;
        });
        if (i > 0) {
          $('#responsibility').prop("disabled", false);
          $('#material_status').prop("disabled", false);
        }
      } else {
        $('#list-part-number :checkbox').each(function() {
          this.checked = false;
          $('#responsibility').prop("disabled", true);
          $('#material_status').prop("disabled", true);
        });
      }
    });

    $('#list-part-number').on('click', '.check', function() {
      if ($(this).is(':checked')) {
        $('#responsibility').prop("disabled", false);
        $('#material_status').prop("disabled", false);
      } else {
        if ($('.check').filter(':checked').length < 1) {
          $('#responsibility').attr('disabled', true);
          $('#material_status').prop("disabled", true);
        }
      }
    });

    $('#responsibility, #material_status').on('click', function() {
      if($('select[name='+$(this).attr('id')+']').val() !== '') {
        var target = [];
        var column = $(this).data('column');
        var value = $('select[name='+$(this).attr('id')+']').val();

        $("input:checkbox[name=materials\\[\\]]:checked").each(function(){
          target.push($(this).val());
        });

        $(this).prop('disabled', true);
        $(this).text('Processing...');

        setTimeout(() => {
          $.ajax({
            url: '<?= base_url("index.php/api/production_control/update_material_multiple"); ?>',
            type: 'POST',
            data: { 
              target: target,
              column: column,
              value: value
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == "success") {
                if(column == 'RESPONSIBILITY') {
                  var alias = 'Responsibility';
                } else {
                  var alias = 'Material Status';
                }

                swal({
                  title: "Success",
                  text: "Successfully update " + alias,
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function() {
                    list_part_number_table.ajax.reload(null, false);
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error!');
              console.log(error);
            },
            complete: function() {
              $('#responsibility').text('Change');
              $('#material_status').text('Change');
              $('select[name=responsibility]').val('');
              $('select[name=material_status]').val('');
              $('#responsibility').prop('disabled', true);
              $('#material_status').prop('disabled', true);
              $('#select_all_material').prop('checked', false);
            }
          });
        }, 2000);
      } else {
        swal({
          title: "Whoops!",
          text: "Please choose option berofe click change",
          icon: "error",
          button: true
        });
      }
    });
    
    $('#form-sync .upload-file-trigger').on('click', function() {
      $('#form-sync [name=file]').trigger('click');
    });

    $('#form-sync [name=file]').on('change', function() {
      var re = this.value;
      var filename = "";
      if (re) {
        var startIndex = (re.indexOf('\\') >= 0 ? re.lastIndexOf('\\') : re.lastIndexOf('/'));
        var filename = re.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
          filename = filename.substring(1);
        }

        $('#form-sync .file-return').show().text(filename);
      } else {
        $('#syncModal #form-sync .file-return').text('').hide();
      }
    });

    $("#form-sync").submit(function(e){
      swal({
        title: 'WARNING!',
        text: "Don't refresh or reload this page, file is uploading!",
        icon: 'warning',
        timer: 2000,
        buttons: false
      });

      e.preventDefault();
      var formData = new FormData($(this)[0]);

      $('#syncModal .close').hide();
      $('#syncModal .cancel').hide();
      $('#submit_sync').text("Processing...").attr("disabled", true);
      
      setTimeout(function() {
        $.ajax({
          url: '<?= base_url("index.php/api/import/sync_order")?>',
          type: 'POST',
          data: formData,
          async: false,
          cache: false,
          contentType: false,
          enctype: 'multipart/form-data',
          processData: false,
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              hideModal('syncModal');
              $('#syncModal #form-sync [name=file]').val('');
              $('#syncModal #form-sync .file-return').text('').hide();
              swal({
                title: "Success",
                text: "Successfully synchronize order!",
                icon: "success",
                timer: 5000,
                buttons: false
              }).then(
                function() {
                  list_order_table.ajax.reload(null, false);
                }
              );
            } else if (response.status == "failed") {
              swal({
                title: 'Please check your file!',
                text: response.error_message,
                icon: 'error'
              });
            }
          },
          error: function(request, status, error) {
            alert('Something error when synchronizing!');
            console.log(error);
          },
          complete: function() {
            $('#syncModal .close').show();
            $('#syncModal .cancel').show();
            $('#submit_sync').html('<i class="material-icons">sync</i> Sync').attr("disabled", false);
          }
        });
      }, 1000);
    });
  });
</script>