<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  $('#approve').prop("disabled", true);
  $('#decline').prop("disabled", true);

  var isListNewOrderLoaded = false;
  var isListIncomingOrderLoaded = false;

  var user_group = '<?= get_session("work_center_group") ?>';

  if (user_group != 'Shop') {
    $('.content-body').css('padding-top', '0');
    $('.tab-menu').hide();
    $('.card.tab-content').css('border-radius', '8px');
  } else {
    $('.tab-menu').show();
  }

  function get_work_center(table) {
    return $(".filter_by_work_center[data-target='"+table+"']").val() || '';
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_new_order_table = $('#list-new-order').DataTable({
      serverSide: true,
      processing: true,
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: '500px',
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/production_control/ajax_list_new_order"); ?>',
        type: 'POST',
        data: function(d) {
          d.project_status = 'REL';
          d.work_center = get_work_center('list-new-order');
        },
        complete: function(json, type) {
          isListNewOrderLoaded = true;
        }
      },
      columns: [           
        {data: 'check', name: 'check', orderable: false},       
        {data: 'no', name: 'no', orderable: false},     
        {data: 'project', name: 'project'},       
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
        {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
        {data: 'origin', name: 'origin'},
        {data: 'moving', name: 'moving'},
        {data: 'status', name: 'status'},
        {data: 'remarks', name: 'remarks', orderable: false},
        {data: 'part_name', name: 'part_name', orderable: false},
        {data: 'qty', name: 'qty', orderable: false},
        {data: 'location', name: 'location', orderable: false},
      ],
      order: [[ 0, '' ]]
    });

    $('[data-tab="tab-list-new-order"]').click(function() {
      if (!isListNewOrderLoaded) {
        list_new_order_table.ajax.reload(null, false);
        isListNewOrderLoaded = true;
      }
    });

    if (user_group == 'Shop') {
      var list_incoming_order_table = $('#list-incoming-order').DataTable({
        serverSide: true,
        processing: true,
        paging: true,
        // sScrollX: true,
        scrollX: true,
        scrollY: '500px',
        scrollCollapse: true,
        lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
        language: {
          processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
        },
        ajax: {
          url: '<?= base_url("index.php/api/production_control/ajax_list_incoming_order"); ?>',
          type: 'POST',
          data: function(d) {
            d.project_status = 'REL';
            d.work_center = get_work_center('list-incoming-order');
          }
        },
        columns: [           
          {data: 'no', name: 'no', orderable: false},     
          {data: 'project', name: 'project'},       
          {data: 'order', name: 'order'},
          {data: 'type', name: 'type'},
          {data: 'desc', name: 'desc'},
          {data: 'reg', name: 'reg'},
          {data: 'target_date', name: 'target_date'},
          {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
          {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
          {data: 'origin', name: 'origin'},
          {data: 'moving', name: 'moving'},
          {data: 'status', name: 'status'},
          {data: 'remarks', name: 'remarks', orderable: false},
          {data: 'part_name', name: 'part_name', orderable: false},
          {data: 'qty', name: 'qty', orderable: false},
          {data: 'location', name: 'location', orderable: false},
        ],
        order: [[ 0, '' ]]
      });

      $('[data-tab="tab-list-incoming-order"]').click(function() {
        if (!isListIncomingOrderLoaded) {
          list_incoming_order_table.ajax.reload(null, false);  
          isListIncomingOrderLoaded = true;
        }
      });

      $('#incoming-order-search').on('keyup change', function (e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_incoming_order_table.search(this.value).draw();
        }
      });

      $('#tab-list-incoming-order tfoot th').each(function(i) {
        var title = $(this).text();
        var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center Origin', 'Work Center Moving', 'Progress Status', 'Remarks', 'Part Name', 'Qty', 'SAP Status'];
        if (array.includes(title)) {
          $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
        }
      });

      list_incoming_order_table.columns().eq(0).each(function(colId) {
        $('input', list_incoming_order_table.column(colId).footer()).on('keyup change', function(e) {
          if (e.keyCode == 13 || $(this).val() == '') {
            list_incoming_order_table.column(colId).search(this.value).draw();
          }
        });
      });
    }

    $('#list-new-order, #list-incoming-order').on('click', '.order-number', function(event) {
      event.preventDefault();
      var aufnr = $(this).attr('aufnr');
      $('#orderModal .modal-body .ovrly').show();
      
      $('#orderModal .modal-header .title').text('ORDER ' + aufnr);

      $('#orderModal .modal-body iframe').attr('src', 'http://gmftjapps.com/sp/spviewcrm.php?ord=' + aufnr);
    
      $('#orderModal .modal-body iframe').on('load', function () {
        $('#orderModal .modal-body .ovrly').hide();
      });
    });

    // Custome page length
    $('.page-length').on('change', function() {
      if($(this).data('target') == 'list-new-order') {
        list_new_order_table.page.len($(this).val()).draw();
      } else {
        list_incoming_order_table.page.len($(this).val()).draw();
      }
    });

    // Custom filter
    $('.filter_by_work_center').on('change', function(){
      if($(this).data('target') == 'list-new-order') {
        list_new_order_table.ajax.reload(null, false);
      } else {
        list_incoming_order_table.ajax.reload(null, false);
      }
    });

    $('#order-search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_new_order_table.search(this.value).draw();
      }
    });

    $('#tab-list-new-order tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center Origin', 'Work Center Moving', 'Progress Status', 'Remarks', 'Part Name', 'Qty', 'SAP Status'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_new_order_table.columns().eq(0).each(function(colId) {
      $('input', list_new_order_table.column(colId).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_new_order_table.column(colId).search(this.value).draw();
        }
      });
    });

    $('#select_all').click(function(event) {
      if (this.checked) {
        $(':checkbox').each(function() {
          this.checked = true;
          $('#approve').prop("disabled", false);
          $('#decline').prop("disabled", false);
        });
      } else {
        $(':checkbox').each(function() {
          this.checked = false;
          $('#approve').prop("disabled", true);
          $('#decline').prop("disabled", true);
        });
      }
    });

    $('#list-new-order').on('click', '.check', function() {
      if ($(this).is(':checked')) {
        $('#approve').prop("disabled", false);
        $('#decline').prop("disabled", false);
      } else {
        if ($('.check').filter(':checked').length < 1) {
          $('#approve').attr('disabled', true);
          $('#decline').prop("disabled", true);
        }
      }
    });

    $('#approve').on('click', function() {
      var aufnr = [];
      var work_center_origin = [];
      var work_center_moving = [];
      
      $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
        aufnr.push($(this).val());
        work_center_origin.push($(this).attr('work_center_origin'));
        work_center_moving.push($(this).attr('work_center_moving'));
      });

      var text = (aufnr.length > 1) ? 'You will approve ' + aufnr.length + ' order!' : 'You will approve order ' + aufnr[0] + '!';

      swal({
        title: "Are you sure?",
        text: text,
        icon: "warning",
        buttons: [
          'No',
          'Yes'
        ],
      }).then(function(isConfirm) {
        if (isConfirm) {
          var act = $(this).attr('id');

          $.ajax({
            url: '<?= base_url("index.php/api/production_control/approve_order"); ?>',
            type: 'POST',
            data: { 
              aufnr: aufnr,
              work_center_origin: work_center_origin,
              work_center_moving: work_center_moving
            },
            beforeSend: function() {
              $('#approve').prop('disabled', true);
              $('#approve').text('Processing...');
            },
            success: function(response) {
              var response = $.parseJSON(response);
              
              if (response.status == "success") {
                swal({
                  title: "New Order Approved",
                  text: "Successfully approve New Order!",
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function() {
                    list_new_order_table.ajax.reload(null, false);
                    // window.location = "<?= base_url('index.php/production_control/all_order'); ?>";
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error when approve order!');
              console.log(error);
            },
            complete: function() {
              $('#approve').html('<i class="material-icons" style="margin: 0 0.3em 0 -0.1em">check</i>Approve');
              $('#decline').prop('disabled', true);
              $('#select_all').prop('checked', false);
            }
          });
        }
      });
    });

    $('#decline').on('click', function() {
      var aufnr = [];
      
      $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
        aufnr.push($(this).val());
      });

      var text = (aufnr.length > 1) ? 'You will decline ' + aufnr.length + ' order!' : 'You will decline order ' + aufnr[0] + '!';

      swal({
        title: "Are you sure?",
        text: text,
        icon: "warning",
        buttons: [
          'No',
          'Yes'
        ],
        dangerMode: true,
      }).then(function(isConfirm) {
        if (isConfirm) {
          $.ajax({
            url: '<?= base_url("index.php/api/production_control/cancel_move_order"); ?>',
            type: 'POST',
            data: { 
              aufnr: aufnr,
              act: 'decline'
            },
            beforeSend: function() {
              $('#decline').prop('disabled', true);
              $('#decline').text('Processing...');
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == "success") {
                swal({
                  title: "Success",
                  text: response.message,
                  icon: "success",
                  timer: 1000,
                  buttons: false
                });
                list_new_order_table.ajax.reload(null, false);
              }
            },
            error: function(request, status, error) {
              alert('Something error when decline order!');
              console.log(error);
            },
            complete: function() {
              $('#approve').prop('disabled', true);
              $('#decline').html('<i class="material-icons" style="margin: 0 0.3em 0 -0.1em">check</i>Decline');
              $('#select_all').prop('checked', false);
            }
          });
        }
      });
    });
  });
</script>