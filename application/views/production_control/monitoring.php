<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="PT. Sinergi Informatika Semen Indonesia">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<!-- title -->
  <title>SMART TV Production Control - Furnishing & Upholstery Services</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">

	<!-- icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- style -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">

  <!-- fonts -->
	<link rel="stylesheet" href="<?= base_url('assets/fonts/cerebri-sans.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
  <div class="monitoring dark">
    <div class="container">
      <div class="monitoring-board">
        <div class="monitoring-header display-flex space-between align-items-center">
          <a class="back" href="<?= base_url('index.php/'.get_session('homepage_url')); ?>"><i class="material-icons">arrow_back</i>Back to Dashboard</a>
          <h1 class="title">SMART TV DISPLAY PRODUCTION CONTROL</h1>
          <div class="mode"><span>Light</span><input type='checkbox' class='switch'><span>Dark</span></div>
        </div>
        <ul class="list-item owl-carousel">
          <?php foreach($top_project as $item): ?>
          <li class="item revision-<?= trim($item->REVNR); ?>">
            <div class="card project-summary" revision="<?= trim($item->REVNR); ?>">
              <i class="material-icons">arrow_forward</i>
              <div class="heading">
                <h3 class="aircraft-reg"><?= trim($item->TPLNR); ?></h3>
                <span class="description"><?= trim($item->REVTX); ?></span>
                <span class="day-maintenance">Day 0 of 0</span>
              </div>
              <div class="close-summary display-flex align-items-center">
                <div class="close-summary-item">
                  <span class="value jc">0%</span>
                  <div class="order-type">Jobcard</div>
                </div>
                <div class="close-summary-item">
                  <span class="value mdr">0%</span>
                  <div class="order-type">MDR</div>
                </div>
                <div class="close-summary-item">
                  <span class="value pds">0%</span>
                  <div class="order-type">PD Sheet</div>
                </div>
                <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
              </div>
            </div>
            <div class="card detail-summary hilite">
              <div class="heading">
                <h4 class="title">HILITE</h4>
              </div>
              <div class="summary-content">
                <marquee class="hilite-text" direction="up" scrollamount="1.5" onmouseout="this.start();" onmouseover="this.stop();"></marquee>
                <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
              </div>
            </div>
            <div class="card detail-summary material">
              <div class="heading">
                <h4 class="title">MATERIAL STATUS</h4>
              </div>
              <div class="summary-content">
                <ul class="list-material">
                  <li class="list-material-item">
                    <div class="color-identity"></div>
                    <div class="status">Status</div>
                    <div class="value">0</div>
                  </li>
                </ul>
                <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
              </div>
            </div>
            <div class="card detail-summary progress-status">
              <div class="heading">
                <h4 class="title">PROGRESS STATUS</h4>
              </div>
              <div class="summary-content hangar">
                <div class="chart" style="width:100%">
                  <canvas class="progress-status-chart-<?= trim($item->REVNR); ?>" width="100%"></canvas>
                  <ul class="legend-area legend-center small" style="margin-bottom:10px">
                    <li class="item jc"><span>JC</span></li>
                    <li class="item mdr"><span>MDR</span></li>
                    <li class="item pds"><span>PDS</span></li>
                  </ul>
                </div>
                <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
              </div>
            </div>
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
  </div>

  <!-- script -->
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/chart.js/chart.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>
  <script src="https://js.pusher.com/4.3/pusher.min.js"></script>

  <script>
    var i = 0;
    var monitoringMode = localStorage.getItem('monitoringMode') || 'dark';
    $('.monitoring').removeClass(monitoringMode == 'light' ? 'dark' : monitoringMode);
    $('.monitoring').addClass(monitoringMode == 'light' ? monitoringMode : 'dark');

    $('.monitoring').slimScroll({
      height: '100vh',
      color: '#465368'
    });

    function get_percentage(value, total, percentage = true) {
      if(total > 0) {
        if (percentage) {
          return Number(((value / total) * 100).toFixed(2)) + '%';
        } else {
          return Number(((value / total) * 100).toFixed(2));
        }
      } else {
        if (percentage) {
          return '0%';
        } else {
          return 0;
        }
      }
    }

    function getOrderClosePercentageClass(day_maintenance_percentage, task_percentage) {
      var task = task_percentage / 100;

      if (task < 0) {
        return 'na';
      } else if (task == 0 || (day_maintenance_percentage - task) > 0.3) {
        return 'oot';
      } else if (task == 1 || (day_maintenance_percentage - task) <= 0) {
        return 'normal';
      } else if ((day_maintenance_percentage - task) > 0 && (day_maintenance_percentage - task) <= 0.3 ) { 
        return 'warning';
      }
    }

    var topProject = $.parseJSON('<?= json_encode($top_project); ?>').length;
    var items = topProject > 3 ? 3 : topProject;

    // init carousel
    var carousel = $('.owl-carousel').owlCarousel({
      items: items,
      nav: true,
      dots: false,
      margin: 40,
      slideBy: 1,
      loop: true,
      // smartSpeed:1000,
      navSpeed: 1000,
      autoplay: false,
      autoplayTimeout: 10000,
      autoplaySpeed: 1000,
      autoplayHoverPause: false,
    });

    var workCenterGroup = '<?= get_session('work_center_group'); ?>' || '';
    if (workCenterGroup == 'Shop') {
      $('.monitoring-header .title').text('SMART TV DISPLAY PRODUCTION CONTROL SHOP');
    } else {
      $('.monitoring-header .title').text('SMART TV DISPLAY PRODUCTION CONTROL HANGAR');
    }

    $(document).ready(function() {
      // Pusher
      // Pusher.logToConsole = true;

      var pusher = new Pusher('c586033a29a9db975254', {
        cluster: 'ap1',
        forceTLS: true
      });

      // pusher top project
      var channel = pusher.subscribe('CRM-TJ-development');
      channel.bind('update_top_project', function(data) {
        location.reload();
      });

      // pusher hilite
      channel.bind('update_hilite', function(data) {
        var data = $.parseJSON(data);
        $.each(data, function(revision, hilite) {
          setHilite(revision, hilite);
        });
      });

      // pusher material status
      channel.bind('update_material_status', function(data) {
        var data = $.parseJSON(data);      

        $.each(data, function(revision, valRevision) {
          $.each(data[revision], function(item, val) {
            if (val.name == 'Actual Nil Stock') {
              value = val.value.csp + ' <span>(CSP)</span> ' + val.value.rfq + ' <span>(RFQ)</span>';
            } else {
              value = val.value;
            }

            setMaterialStatusSingle(revision, val.name, value);
          });
        });
      });

      var chartStyle = {
        // fontColor: '#4e6082'
        fontColor: '#adb4c3'
      };

      if(monitoringMode == 'dark') {
        $('.mode .switch').prop('checked', true);
        chartStyle.gridLineColor = 'rgba(25, 40, 64, 1)';
      } else {
        $('.mode .switch').prop('checked', false);
        chartStyle.gridLineColor = 'rgba(234, 234, 234, .7)';
      }

      var topProject = $.parseJSON('<?= json_encode($top_project); ?>');
      var progressStatusChart = [];

      var revision_list = [];

      $.each(topProject, function(key, val) {
        var revision = val.REVNR;
        revision_list.push(revision);

        var initValue = [];
        progressStatusChart[revision] = setProgressStatusChart(revision, initValue, initValue, initValue);
      });

      $.ajax({
        url: '<?= base_url("index.php/api/production_control/get_data_top_project"); ?>',
        type: 'POST',
        data: {
          revision: revision_list,
          progress_status: $.parseJSON('<?= json_encode($progress_status_label); ?>')
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;
            $.each(data, function(key, val) {
              var revision = key;
              
              // day of maintenance
              $('.revision-'+revision+' .project-summary .day-maintenance').text('Day ' + val.maintenance.actual + ' of ' + val.maintenance.plan);

              var day = val.maintenance.actual;
              var totalDay = val.maintenance.plan;
              var maintenancePercentage = (get_percentage(day, totalDay, false) / 100);

              if (maintenancePercentage <= 0.5) {
                $('.revision-'+revision+' .project-summary .day-maintenance').addClass('normal');
              } else if (maintenancePercentage > 0.5 && maintenancePercentage <= 0.8) {
                $('.revision-'+revision+' .project-summary .day-maintenance').addClass('warning');
              } else {
                $('.revision-'+revision+' .project-summary .day-maintenance').addClass('oot');
              }

              // order close percentage
              var orderClosePercentage = val.order_close_percentage;
              var jcPercentageValue = orderClosePercentage.GA01 == '-1' ? 'N/A' : orderClosePercentage.GA01 + '%';
              $('.revision-'+revision+' .project-summary .close-summary .value.jc').text(jcPercentageValue);

              var jcClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA01);
              $('.revision-'+revision+' .project-summary .close-summary .value.jc').addClass(jcClass);

              var mdrPercentageValue = orderClosePercentage.GA02 == '-1' ? 'N/A' : orderClosePercentage.GA02 + '%';
              $('.revision-'+revision+' .project-summary .close-summary .value.mdr').text(mdrPercentageValue);

              var mdrClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA02);
              $('.revision-'+revision+' .project-summary .close-summary .value.mdr').addClass(mdrClass);

              var pdsPercentageValue = orderClosePercentage.GA05 == '-1' ? 'N/A' : orderClosePercentage.GA05 + '%';
              $('.revision-'+revision+' .project-summary .close-summary .value.pds').text(pdsPercentageValue);

              var pdsClass = getOrderClosePercentageClass(maintenancePercentage, orderClosePercentage.GA05);
              $('.revision-'+revision+' .project-summary .close-summary .value.pds').addClass(pdsClass);

              $('.revision-'+revision+' .project-summary .close-summary .ovrly').remove();

              // hilite
              setHilite(revision, val.hilite);

              // material status
              var materialStatus = val.material_status;
              var listMaterialStatus = "";
              $.each(materialStatus, function(key, material) {
                var value = material.name == 'Actual Nil Stock' ? material.value.csp + ' <span>(CSP)</span> ' + material.value.rfq + ' <span>(RFQ)</span>' : material.value;
                listMaterialStatus += '<li class="list-material-item" status="'+material.name+'">' +
                  '<div class="color-identity" style="background:' + material.color + '"></div>' +
                  '<div class="status">' + material.name + '</div>' +
                  '<div class="value" style="color:' + material.color + '">' + value + '</div>' +
                '</li>';
              });
              $('.revision-'+revision+' .material .list-material').html(listMaterialStatus);
              $('.revision-'+revision+' .material .ovrly').remove();
              
              var listMaterialStatusLength = $('.revision-'+revision+' .material .list-material-item').length;
              if (materialStatus.length === listMaterialStatusLength) $('.revision-'+revision+' .material .ovrly').remove();

              var jc_status = [];
              $.each(val.progress_status.GA01, function(key, value){
                jc_status.push(value);
              });

              var mdr_status = [];
              $.each(val.progress_status.GA02, function(key, value){
                mdr_status.push(value);
              });

              var pds_status = [];
              $.each(val.progress_status.GA05, function(key, value){
                pds_status.push(value);
              });
              
              // progress status chart
              for (let i = 0; i < progressStatusChart[revision].length; i++) {
                progressStatusChart[revision][i].data.datasets[0].data = jc_status;
                progressStatusChart[revision][i].data.datasets[1].data = mdr_status;
                progressStatusChart[revision][i].data.datasets[2].data = pds_status;
                progressStatusChart[revision][i].update();
              }

              $('.revision-'+revision+' .progress-status .ovrly').remove();
            });

            carousel.trigger('play.owl.autoplay');
          }
        },
        error: function(request, status, error) {
          alert('Something error when loading projects!');
          $('.monitoring .list-item').html('');
          console.log(error);
        }
      });

      runMaterialTransition();

      // switch monitoring mode
      $('.mode .switch').click(function() {
        if ($(this).is(':checked')) {
          $('.monitoring').removeClass('light');
          $('.monitoring').addClass('dark');
          
          // update monitoringMode
          localStorage.setItem('monitoringMode', 'dark');
          
          // update ProgressStatusChartStyle
          $.each(topProject, function(key, val) {
            var revision = val.REVNR;
            updateProgressStatusChartStyle(revision, 'rgba(25, 40, 64, 1)');
          });
        } else {
          $('.monitoring').removeClass('dark');
          $('.monitoring').addClass('light');
          
          // // update monitoringMode
          localStorage.setItem('monitoringMode', 'light');

          // update ProgressStatusChartStyle
          $.each(topProject, function(key, val) {
            var revision = val.REVNR;
            updateProgressStatusChartStyle(revision, 'rgba(234, 234, 234, .7)');
          });
        }
      });    

      // set hilite
      function setHilite(revision, hilite) {
        $('.revision-'+revision+' .hilite .hilite-text').text(hilite);
        $('.revision-'+revision+' .hilite .ovrly').remove();
      }

      // set material status
      function setMaterialStatusSingle(revision, status, value) {
        $('.revision-'+revision+' .material .list-material [status="'+status+'"] .value').html(value);
      }

      // progress status chart template
      function setProgressStatusChart(revision, jc, mdr, pds) {
        var progressStatusChartLabel = [];
        $.each($.parseJSON('<?= json_encode($progress_status_label); ?>'), function(key, val){
          var status = '';
          switch (val) {
            case 'Open': status = 'Open'; break;
            case 'Waiting RO': status = 'W RO'; break;
            case 'Waiting Material': status = 'W Material'; break;
            case 'Progress in Hangar': status = 'Hangar'; break;
            case 'Progress in W101': status = 'W101'; break;
            case 'Progress in W102': status = 'W102'; break;
            case 'Progress in W103': status = 'W103'; break;
            case 'Progress in W401': status = 'W401'; break;
            case 'Progress in W402': status = 'W402'; break;
            case 'Progress in W501': status = 'W501'; break;
            case 'Progress in W502': status = 'W502'; break;
            case 'Sent to Other': status = 'Other'; break;
            case 'Close': status = 'Close'; break;
          }
          progressStatusChartLabel.push(status);
        });
        
        var length = $(".progress-status-chart-" + revision).length;
        var progressStatusChart = [];
        for (let i = 0; i < length; i++) {
          var progressStatusChartTarget = document.getElementsByClassName("progress-status-chart-" + revision)[i];
          var progressStatusChartData = {
            labels: progressStatusChartLabel,
            datasets: [
              {
                label: "Jobcard",
                backgroundColor: "#13ce66",
                data: jc,
              }, 
              {
                label: "MDR",
                backgroundColor: "#ffab00",
                data: mdr,
              },
              {
                label: "PD Sheet",
                backgroundColor: "#0065FF",
                data: pds,
              }
            ]
          }
          progressStatusChart[i] = new Chart(progressStatusChartTarget, {
            type: 'bar',
            data: progressStatusChartData,
            options: {
              // cornerRadius: 4,
              responsive: true,
              maintainAspectRatio: false,
              scales: {
                yAxes: [{
                  ticks: {
                    fontColor: chartStyle.fontColor,
                    fontSize: 10,
                    beginAtZero: true,
                    userCallback: function(label, index, labels) {
                      if (Math.floor(label) === label) return label;
                    },
                  },
                  gridLines: {
                    zeroLineColor: chartStyle.gridLineColor,
                    color: chartStyle.gridLineColor,
                  }
                }],
                xAxes: [{
                  ticks: {
                    fontColor: chartStyle.fontColor,
                    fontSize: 10
                  },
                  barPercentage: 0.6,
                  // categoryPercentage: 0.6,
                  gridLines: {
                    zeroLineColor: chartStyle.gridLineColor,
                    color: chartStyle.gridLineColor,
                  }
                }]
              },
              legend: {
                display: false
              },
            }
          });
        }

        return progressStatusChart;
      }
  
      // update progress status chart style
      function updateProgressStatusChartStyle(revision, style) {
        for (let i = 0; i < progressStatusChart[revision].length; i++) {
          progressStatusChart[revision][i].options.scales.yAxes[0].gridLines.zeroLineColor = style;
          progressStatusChart[revision][i].options.scales.yAxes[0].gridLines.color = style;
          progressStatusChart[revision][i].options.scales.xAxes[0].gridLines.zeroLineColor = style;
          progressStatusChart[revision][i].options.scales.xAxes[0].gridLines.color = style;
          progressStatusChart[revision][i].update();
        }
      }

      // manhours chart template
      function setManhoursChart(revision, plan, actual) {
        var manhoursChartTarget = $("#manhours-chart-" + revision);
        var manhoursChartData = {
          labels: ["Hangar", "W101", "W102", "W103", "W401", "W402", "W501", "W502"],
          datasets: [
            {
              label: "Manhours Plan",
              backgroundColor: "#FFAB00",
              data: plan,
            }, 
            {
              label: "Manhours Actual",
              backgroundColor: "#13ce66",
              data: actual,
            }
          ]
        };
        var manhoursChart = new Chart(manhoursChartTarget, {
          type: 'bar',
          data: manhoursChartData,
          options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
              yAxes: [{
                ticks: {
                  fontColor: manhoursStyle.fontColor,
                  fontSize: 10,
                  beginAtZero: true,
                  userCallback: function(label, index, labels) {
                    if (Math.floor(label) === label) return label;
                  },
                },
                gridLines: {
                  zeroLineColor: manhoursStyle.gridLineColor,
                  color: manhoursStyle.gridLineColor,
                }
              }],
              xAxes: [{
                ticks: {
                  fontColor: manhoursStyle.fontColor,
                  fontSize: 10
                },
                barPercentage: 0.4,
                categoryPercentage: 0.6,
                gridLines: {
                  zeroLineColor: manhoursStyle.gridLineColor,
                  color: manhoursStyle.gridLineColor,
                }
              }]
            },
            legend: {
              display: false
            },
          }
        });

        return manhoursChart;
      }

      // update manhours chart style
      function updateManhoursChartStyle(revision, style) {
        manhoursChart[revision].options.scales.yAxes[0].gridLines.zeroLineColor = style;
        manhoursChart[revision].options.scales.yAxes[0].gridLines.color = style;
        manhoursChart[revision].options.scales.xAxes[0].gridLines.zeroLineColor = style;
        manhoursChart[revision].options.scales.xAxes[0].gridLines.color = style;
        manhoursChart[revision].update();
      }

      // run material status transition
      function runMaterialTransition() {
        var listMaterialContainer = $('.material .list-material');
        var itemHeight = $('.material .list-material').height();

        setInterval(function() {
          listMaterialContainer.stop().animate({scrollTop: itemHeight}, 1000, 'linear', function() {
            var el = $('.list-material-item:nth-child(1), .list-material-item:nth-child(2), .list-material-item:nth-child(3)', this);
            $(this).scrollTop(0).find('.list-material-item:last').after(el);
            // $(this).scrollTop(0).find('.list-material-item:last').after($('.list-material-item:first', this));
          });
        }, 5000);
        
      }  

      $('.project-summary').click(function(){
        var revision = $(this).attr('revision');
        window.open('<?= base_url('index.php/dashboard/detail_project/'); ?>' + revision, '_blank');
      });
      
    });
  </script>
  
  <script>
    $(document).on('keydown', function (e) {
      if (e.keyCode == 13) {
        if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
          if (document.documentElement.requestFullscreen) {
            document.documentElement.requestFullscreen();
          } else if (document.documentElement.msRequestFullscreen) {
            document.documentElement.msRequestFullscreen();
          } else if (document.documentElement.mozRequestFullScreen) {
            document.documentElement.mozRequestFullScreen();
          } else if (document.documentElement.webkitRequestFullscreen) {
            document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
          }
        } else {
          if (document.exitFullscreen) {
            document.exitFullscreen();
          } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
          } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
          } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
          }
        }
      } 
    });
  </script>
</body>
</html>