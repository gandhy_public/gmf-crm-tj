<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var isListCloseOrderLoaded = false;
  var isListLogOrderLoaded = false;

  function get_project_status(table) {
    return $(".filter_by_project_status[data-target='"+table+"']").val();
  }

  function get_work_center(table) {
    return $(".filter_by_work_center[data-target='"+table+"']").val() || '';
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var show = getUrlParameter('show');
    if (show == 'list_order') {
      $('.filter_by_work_center').parents('.option-item').hide();
      $('.filter_by_project_status').parents('.option-item').hide();
    }

    var list_close_order_table = $('#list-close-order').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/production_control/ajax_list_close_order"); ?>',
        type: "POST",
        data: function(d) {
          d.project_status = get_project_status('list-close-order');
          d.work_center = get_work_center('list-close-order');
        },
        complete: function(json, type) { 
          isListCloseOrderLoaded = true; 
        }
      },
      columns: [       
        {data: 'check', name: 'check', orderable: false},    
        {data: 'no', name: 'no', orderable: false},   
        {data: 'project', name: 'project'},         
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
        {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
        {data: 'origin', name: 'origin'},
        {data: 'moving', name: 'moving'},
        {data: 'remarks', name: 'remarks', orderable: false},
        {data: 'part_name', name: 'part_name', orderable: false},
        {data: 'qty', name: 'qty', orderable: false},
        {data: 'sap_status', name: 'sap_status'},
      ],
      order: [[ 0, '' ]]
    });

    var list_log_order_table = $('#list-log-order').DataTable({
      serverSide: true,
      processing: true,
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: '500px',
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/production_control/ajax_list_log_order"); ?>',
        type: 'POST',
        data: function(d) {
          d.project_status = get_project_status('list-log-order');
          d.work_center = get_work_center('list-log-order');
        }
      },
      columns: [           
        {data: 'no', name: 'no', orderable: false},     
        {data: 'project', name: 'project'},       
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'plan_manhours', name: 'plan_manhours', searchable: false},
        {data: 'actual_manhours', name: 'actual_manhours', searchable: false},
        {data: 'origin', name: 'origin'},
        {data: 'moving', name: 'moving'},
        {data: 'status', name: 'status'},
        {data: 'remarks', name: 'remarks', orderable: false},
        {data: 'part_name', name: 'part_name', orderable: false},
        {data: 'qty', name: 'qty', orderable: false},
        {data: 'location', name: 'location', orderable: false},
      ],
      order: [[ 0, '' ]]
    });

    $('[data-tab="tab-list-close-order"]').click(function() {
      if (!isListCloseOrderLoaded) {
        list_close_order_table.ajax.reload(null, false);
        isListCloseOrderLoaded = true;
      }
    });

    $('[data-tab="tab-list-log-order"]').click(function() {
      if (!isListLogOrderLoaded) {
        list_log_order_table.ajax.reload(null, false);  
        isListLogOrderLoaded = true;
      }
    });

    $('#list-close-order, #list-log-order').on('click', '.order-number', function(event) {
      event.preventDefault();
      var aufnr = $(this).attr('aufnr');
      $('#orderModal .modal-body .ovrly').show();
      
      $('#orderModal .modal-header .title').text('ORDER ' + aufnr);

      $('#orderModal .modal-body iframe').attr('src', 'http://gmftjapps.com/sp/spviewcrm.php?ord=' + aufnr);
    
      $('#orderModal .modal-body iframe').on('load', function () {
        $('#orderModal .modal-body .ovrly').hide();
      });
    });

    // Custome page length
    $('.page-length').on('change', function(){
      if($(this).data('target') == 'list-close-order') {
        list_close_order_table.page.len($(this).val()).draw();
      } else {
        list_log_order_table.page.len($(this).val()).draw();
      }
    });

     // Custom filter
     $('.filter_by_project_status, .filter_by_work_center').on('change', function(){
      if($(this).data('target') == 'list-close-order') {
        list_close_order_table.ajax.reload(null, false);
      } else {
        list_log_order_table.ajax.reload(null, false);
      }
    });

    $('#close-order-search, #log-order-search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        if($(this).data('target') == 'list-close-order') {
          list_close_order_table.search(this.value).draw();
        } else {
          list_log_order_table.search(this.value).draw();
        }
      }
    });

    $('#tab-list-close-order tfoot th, #tab-list-log-order tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center Origin', 'Work Center Moving', 'Progress Status', 'Remarks', 'Part Name', 'Qty', 'SAP Status'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_close_order_table.columns().eq(0).each(function(colIdx) {
      $('input', list_close_order_table.column(colIdx).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_close_order_table.column(colIdx).search(this.value).draw();
        }
      });
    });

    list_log_order_table.columns().eq(0).each(function(colId) {
      $('input', list_log_order_table.column(colId).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_log_order_table.column(colId).search(this.value).draw();
        }
      });
    });

    $('#select_all').click(function(event) {
      if (this.checked) {
        var i = 0;
        $('.check.item:not(:disabled)').each(function() {
          this.checked = true;
          i++;
        });
        if (i > 0) {
          $('#update_status').prop("disabled", false);
        }
      } else {
        $(':checkbox').each(function() {
          this.checked = false;
          $('#update_status').prop("disabled", true);
        });
      }
    });

    $('#update_status').prop("disabled", true);
    $('#list-close-order').on('click', '.check', function() {
      if ($(this).is(':checked')) {
        $('#move_area').prop("disabled", false);
        $('#update_status').prop("disabled", false);
      } else {
        if ($('.check').filter(':checked').length < 1) {
          $('#move_area').attr('disabled', true);
          $('#update_status').prop("disabled", true);
        }
      }
    });

    $('#update_status').on('click', function() {
      if($('select[name=update_status]').val() !== '') {
        var aufnr = [];
        var status = $('select[name=update_status]').val();

        $("input:checkbox[name=orders\\[\\]]:checked").each(function(){
          aufnr.push($(this).val());
        });
          
        $.ajax({
          url: '<?= base_url("index.php/api/production_control/update_progress_status"); ?>',
          type: 'POST',
          data: { 
            aufnr: aufnr,
            status: status
          },
          beforeSend: function() {
            $('#update_status').prop('disabled', true);
            $('#update_status').text('Processing...');
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              swal({
                title: "Progress Status Updated",
                text: "Successfully update Progress Status!",
                icon: "success",
                timer: 1000,
                buttons: false
              }).then(
                function() {
                  list_close_order_table.ajax.reload(null, false);
                }
              );
            }
          },
          error: function(request, status, error) {
            alert('Something error when update progress status!');
            console.log(error);
          },
          complete: function() {
            $('#update_status').prop('disabled', false);
            $('#update_status').text('Update');
            $('select[name=update_status]').val('');
            $('#update_status').attr('disabled', true);
            $('#select_all').prop('checked', false);
          }
        });
      } else {
        swal({
          title: "Choose Status",
          text: "Please choose status berofe click update",
          icon: "error",
          button: true
        });
      }
    });
  });
</script>