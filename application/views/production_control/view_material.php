<div class="content-header"></div>
<div class="content-body">
  <div class="card">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">List Material</h3>
      </div>
      <div class="option-box">
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length' data-target='list-part-number'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="toolbar">
        <div class="toolbar-full">
          <input class="toolbar-item full" id="order_search" type="search" placeholder="Fill and enter to search">
          <div class="toolbar-item">
            <div class="single-button export_to_excel excel disabled" data-target="list-part-number"><img class="icon" src="<?= base_url('assets/img/excel.svg'); ?>"/></div>
          </div>
        </div>
      </div>
      <table id="list-part-number" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th rowspan="2">No</th>
            <th rowspan="2">Project</th>
            <th rowspan="2">Order</th>
            <th rowspan="2">Part Number</th>
            <th rowspan="2">Alternate Part Number</th>
            <th rowspan="2">Type</th>
            <th rowspan="2">Material Desc.</th>
            <th rowspan="2">Material Type</th>
            <th rowspan="2">Qty on the task</th>
            <th rowspan="2">Qty on all task</th>
            <th rowspan="2">UoM</th>
            <th rowspan="2">Responsibility</th>
            <th rowspan="2">Material Fulfillment Status</th>
            <th rowspan="2" style="min-width: 150px;">Remarks</th>
            <th class="small">Actual Nil Stock</th>
            <th class="small" colspan="2">Ordered By Purchasing</th>
            <th class="small" colspan="3">Shipment/Custom Process</th>
            <th class="small">Provision in Store</th>
            <th class="small">Preloaded in Hangar/Shop Store</th>
            <th class="small" colspan="3">Delivered to Production</th>
          </tr>
          <tr>
            <th class="small">CSP/RFQ</th>
            <th class="small">PO Number</th>
            <th class="small">Lead Time</th>
            <th class="small">AWB</th>
            <th class="small">INB</th>
            <th class="small">SP</th>
            <th class="small">STO</th>
            <th class="small">Storage Location</th>
            <th class="small">Date Delivered</th>
            <th class="small">Qty Delivered</th>
            <th class="small">Receiver Name</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Part Number</th>
            <th>Alternate Part Number</th>
            <th>Type</th>
            <th>Material Desc.</th>
            <th>Material Type</th>
            <th>Qty on the task</th>
            <th>Qty on all task</th>
            <th>UoM</th>
            <th>Responsibility</th>
            <th>Material Fulfillment Status</th>
            <th>Remarks</th>
            <th>CSP/RFQ</th>
            <th>PO Number</th>
            <th>Lead Time</th>
            <th>AWB</th>
            <th>INB</th>
            <th>SP</th>
            <th>STO</th>
            <th>Storage Location</th>
            <th>Date Delivered</th>
            <th>Qty Delivered</th>
            <th>Receiver Name</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  <form id="export_excel_form" action="<?= base_url('index.php/api/export/excel'); ?>" method="POST" style="display:none;">
    <input type="hidden" name="page">
    <textarea type="hidden" name="query"></textarea>
  </form>
</div>

<?php require_once('order_modal.php'); ?>
<?php require_once('sync_modal.php'); ?>