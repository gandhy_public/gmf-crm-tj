<div class="content-header"></div>
<div class="content-body" style="padding-top:40px;">
  <ul class="tab-menu">
    <li class="tab-link active" data-tab="tab-list-new-order">Waiting Approval</li>
    <li class="tab-link" data-tab="tab-list-incoming-order">Incoming Order</li>
  </ul>
  <div class="card tab-content active" id="tab-list-new-order" style="border-radius: 0px 8px 8px 8px;">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">Waiting Approval <span class="new">NEW ORDER</span></h3>
      </div>
      <div class="option-box">
        <?php
        $work_center = get_session('work_center');
        if (empty($work_center) || count($work_center) > 1):?>
        <div class="option-item">
          <span>Work Center</span>
          <select class='filter_by_work_center' data-target='list-new-order'>
            <option value="">All</option>
            <?php foreach ($this->work_center as $item): 
              if (!empty($work_center)) {
                if (in_array($item->WORK_CENTER, $work_center)) {
                  echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                }
              } else {
                echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
              }
            endforeach; ?>
          </select>
        </div>
        <?php endif; ?>
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length' data-target='list-new-order'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="toolbar">
        <?php 
        $class = 'full';
        if (is_has_feature('move_order')):
        ?>
          <div class="toolbar-left">
            <div class="toolbar-item">
              <button class="button is-success" type="button" id="approve" disabled><i class="material-icons" style="margin: 0 0.3em 0 -0.2em">check</i>Approve</button>
            </div>
            <div class="toolbar-item" style="margin-left: 10px">
              <button class="button is-danger" type="button" id="decline" disabled><i class="material-icons" style="margin: 0 0.3em 0 -0.2em">close</i>Decline</button>
            </div>
          </div>
        <?php 
        $class = 'right';
        endif; 
        ?>
        <div class="toolbar-<?= $class; ?>">
          <input class="toolbar-item full" id="order-search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="list-new-order" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>
              <div class="checkbox">
                <label><input id="select_all" class="check" type="checkbox"><span></span></label>
              </div>
            </th>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>#</th>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  <div class="card tab-content" id="tab-list-incoming-order">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">Incoming Order</h3>
      </div>
      <div class="option-box">
        <?php
        $work_center = get_session('work_center');
        if (empty($work_center) || count($work_center) > 1):?>
        <div class="option-item">
          <span>Work Center</span>
          <select class='filter_by_work_center' data-target='list-incoming-order'>
            <option value="">All</option>
            <?php foreach ($this->work_center as $item): 
              if (!empty($work_center)) {
                if (in_array($item->WORK_CENTER, $work_center)) {
                  echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
                }
              } else {
                echo "<option value='$item->WORK_CENTER'>$item->WORK_CENTER</option>";
              }
            endforeach; ?>
          </select>
        </div>
        <?php endif; ?>
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length' data-target='list-incoming-order'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="toolbar">
        <div class="toolbar-full">
          <input class="toolbar-item full" id="incoming-order-search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="list-incoming-order" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Work Center Origin</th>
            <th>Work Center Moving</th>
            <th>Progress Status</th>
            <th>Remarks</th>
            <th>Part Name</th>
            <th>Qty</th>
            <th>Location</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<?php require_once('order_modal.php'); ?>