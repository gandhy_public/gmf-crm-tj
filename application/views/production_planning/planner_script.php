<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/select2/select2.full.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/visjs/vis.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script type="text/javascript">
  function get_project_status() {
    return $(".filter_by_project_status").val();
  }

  function get_location() {
    return $(".filter_by_location").val() || '';
  }

  $(document).ready(function() {
    var planningForm = $('#planningForm');
    var planningFormOrigin = '';
    var planningFormChanged = false;

    loadProjectPlanning();

    if ("<?= is_has_feature('prod_planning') ?>") {
      var columns = [
        {data: 'no', name: 'no', orderable: false},            
        {data: 'project', name: 'project'},
        {data: 'reg', name: 'reg'},
        {data: 'hangar', name: 'hangar'},
        {data: 'maint', name: 'maint'},
        {data: 'status', name: 'status', orderable: false},
        {data: 'planning', name: 'planning', orderable: false},
        {data: 'hilite', name: 'hilite', orderable: false},
        {data: 'start', name: 'start'},
        {data: 'finish', name: 'finish'},
      ];

      if("<?= is_has_feature('prod_planning_act') ?>") {
        columns.splice(2, 0, 
          {data: 'top_project', name: 'top_project', orderable: false},
          {data: 'update_date', name: 'update_date'}
        );
      }   
    
      var list_project_table = $('#list-projects').DataTable({ 
        processing: true, //Feature control the processing indicator.
        serverSide: true, //Feature control DataTables' server-side processing mode.
        responsive: true,
        paging: true,
        scrollX: true,
        scrollY: "500px",
        scrollCollapse: true,
        lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
        language: {
          processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
        },
        ajax: {
          url: '<?= base_url("index.php/api/production_planning/ajax_list_projects"); ?>',
          type: "POST",
          data: function(d) {
            d.project_status = get_project_status();
            d.location = get_location();
          },
          complete: function(json, type) {
            $('.single-button.upload-project').removeClass('disabled');
          }
        },
        columns: columns,
        order: [[ 0, '' ]],
        drawCallback: function() {
          display_tooltip('i.sync', 'Synchronize Project');
          display_tooltip('i.view-plan', 'View Planning');
          display_tooltip('i.set-plan', 'Set Planning');
          display_tooltip('i.hilite', 'Hilite');
          display_tooltip('span.status-planning', 'View Log Planning');
        }
      });

      $('.toggle-sidebar').on('click', function(){
        list_project_table.columns.adjust().draw();
      });

      // Custome page length
      $('.page-length').on('change', function(){
        list_project_table.page.len($(this).val()).draw();
      });

      // Custom filter
      $('.filter_by_project_status, .filter_by_location').on('change', function(){
        list_project_table.ajax.reload(null, false);
      });

      $('.filter_by_location').on('change', function() {
        if($(this).val() != '') {
          $('.title-area .all').text($(this).val());
        }else{
          $('.title-area .all').text('ALL LOCATION');
        }
      });

      $('#tab-list-project tfoot th').each(function(i) {
        var title = $(this).text();
        var array = ['Project', 'A/C REG', 'Hangar', 'Maint Type', 'Status'];
        if (array.includes(title)) {
          $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
        }
      });

      // Apply the search
      list_project_table.columns().eq(0).each(function(colIdx) {
        $('input', list_project_table.column(colIdx).footer()).on('keyup change', function(e) {
          if (e.keyCode == 13 || $(this).val() == '') {
            list_project_table.column(colIdx).search(this.value).draw();
          }
        });
      });

      $('#project_search').on('keyup change', function (e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_project_table.search(this.value).draw();
        }
      });

      // Unset Top Project
      $('#list-projects').on('click', '.switch', function() {
        var revision = $(this).attr('revnr');
        var url = '<?= base_url("index.php/api/production_planning/toggle_top_project"); ?>';
        
        $('.switch').each(function() {
          $(this).prop("disabled", true);
        });

        var status = 0;
        var message = "";
        if($(this).is(':checked')) {
          status = 1;
          message = 'Successfully set revision ' + revision + ' as Top Project!';
        } else {
          status = 0;
          message = 'Successfully unset revision ' + revision + ' as Top Project!';
        }

        $.ajax({
          url: url,
          type: 'POST',
          data: {
            revision: revision,
            status: status,
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              display_toast('success', message);
              list_project_table.ajax.reload(null, false);
              $('.switch').each(function() {
                $(this).prop("disabled", false);
              });
              $('#project-' + revision).prop("checked", true);
              $('#project-' + revision).prop("disabled", true);
            }
          },
          error: function(request, status, error) {
            alert('Something error when processing Top Project!');
            console.log(error);
          }
        });
      });

      $('.modal-box').on('click', '.cancel, .close', function(event) {
        event.preventDefault();
        var modal = $(this).attr('modal-target');
        
        if (planningFormChanged && modal == 'planningModal') {
          swal({
            title: "You have unsaved changes!",
            text: "Do you want to continue without saving these changes?",
            icon: "warning",
            buttons: [
              'Cancel',
              'Yes!'
            ],
            dangerMode: true,
          }).then(function(isConfirm) {
            if (isConfirm) {
              hideModal(modal);
              clearFormPlanning();
              clearFormHilite();
              clearLogPlanning();
            }
          });
        } else {
          hideModal(modal);
          clearFormPlanning();
          clearFormHilite();
          clearLogPlanning();
        }
      });

      $('#list-projects').on('click', '.sync', function() {
        var revision = $(this).attr('revnr');
        
        swal({
          title: "Are you sure?",
          text: "Project " + revision + " will be synchronized",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, synchronize!'
          ]
        }).then(function(isConfirm) {
          if (isConfirm) {
            // $('#list-projects .sync').addClass("disabled");
            $.ajax({
              url: '<?= base_url("index.php/api/Sapi/create_job_by_revision"); ?>',
              type: "POST",
              data: {
                revision: revision
              },
              success: function(response) {
                var response = $.parseJSON(response);
                
                if (response.status == 'success') {
                  display_toast('info', 'Synchronizing...');
                } else {
                  display_toast('error', response.error_message);
                  setTimeout(() => {
                    display_toast('error', 'Please wait until the others sync process is done');
                  }, 1000);
                }
              },
              error: function(request, status, error) {
                alert('Something error when syncronizing!');
                console.log(error);
              }
            });
          }
        });
      });

      // Initial Planning
      $('#list-projects').on('click', '.set-plan, .view-plan', function(event) {
        event.preventDefault();
        var act = $(this).attr('act');
        var revision = $(this).attr('revnr');

        $('#planningModal [name=revnr]').val(revision);
        
        // info
        $('#planningModal .planning-info .revision .value').text(revision);

        if(act == 'create') {
          $('#planningModal .planning-title').text('Create Project Planning');
          $('#planningModal #planningForm').attr('act', 'create');
          $('#planningModal #submit_planning_button').html('<i class="material-icons">check</i> Create').show();

          $('#planningForm .ovrly').hide();

          select2PlanningInit();

          addRowPlanning();
        } else {
          $('#planningModal .planning-title').text('View & Edit Project Planning');
          $('#planningModal #planningForm').attr('act', 'edit');
          $('#planningModal #submit_planning_button').html('<i class="material-icons">check</i> Update');

          $('#planningForm .ovrly').show();

          var status = $(this).attr('status');

          if (status == 'NEED REVIEW' || '<?= !is_has_feature("update_planning") ?>') {
            $('#planningModal .add-row').addClass('disabled');
            $('#submit_planning_button').hide();

            if ('<?= get_session("user_role"); ?>' == 'Controller Hangar') {
              $('#confirm_planning_button').show();  
            }
          } else {
            $('#planningModal .add-row').removeClass('disabled');
            $('#submit_planning_button').show().prop('disabled', true);
            $('#confirm_planning_button').hide();
          }
          
          select2PlanningInit();

          $.ajax({
            url: '<?= base_url("index.php/api/production_planning/initial_planning"); ?>',
            type: 'POST',
            data: {
              revision: revision,
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == "success") {
                var data = response.data;
                var project_planning_info = response.data.project_planning_info;
                var project_planning = response.data.project_planning;

                var html = '';

                $('#planningModal .planning_item').remove();
                if(project_planning !== null) {
                  $('#planningModal .planning-info .created_by .value').text(project_planning_info.CREATED_BY || '-');
                  $('#planningModal .planning-info .reviewed_by .value').text(project_planning_info.REVIEWED_BY || '-');
                  $('#planningModal .planning-info .edited_by .value').text(project_planning_info.EDITED_BY || '-');

                  $('.select2_area').select2("destroy");
                  $('.select2_phase').select2("destroy");

                  var rowCount = project_planning.length;

                  $.each(project_planning, function(key, item, i = 0) {
                    var start_date = '', end_date = '';

                    if(item.START_DATE && (item.START_DATE !== '0000-00-00' && item.START_DATE !== '1900-01-01')) {
                      start_date = item.START_DATE;
                    } else {
                      start_date = '';
                    }
                    
                    if(item.END_DATE && (item.END_DATE !== '0000-00-00' && item.END_DATE !== '1900-01-01')) {
                      end_date = item.END_DATE;
                    } else {
                      end_date = '';
                    }
                    
                    var id = item.ID_PROJECT_PLANNING;

                    // clone form
                    var template = $('#planningModal .planning_item_template').clone(true);

                    template.insertBefore("#planningModal .planning_item_template");
                    template.removeClass("planning_item_template").addClass("planning_item");

                    var disabled = (status == 'NEED REVIEW' || '<?= !is_has_feature("update_planning") ?>') ? true : false;

                    // set name in form
                    template.find('.select2_area').attr({name: 'area[]', id_project_planning: id}).val(item.AREA).prop('disabled', disabled);
                    template.find('.select2_phase').attr({name: 'phase[]'}).val(item.PHASE).prop('disabled', disabled);
                    template.find('.start_date').attr({name: 'start_date[]', 'data-toggle': 'datepicker'}).val(start_date).prop('disabled', disabled);
                    template.find('.end_date').attr({name: 'end_date[]', 'data-toggle': 'datepicker'}).val(end_date).prop('disabled', disabled);
                    template.find('.remarks').attr({name: 'remarks[]'}).val(item.REMARKS).prop('disabled', disabled);

                    i++;
                    if (i === rowCount) {
                      planningFormOrigin = $('#planningForm').serialize();
                    }
                  });

                  select2PlanningInit();
                  
                  // disabled remove
                  var len = $('#planningModal .planning_item').length;
                  if (len == 1 || status == 'NEED REVIEW') {
                    $('#planningModal .remove-row').addClass('disabled');
                  } else {
                    $('#planningModal .remove-row').removeClass('disabled');
                  }

                  $('[data-toggle="datepicker"]').datepicker({
                    autoHide: true,
                    format: 'yyyy-mm-dd',
                  });
                }
              }
            },
            error: function(request, status, error) {
              alert('Something error when getting planning data!');
              console.log(error);
            },
            complete: function() {
              $('#planningForm .ovrly').hide();
            }
          });
        }
      });

      // Add Plannning
      $('#planningModal .add-row').click(function() {
        if(!$(this).hasClass('disabled')) {
          addRowPlanning();
        }
      });

      // Remove Planning
      $('#planningModal .remove-row:not(.disabled)').click(function() {
        if(!$(this).hasClass('disabled')) {
          // get planning item 
          var index = $('.planning_item').index($(this).parents('.planning_item'));
          var planning_item = $('.planning_item').eq(index);
          
          // set removed id project planning exist to var
          var deleted_planning = $('#planningModal [name=deleted_planning]').val();
          var attr = planning_item.find('[name="area\\[\\]"]').attr('id_project_planning');
          if(typeof attr !== typeof undefined && attr !== false) {
            deleted_planning += attr;
            $('#planningModal [name=deleted_planning]').val(deleted_planning + '-');
          }

          // remove planning item
          planning_item.remove();
          
          // disable remove button if planning item only 1
          var len = $('#planningModal .planning_item').length;
          if (len == 1) {
            $('#planningModal .remove-row').addClass('disabled');
          } else {
            $('#planningModal .remove-row').removeClass('disabled');
          }

          if ($('#planningForm').attr('act') == 'edit') {
            planningFormChanged = planningForm.serialize() !== planningFormOrigin;
            $('#submit_planning_button').prop('disabled', !planningFormChanged);
          }
        }
      });

      $('#planningForm select, #planningForm input').on('change input', function() {
        if ($('#planningForm').attr('act') == 'edit') {
          planningFormChanged = planningForm.serialize() !== planningFormOrigin;
          $('#submit_planning_button').prop('disabled', !planningFormChanged);
        }
      });

      function addRowPlanning() {
        // destroy select2
        $('.select2_area').select2("destroy");
        $('.select2_phase').select2("destroy");

        // destroy datepicker
        $('[data-toggle="datepicker"]').datepicker('destroy');

        // clone form
        var len = $('#planningModal .planning_item').length;
        var template = $('#planningModal .planning_item_template').clone(true);

        template.insertBefore("#planningModal .planning_item_template");
        template.removeClass("planning_item_template").addClass("planning_item");

        // set name in form
        template.find('.select2_area').attr('name', 'area[]');
        template.find('.select2_phase').attr('name', 'phase[]');
        template.find('.start_date').attr('name', 'start_date[]');
        template.find('.start_date').attr('data-toggle', 'datepicker');
        template.find('.end_date').attr('name', 'end_date[]');
        template.find('.end_date').attr('data-toggle', 'datepicker');
        template.find('.remarks').attr('name', 'remarks[]');

        // set select2 and datepicker
        select2PlanningInit();

        // disabled remove
        if (len == 0) {
          template.find('.remove-row').addClass('disabled');
        } else {
          $('#planningModal .remove-row').removeClass('disabled');
        }

        if ($('#planningForm').attr('act') == 'edit') {
          planningFormChanged = planningForm.serialize() !== planningFormOrigin;
          $('#submit_planning_button').prop('disabled', !planningFormChanged);
        }

        display_tooltip('#planningModal i.add-row:not(.disabled)', 'Add new row');
        display_tooltip('#planningModal i.remove-row:not(.disabled)', 'Remove this row');

        $('[data-toggle="datepicker"]').datepicker({
          autoHide: true,
          format: 'yyyy-mm-dd',
        });
      }

      function clearFormPlanning(modal) {
        $('#planningModal .planning_item').remove();
        $('#planningModal [name=revnr]').val('');
        $('#planningModal [name=deleted_planning]').val('');

        $('#planningModal .planning-info .revision .value').text('-');
        $('#planningModal .planning-info .created_by .value').text('-');
        $('#planningModal .planning-info .reviewed_by .value').text('-');
        $('#planningModal .planning-info .edited_by .value').text('-');
      }

      function clearFormHilite() {
        $('#hiliteForm [name=revnr]').val('');
        $('#hiliteForm [name=hilite]').val('');
      }
      
      function clearLogPlanning() {
        $('#logModal .log-planning-list').html('');
      }

      function select2PlanningInit() {
        $('#planningModal .select2_area').select2({
          placeholder: 'Choose Area',
          dropdownAutoWidth : true,
          width: '100%',
          minimumResultsForSearch: -1
        });

        $('#planningModal .select2_phase').select2({
          placeholder: 'Choose Phase',
          dropdownAutoWidth : true,
          width: '100%',
          minimumResultsForSearch: -1
        });
      }

      // Submit Planning
      $('#planningForm').submit(function(event) {
        event.preventDefault();
        var modal = $(this).attr('modal-target');
        var url = $(this).attr('action');
        var act = $(this).attr('act');

        if (act == 'create' || (act == 'edit' && planningFormChanged )) {
          var deleted_planning = $('#planningModal [name=deleted_planning]').val();
          if(deleted_planning !== '') {
            deleted_planning = deleted_planning.slice(0, -1).split('-');
          } else {
            deleted_planning = [];
          }
          
          var data = {
            revision: $(this).find('[name=revnr]').val(),
            id_project_planning: $(this).find('[name="area\\[\\]"]').map(function(){
              var attr = $(this).attr('id_project_planning');
              return (typeof attr !== typeof undefined && attr !== false) ? attr : 0;
            }).get(),
            act: act,
            area: $(this).find('[name="area\\[\\]"]').map(function(){return $(this).val();}).get(),
            phase: $(this).find('[name="phase\\[\\]"]').map(function(){return $(this).val();}).get(),
            start_date: $(this).find('[name="start_date\\[\\]"]').map(function(){return $(this).val();}).get(),
            end_date: $(this).find('[name="end_date\\[\\]"]').map(function(){return $(this).val();}).get(),
            remarks: $(this).find('[name="remarks\\[\\]"]').map(function(){return $(this).val();}).get(),
            deleted_planning: deleted_planning
          };

          $.ajax({  
            url: '<?= base_url("index.php/api/production_planning/save_planning"); ?>',
            type: 'POST',
            data: data,
            beforeSend: function() {
              $('#planningForm .remove-row').addClass('disabled');
              $('#planningForm .cancel').hide();
              $('#submit_planning_button').prop('disabled', true);
              $('#submit_planning_button').text('Processing...');
            },
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == 'success') {
                var text = act == 'create' ? 'Success Create Project Planning' : 'Success Update Project Planning';
                swal({
                  title: text,
                  text: text + " Revision " + data.revision,
                  icon: "success",
                  timer: 1000,
                  buttons: false
                }).then(
                  function(){
                    hideModal(modal);
                    list_project_table.ajax.reload(null, false);
                    $('#planningModal .planning_item').remove();
                    planningFormChanged = false;
                  }
                );
              }
            },
            error: function(request, status, error) {
              alert('Something error when submit project planning!');
              console.log(error);
            },
            complete: function() {
              $('#planningForm .cancel').show();
              $('#submit_planning_button').prop('disabled', false);
              $('#submit_planning_button').html('<i class="material-icons">check</i> Submit');
              clearFormPlanning();
            }
          });  
        } else {
          $('#planningModal .planning_item').remove();
          clearFormPlanning();
        }
      });

      // Confirm Planning
      $('#planningForm #confirm_planning_button').click(function(event) {
        event.preventDefault();

        var revision = $('#planningForm [name=revnr]').val();
        
        $.ajax({  
          url: '<?= base_url("index.php/api/production_planning/confirm_planning"); ?>',
          type: 'POST',
          data: {
            revision: revision
          },
          beforeSend: function() {
            $('#planningForm .cancel').hide();
            $('#confirm_planning_button').prop('disabled', true);
            $('#confirm_planning_button').text('Processing...');
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == 'success') {
              swal({
                title: 'Success Confirm Project Planning',
                text: 'Success Confirm Project Planning Revision ' + revision,
                icon: "success",
                timer: 1000,
                buttons: false
              }).then(
                function(){
                  var modal = $('#planningForm').attr('modal-target');
                  hideModal(modal);
                  list_project_table.ajax.reload(null, false);
                  $('#planningModal .planning_item').remove();
                }
              );
            }
          },
          error: function(request, status, error) {
            alert('Something error when submit project planning!');
            console.log(error);
          },
          complete: function() {
            $('#planningForm .cancel').show();
            $('#confirm_planning_button').prop('disabled', false);
            $('#confirm_planning_button').html('<i class="material-icons">check</i> Save');
          }
        });
      });

      // Initial Hilite
      $('#list-projects').on('click', '.hilite', function(event) {
        event.preventDefault();
        var revision = $(this).attr('revnr');
        $('#hiliteForm .subtitle.revnr').text('Revision: '+revision);
        $('#hiliteForm [name=revnr]').val(revision);

        $.ajax({
          url: '<?= base_url("index.php/api/production_planning/get_hilite"); ?>',
          type: 'POST',
          data: {
            revision: revision,
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              $('#hiliteForm [name=hilite]').val(response.data);
            }
          },
          error: function(request, status, error) {
            alert('Something error when getting hilite!');
            console.log(error);
          },
        });
      });

      // Submit Hilite
      $('#hiliteForm').submit(function(event) {
        event.preventDefault();
        var modal = $(this).attr('modal-target');
        var url = $(this).attr('action');

        var revision = $(this).find('[name=revnr]').val();
        
        $.ajax({
          url: '<?= base_url("index.php/api/production_planning/save_hilite"); ?>',
          type: 'POST',
          data: {
            revision: revision,
            hilite: $(this).find('[name=hilite]').val()
          },
          beforeSend: function() {
            $('#hiliteForm .cancel').hide();
            $('#hiliteForm #submit_hilite').prop('disabled', true).text('Processing...');
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == 'success') {
              swal({
                title: 'Success',
                text: "Success Save Hilite Revision Number " + revision,
                icon: "success",
                timer: 1000,
                buttons: false
              }).then(
                function(){
                  hideModal(modal);
                  $('#hiliteForm [name=revnr]').val('');
                  $('#hiliteForm [name=hilite]').val('');
                }
              );
            }
          },
          error: function(request, status, error) {
            alert('Something error when submit hilite!');
            console.log(error);
          },
          complete: function() {
            $('#hiliteForm .cancel').show();
            $('#hiliteForm #submit_hilite').prop('disabled', false).html('<i class="material-icons">check</i> Save');
          }
        });
      });

      $('#list-projects').on('click', '.status-planning', function(event) {
        event.preventDefault();
        var revision = $(this).attr('revnr');
        var status = $(this).attr('status');
        $('#logModal .planning-info .revision .value').text(revision);
        $('#logModal .planning-info .status-plan .value').text(status);

        $.ajax({
          url: '<?= base_url("index.php/api/production_planning/get_log_planning"); ?>',
          type: 'POST',
          data: {
            revision: revision,
          },
          beforeSend: function() {
            $('#logModal .ovrly').show();
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == 'success') {
              var data = response.data;

              $.each(data, function(index, val) {
                var action = val.ACTION.toLowerCase();
                var date = val.INSERT_DATE.substring(0, val.INSERT_DATE.length - 4);

                var icon = (action == 'confirm') ? 'assignment_turned_in' : 'create';

                var list = '<li class="planning-list-block">';
                list += '<div class="marker '+action+'-icon"><i class="material-icons">'+icon+'</i></div>';
                list += '<div class="planning-list-content"><h4><span>'+val.USER_NAME+'</span> '+action+' planning</h4><span class="date">'+date+'</span></div>';
                list += '</li>';
                $('#logModal .log-planning-list').append(list);   
              });
            }
          },
          error: function(request, status, error) {
            alert('Something error when getting log planning!');
            console.log(error);
          },
          complete: function() {
            $('#logModal .ovrly').hide();
          },
        });
      });

      $('#form-sync .upload-file-trigger').on('click', function() {
        $('#form-sync [name=file]').trigger('click');
      });

      $('#form-sync [name=file]').on('change', function() {
        var re = this.value;
        var filename = "";
        if (re) {
          var startIndex = (re.indexOf('\\') >= 0 ? re.lastIndexOf('\\') : re.lastIndexOf('/'));
          var filename = re.substring(startIndex);
          if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            filename = filename.substring(1);
          }

          $('#form-sync .file-return').show().text(filename);
        } else {
          $('#syncModal #form-sync .file-return').text('').hide();
        }
      });

      $("#form-sync").submit(function(e){
        swal({
          title: 'WARNING!',
          text: "Don't refresh or reload this page, file is uploading!",
          icon: 'warning',
          timer: 2000,
          buttons: false
        });

        e.preventDefault();
        var formData = new FormData($(this)[0]);

        $('#syncModal .close').hide();
        $('#syncModal .cancel').hide();
        $('#submit_sync').text("Processing...").attr("disabled", true);
        
        setTimeout(function() {
          $.ajax({
            url: '<?= base_url("index.php/api/import/sync_production_planning")?>',
            type: 'POST',
            data: formData,
            async: false,
            cache: false,
            contentType: false,
            enctype: 'multipart/form-data',
            processData: false,
            success: function(response) {
              var response = $.parseJSON(response);

              if (response.status == 'success') {
                hideModal('syncModal');
                $('#syncModal #form-sync [name=file]').val('');
                $('#syncModal #form-sync .file-return').text('').hide();
                swal({
                  title: "Success",
                  text: "Successfully upload Project Planning & Hilite!",
                  icon: "success",
                  timer: 2000,
                  buttons: false
                }).then(
                  function() {
                    list_project_table.ajax.reload(null, false);
                  }
                );
              } else if (response.status == "failed") {
                swal({
                  title: 'Please check your file!',
                  text: response.error_message,
                  icon: 'error'
                });
              }
            },
            error: function(request, status, error) {
              alert('Something error when import data from excel!');
              console.log(error);
            },
            complete: function() {
              $('#syncModal .close').show();
              $('#syncModal .cancel').show();
              $('#submit_sync').text('Upload').attr("disabled", false);
            }
          });
        }, 1000);
      });
    }

    // Project Timeline

    var timelineTarget = document.getElementById('timeline');
    var timeline, items, groups;

    $('.filter_planning_by_timeframe').change(function(){
      var scale = $(this).val();
      changeViewScaleTimeline(timeline, scale);
    });

    $('.filter_planning_by_area').on('change', function() {
      loadProjectPlanning();
    });

    $('.filter_planning_by_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') loadProjectPlanning();
    });

    function loadProjectPlanning() {
      var timeframe = $('.filter_planning_by_timeframe').val();
      var area = $('.filter_planning_by_area').val() || '';
      var search = $('.filter_planning_by_search').val();
      
      $.ajax({
        url: '<?= base_url("index.php/api/dashboard/get_project_planning_detail_all"); ?>',
        type: 'POST',
        data: {
          search: search,
          area: area,
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;

            $('.project-planning .ovrly').remove();
          
            var options = {
              width: "100%",
              maxHeight: "600px",
              horizontalScroll: true, 
              verticalScroll: true, 
              zoomable: false,
            };

            if (data.items.length == 0) {
              options.start = getCurrentDate();
            }

            $('.vis-timeline').css('visibility', 'visible !important');

            if(timeline !== undefined) {
              items = new vis.DataSet(data.items);
              groups = new vis.DataSet(data.groups);

              // destroy if action 'redraw'
              timeline.destroy();

              timeline = new vis.Timeline(timelineTarget, items, groups, options);
              changeViewScaleTimeline(timeline, timeframe);
            } else {
              items = new vis.DataSet(data.items);
              groups = new vis.DataSet(data.groups);

              timeline = new vis.Timeline(timelineTarget, items, groups, options);
              changeViewScaleTimeline(timeline, timeframe);
            }
          }
        },
        error: function(request, status, error) {
          alert('Something error when getting project planning data!');
          console.log(error);
        }
      });
    }
  });
</script>