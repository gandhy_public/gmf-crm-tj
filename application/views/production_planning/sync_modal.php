<div class="modal-box" id="syncModal">
  <div class="modal sm">
    <span class="close" modal-target="syncModal"></span>
    <div class="modal-header">
      <h2 class="title">Project Planning & Hilite</h2>
    </div>
    <form id="form-sync" class="form-sync" enctype="multipart/form-data">
      <div class="modal-body">
        <input type="file" name="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" required>
        <div class="upload-area">
          <div class="upload-file-trigger">
            <i class="material-icons">backup</i>
            <p>Click to choose a file</p>
            <p class="file-return"></p>
          </div>
          <div class="bottom-content">
            <p>Please download <a href="<?= base_url("public/TEMPLATE_PRODUCTION_PLANNING.xlsx");?>" style="font-size: 12px;font-weight: 500;">format file</a></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button class="button is-primary is-default cancel" modal-target="syncModal">Back</button>
        <button id="submit_sync" class="button is-primary" type="submit">Upload</button>
      </div>
    </form>
  </div>
</div>