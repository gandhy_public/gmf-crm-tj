<div class="modal-box" id="hiliteModal">
  <div class="modal sm">
    <span class="close" modal-target="hiliteModal"></span>
    <form id="hiliteForm" modal-target="hiliteModal" method="POST">
      <div class="modal-header">
        <h2 class="title">Hilite</h2>
        <span class="subtitle revnr v2">Revision: </span>
      </div>
      <div class="modal-body">
        <input type="hidden" name="revnr" value=""/>
        <textarea name="hilite" rows="10" style="width:100%;font-size:14px;line-height:22px"></textarea>
        <!-- <div class="char-left" style="margin-top:5px;display:none;color: inherit;font-size: 13px;opacity: .7;"><span>255</span> characters left</div> -->
      </div>
      <div class="modal-footer">
        <button class="button is-primary is-default cancel" modal-target="hiliteModal">Back</button>
        <button id="submit_hilite" class="button is-primary is-success" type="submit"><i class="material-icons">check</i> Save</button>
      </div>
    </form>
  </div>
</div>