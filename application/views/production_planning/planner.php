<div class="content-header"></div>
<div class="content-body">
  <?php if(is_has_feature('prod_planning')): ?>
  <div class="card" style="margin-bottom:15px">
    <div class="card-header">
      <div class="title-area">
        <?php $location = get_session('user_role') == "Administrator" ? "ALL LOCATION" : implode(", ", get_session('work_center')); ?>
        <h3 class="title">List Projects <span class="all"><?= $location; ?></span></h3>
      </div>
      <div class="option-box">
        <?php
        $work_center = get_session('work_center');
        if (empty($work_center) || count($work_center) > 1):?>
          <div class="option-item">
            <span>Location</span>
            <select class='filter_by_location'>
              <option value="">All Location</option>
              <?php foreach ($this->plant as $val): ?>
                <option value="<?= $val->PLANT ?>"><?= $val->PLANT_DESC ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        <?php endif; ?>
        <div class="option-item">
          <span>Revision Status</span>
          <select class='filter_by_project_status'>
            <option value="CRTD">Created</option>
            <option value="REL" selected>Release</option>
            <option value="CLSD">Close</option>
          </select>
        </div>
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content" id="tab-list-project">
      <div class="toolbar">
        <div class="toolbar-full">
          <input class="toolbar-item full" id="project_search" type="search" placeholder="Fill and enter to search">
          <?php if (is_has_feature('sync_planning')): ?>
          <div class="toolbar-item">
            <div class="single-button upload-project display-flex align-items-center justify-center disabled" style="background:#0065FF"><i class="material-icons warning sync_order" data-type="modal" modal-target="syncModal">cloud_upload</i></div>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <table id="list-projects" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Project</th>
            <?php if (is_has_feature('prod_planning_act')): ?>
            <th>Top</th>
            <th>Last Updated</th>
            <?php endif; ?>
            <th>A/C REG</th>
            <th>Hangar</th>
            <th>Maint Type</th>
            <th>Status</th>
            <th>Planning</th>
            <th>Hilite</th>
            <th>Start</th>
            <th>Finish</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Project</th>
            <?php if (is_has_feature('prod_planning_act')): ?>
            <th>Top</th>
            <th>Last Updated</th>
            <?php endif; ?>
            <th>A/C REG</th>
            <th>Hangar</th>
            <th>Maint Type</th>
            <th>Status</th>
            <th>Planning</th>
            <th>Hilite</th>
            <th>Start</th>
            <th>Finish</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  <?php endif; ?>
  <div class="is-row" style="margin-top:0px;margin-bottom:0">
    <div class="is-col">
      <div class="card project-planning">
        <div class="card-header no-border">
          <div class="title-area">
            <h3 class="title">Project Planning</h3>
          </div>
          <div class="option-box">
            <div class="option-item">
              <span>Timeframe</span>
              <select class="filter_planning_by_timeframe">
                <option value="daily" selected>Daily</option>
                <option value="weekly">Weekly</option>
                <option value="monthly">Monthly</option>
              </select>
            </div>
            <div class="option-item">
              <span>Area</span>
              <select class='filter_planning_by_area'>
                <option value="">All Area</option>
                <?php foreach ($area as $val): ?>
                  <option value="<?= $val ?>"><?= $val ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="option-item">
              <span>Search</span>
              <input class="filter_planning_by_search" type="search" name="search" placeholder="Type Revision or A/C Reg">
            </div>
          </div>
        </div>
        <div class="card-content" style="min-height: 100px">
          <div id="timeline"></div>
          <ul class="legend-area legend-center" style="margin-top: 15px">
            <li class="item removal">
              <span>Removal</span>
            </li>
            <li class="item inspection">
              <span>Inspection</span>
            </li>
            <li class="item rectification">
              <span>Rectification</span>
            </li>
            <li class="item installation">
              <span>Installation</span>
            </li>
            <li class="item weighing">
              <span>Weighing</span>
            </li>
            <li class="item painting">
              <span>Painting</span>
            </li>
            <li class="item rts">
              <span>RTS</span>
            </li>
          </ul>
        </div>
        <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
      </div>
    </div>
  </div>
</div>

<?php require_once('planning_modal.php'); ?>
<?php require_once('hilite_modal.php'); ?>
<?php require_once('sync_modal.php'); ?>
<?php require_once('log_modal.php'); ?>