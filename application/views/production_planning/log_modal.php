<div class="modal-box" id="logModal">
  <div class="modal sm">
    <span class="close" modal-target="logModal"></span>
      <div class="modal-header">
        <h2 class="title">Project Planning Activity</h2>
        <div class="planning-info">
          <ul class="display-flex align-items-center">
            <li class="revision">
              <span class="title">Revision</span>
              <span class="value">-</span>
            </li>
            <li class="status-plan">
              <span class="title">Status</span>
              <span class="value">-</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="modal-body" style="position: relative;">
        <div class="inner-body" style="max-height: 400px;overflow-y: scroll;">
          <ul class="log-planning-list">
          </ul>
        </div>
        <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
      </div>
  </div>
</div>