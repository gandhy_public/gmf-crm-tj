<div class="modal-box" id="planningModal">
  <div class="modal slg">
    <form id="planningForm" act="create" modal-target="planningModal" method="POST" style="position:relative">
      <span class="close" modal-target="planningModal"></span>
      <div class="modal-header">
        <h2 class="title planning-title">Project Planning</h2>
        <div class="planning-info">
          <ul class="display-flex align-items-center">
            <li class="revision">
              <span class="title">Revision</span>
              <span class="value">-</span>
            </li>
            <li class="created_by">
              <span class="title">Created By</span>
              <span class="value">-</span>
            </li>
            <li class="reviewed_by">
              <span class="title">Reviewed By</span>
              <span class="value">-</span>
            </li>
            <li class="edited_by">
              <span class="title">Last Edited By</span>
              <span class="value">-</span>
            </li>
          </ul>
        </div>
      </div>
      <div class="modal-body">
        <input type="hidden" name="revnr" value=""/>
        <input type="hidden" name="deleted_planning" value=""/>
        <table class="is-bordered">
          <thead>
            <tr>
              <th style="width:15%">Area</th>
              <th style="width:15%">Phase</th>
              <th style="width:15%">Start Date</th>
              <th style="width:15%">End Date</th>
              <th>Remarks</th>
              <th style="width:40px">
                <div class="action">
                  <i class='material-icons success add-row'>add_circle_outline</i>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr class="planning_item_template">
              <td>
                <select class="select2 select2_area">
                  <option value=""></option>
                  <?php foreach ($area as $val): ?>
                    <option value="<?= $val ?>"><?= $val ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
              <td>
                <select class="select2 select2_phase">
                  <option value=""></option>
                  <?php foreach ($phase as $val): ?>
                    <option value="<?= $val ?>"><?= $val ?></option>
                  <?php endforeach; ?>
                </select>
              </td>
              <td>
                <div class="is-prepend"><span class="material-icons">event</span><input type="text" class="datepicker start_date"></div>
              </td>
              <td>
                <div class="is-prepend"><span class="material-icons">event</span><input type="text" class="datepicker end_date"></div>
              </td>
              <td>
                <input type="text" class="remarks">
              </td>
              <td>
                <div class='action'>
                  <i class='material-icons danger remove-row'>remove_circle_outline</i>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <a href="" class="button is-primary is-default cancel" modal-target="planningModal">Back</a>
        <button id="submit_planning_button" class="button is-primary is-success" type="submit" style="display: none"><i class="material-icons">check</i> Save</button>
        <button id="confirm_planning_button" class="button is-primary is-success" style="display: none"><i class="material-icons">check</i> Confirm</button>
      </div>
      <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    </form>
  </div>
</div>