<div class="content-header"></div>
<div class="content-body">
  <div class="card">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">Daily Menu History</h3>
      </div>
      <div class="option-box">
        <?php if (empty(get_session('work_center'))):?>
          <div class="option-item">
            <span>Work Center</span>
            <select class='filter_by_work_center' data-target='list-orders'>
              <option value="">All</option>
              <?php foreach ($this->work_center as $item): ?>
              <option value="<?= $item->WORK_CENTER; ?>"><?= $item->WORK_CENTER; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        <?php else: ?>
          <input type="hidden" class='filter_by_work_center'>
        <?php endif; ?>
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content" id="tab-list-report">
      <div class="toolbar">
        <div class="toolbar-full">
          <input class="toolbar-item full" id="report_search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="report-history" class="display is-striped is-bordered" style="width:100%;">
        <thead>
          <tr>
            <th>No</th>
            <th>Document Number</th>
            <th>Assignment Date</th>
            <th>Created By</th>
            <th>Created At</th>
            <th style="width:70px">Action</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>No</th>
            <th>Document Number</th>
            <th>Assignment Date</th>
            <th>Created By</th>
            <th>Created At</th>
            <th>Action</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>