<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="PT. Sinergi Informatika Semen Indonesia">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<!-- title -->
  <title>GMF AeroAsia - Furnishing & Upholstery Services</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">

	<!-- icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- css -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/bootstrap/css/dropdown.bootstrap.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/visjs/vis-timeline-graph2d.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/metismenu/css/metismenu.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/datepicker/css/datepicker.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/jquery-dropdown/css/jquery.dropdown.min.css'); ?>">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url('assets/vendor/bootoast/bootoast.min.css'); ?>">
	<!-- <link rel="stylesheet" href="https://cdn.datatables.net/v/dt/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.css"> -->
	<link rel="stylesheet" href="<?= base_url('assets/css/dashboard.css'); ?>">


	<!-- fonts -->
	<link rel="stylesheet" href="<?= base_url('assets/fonts/cerebri-sans.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/fonts/skripsweet.css'); ?>">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
  <div class="container">
    <table id="list-reports" class="nowrap is-striped is-bordered" style="width:100%">
      <thead>
        <tr>
          <th>No</th>
          <th>Order</th>
          <th>A/C REG</th>
          <th>Description</th>
          <th>Target</th>
          <th>Highlight</th>
        </tr>
      </thead>
    </table>
    <div style="position: relative; float:right; margin-top: 30px">
      <table class="is-bordered sign" style="margin-bottom: 10px">
        <thead>
          <tr>
            <th>Prepared By : <span><?= get_session('name'); ?></span></th>
            <th>Approved By : <span>(Manager/SVP)</span></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="padding: 25px 0.8em;">Sign:</td>
            <td style="padding: 25px 0.8em;">Sign:</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
  <script src="<?= base_url('assets/js/app.js'); ?>"></script>

  <script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var table = $('#list-reports').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: false,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/daily/ajax_list_daily_report_detail/' . $docno); ?>",
        type: "POST",
        complete: function (json, type) { //type return "success" or "parsererror"
          $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'yyyy-mm-dd',
          });
          window.print();
        }
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'order', name: 'order', orderable: false},
        {data: 'reg', name: 'reg', orderable: false},
        {data: 'desc', name: 'desc', orderable: false},            
        {data: 'plan_target', name: 'plan_target', orderable: false},
        {data: 'highlight', name: 'highlight', orderable: false}
      ],
      order: [[ 0, '' ]],
      bInfo : false,
      bPaginate: false
    });
  });
  </script>

</body>
</html>