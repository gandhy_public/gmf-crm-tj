<div class="content-header"></div>
<div class="content-body">
  <div class="card">
    <div class="download-button display-flex">
      <div class="single-button print disabled"><i class="material-icons">print</i></div>
      <div class="single-button export_to_excel excel disabled" style="margin-left: 10px"><img class="icon" src="<?= base_url('assets/img/excel.svg'); ?>"/></div>
    </div>
    <div class="card-header no-border">
      <div class="title-area ctr">
        <h3 class="title" style="display: block">Daily Menu</h3>
        <?php
          $date = new DateTime($created_at);
          $result = $date->format('M d, Y H:i:s');
        ?>
        <span class="subtitle">Created at <strong><?= $result; ?></strong></span>
      </div>
    </div>
    <div class="card-content left-toolbar clearfix">
      <table id="list-reports" class="nowrap is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
            <th>Target</th>
            <th>Highlight</th>
            <th>Status</th>
          </tr>
        </thead>
      </table>
      <div style="position: relative; float:right; margin-top: 20px">
        <table class="is-bordered sign" style="margin-bottom: 10px">
          <thead>
            <tr>
              <th>Prepared By : <span><?= get_session('name'); ?></span></th>
              <th>Approved By : <span>(Manager/SVP)</span></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td style="padding: 25px 0.8em;">Sign:</td>
              <td style="padding: 25px 0.8em;">Sign:</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <form id="export_excel_form" action="<?= base_url('index.php/api/export/excel'); ?>" method="POST" style="display:none;">
    <input type="hidden" name="page" value="Daily Menu">
    <textarea type="hidden" name="query"></textarea>
  </form>
</div>