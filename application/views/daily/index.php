<div class="content-header"></div>
<div class="content-body">
  <div id="draft-daily-menu" class="card" style="margin-bottom:15px;display:none;">
    <div class="ovrly"><div class="ldg-ellipsis load-data"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div></div>
    <form id="form-daily-menu">
      <div class="card-header">
        <div class="title-area">
          <h3 class="title">Daily Menu</h3>
        </div>
      </div>
      <div class="card-content" style="position:relative">
        <input type="hidden" name="deleted_daily_menu" val="">
        <table class="draft-content is-striped is-bordered" style="white-space: pre-wrap;">
          <thead>
            <tr>
              <th>Revision</th>
              <th>Order</th>
              <th>Type</th>
              <th>Description</th>
              <th>A/C REG</th>
              <th>Target Date</th>
              <th>Pmhrs (hours)</th>
              <th>Amhrs (hours)</th>
              <th>Plan Target</th>
              <th>Highlight</th>
              <?php if ($act == 'edit'): ?>
                <th>Status</th>
              <?php endif; ?>
              <th style="width: 50px;"></th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
      <div class="card-footer display-flex">
        <div class="is-row daily-menu-info">
          <div class="is-col">
            <label>Plan Manhours Detail</label>
            <table class="pmhrs-detail is-bordered" style="min-width:300px">
              <thead>
                <tr>
                  <th>Revision</th>
                  <th>Pmhrs (hours)</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td class="total-pmhrs"></td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div class="is-col">
            <div class="form-item" style="min-width: 300px;">
              <label>Assignment Date</label>
              <div class="is-prepend"><span class="material-icons">event</span><input type="text" class="datepicker" name="assignment_date" data-toggle="datepicker" required></div>
            </div>
            <a href="" id="cancel_create_daily_menu" class="button is-primary is-default cancel">Cancel</a>
            <button id="submit_daily_menu" class="button is-primary is-success" type="submit"><i class="material-icons">check</i>Save</button>
          </div>
        </div>
      </div>
    </form>
  </div>
  <div class="card">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">List Order</h3>
      </div>
      <div class="option-box">
        <?php if (empty(get_session('work_center'))):?>
        <div class="option-item">
          <span>Work Center</span>
          <select class='filter_by_work_center'>
            <option value="">All</option>
            <?php foreach ($this->work_center as $item): ?>
            <option value="<?= $item->WORK_CENTER; ?>"><?= $item->WORK_CENTER; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <?php endif; ?>
        <div class="option-item">
          <span>Revision Status</span>
          <select class='filter_by_project_status' data-target='list-orders'>
            <option value="CRTD">Created</option>
            <option value="REL" selected>Release</option>
          </select>
        </div>
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content" id="tab-list-order">
      <input type="hidden" class="selected-order">
      <div class="toolbar">
        <?php 
        $class = 'full';
        if (is_has_feature('create_daily_menu')):
        ?>
          <div class="toolbar-left">
            <div class="toolbar-item">
              <button class="button" type="submit" id="add-to-list" disabled>Add to draft</button>
            </div>
          </div>
        <?php 
        $class = 'right';
        endif; 
        ?>
        <div class="toolbar-<?= $class; ?>">
          <input class="toolbar-item full" id="order_search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="list-orders" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>
              <div class="checkbox">
                <label><input id="select_all" type="checkbox"><span></span></label>
              </div>
            </th>
            <th class="no">No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Work Center</th>
            <th>Status</th>
            <th>Location</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>#</th>
            <th>No</th>
            <th>Project</th>
            <th>Order</th>
            <th>Type</th>
            <th>Description</th>
            <th>A/C REG</th>
            <th>Target Date</th>
            <th>Work Center</th>
            <th>Status</th>
            <th>Location</th>
            <th>Pmhrs (hours)</th>
            <th>Amhrs (hours)</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>