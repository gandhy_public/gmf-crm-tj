<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="author" content="PT. Sinergi Informatika Semen Indonesia">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<!-- title -->
  <title>SMART TV Daily Menu - Furnishing & Upholstery Services</title>

	<!-- favicon -->
	<link rel="shortcut icon" href="<?= base_url('assets/img/favicon.png'); ?>">

	<!-- icons -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- style -->
  <link rel="stylesheet" href="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('assets/css/app.css'); ?>">

  <!-- fonts -->
	<link rel="stylesheet" href="<?= base_url('assets/fonts/cerebri-sans.css'); ?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
  <div class="monitoring dark daily">
    <div class="container">
      <div class="monitoring-board">
        <div class="monitoring-header display-flex space-between align-items-center">
          <a class="back" href="<?= base_url('index.php/'.get_session('homepage_url')); ?>"><i class="material-icons">arrow_back</i>Back to Dashboard</a>
          <h1 class="title">SMART TV DISPLAY DAILY MENU</h1>
          <a class="back" href="#" style='visibility:hidden'><i class="material-icons">arrow_back</i>Back to Dashboard</a>
        </div>
        <div class="daily-menu-content">
          <?php if ($docno != ''): ?>
            <table id="list-reports" class="nowrap is-striped is-bordered" style="width:100%;white-space:wrap">
              <thead>
                <tr>
                  <th>Order</th>
                  <th>Type</th>
                  <th>Description</th>
                  <th>A/C REG</th>
                  <th>Target Date</th>
                  <th>Pmhrs (hours)</th>
                  <th>Amhrs (hours)</th>
                  <th>Target</th>
                  <th>Highlight</th>
                  <th>Status</th>
                </tr>
              </thead>
            </table>
          <?php else: ?>
            <div class="notask" style="text-align:center;font-size: 28px;font-weight: 500;">No Task Today</div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>

  <!-- script -->
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
  <script src="https://js.pusher.com/4.3/pusher.min.js"></script>

  <script>
    $(document).ready(function() {
      if ('<?= $docno ?>' != '') {
        var table = $('#list-reports').DataTable({ 
          processing: true, //Feature control the processing indicator.
          serverSide: true, //Feature control DataTables' server-side processing mode.
          paging: false,
          // sScrollX: true,
          scrollX: true,
          scrollY: "500px",
          scrollCollapse: true,
          lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
          language: {
            processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
          },
          ajax: {
            url: "<?= base_url('index.php/api/daily/ajax_list_daily_report_detail/' . $docno); ?>",
            type: "POST",
            complete: function (json, type) { //type return "success" or "parsererror"
              
            }
          },
          columns: [            
            {data: 'order', name: 'order', orderable: false},
            {data: 'type', name: 'type', orderable: false},
            {data: 'desc', name: 'desc', orderable: false},    
            {data: 'reg', name: 'reg', orderable: false},
            {data: 'target_date', name: 'target_date', orderable: false},
            {data: 'pmhrs', name: 'pmhrs', orderable: false},
            {data: 'amhrs', name: 'amhrs', orderable: false},        
            {data: 'plan_target', name: 'plan_target', orderable: false},
            {data: 'highlight', name: 'highlight', orderable: false},
            {data: 'status', name: 'status', orderable: false}
          ],
          order: [[ 0, '' ]],
          // bInfo : false,
          bPaginate: false
        });  

        var pusher = new Pusher('c586033a29a9db975254', {
          cluster: 'ap1',
          forceTLS: true
        });

        // pusher top project
        var channel = pusher.subscribe('CRM-TJ-development');
        
        channel.bind('update_daily_menu', function(data) {
          var data = $.parseJSON(data);

          console.log(data);
          console.log(data.assign_date == '<?= $assign_date ?>');
          console.log(data.user_id == '<?= get_session("id_user"); ?>');
          
          if (data.assign_date == '<?= $assign_date ?>' && data.user_id == '<?= get_session("id_user"); ?>') {
            table.ajax.reload(null, false);
          }
        });
      }
    });
  </script>
</body>
</html>