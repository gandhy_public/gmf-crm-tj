<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  function get_work_center() {
    return $(".filter_by_work_center").val();
  }

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_report_table = $('#report-history').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/daily/ajax_list_report_history'); ?>",
        type: "POST",
        data: function(d) {
          d.work_center = get_work_center();
        },
        complete: function(json, type) {
          $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'yyyy-mm-dd',
          });
        }
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'docno', name: 'docno'},
        {data: 'assignment_date', name: 'assignment_date'},
        {data: 'created_by', name: 'created_by'},
        {data: 'created_at', name: 'created_at'},
        {data: 'action', name: 'action', orderable: false}
      ],
      order: [[ 0, '' ]],
      drawCallback: function() {
        display_tooltip('i.detail', 'Detail');
        display_tooltip('i.edit', 'Edit');
        display_tooltip('i.print', 'Print');
      }
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_report_table.page.len($(this).val()).draw();
    });

    // Custom filter
    $('.filter_by_work_center').on('change', function(){
      list_report_table.ajax.reload(null, false);
    });

    $('#report_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_report_table.search(this.value).draw();
      }
    });

    $('#tab-list-report tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Document Number', 'Created By'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_report_table.columns().eq(0).each(function(colIdx) {
      $('input', list_report_table.column(colIdx).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_report_table.column(colIdx).search(this.value).draw();
        }
      });
    });

    $('#report-history').on('click', '.action i', function() {
      var link = $(this).attr('link');
      if ($(this).hasClass('print')) {
        window.open(link, '_blank');
      } else {
        window.location.href = link;
      }
    });

    // update single column
    $('#report-history').on('change', '.input-date', function(e) {
      var docno = $(this).data('docno');
      var column = $(this).data('column');
      // var alias = $(this).data('alias');
      var value = $(this).val();

      var that = $(this);

      if (value != '') {
        $.ajax({
          url: '<?= base_url("index.php/api/daily/update_daily_report_per_column"); ?>',
          type: 'POST',
          data: {
            docno: docno,
            column: column,
            value: value
          },
          beforeSend: function() {
            that.prop('disabled', true);
          },
          success: function(response) {
            var response = $.parseJSON(response);
            
            if (response.status == 'success') {
              display_toast('info', 'Successfully update assignment date #'+ docno);
            }
          },
          error: function(request, status, error) {
            alert('Something error when update assignment date!');
            console.log(error);
          },
          complete: function() {
            that.prop('disabled', false);
          }
        });        
      }
    });
  });
</script>