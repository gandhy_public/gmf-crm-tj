<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var query = "";

  document.addEventListener("DOMContentLoaded", function(event) {
    var table = $('#list-reports').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: false,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/daily/ajax_list_daily_report_detail/' . $docno); ?>",
        type: "POST",
        complete: function (json, type) { //type return "success" or "parsererror"
          $('[data-toggle="datepicker"]').datepicker({
            autoHide: true,
            format: 'yyyy-mm-dd',
          });
          query = json.responseJSON.query;
          $('.single-button.print').removeClass('disabled');
          $('.single-button.export_to_excel').removeClass('disabled');
        }
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'order', name: 'order', orderable: false},
        {data: 'type', name: 'type', orderable: false},
        {data: 'desc', name: 'desc', orderable: false},    
        {data: 'reg', name: 'reg', orderable: false},
        {data: 'pmhrs', name: 'pmhrs', orderable: false},
        {data: 'amhrs', name: 'amhrs', orderable: false},        
        {data: 'plan_target', name: 'plan_target', orderable: false},
        {data: 'highlight', name: 'highlight', orderable: false},
        {data: 'status', name: 'status', orderable: false}
      ],
      order: [[ 0, '' ]],
      bInfo : false,
      bPaginate: false
    });

    // print
    $(".single-button.print").on("click", function() {
      if ($(this).hasClass('disabled') != true) {
        window.open('<?= base_url("index.php/daily/print_to_device/".$docno); ?>', '_blank');  
      }
    });

    // Export to excel
    $(".single-button.export_to_excel").on("click", function() {
      if ($(this).hasClass('disabled') != true) {
        $('#export_excel_form [name=query]').val(query);
        $('#export_excel_form').submit();
      }
    });
  });
</script>