<div style="max-width: 1000px; margin: 0 auto; color: #465368;">
  <div class="header">
    <h2 class="title">DAILY MENU</h2>
    <!-- <span class="subtitle">Created at Oct 28, 2018 12:20:42</span> -->
  </div>
  <!-- <table style="border:none; width: 100%">
    <tr>
      <td style="text-align: center;">
        <h2>DAILY MENU SEATSHOP</h2>
      </td>
    </tr>
    <tr>
      <td style="text-align: center;">Created at Oct 28, 2018 12:20:42</td>
    </tr>
  </table> -->
  <!-- </br> -->
  <!-- <table width="100%" style="color:#465368;font-size:12px;">
    <thead>
      <tr style="font-weight:bold;background-color:#2AA384;color:#fff;font-size:13px;">
        <th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:5%">No</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:10%">Docno</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:10%">Order</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:10%">A/C Reg</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:10%">Pmhrs</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:25%">Description</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:10%">Target</th>
				<th style="text-align:center;border-right:1px solid #ddd;padding:5px 0px;width:20%">Highlight</th>
      </tr>
    </thead>
    <tbody>
      
    </tbody> -->
  <!-- </table> -->
  <table cellpadding="6" rowpading="6">
    <thead>
      <tr style="font-weight:bold;background-color:#2AA384;color:#fff">
        <th style="text-align:center;border-right:1px solid #ddd;width:5%">NO</th>
        <th style="text-align:center;border-right:1px solid #ddd;width:15%">ORDER</th>
        <th style="text-align:center;border-right:1px solid #ddd;width:10%">A/C REG</th>
        <th style="text-align:center;border-right:1px solid #ddd;width:20%">DESCRIPTION</th>
        <th style="text-align:center;border-right:1px solid #ddd;width:25%">TARGET</th>
        <th style="text-align:center;width:25%">HIGHLIGHT</th>
      </tr>
    </thead>
    <tbody>
      <?php 
      $no = 1;
      foreach ($daily_menu as $item): ?>
        <tr>
          <td style="width:5%;text-align:center;border-left:1px solid #ddd;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $no; ?></td>
          <td style="width:15%;text-align:left;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $item->AUFNR; ?></td>
          <td style="width:10%;text-align:left;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $item->TPLNR; ?></td>
          <td style="width:20%;text-align:left;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $item->KTEXT; ?></td>
          <td style="width:25%;text-align:left;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $item->PLAN_TARGET; ?></td>
          <td style="width:25%;text-align:left;border-right:1px solid #ddd;border-bottom:1px solid #ddd;"><?= $item->HIGHLIGHT; ?></td>
        </tr>
      <?php 
      $no++;
      endforeach; ?>
    </tbody>
  </table>
</div>

<style>
  .header {
    text-align: center;
  }

  .header .title {
    font-size: 16px;
    margin-bottom: 0px;
  }

  .header .subtitle {
    font-size: 12px;
    opacity: .7;
  }
  
  .table {
    width: 100%;
    color: #465368;
    font-size: 12px;
  }

  table thead tr {
    font-weight: bold; 
    background-color: #2AA384; 
    color: #fff ; 
    font-size: 12px;
  }

  table thead tr th {
    text-align: center;
    border-right: 1px solid #ddd;
    padding: 5px 0px;
    line-height: 30px;
  }
</style>
