<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  var hiddenOrder = [''];
  var totalPmhrs = {
    revision: [],
    manhours: []
  };

  var dailyMenuForm = $('#form-daily-menu');
  var dailyMenuFormOrigin = '';
  var dailyMenuFormChanged = false;

  var action = '<?= $act; ?>';
  if(action == 'create') {
    $('#draft-daily-menu .ovrly').hide();

    // change title
    $('#form-daily-menu .card-header .title').text('Create Daily Menu');
    $('#draft-daily-menu #submit_daily_menu').html('<i class="material-icons">check</i>Create');
  } else {
    $('#draft-daily-menu .ovrly').show();

    // change title
    var docno = '<?= $docno; ?>';
    $('#draft-daily-menu').show();
    $('#form-daily-menu .card-header .title').html('Edit Daily Menu <span>'+docno+'</span>');
    $('#draft-daily-menu #submit_daily_menu').html('<i class="material-icons">check</i>Update').prop('disabled', true);

    // set selected order
    var order = $.parseJSON('<?= $aufnr; ?>');
    hiddenOrder = hiddenOrder.concat(order);
    
    $.ajax({
      url: '<?= base_url("index.php/api/daily/get_daily_menu"); ?>',
      type: 'POST',
      data: {
        docno: '<?= $docno; ?>'
      },
      success: function(response) {
        var response = $.parseJSON(response);

        if (response.status == "success") {
          var data = response.data;
          $('#draft-daily-menu .ovrly').hide();

          var i = 0;
          var rowCount = data.length;
          var pmhrs = 0;

          $.each(data, function(index, val) {
            var type_color = '';
            if (val.AUART == 'Jobcard') {
              type_color = 'is-success';
            } else if (val.AUART == 'MDR') {
              type_color = 'is-warning';
            } else if (val.AUART == 'PD Sheet') {
              type_color = 'is-info';
            }

            var targetDateVal = (val.TARGET_DATE == '1900-01-01' || val.TARGET_DATE == null) ? '' : val.TARGET_DATE;

            var targetDateBg = '';
            if (targetDateVal != '' && targetDateVal != null) {
              var diff = dateDiffInDays(new Date(), new Date(targetDateVal));
              if (diff <= 0) {
                targetDateBg = 'background:#ff4949;color:#fff;';
              } else if(diff > 0 && diff <= 3) {
                targetDateBg = 'background:#FFAB00;color:#fff;';
              } else {
                targetDateBg = 'background:#FFAB00;color:#fff;';
              }
            } 

            var revnr = val.REVNR == "Non Project" ? "<span class='is-label is-dark'>"+val.REVNR+"</span>" : "<a href='"+ "<?= base_url('index.php/dashboard/detail_project/'); ?>" + val.REVNR +"' target='_blank'>"+val.REVNR+"</a>";

            var row = '';
            row += '<td>'+revnr+'</td>';
            row += '<td>'+val.AUFNR+'</td>';
            row += '<td><span class="is-label '+type_color+'">'+val.AUART+'</span></td>';
            row += '<td>'+val.KTEXT+'</td>';
            row += '<td>'+val.TPLNR+'</td>';
            row += '<td><span class="is-label-lg" style="'+targetDateBg+'white-space: nowrap">'+targetDateVal+'</span></td>';
            row += '<td>'+val.PMHRS+'</td>';
            row += '<td>'+val.AMHRS+'</td>';
            row += '<td><input type="text" name="plan_target[]" value="'+val.PLAN_TARGET+'" id_daily_report="'+val.ID_DAILY_REPORT+'" required/><input type="hidden" name="aufnr[]" value="'+val.AUFNR+'"/></td>';
            row += '<td><input type="text" name="highlight[]" value="'+val.HIGHLIGHT+'"/></td>';
            row += '<td><input type="text" name="status[]"/></td>';
            row += '<td><div class="action"><i class="material-icons danger remove-row" id_daily_report='+val.ID_DAILY_REPORT+' revnr="'+val.REVNR+'" aufnr="'+val.AUFNR+'" pmhrs="'+val.PMHRS+'">remove_circle_outline</i></div></td>';
            
            $('#draft-daily-menu table.draft-content tbody').append('<tr>'+row+'</tr>');

            // total
            pmhrs += parseFloat(val.PMHRS);
            
            updateTotalPmhrs(val.REVNR, val.PMHRS);

            i++;
            if (i == rowCount) {
              dailyMenuFormOrigin = $('#form-daily-menu').serialize();
              $('#draft-daily-menu table.pmhrs-detail .total-pmhrs').text(pmhrs);
            }
          });

          $('#draft-daily-menu [name=assignment_date]').val(response.data[0].ASSIGNMENT_DATE);
        }
      },
      error: function(request, status, error) {
        alert('Something error when get daily menu data!');
        console.log(error);
      },
    });
  }

  function updateTotalPmhrs(revision, value, operator = '+') {
    var value = parseFloat(value);
    if (totalPmhrs.revision.includes(revision)) {
      var index = totalPmhrs.revision.indexOf(revision);
      var revisionTarget = totalPmhrs.revision[index];

      if (operator == '+') {
        totalPmhrs.manhours[index] += value; 
        $('#draft-daily-menu table.pmhrs-detail tbody tr.'+revisionTarget+' td:last-child').text(totalPmhrs.manhours[index]);
      } else {
        var newManhours = totalPmhrs.manhours[index] - value;

        if (newManhours <= 0) {
          var removedRevision = totalPmhrs.revision.splice(index, 1);
          var removedManhours = totalPmhrs.manhours.splice(index, 1); 

          var revisionTarget = removedRevision == 'Non Project' ? 'Non' : removedRevision;
            
          $('#draft-daily-menu table.pmhrs-detail tbody tr.'+revisionTarget).remove();
        } else {
          totalPmhrs.manhours[index] = newManhours;
          console.log(revisionTarget);
          
          $('#draft-daily-menu table.pmhrs-detail tbody tr.'+revisionTarget+' td:last-child').text(newManhours);
        }
      }
    } else {
      totalPmhrs.revision.push(revision);
      totalPmhrs.manhours.push(parseFloat(value));

      var row = '<td>'+revision+'</td><td>'+value+'</td>';
      $('#draft-daily-menu table.pmhrs-detail tbody').append('<tr class="'+revision+'">'+row+'</tr>');
    }

    var total = totalPmhrs.manhours.reduce(add, 0);
    $('#draft-daily-menu table.pmhrs-detail tfoot tr td:last-child').text(total);
  }

  function get_project_status() {
    return $(".filter_by_project_status").val();
  }

  function get_work_center() {
    return $(".filter_by_work_center").val() || '';
  }

  function get_hidden_order() {
    return this.hiddenOrder;
  }

  $('[data-toggle="datepicker"]').datepicker({
    autoHide: true,
    format: 'yyyy-mm-dd',
  });

  document.addEventListener("DOMContentLoaded", function(event) {
    var list_order_table = $('#list-orders').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/daily/ajax_list_orders'); ?>",
        type: "POST",
        data: function(d) {
          d.project_status = get_project_status();
          d.work_center = get_work_center();
          d.hidden_order = get_hidden_order();
        }
      },
      columns: [            
        {data: 'check', name: 'check', orderable: false},       
        {data: 'no', name: 'no', orderable: false},    
        {data: 'project', name: 'project'},        
        {data: 'order', name: 'order'},
        {data: 'type', name: 'type'},
        {data: 'desc', name: 'desc'},
        {data: 'reg', name: 'reg'},
        {data: 'target_date', name: 'target_date'},
        {data: 'work_center', name: 'work_center'},
        {data: 'status', name: 'status'},
        {data: 'location', name: 'location'},
        {data: 'plan_manhours', name: 'plan_manhours'},
        {data: 'actual_manhours', name: 'actual_manhours'},
      ],
      order: [[ 0, '' ]]
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_order_table.page.len($(this).val()).draw();
    });

    // Custom filter
    $('.filter_by_work_center').on('change', function() {
      list_order_table.ajax.reload(null, false);
    });

    // Custom filter
    $('.filter_by_project_status, .filter_by_work_center').on('change', function(){
      list_order_table.ajax.reload(null, false);
    });

    $('#order_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_order_table.search(this.value).draw();
      }
    });

    $('#tab-list-order tfoot th').each(function(i) {
      var title = $(this).text();
      var array = ['Project', 'Order', 'Type', 'Description', 'A/C REG', 'Work Center', 'Status', 'Location'];
      if (array.includes(title)) {
        $(this).html('<input type="text" placeholder="Search ' + title + '" data-index="' + i + '"/>');
      }
    });

    // Apply the search
    list_order_table.columns().eq(0).each(function(colIdx) {
      $('input', list_order_table.column(colIdx).footer()).on('keyup change', function(e) {
        if (e.keyCode == 13 || $(this).val() == '') {
          list_order_table.column(colIdx).search(this.value).draw();
        }
      });
    });

    $('#select_all').click(function(event) {
      if (this.checked) {
        $('#add-to-list').prop("disabled", false);
        $('#list-orders tbody :checkbox').each(function(index, el) {
          this.checked = true;
          $('#create-daiy-menu').prop("disabled", false);
        });
      } else {
        $('#add-to-list').prop("disabled", true);
        $('#list-orders tbody :checkbox').each(function(index, el) {
          this.checked = false;
          // var aufnr = $(el).val();
          // var index = selectedOrder.indexOf(aufnr);
          // if(index > -1) selectedOrder.splice(index, 1);
          $('#create-daiy-menu').prop("disabled", true);
        });
      }
    });

    $('#add-to-list').prop("disabled", true);
    $('#list-orders').on('click', '.check', function() {
      var aufnr = $(this).val();
      if ($(this).is(':checked')) {
        $('#add-to-list').prop("disabled", false);
      } else {
        if ($('.check').filter(':checked').length < 1) {
          $('#add-to-list').attr('disabled', true);
        }
      }
    });

    // remove row from draft
    $('#draft-daily-menu').on('click', 'table.draft-content .remove-row:not(.disabled)', function() {
      var revnr = $(this).attr('revnr');
      var aufnr = $(this).attr('aufnr');
      var pmhrs = $(this).attr('pmhrs');

      var index = hiddenOrder.indexOf(aufnr);
      if(index > -1) hiddenOrder.splice(index, 1);
      $(this).parents('tr').remove();
      
      list_order_table.ajax.reload(null, false);

      if(hiddenOrder.length <= 2 && action == 'edit') {
        $('#draft-daily-menu table.draft-content .remove-row').addClass('disabled');
      }

      if(hiddenOrder.length <= 1 && action == 'create') {
        $('#draft-daily-menu').hide();
      }

      // set removed id daily menu exist to var
      var deleted_daily_menu = $('#draft-daily-menu [name=deleted_daily_menu]').val();
      var id_daily_report = $(this).attr('id_daily_report');
      if (id_daily_report != 0) {
        deleted_daily_menu += id_daily_report;
        $('#draft-daily-menu [name=deleted_daily_menu]').val(deleted_daily_menu + '-');        
      }

      if (action == 'edit') {
        dailyMenuFormChanged = dailyMenuForm.serialize() !== dailyMenuFormOrigin;
        $('#submit_daily_menu').prop('disabled', !dailyMenuFormChanged);
      }

      // update plan manhours detail
      updateTotalPmhrs(revnr, pmhrs, '-');
    });

    // cancel create dailt menu
    $('#draft-daily-menu').on('click', '#cancel_create_daily_menu', function(event) {
      event.preventDefault();

      if (action == 'create') {
        text = "You will cancel create daily menu";
      } else {
        text = "You will cancel edit daily menu";
      }

      swal({
        title: "Are you sure?",
        text: text,
        icon: "warning",
        buttons: [
          'No',
          'Yes'
        ],
        dangerMode: true,
      }).then(function(isConfirm) {
        if (isConfirm) {
          // if (action == 'create') {
          //   $('#draft-daily-menu table.draft-content tbody').html('');
          //   $('#draft-daily-menu').hide();
          //   $.each(hiddenOrder, function(key, aufnr){
          //     if(aufnr !== ''){
          //       $('#list-orders input#order-'+aufnr).parents('tr').show();
          //       $('#list-orders input#order-'+aufnr).prop('checked', false);
          //       $('#list-orders input#order-'+aufnr).removeClass('hidden');
          //     }
          //   });
          //   hiddenOrder = [''];
          //   totalPmhrs.revision = [];
          //   totalPmhrs.manhours = [];
          //   };
          // } else {
          window.location.href = "<?= base_url('index.php/daily/detail_daily_menu/'.$docno); ?>";
          // }
        }
      });
    });

    $('#add-to-list').on('click', function() {
      $(this).prop("disabled", true);
      // list_order_table.ajax.reload(null, false);

      $('#draft-daily-menu').show();

      $('#select_all').prop('checked', false);
      var count = 0;
      $("#list-orders input:checkbox[name=orders\\[\\]]:checked").not('.hidden').each(function(){
        var revnr = $(this).attr('revnr');
        var aufnr = $(this).val();
        var pmhrs = $(this).closest('tr').children().eq(11).html();
        hiddenOrder.push(aufnr);

        // hide selected order
        $(this).parents('tr').hide();

        // index of copied columns
        var column = [2, 3, 4, 5, 6, 7, 11, 12];
        var row = "";
        for (var i = 0; i < column.length; i++) {
          var col = $(this).closest('tr').children().eq(column[i]).html();
          row += '<td>'+col+'</td>';
        }

        var ktext = $(this).closest('tr').children().eq(5).html();

        row += '<td><input type="text" name="plan_target[]" id_daily_report="0" required/><input type="hidden" name="aufnr[]" value="'+aufnr+'"/></td>';
        row += '<td><input type="text" name="highlight[]"/></td>';
        if (action == 'edit') {
          row += '<td><input type="text" name="status[]"/></td>';
        }
        row += '<td><div class="action"><i class="material-icons danger remove-row" id_daily_report="0" revnr="'+revnr+'" aufnr="'+aufnr+'" pmhrs="'+pmhrs+'">remove_circle_outline</i></div></td>';

        $('#draft-daily-menu table.draft-content tbody').append('<tr>'+row+'</tr>');

        updateTotalPmhrs(revnr, pmhrs);
        
        $(this).addClass('hidden');
        count++;

        $('#draft-daily-menu table.draft-content .remove-row').removeClass('disabled');

        display_tooltip('#draft-daily-menu i.remove-row:not(.disabled)', 'Remove this row');
      });

      display_toast('info', count + ' order has been added');

      if (action == 'edit') {
        dailyMenuFormChanged = dailyMenuForm.serialize() !== dailyMenuFormOrigin;
        $('#submit_daily_menu').prop('disabled', !dailyMenuFormChanged);
      }
    });

    $('#form-daily-menu').on('change input', 'input', function() {
      if (action == 'edit') {
        dailyMenuFormChanged = dailyMenuForm.serialize() !== dailyMenuFormOrigin;
        $('#submit_daily_menu').prop('disabled', !dailyMenuFormChanged);
      }
    });

    $('#form-daily-menu').submit(function(event) {
      event.preventDefault();

      var deleted_daily_menu = $('#form-daily-menu [name=deleted_daily_menu]').val();
      if(deleted_daily_menu !== '') {
        deleted_daily_menu = deleted_daily_menu.slice(0, -1).split('-');
      } else {
        deleted_daily_menu = [];
      }

      var data = {
        id_daily_report: $('#form-daily-menu [name="plan_target\\[\\]"]').map(function(){
          return $(this).attr('id_daily_report');
        }).get(),
        aufnr: $('#form-daily-menu [name="aufnr\\[\\]"]').map(function(){return $(this).val();}).get(),
        plan_target: $('#form-daily-menu [name="plan_target\\[\\]"]').map(function(){return $(this).val();}).get(),
        highlight: $('#form-daily-menu [name="highlight\\[\\]"]').map(function(){return $(this).val();}).get(),
        type: action,
        docno: '<?= $docno; ?>',
        assignment_date: $('#form-daily-menu [name="assignment_date"]').val(),
        deleted_daily_menu: deleted_daily_menu
      };

      if (action == 'edit') {
        data.status = $('#form-daily-menu [name="status\\[\\]"]').map(function(){return $(this).val();}).get();
      } else {
        data.status = '';
      }

      $.ajax({
        url: '<?= base_url("index.php/api/daily/submit_daily_menu"); ?>',
        type: 'POST',
        data: data,
        beforeSend: function() {
          $('#draft-daily-menu table.draft-content .remove-row').addClass('disabled');
          $('#cancel_create_daily_menu').hide();
          $('#submit_daily_menu').prop("disabled", true);
          $('#submit_daily_menu').text('Processing...');
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;

            if (action == 'create') {
              var title = "Create Daily Menu Success";
              var text = "Successfully create Daily Menu!";
            } else {
              var title = "Edit Daily Menu Success";
              var text = "Successfully edit Daily Menu!";
            }
            $('#draft-daily-menu').hide('disabled');

            swal({
              title: title,
              text: text,
              icon: "success",
              timer: 1000,
              buttons: false
            }).then(function() {
              window.location.href = "<?= base_url('index.php/daily/detail_daily_menu/'); ?>" + data.docno;
            });
          }
        },
        error: function(request, status, error) {
          alert('Something error when submit daily menu!');
          console.log(error);
        }
      });
    });
  });
</script>