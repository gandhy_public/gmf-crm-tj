<div class="content-header"></div>
<div class="content-body">
  <div class="card">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">Synchronize Job List</h3>
      </div>
      <div class="option-box">
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="toolbar">
        <div class="toolbar-left">
          <div class="toolbar-item">
            <button class="button" type="button" id="check-status" disabled>Check Current Status</button>
          </div>
        </div>
        <div class="toolbar-right">
          <input class="toolbar-item full" id="sync_search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="list-sync" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>CRM</th>
            <th>Project</th>
            <th>Date Created</th>
            <th>Excute By</th>
            <th>Job Status</th>
            <th>Interface Status</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>