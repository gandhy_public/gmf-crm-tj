<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var list_sync_table = $('#list-sync').DataTable({ 
      processing: true, //Feature control the processing indicator.
      serverSide: true, //Feature control DataTables' server-side processing mode.
      responsive: true,
      paging: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: '<?= base_url("index.php/api/synchronize/ajax_list_sync"); ?>',
        type: "POST",
        data: function(d) {
        },
        complete: function(json, type) {
          $('#check-status').prop('disabled', false);
        }
      },
      columns: [
        {data: 'crm', name: 'crm'},
        {data: 'project', name: 'project'},
        {data: 'created_at', name: 'created_at'},
        {data: 'created_by', name: 'created_by'},
        {data: 'job_status', name: 'job_status'},
        {data: 'interface_status', name: 'interface_status'}
      ],
      order: [[ 0, '' ]],
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_sync_table.page.len($(this).val()).draw();
    });

    $('#sync_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_sync_table.search(this.value).draw();
      }
    });

    $('#check-status').click(function() {
      $.ajax({
        url: '<?= base_url("index.php/api/sapi/read_job"); ?>',
        type: 'POST',
        beforeSend: function() {
          $('#check-status').prop('disabled', true).text('Checking...');
        },
        success: function(response) {
          var response = $.parseJSON(response);
          
          if (response.status == 'success') {
            if (response.data[0].RFCSTATUS == 'DONE') {
              swal({title: 'Syncronized!', text: 'Done!', icon: 'success'});
              list_sync_table.ajax.reload(null, false);
            } else {
              swal({title: 'Syncronizing...', text: 'Please Wait', icon: 'info'});
            }
          } else {
            swal({title: 'No Task!', icon: 'info'});
          }
        },
        error: function(request, status, error) {
          alert('Something error when checking synchronize status!');
          console.log(error);
        },
        complete: function() {
          $('#check-status').prop('disabled', false).text('Check Current Status');
        }
      });
    });
  });
</script>
