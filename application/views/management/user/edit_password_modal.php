<div class="modal-box" id="editPasswordModal">
  <div class="modal sm">
    <form id="editPasswordForm" modal-target="editPasswordModal" method="POST">
      <span class="close" modal-target="editPasswordModal"></span>
      <div class="modal-header">
        <h2 class="title">Edit Password User</h2>
      </div>
      <div class="modal-body">
        <input type="hidden" name="id_user">
        <div class="form-item">
          <label>New Password</label>
          <input type="password" name="password" required>
        </div>
        <div class="form-item" style="margin-bottom: 0">
          <label>Confirm New Password</label>
          <input type="password" name="confirm_password" required>
        </div>
      </div>
      <div class="modal-footer">
        <a href="" class="button is-primary is-default cancel" modal-target="editPasswordModal">Cancel</a>
        <button id="edit_password_button" class="button is-primary is-success" type="submit" disabled><i class="material-icons">check</i> Save</button>
      </div>
    </form>
  </div>
</div>