<script src="<?= base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var list_users_table = $('#list-users').DataTable({ 
      processing: true,
      serverSide: true,
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/management/ajax_list_users'); ?>",
        type: "POST",
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'name', name: 'name'},
        {data: 'username', name: 'username'},
        {data: 'user_role', name: 'user_role'},
        {data: 'work_center', name: 'work_center'},
        {data: 'switch', name: 'switch'},
        {data: 'action', name: 'action', orderable: false}
      ],
      order: [[ 0, '' ]],
      drawCallback: function() {
        display_tooltip('i.edit-user:not(.disabled)', 'Edit User');
        display_tooltip('i.edit-password:not(.disabled)', 'Edit Password');
        display_tooltip('i.delete-user:not(.disabled)', 'Delete User');
      }
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_users_table.page.len($(this).val()).draw();
    });

    $('#user_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_users_table.search(this.value).draw();
      }
    });

    // Add User Function
    $('.modal-box').on('click', '.cancel, .close', function(event) {
      event.preventDefault();
      var modal = $(this).attr('modal-target');
      hideModal(modal);
      $('#addUserForm input, #addUserForm select').val('');
      $('#editUserForm input, #editUserForm select').val('');
      $('#addUserForm input, #addUserForm select').css({'border': '1px solid #eaeaea', 'background': '#fff'});
      $('#editUserForm input, #addUserForm select').css({'border': '1px solid #eaeaea', 'background': '#fff'});
      $('#editPasswordForm input, #editPasswordForm select').val('');

      $('#addUserForm .work_center').hide();
      $('#editUserForm .work_center').hide();
      $('#addUserForm .work_center [name="work_center\\[\\]"]').each(function() {
        this.checked = false;
      });
      $('#editUserForm .work_center [name="work_center\\[\\]"]').each(function() {
        this.checked = false;
      });
      $('#editUserForm .modal-footer').hide();

      $('.form-item').find('.list-work-center').css('border', '1px solid #eaeaea');
      $('.form-item').find('span.is-error').remove();
    });

    $('#addUserForm').on('change', 'input[name=ldap]', function() {
      if($(this).is(":checked")) {
        $('#addUserForm').find('input[name=password]').prop('disabled', true);
      } else {
        $('#addUserForm').find('input[name=password]').prop('disabled', false);
      }
    });

    $('#addUserForm').on('change', 'select[name=user_group_id]', function() {
      var user_role = $("#addUserForm select[name=user_group_id]").val().split('-')[1];
      
      if(user_role == 'Admin Hangar' || user_role == 'Controller Hangar') {
        $('#addUserForm .work_center.hangar').show();
        $('#addUserForm .work_center.shop').hide();

        $('#addUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      } else if(user_role == 'Controller Shop') {
        $('#addUserForm .work_center.hangar').hide();
        $('#addUserForm .work_center.shop').show();
        
        $('#addUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      } else {
        $('#addUserForm .work_center').hide();
        $('#addUserForm .work_center [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#editUserForm').on('change', 'select[name=user_group_id]', function() {
      var user_role = $("#editUserForm select[name=user_group_id]").val().split('-')[1];

      if(user_role == 'Admin Hangar' || user_role == 'Controller Hangar') {
        $('#editUserForm .work_center.hangar').show();
        $('#editUserForm .work_center.shop').hide();
        
        $('#editUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      } else if(user_role == 'Controller Shop'){
        $('#editUserForm .work_center.hangar').hide();
        $('#editUserForm .work_center.shop').show();
        
        $('#editUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      } else {
        $('#editUserForm .work_center').hide();
        $('#editUserForm .work_center [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#addUserForm').on('focus', 'input', function () {
      $(this).removeClass('is-error');
      $('.form-item').find('span.is-error').remove();
    })

    $('#addUserForm, #editUserForm').on('click', '[name="work_center\\[\\]"]', function() {
      $('.form-item').find('.list-work-center').css('border', '1px solid #eaeaea');
      $('.form-item').find('span.is-error').remove();
    });

    $('#addUserForm .work_center.hangar [is_all=true]').click(function(event) {
      if (this.checked) {
        $('#addUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = true;
        });
      } else {
        $('#addUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#addUserForm .work_center.shop [is_all=true]').click(function(event) {
      if (this.checked) {
        $('#addUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = true;
        });
      } else {
        $('#addUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#editUserForm .work_center.hangar [is_all=true]').click(function(event) {
      if (this.checked) {
        $('#editUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = true;
        });
      } else {
        $('#editUserForm .work_center.hangar [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#editUserForm .work_center.shop [is_all=true]').click(function(event) {
      if (this.checked) {
        $('#editUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = true;
        });
      } else {
        $('#editUserForm .work_center.shop [name="work_center\\[\\]"]').each(function() {
          this.checked = false;
        });
      }
    });

    $('#addUserForm').submit(function(event) {
      event.preventDefault();

      var user_role = $(this).find('[name=user_group_id]').val().split('-')[1];
      var work_center = $(this).find('[name="work_center\\[\\]"]:checked').map(function(){return $(this).val();}).get();
      
      try {
        if ((user_role == 'Admin Hangar' || user_role == 'Controller Hangar' || user_role == 'Controller Shop') && work_center.length == 0) throw 'work center empty';
        var work_center = $(this).find('[name="work_center\\[\\]"]:not([is_all=true]):checked').map(function(){return $(this).attr('work_center');}).get();
        work_center = work_center.length > 0 ? work_center.join() : '';
        
        var data = {
          name: $(this).find('[name=name]').val(),
          username: $(this).find('[name=username]').val(),
          ldap: $(this).find('[name=ldap]').is(':checked'),
          password: $(this).find('[name=password]').val(),
          user_group_id: $(this).find('[name=user_group_id]').val().split('-')[0],
          work_center: work_center
        };

        $.ajax({  
          url: "<?= base_url('index.php/api/management/create_user'); ?>",
          type: 'POST',
          data: data,
          beforeSend: function() {
            $('#addUserForm .close, #addUserForm .cancel').hide();
            $('#add_user_button').prop('disabled', true);
            $('#add_user_button').text('Processing...');
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              swal({
                title: "Success",
                text: response.message,
                icon: "success",
                timer: 2000,
                buttons: false
              }).then(
                function(){
                  hideModal('addUserModal');
                  $('#addUserForm input, #addUserForm select').val('');
                  $('#editUserForm input, #editUserForm select').val('');
                  $('#editPasswordForm input, #editPasswordForm select').val('');
                  
                  $('#addUserForm .work_center').hide();
                  $('#editUserForm .work_center').hide();
                  $('#addUserForm .work_center [name="work_center\\[\\]"]').each(function() {
                    this.checked = false;
                  });
                  $('#editUserForm .work_center [name="work_center\\[\\]"]').each(function() {
                    this.checked = false;
                  });
                  list_users_table.ajax.reload();
                }
              );
            } else if(response.status == "failed" && typeof response.message !== "undefined") {

            } else if(response.status == "failed" && typeof response.validation !== "undefined") {
              $.each(response.validation, function(key, item) {
                var message = "<span class='is-error'>" + item.message + "</span>";
                $(".form-" + item.field).find('input').addClass("is-error");
                $(".form-" + item.field).append(message);
              });
            }
          },
          error: function(request, status, error) {
            alert('Something error when submit user!');
            console.log(error);
          },
          complete: function() {
            $('#addUserForm .close, #addUserForm .cancel').show();
            $('#add_user_button').prop('disabled', false);
            $('#add_user_button').html('<i class="material-icons">check</i> Add User');
          }
        });
      } catch (error) {
        if (error == 'work center empty') {
          $("#addUserForm .work_center .list-work-center").css('border', '1px solid #FF4949');
          $("#addUserForm .work_center").append("<span class='is-error'>Please choose Work Center!</span>");
        }
      }
    });

    $('#editUserForm').submit(function(event) {
      event.preventDefault();

      var user_role = $(this).find('[name=user_group_id]').val().split('-')[1];
      var work_center = $(this).find('[name="work_center\\[\\]"]:checked').map(function(){return $(this).val();}).get();

      try {
        if ((user_role == 'Admin Hangar' || user_role == 'Controller Hangar' || user_role == 'Controller Shop') && work_center.length == 0) throw 'work center empty';
        var work_center = $(this).find('[name="work_center\\[\\]"]:not([is_all=true]):checked').map(function(){return $(this).attr('work_center');}).get();
        work_center = work_center.length > 0 ? work_center.join() : '';
        
        var data = {
          id_user: $(this).find('[name=id_user]').val(),
          name: $(this).find('[name=name]').val(),
          username: $(this).find('[name=username]').val(),
          user_group_id: $(this).find('[name=user_group_id]').val().split('-')[0],
          work_center: work_center
        };

        $.ajax({  
          url: "<?= base_url('index.php/api/management/edit_user'); ?>",
          type: 'POST',
          data: data,
          beforeSend: function() {
            $('#editUserForm .close, #editUserForm .cancel').hide();
            $('#edit_user_button').prop('disabled', true);
            $('#edit_user_button').text('Processing...');
          },
          success: function(response) {
            var response = $.parseJSON(response);

            if (response.status == "success") {
              swal({
                title: "Success",
                text: response.message,
                icon: "success",
                timer: 2000,
                buttons: false
              }).then(
                function(){
                  hideModal('editUserModal');
                  $('#editUserForm .modal-footer').hide();
                  
                  $('#addUserForm input, #addUserForm select').val('');
                  $('#editUserForm input, #editUserForm select').val('');
                  $('#editPasswordForm input, #editPasswordForm select').val('');

                  $('#addUserForm .work_center').hide();
                  $('#editUserForm .work_center').hide();
                  $('#addUserForm .work_center [name="work_center\\[\\]"]').each(function() {
                    this.checked = false;
                  });
                  $('#editUserForm .work_center [name="work_center\\[\\]"]').each(function() {
                    this.checked = false;
                  });

                  list_users_table.ajax.reload(null, false);
                }
              );
            } else if(response.status == "failed" && typeof response.message !== "undefined") {

            } else if(response.status == "failed" && typeof response.validation !== "undefined") {
              $.each(response.validation, function(key, item) {
                var message = "<span class='is-error'>" + item.message + "</span>";
                $(".form-" + item.field).find('input').addClass("is-error");
                $(".form-" + item.field).append(message);
              });
            }
          },
          error: function(request, status, error) {
            alert('Something error when edit user!');
            console.log(error);
          },
          complete: function() {
            $('#editUserForm .close, #editUserForm .cancel').show();
            $('#edit_user_button').prop('disabled', false);
            $('#edit_user_button').html('<i class="material-icons">check</i> Save');
          }
        });
      } catch (error) {
        if (error == 'work center empty') {
          $("#editUserForm .work_center .list-work-center").css('border', '1px solid #FF4949');
          $("#editUserForm .work_center").append("<span class='is-error'>Please choose Work Center!</span>");
        }
      }
    });

    $('#editPasswordForm').on('keyup', '[name=password], [name=confirm_password]', function() {
      if($('#editPasswordForm [name=confirm_password]').val() !== '') {
        if($('#editPasswordForm [name=password]').val() === $('#editPasswordForm [name=confirm_password]').val()) {
          $('#editPasswordForm').find('[type=submit]').prop('disabled', false);
        } else {
          $('#editPasswordForm').find('[type=submit]').prop('disabled', true);
        }
      }
    });

    $('#editPasswordForm').submit(function(event) {
      event.preventDefault();
      var data = {
        id_user: $(this).find('[name=id_user]').val(),
        password: $(this).find('[name=password]').val(),
      };
      
      $.ajax({  
        url: "<?= base_url('index.php/api/management/edit_password'); ?>",
        type: 'POST',
        data: data,
        beforeSend: function() {
          $('#editPasswordForm .close, #editPasswordForm .cancel').hide();
          $('#edit_password_button').prop('disabled', true);
          $('#edit_password_button').text('Processing...');
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            swal({
              title: "Success",
              text: response.message,
              icon: "success",
              timer: 2000,
              buttons: false
            }).then(
              function(){
                hideModal('editPasswordModal');
                list_users_table.ajax.reload(null, false);
              }
            );
          } else if(response.status == "failed" && typeof response.message !== "undefined") {

          }
        },
        error: function(request, status, error) {
          alert('Something error when edit password!');
          console.log(error);
        },
        complete: function() {
          $('#editPasswordForm .close, #editPasswordForm .cancel').show();
          $('#editPasswordForm input, #editPasswordForm select').val('');
          $('#edit_password_button').html('<i class="material-icons">check</i> Save');
          $('#edit_password_button').prop('disabled', true);
        }
      });
    });

    $('#list-users').on('click', '.edit-user', function() {
      var id_user = $(this).attr('id-user');
      $.ajax({
        url: "<?= base_url('index.php/api/management/get_user_by_id'); ?>",
        type: "POST",
        data: {
          id_user: id_user
        }, 
        beforeSend: function() {
          $('#editUserForm .modal-body').append(getLoaderOverlay());
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;
            
            $('#editUserForm .modal-footer').show();
            $('#editUserForm input[name=id_user]').val(data.ID_USER);
            $('#editUserForm input[name=name]').val(data.NAME);
            $('#editUserForm input[name=username]').val(data.USERNAME);
            $('#editUserForm select[name=user_group_id]').val(data.USER_GROUP_ID+'-'+data.USER_ROLE);
            
            if (data.USER_ROLE == 'Admin Hangar' || data.USER_ROLE == 'Controller Hangar' || data.USER_ROLE == 'Controller Shop') { // if hangar or shop
              var workCenter = data.WORK_CENTER.split(',');
              console.log(workCenter);
              
              var target = data.USER_ROLE == 'Controller Shop' ? '.shop' : '.hangar';

              var workCenterHangar = $.parseJSON('<?= json_encode($work_center_group['hangar']); ?>');
              var workCenterShop = $.parseJSON('<?= json_encode($work_center_group['shop']); ?>');

              $('#editUserForm .work_center' + target).show();
              if (((data.USER_ROLE == 'Admin Hangar' || data.USER_ROLE == 'Controller Hangar' ) && workCenterHangar.length == workCenter.length) || (data.USER_ROLE == 'Controller Shop' && workCenterShop.length == workCenter.length)) {
                $('#editUserForm .work_center' + target + ' [name="work_center\\[\\]"]').each(function() {
                  this.checked = true;
                });
              } else {
                $.each(workCenter, function(index, val) {
                  var workCenterItem = $('#editUserForm .work_center' + target + ' [work_center='+val+']').prop('checked', true);
                });
              }
            }
          }
        },
        error: function(request, status, error) {
          alert('Something error when loading user data!');
          console.log(error);
        },
        complete: function() {
          $('#editUserForm .modal-body .ovrly').remove();
        }
      });
    });

    $('#list-users').on('click', '.edit-password', function() {
      var id_user = $(this).attr('id-user');
      $('#editPasswordForm [name=id_user]').val(id_user);
    });

    $('#list-users').on('click', '.switch', function(event) {
      event.preventDefault();
      event.stopPropagation();
      var status = $(this).is(":checked");
      var id_user = $(this).attr("id-user");
      var name = $(this).attr("name");
      
      if($(this).is(":checked") == false) {
        swal({
          title: "Are you sure?",
          text: "You will deactivate " + name + "!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "<?= base_url('index.php/api/management/toggle_user_status'); ?>",
              type: "POST",
              data: {
                id_user: id_user,
                status: 0,
              },
              success: function(response) {
                var response = $.parseJSON(response);
                if (response.status == "success") {
                  swal({
                    title: "Success",
                    text: "Successfully deactivate " + name + "!",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  });
                  list_users_table.ajax.reload(null, false);
                }
              },
              error: function(request, status, error) {
                alert('Something error when deactivate user status!');
                console.log(error);
              },
            });
          }
        });
      } else {
        swal({
          title: "Are you sure?",
          text: "You will activate " + name + "!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ]
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "<?= base_url('index.php/api/management/toggle_user_status'); ?>",
              type: "POST",
              data: {
                id_user: id_user,
                status: 1,
              },
              success: function(response) {
                var response = $.parseJSON(response);
                if (response.status == "success") {
                  swal({
                    title: "Success",
                    text: "Successfully activate " + name + "!",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  });
                  list_users_table.ajax.reload(null, false);
                }
              },
              error: function(request, status, error) {
                alert('Something error when activate user status!');
                console.log(error);
              },
            });
          }
        });
      }
    });

    $('#list-users').on('click', '.delete-user', function(event) {
      event.preventDefault();
      event.stopPropagation();
      var id_user = $(this).attr("id-user");
      var name = $(this).attr("name");
      
      if (!$(this).hasClass("disabled")) {
        swal({
          title: "Are you sure?",
          text: "You will delete " + name + "!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "<?= base_url('index.php/api/management/delete_user'); ?>",
              type: "POST",
              data: {
                id_user: id_user
              },
              success: function(response) {
                var response = $.parseJSON(response);
                if (response.status == "success") {
                  swal({
                    title: "Success",
                    text: "Successfully delete user " + name + "!",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  });
                  list_users_table.ajax.reload();
                }
              },
              error: function(request, status, error) {
                alert('Something error when delete user!');
                console.log(error);
              },
            });
          }
        });
      }
    });
  });
</script>