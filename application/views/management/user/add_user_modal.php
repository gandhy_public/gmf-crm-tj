<div class="modal-box" id="addUserModal">
  <div class="modal sm">
    <form id="addUserForm" modal-target="addUserModal" method="POST" action="<?= base_url('index.php/management/add_user'); ?>">
      <span class="close" modal-target="addUserModal"></span>
      <div class="modal-header">
        <h2 class="title">Add New User</h2>
      </div>
      <div class="modal-body">
        <div class="form-item">
          <label>Name</label>
          <input type="text" name="name" required>
        </div>
        <div class="form-item form-username">
          <label>Username</label>
          <input type="text" name="username" required>
          <label class="is-checkbox">
            <div class="checkbox"><label><input type="checkbox" name="ldap"/><span>Using LDAP?</span></label></div>
          </label>
        </div>
        <div class="form-item">
          <label>Password</label>
          <input type="password" name="password">
        </div>
        <div class="form-item">
          <label>User Group</label>
          <select name="user_group_id" required>
            <option value="">Select Group</option>
            <?php foreach($user_group as $item):?>
            <option value="<?= $item->ID_USER_GROUP.'-'.$item->USER_ROLE; ?>"><?= $item->USER_GROUP; ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-item work_center hangar" style="display:none;margin:15px 0 0;">
          <label>Work Center</label>
          <div class="list-work-center display-flex">
            <div class="item display-flex">
              <div class="checkbox">
                <label><input type="checkbox" name="work_center[]" value="all" is_all="true"><span>ALL</span></label>
              </div>
            </div>
            <?php foreach($work_center_group['hangar'] as $val):?>
            <div class="item display-flex">
              <div class="checkbox">
                <label><input type="checkbox" name="work_center[]" value="<?= $val; ?>" work_center="<?= $val; ?>"><span><?= $val; ?></span></label>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="form-item work_center shop" style="display:none;margin:15px 0 0;">
          <label>Work Center</label>
          <div class="list-work-center display-flex">
            <div class="item display-flex">
              <div class="checkbox">
                <label><input type="checkbox" name="work_center[]" value="all" is_all="true"><span>ALL</span></label>
              </div>
            </div>
            <?php foreach($work_center_group['shop'] as $val):?>
            <div class="item display-flex">
              <div class="checkbox">
                <label><input type="checkbox" name="work_center[]" value="<?= $val; ?>" work_center="<?= $val; ?>"><span><?= $val; ?></span></label>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a href="" class="button is-primary is-default cancel" modal-target="addUserModal">Cancel</a>
        <button id="add_user_button" class="button is-primary is-success" type="submit"><i class="material-icons">check</i> Add User</button>
      </div>
    </form>
  </div>
</div>