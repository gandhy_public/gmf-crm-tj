<div class="content-header"></div>
<div class="content-body">
  <div class="card tab-content active" id="tab-list-users">
    <div class="card-header">
      <div class="title-area">
        <h3 class="title">List Users</h3>
      </div>
      <div class="option-box">
        <div class="option-item length-data">
          <span>Show</span>
          <select class='page-length'>
            <option value="10">10</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="-1">All</option>
          </select>
        </div>
      </div>
    </div>
    <div class="card-content">
      <div class="toolbar">
        <div class="toolbar-left">
          <div class="toolbar-item">
            <button class="button" id="add-new-user" data-type="modal" modal-target="addUserModal">Add New User</button>
          </div>
        </div>
        <div class="toolbar-right">
          <input class="toolbar-item full" id="user_search" type="search" placeholder="Fill and enter to search">
        </div>
      </div>
      <table id="list-users" class="display is-striped is-bordered" style="width:100%">
        <thead>
          <tr>
            <th>NO</th>
            <th>NAME</th>
            <th>USERNAME</th>
            <th>USER ROLE</th>
            <th>WORK CENTER</th>
            <th style="width:40;">STATUS</th>
            <th style="width:100px;">ACTION</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <th>NO</th>
            <th>NAME</th>
            <th>USERNAME</th>
            <th>USER ROLE</th>
            <th>WORK CENTER</th>
            <th>STATUS</th>
            <th>ACTION</th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>

<?php 
require_once('add_user_modal.php'); 
require_once('edit_user_modal.php');
require_once('edit_password_modal.php');
?>