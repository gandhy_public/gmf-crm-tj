<script src="<?= base_url('assets/vendor/datatables/js/datatables.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/datepicker/js/datepicker.min.js'); ?>"></script>
<script src="<?= base_url('assets/vendor/jquery-dropdown/js/jquery.dropdown.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/app.js'); ?>"></script>

<script>
  document.addEventListener("DOMContentLoaded", function(event) {
    var list_groups_table = $('#list-groups').DataTable({ 
      processing: true,
      serverSide: true,
      paging: true,
      // sScrollX: true,
      scrollX: true,
      scrollY: "500px",
      scrollCollapse: true,
      lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
      language: {
        processing: '<div class="ldg-ellipsis"><span class="dot"></span><span class="dot"></span><span class="dot"></span></div>',
      },
      ajax: {
        url: "<?= base_url('index.php/api/management/ajax_list_groups'); ?>",
        type: "POST",
      },
      columns: [            
        {data: 'no', name: 'no', orderable: false},            
        {data: 'group_name', name: 'group_name'},
        {data: 'status', name: 'status'},
        {data: 'action', name: 'action', orderable: false}
      ],
      order: [[ 0, '' ]],
      drawCallback: function() {
        display_tooltip('i.edit', 'Edit');
      }
    });

    // Custome page length
    $('.page-length').on('change', function(){
      list_groups_table.page.len($(this).val()).draw();
    });

    $('#group_search').on('keyup change', function (e) {
      if (e.keyCode == 13 || $(this).val() == '') {
        list_groups_table.search(this.value).draw();
      }
    });

    $('#create-new-group').click(function() {
      $('#groupForm h2.title').text('Create Menu Group');
      $('#groupForm .button-name').text('Create Menu');
      $('#groupForm input').prop("disabled", false);
      $('#groupForm [name=id_user_group]').remove();
      $('#groupForm [name=user_group]').val('');
      $('#groupForm input[type=checkbox]').prop('checked', false);
      $('#groupForm').attr("dest", "create_group");
      $('#groupForm .modal-footer').show();
    });

    $('#list-groups').on('click', '.edit', function() {
      $('#groupForm h2.title').text('Edit Menu Group');
      $('#groupForm .button-name').text('Update Menu');
      $('#groupForm input').prop("disabled", false);
      $('#groupForm').attr("dest", "update_group");
      $('#groupForm .modal-footer').hide();

      var id_user_group = $(this).attr('id-user-group');
      $.ajax({  
        url: "<?= base_url('index.php/api/management/get_menu_by_group_id'); ?>",
        type: 'POST',
        data: {
          id_user_group: id_user_group
        },
        beforeSend: function() {
          $('#groupForm .modal-body').append(getLoaderOverlay());
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var data = response.data;

            $('#groupForm .modal-footer').show();
            $('#groupForm .modal-body').before('<input type="hidden" name="id_user_group" value="'+ data.id_user_group +'">');
            $('#groupForm input[name=user_group]').val(data.user_group);
            $('#groupForm select[name=user_role_id]').val(data.user_role_id);
            $.each(data.menu, function(key, item) {
              $.each(item, function(key2, inner) {
                $('[menu-id=menu-'+inner.id_menu+']').prop("checked", true);
              });
            });
          }
        },
        error: function(request, status, error) {
          alert('Something error when loading user group!');
          console.log(error);
        },
        complete: function() {
          $('#groupForm .modal-body .ovrly').remove();
        }
      });
    });

    $('#groupForm').submit(function(event) {
      event.preventDefault();
      var dest = $('#groupForm').attr('dest');
      var data = $(this).serializeArray();
      
      $.ajax({  
        url: "<?= base_url('index.php/api/management/'); ?>" + dest,
        type: 'POST',
        data: $(this).serializeArray(),
        beforeSend: function() {
          $('#add_new_group').prop('disabled', true);
          $('#add_new_group').text('Processing...');
        },
        success: function(response) {
          var response = $.parseJSON(response);

          if (response.status == "success") {
            var message = response.message;
            
            swal({
              title: "Success",
              text: message,
              icon: "success",
              timer: 2000,
              buttons: false
            }).then(
              function(){
                hideModal('groupModal');
                $('#groupForm .modal-footer').hide();
                list_groups_table.ajax.reload();
              }
            );
          }
        },
        error: function(request, status, error) {
          alert('Something error when submit data!');
          console.log(error);
        },
        complete: function() {
          $('#add_new_group').prop('disabled', false);
          $('#add_new_group').html('<i class="material-icons">check</i> Create');
        }
      });
    });

    $('.modal-box').on('click', '.cancel, .close', function(event) {
      event.preventDefault();
      var modal = $(this).attr('modal-target');
      hideModal(modal);
      $('#groupForm [name=id_user_group]').remove();
      $('#groupForm [name=user_group]').val('');
      $('#groupForm [type=checkbox]').prop("checked", false);
      $('#groupForm .modal-footer').hide();
    });

    $('#list-groups').on('click', '.switch', function(event) {
      event.preventDefault();
      event.stopPropagation();
      var status = $(this).is(":checked");
      var id_user_group = $(this).attr("id-user-group");
      var user_group = $(this).attr("user-group");
      
      if($(this).is(":checked") == false) {
        swal({
          title: "Are you sure?",
          text: "You will deactivate group " + user_group + "!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ],
          dangerMode: true,
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "<?= base_url('index.php/api/management/toggle_group_status'); ?>",
              type: "POST",
              data: {
                id_user_group: id_user_group,
                status: 0,
              },
              success: function(response) {
                var response = $.parseJSON(response);

                if (response.status == "success") {
                  swal({
                    title: "Success",
                    text: "Successfully deactivate group " + user_group + "!",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  });
                  list_groups_table.ajax.reload(null, false);
                } else {
                  swal({
                    title: "Something's not right",
                    text: response.error_message,
                    icon: "error",
                  });
                }
              },
              error: function(request, status, error) {
                alert('Something error when deactivate user group!');
                console.log(error);
              },
            });
          }
        });
      } else {
        swal({
          title: "Are you sure?",
          text: "You will activate group " + user_group + "!",
          icon: "warning",
          buttons: [
            'No, cancel it!',
            'Yes, I am sure!'
          ]
        }).then(function(isConfirm) {
          if (isConfirm) {
            $.ajax({
              url: "<?= base_url('index.php/api/management/toggle_group_status'); ?>",
              type: "POST",
              data: {
                id_user_group: id_user_group,
                status: 1,
              },
              success: function(response) {
                var response = $.parseJSON(response);

                if (response.status == "success") {
                  swal({
                    title: "Success",
                    text: "Successfully activate group " + user_group + "!",
                    icon: "success",
                    timer: 2000,
                    buttons: false
                  });
                  list_groups_table.ajax.reload(null, false);
                }
              },
              error: function(request, status, error) {
                alert('Something error when activate user group!');
                console.log(error);
              },
            });
          }
        });
      }
    });
  });
</script>