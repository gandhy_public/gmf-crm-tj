<div class="modal-box" id="groupModal">
  <div class="modal sm">
    <form id="groupForm" modal-target="groupModal" method="POST" dest="create_group">
      <span class="close" modal-target="groupModal"></span>
      <div class="modal-header">
        <h2 class="title">Menu Management</h2>
      </div>
      <div class="modal-body" style="position: relative">
        <div class="is-row" style="margin-bottom: 15px">
          <div class="is-col">
            <label>Group Name</label>
            <input type="text" name="user_group" required/>
          </div>
          <div class="is-col">
            <label>User Role</label>
            <select name="user_role_id" required>
              <option value="">Select Role</option>
              <?php foreach($user_role as $item):?>
              <option value="<?= $item->ID_USER_ROLE; ?>"><?= $item->USER_ROLE; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <?php 
        $modul = array_keys($data_menu); 
        for($i = 0; $i < count($modul); $i++):
        ?>
        <div class="form-item menu-list">
          <label>Select Menu in Module <?= $modul[$i]?></label>
          <?php for($j = 0; $j < count($data_menu[$modul[$i]]); $j++):?>
          <div class="option">
            <div class="checkbox">
              <label><input type="checkbox" menu-id="<?= "menu-".$data_menu[$modul[$i]][$j]['id_menu'] ?>" name="<?= $modul[$i]; ?>[]" value="<?= $data_menu[$modul[$i]][$j]['id_menu']?>_<?= $data_menu[$modul[$i]][$j]['modul_id']?>"><span></span></label>
            </div>
            <span><?= $data_menu[$modul[$i]][$j]['menu']?></span>
          </div>
          <?php endfor; ?>
        </div>
        <?php endfor; ?>
      </div>
      <div class="modal-footer" style="display:none">
        <a href="" class="button is-primary is-default cancel" modal-target="groupModal">Cancel</a>
        <button id="add_new_group" class="button is-primary is-success" type="submit"><i class="material-icons">check</i>Create</button>
      </div>
    </form>
  </div>
</div>