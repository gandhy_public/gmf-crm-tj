<?php

function get_date_format($date) {
  if(!empty($date)) {
    $d = substr($date, 4, 4).substr($date, 2, 2).substr($date, 0, 2);
    return date('d M Y', strtotime($d));
  }
  return '';
}

function get_date_formatv2($date) {
  if(!empty($date)) {
    return date('d M Y', strtotime($date));
  }
  return '';
}

function date_to_array($date) {
  if(!empty($date)) {
    return [substr($date, 0, 4),substr($date, 4, 2),substr($date, 6, 2)];
  }
  return '';
}

function date_to_timeline_format($date) {
  if(!empty($date)) {
    return substr($date, 0, 4).'-'.substr($date, 4, 2).'-'.substr($date, 6, 2);
  }
  return '';
}

function this_year() {
  $now = new DateTime();
  return $now->format("Y");
}

// function get_date_format($date) {
//   if(!empty($date)) {
//     $d = substr($date, 4, 4).substr($date, 2, 2).substr($date, 0, 2);
//     return date('d M Y', strtotime($d));
//   }
//   return '';
// }

function convert_to_number($value) {
	return strrev(implode('.',str_split(strrev(strval($value)),3)));
}

function convert_location($loc) {
  $location = "";
  switch ($loc) {
    case 'GAH1':
      $location = "Hangar 1";
      break;
    case 'GAH3':
      $location = "Hangar 3";
      break;
    case 'GAH4':
      $location = "Hangar 4";
      break;
  }
  return $location;
}

// function get_work_center() {
//   $work_center = "";
//   switch (get_session('work_center')) {
//     case 'GAH1':
//       $work_center = "Hangar 1";
//       break;
//     case 'GAH3':
//       $work_center = "Hangar 3";
//       break;
//     case 'GAH4':
//       $work_center = "Hangar 4";
//       break;
//     case 'W101':
//       $work_center = "Seat Shop 1";
//       break;
//     case 'W102':
//       $work_center = "Seat Shop 2";
//       break;
//     case 'W103':
//       $work_center = "Interior Painting";
//       break;
//     case 'W401':
//       $work_center = "Monument Shop 1";
//       break;
//     case 'W402':
//       $work_center = "Monument Shop 2";
//       break;
//     case 'W501':
//       $work_center = "Laundry Shop";
//       break;
//     case 'W502':
//       $work_center = "Sewing Shop";
//       break;
//   }
//   return $work_center;
// }

function raw_datetime_format($date){
  $raw_start_date = strtotime($date);
  return date('Y-M-d H:s', $raw_start_date);
}

function get_percentage($value, $total) {
  return @round(($value / $total) * 100, 2).'%';
}