<?php
 
// $GLOBALS['group_level'] = array(1,2,3);

function is_has_session($session) {
  if((!$session || !isset($_COOKIE['crm_tj_menu'])) && !isset($_COOKIE['crm_tj_remember'])) {
    redirect('logout', 'refresh');
  } else {
    return true;
  }
}

// function is_has_access($current_path, $access_menu) {
//   $menu_path = array();
//   foreach ($access_menu as $value) {
//     array_push($menu_path, $value->PATH);	
//   }

//   if(!in_array($current_path, $menu_path)) {
//     redirect('/', 'refresh');
//   } 		
// }

function get_session($param) {
  $session = $_SESSION['crm_sess_logged_in'];
  return $session[$param];
}

function is_has_access($page, $redirect = true) {
  if (in_array(strtolower($page), get_session('menu'))) {
    return true;
  } else {
    return $redirect == true ? redirect(get_session('homepage_url'), '/') : false;
  }
}

function is_has_feature($feature) {
  return in_array($feature, get_session('features'));
}