<?php

function auart_label_color($auart) {
  $label_color = '';
  if ($auart == 'Jobcard') {
    $label_color = 'is-success';
  } else if ($auart == 'MDR') {
    $label_color = 'is-warning';
  } else if ($auart == 'PD Sheet') {
    $label_color = 'is-info';
  }

  return $label_color;
}

function target_date_bg($target_date) {
  $target_date_bg = '';
  if($target_date != '' && $target_date != '1900-01-01') {
    $target_date = strtotime($target_date);
    $now = time(); 
    $diff = round(($target_date - $now) / (60 * 60 * 24));
    if ($diff > 0 && $diff <= 3) {
      $target_date_bg = 'background:#FFAB00;color:#fff;';
    } elseif ($diff <= 0 ) {
      $target_date_bg = 'background:#FF4949;color:#fff;';
    }
  }

  return $target_date_bg;
}

function job_interface_status($status) {
  $status = strtolower($status);
  $label_color = '';
  if ($status == 'finish' || $status == 'done') {
    $label_color = 'is-success';
  } else if ($status == 'running' || $status == 'created') {
    $label_color = 'is-warning';
  } else if ($status == 'error' || $status == 'cancel') {
    $label_color = 'is-danger';
  }

  return $label_color;
}

function get_url() {
  return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
}

function get_features($user_role) {
  $list_features = [
    'prod_planning' => ['Administrator', 'Admin Hangar', 'Controller Hangar'],
    'prod_planning_act' => ['Administrator', 'Admin Hangar'],
    'create_update_planning' => ['Admin Hangar'],
    'update_planning' => ['Admin Hangar', 'Controller Hangar'],
    'sync_planning' => ['Admin Hangar'],
    'sync_order' => ['Administrator'],
    'move_order' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'],
    'update_order_status' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'],
    'update_order_form' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'], // target date, part name, qty
    'update_remarks' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop', 'Sent to Other'],
    'update_location' => ['Administrator', 'Controller Shop'],
    'view_detail_project' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'],
    'view_detail_order' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'],
    'update_material_form' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop', 'Purchaser'],
    'create_daily_menu' => ['Administrator', 'Admin Hangar', 'Controller Hangar', 'Controller Shop'],
  ];

  $features = [];
  foreach ($list_features as $key => $val) {
    if (in_array($user_role, $val)) {
      $features[] = $key;   
    }
  }

  return $features;
}