<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  
  public function __construct() {
    parent::__construct(); 
    $this->load->model('login_model');
    $this->load->model('user_model');
    $this->load->model('group_management_model');

    if($this->session->userdata('crm_sess_logged_in')) {
      $monitoring_prod = $this->input->cookie('crm_tj_monitoring_prod');
      $monitoring_daily = $this->input->cookie('crm_tj_monitoring_daily');

      if (isset($monitoring_prod) && $monitoring_prod == true && $monitoring_prod != null) {
        redirect('production_control/monitoring', 'refresh');
      } else if (isset($monitoring_daily) && $monitoring_daily == true && $monitoring_daily != null) {
        redirect('daily/monitoring', 'refresh');
      } else {
        redirect(get_session('homepage_url'), 'refresh');
      }
    }
  }

  public function index() {
    $remember = $this->input->cookie('crm_tj_remember');
    if (isset($remember) || $remember != '' || $remember != null) {
      // set session
      $data = $this->user_model->get_user_data($remember);
      $this->set_session($data);

      $monitoring_prod = $this->input->cookie('crm_tj_monitoring_prod');
      $monitoring_daily = $this->input->cookie('crm_tj_monitoring_daily');
      
      if (isset($monitoring_prod) && $monitoring_prod == true && $monitoring_prod != null) {
        redirect('production_control/monitoring', 'refresh');
      } else if (isset($monitoring_daily) && $monitoring_daily == true && $monitoring_daily != null) {
        redirect('daily/monitoring', 'refresh');
      } else {
        redirect(get_session('homepage_url'), 'refresh');
      }
    } else {
      $this->load->view('login');
    }
  }

  function auth() {
    try {
      // check field username
      $username = $this->input->post('username');
      if(!isset($username) || empty($username)) throw new Exception('Username is required!', 1);

      // check field password
      $password = $this->input->post('password');
      if(!isset($password) || empty($password)) throw new Exception('Password is required!', 1);

      // auth username & password
      $auth_user = $this->login_model->auth($username, $password);

      // check response code status
      if($auth_user["code_status"] != "s") throw new Exception($auth_user['message'], 1);

      // update last login user
      $update_last_login = $this->login_model->update_last_login($auth_user['data']->ID_USER);

      $remember = $this->input->post('remember');
      if (isset($remember)) setcookie('crm_tj_remember', $auth_user['data']->USERNAME, 2147485657, "/");

      // set session user
      $result = $this->set_session($auth_user['data']);

      $response = [
        'status'  => true,
        'url'     => $result['homepage_url'],
        'message' => 'Success Login',
        'type'    => 'success'
      ];

      echo json_encode($response);
    } catch (Exception $e) {
      $response = [
        'status'  => false,
        'message' => $e->getMessage(),
        'type'    => 'error'
      ];

      echo json_encode($response);
    }
  }

  private function set_session($data) {
    $work_center_group = '';
    if ($data->USER_ROLE == 'Admin Hangar' || $data->USER_ROLE == 'Controller Hangar') {
      $work_center_group = 'Hangar';
    } else if ($data->USER_ROLE == 'Controller Shop') {
      $work_center_group = 'Shop';
    }

    $work_center = $data->WORK_CENTER;

    $sess = array(
      'id_user'           => $data->ID_USER,    
      'name'              => $data->NAME,
      'username'          => $data->USERNAME,
      'user_group'        => $data->USER_GROUP,
      'user_role'         => $data->USER_ROLE,
      'work_center'       => $work_center == "" ? [] : explode(",", $work_center),
      'work_center_group' => $work_center_group
    );

    // get user menu
    $get_menu = $this->group_management_model->get_menu($data->USER_GROUP_ID);

    $homepage_url = '';
    foreach ($get_menu as $key => $val) {
      $homepage_url = $val[0]['url']; break;
    }

    $sess['homepage_url'] = $homepage_url;

    $menu = [];
    foreach ($get_menu as $sub_menu) {
      foreach ($sub_menu as $val) {
        $menu[] = strtolower($val['menu']);
      }
    }
    // use this session for check if the user has access
    $sess['menu'] = $menu;

    // features
    $sess['features'] = get_features($data->USER_ROLE);

    $this->session->set_userdata('crm_sess_logged_in', $sess);

    // set cookie for menu
    $cookie_name = 'crm_tj_menu';
    $cookie_value = json_encode($get_menu);
    setcookie($cookie_name, $cookie_value, 2147485657, "/");

    return [
      'homepage_url' => base_url().'index.php/' . $homepage_url
    ];
  }

  private function random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    
    for ($i = 0; $i < $length; $i++) {
      $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }

    return $randomString;
  }
  
}