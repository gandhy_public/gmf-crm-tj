<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('user_role_model');
    $this->load->model('user_group_model');
    $this->load->model('work_center_model');
    $this->load->model('group_management_model');
  }

  public function index() {
    redirect(base_url('index.php/management/user'));
  }

  public function user() {
    is_has_access('User');

    $data['page'] = 'User Management';
    $data['script'] = 'management/user/index_script';

    $data['user_role'] = $this->user_role_model->get_all();
    $data['user_group'] = $this->user_group_model->get_all();
    $data['work_center_group'] = $this->work_center_model->get_grouping_by_area();

    $this->view('management/user/index', $data);
  }

  public function group() {
    is_has_access('Group');

    $data['page'] = 'Group Management';
    $data['script'] = 'management/group/index_script';

    $data['user_role'] = $this->user_role_model->get_all();
    
    $data_menu = $this->group_management_model->get_menu_all();
    foreach ($data_menu as $item) {
      $menu_arr[$item->MODUL][] = [
        'id_menu'   => $item->ID_MENU,
        'menu'      => $item->MENU,
        'modul_id'  => $item->MODUL_ID,
      ];
    }
    $data['data_menu'] = $menu_arr;

    $this->view('management/group/index', $data);
  }
}