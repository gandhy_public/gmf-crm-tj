<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('daily_report_model');
  }

  public function index() {
    redirect(base_url('index.php/daily/create_daily_menu'));
  }

  public function create_daily_menu() {
    is_has_access('Create Daily Menu');

    $data['page'] = 'Daily';
    $data['script'] = 'daily/index_script';

    $data['act'] = 'create';
    $data['docno'] = '';
    $data['aufnr'] = json_encode([]);

    $this->view('daily/index', $data);
  }

  public function edit_daily_menu($docno) {
    is_has_access('Create Daily Menu');

    if ($docno) {
      $data['page'] = 'Daily';
      $data['script'] = 'daily/index_script';

      $data['act'] = 'edit';
      $data['docno'] = $docno;
      $data['aufnr'] = json_encode($this->daily_report_model->get_order_by_docno($docno));

      $this->view('daily/index', $data);
    } else {
      redirect(base_url('index.php/daily/create_daily_menu'));
    }
  }

  public function detail_daily_menu($docno) {
    is_has_access('Create Daily Menu');

    if ($docno) {
      $data['page'] = 'Daily';
      $data['script'] = 'daily/daily_report_detail_script';

      $data['docno'] = $docno;
      $data['created_at'] = $this->daily_report_model->get_time_created($docno);

      $this->view('daily/daily_report_detail', $data);
    } else {
      redirect(base_url('index.php/daily/create_daily_menu'));
    }
  }

  public function history() {
    is_has_access('Daily Menu History');

    $data['page'] = 'Daily';
    $data['script'] = 'daily/daily_report_history_script';
    
    $this->view('daily/daily_report_history', $data);
  }

  public function monitoring() {
    is_has_access('Daily Menu');
    
    setcookie('crm_tj_monitoring_daily', true, 2147485657, '/');

    $daily_menu = $this->daily_report_model->get_daily_menu_docno_today();
    if (count($daily_menu) > 0) {
      $data['docno'] = $daily_menu->DOCNO;
      $data['assign_date'] = $daily_menu->ASSIGNMENT_DATE;
    } else {
      $data['docno'] = '';
      $data['assign_date'] = '';
    }

    $this->load->view('daily/monitoring', $data);
  }
}