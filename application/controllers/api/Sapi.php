<?php

class Sapi extends CI_Controller {

  public function __construct() {
      parent::__construct();
      $this->load->database();
      $this->load->model('rfclog_model');

      ini_set("default_socket_timeout", 600);
  }

  public function create_job_by_revision($n = 'ZDIFO_TDE01_RFC_REVNR', $v = 'PM_TJ_REVISION') {
    $created_by = get_session('name');
    $P3 = $this->input->post('revision'); 

    $response = [];
    
    $this->read_job(false); // false mean it's no return

    $rl = $this->rfclog_model->check_already_created($n, $v);

    if ($rl == 0) {            
      $din = file_get_contents("http://192.168.240.107/sched_abs/create_bgjob2.php?p3=$P3");
      if ($din) {
        $din_json = json_decode($din);
        if ($din_json) {
          $submit = [
            'JOBNAME'   => $n,
            'VARIANT'   => $v,
            'RFCSTATUS' => 'CREATED',
            'JOBCOUNT'  => $din_json->JOBCOUNT,
            'EXEC_BY'   => $created_by,
            'REVISION'  => $P3
          ];
          $wl = $this->rfclog_model->create_log($submit);
        }
      }

      $response['status'] = 'success';
      $response['data'] = $wl;
    } else {
      $response['status'] = 'error';
      $response['error_message'] = 'Job already running';
    }

    echo json_encode($response);
  }

  public function read_job($return = true) {
    $response['status'] = null;
    $response['data'] = null;

    $ids = array();
    $rl = $this->rfclog_model->check_already_running();
    
    foreach ($rl as $rl_data) {
      $din = file_get_contents("http://192.168.240.107/sched_abs/read_bgjob.php?n=$rl_data->JOBNAME&c=$rl_data->JOBCOUNT");
      if ($din) {
        $din_json = json_decode($din);
        if ($din_json) {
          $response['status'] = 'success';
          array_push($ids, $rl_data->ID);
          if ($din_json->JOBSTATUS == 'F') {
            $submit = [
              'RFCSTATUS' => 'DONE',
              'JOBSTATUS' => $din_json->JOBSTATUS,
              'ENDTIME'   => raw_datetime_format($din_json->JOBENDDATE . $din_json->JOBENDTIME)
            ];

            $this->rfclog_model->update_log($rl_data->ID, $submit);
          } elseif ($din_json->JOBSTATUS == 'A') {
            $submit = [
              'RFCSTATUS' => 'CANCEL',
              'JOBSTATUS' => $din_json->JOBSTATUS,
              'ENDTIME' => raw_datetime_format($din_json->JOBENDDATE . $din_json->JOBENDTIME)
            ];
            $this->rfclog_model->update_log($rl_data->ID, $submit);
          }
        }
      }
    }

    if($ids) {
      $response['data'] = $this->rfclog_model->get_log_by_ids($ids);
    }

    if ($return) echo json_encode($response);
  }
}
