<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model("user_model");
    $this->load->model("user_role_model");
    $this->load->model("user_group_model");
    $this->load->model("work_center_model");
    $this->load->model("group_management_model");
  }

  public function create_user() {
    $response = [];

    try {
      $data = array(
        'name'          => $this->input->post('name'),
        'username'      => $this->input->post('username'),
        'password'      => $this->input->post('password'),
        'user_group_id' => $this->input->post('user_group_id'),
        'work_center'   => $this->input->post('work_center'),
        'is_active'     => 1,
        'is_superuser'  => 0,
        'created_at'    => date('Y-m-d H:i:s')
      );
  
      $ldap = $this->input->post('ldap');
  
      if($ldap == 'true') {
        $data['password'] = 'using ldap';
        $response = $this->_process_user($data);
      } else {
        $data['password'] = md5($data['password']);
        $response = $this->_process_user($data);
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function edit_user() {
    $response = [];

    try {
      $id_user = $this->input->post('id_user');
      $data = array(
        'name'          => $this->input->post('name'),
        'username'      => $this->input->post('username'),
        'user_group_id' => $this->input->post('user_group_id'),
        'work_center'   => $this->input->post('work_center') == '' ? null : $this->input->post('work_center'),
        'updated_at'    => date('Y-m-d H:i:s')
      );

      $user = $this->user_model->get_user_by_id($id_user);

      $check_username = $this->user_model->check_username($data['username'], $user->USERNAME);

      if($check_username){
        $response['status'] = 'failed';
        $response['validation'][] = array(
          'field'   => 'username',
          'message' => 'Username already exists'
        );
      }else{
        $this->user_model->update_user($data, $id_user);
        $response['status'] = 'success';
        $response['message'] = 'Successfully edit user!';
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function edit_password() {
    $response = [];

    try {
      $id_user = $this->input->post('id_user');
      $data = array(
        'password' => md5($this->input->post('password'))
      );

      $this->user_model->update_user($data, $id_user);

      $response['status'] = 'success';
      $response['message'] = 'Successfully edit password user!';
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_user_by_id() {
    $response = [];

    try {
      $id_user = $this->input->post('id_user');
      $data = $this->user_model->get_user_by_id($id_user);

      $response['status'] = 'success';
      $response['data'] = $data;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function toggle_user_status() {
    $response = [];
    
    try {
      $id_user = $this->input->post('id_user');
      $status = $this->input->post('status');

      $data = [
        'is_active'   => $status,
        'updated_at'  => date('Y-m-d H:i:s')
      ];
      $this->user_model->update_user($data, $id_user);

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function delete_user() {
    $response = [];
    
    try {
      $id_user = $this->input->post('id_user');
      $this->user_model->delete_user($id_user);

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  private function _process_user($data) {
    $response = [];

    $check_username = $this->user_model->check_username($data['username']);
    if($check_username){
      $response['status'] = 'failed';
      $response['validation'][] = array(
        'field'   => 'username',
        'message' => 'Username already exists'
      );
    }else{
      $this->user_model->create_user($data);
      $response['status'] = 'success';
      $response['message'] = 'Successfully add new user!';
    }
    return $response;
  }

  private function _check_ldap($username, $password) {
    $dn = "DC=gmf-aeroasia,DC=co,DC=id";
    $ip_ldap = [
        '0' => "192.168.240.57",
        '1' => "172.16.100.46"
    ];
    for($a=0;$a<count($ip_ldap);$a++){
        $ldapconn = ldap_connect($ip_ldap[$a]);
        if($ldapconn){
            break;
        }else{
            continue;
        }
    }
    
    //$ldapconn = ldap_connect("172.16.100.46") or die ("Could not connect to LDAP server.");
    if ($ldapconn) {
        ldap_set_option(@$ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option(@$ldap, LDAP_OPT_REFERRALS, 0);
        $ldapbind = ldap_bind($ldapconn, "ldap", "aeroasia");
        @$sr = ldap_search($ldapconn, $dn, "samaccountname=$username");
        @$srmail = ldap_search($ldapconn, $dn, "mail=$username@gmf-aeroasia.co.id");
        @$info = ldap_get_entries($ldapconn, @$sr);
        @$infomail = ldap_get_entries($ldapconn, @$srmail);
        @$usermail = substr(@$infomail[0]["mail"][0], 0, strpos(@$infomail[0]["mail"][0], '@'));
        @$bind = @ldap_bind($ldapconn, $info[0][dn], $password);
        /*print("<pre>".print_r(@$info,true)."</pre>");
        print("<pre>".print_r(@$infomail,true)."</pre>");
        print("<pre>".print_r(@$usermail,true)."</pre>");
        print("<pre>".print_r(@$bind,true)."</pre>");
        die();*/
        if(@$info['count']){
            if ((@$info[0]["samaccountname"][0] == $username AND $bind) AND (@$usermail == $username AND $bind)) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    } else {
        echo "LDAP Connection trouble, please try again 2/3 time";
    }
  }

  public function get_menu_by_group_id() {   
    $response = [];
    
    try {
      $id_user_group = $this->input->post('id_user_group');
      $data_menu = $this->group_management_model->get_menu_by_group_id($id_user_group);
      
      foreach ($data_menu as $item) {
        $menu_arr[$item->MODUL][] = [
          'id_menu'   => $item->ID_MENU,
          'menu'      => $item->MENU,
          'modul_id'  => $item->MODUL_ID,
        ];
      }

      $result = [
        'id_user_group' => $data_menu[0]->ID_USER_GROUP,
        'user_group'    => $data_menu[0]->USER_GROUP,
        'user_role_id'  => $data_menu[0]->USER_ROLE_ID,
        'menu'          => $menu_arr
      ];

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function create_group() {
    $response = [];
    
    try {
      $param = [
        'user_group'    => $this->input->post('user_group'),
        'user_role_id'  => $this->input->post('user_role_id'),
        'is_active'     => 1
      ];
  
      $user_group_id = $this->user_group_model->create_group($param);
  
      $modul = array_keys($this->input->post());
      for($i = 2; $i < count($modul); $i++){
        $modul_name = $modul[$i];
        for($j = 0; $j < count($_POST[$modul_name]); $j++){
          $menu_modul = explode("_", $_POST[$modul_name][$j]);
          $menu_id = $menu_modul[0];
          $modul_id = $menu_modul[1];
          $data_insert[] = [
            'user_group_id' => $user_group_id,
            'menu_id'       => $menu_id
          ];
        }
      }
  
      $this->group_management_model->create_menu_management($data_insert);

      $response['status'] = 'success';
      $response['message'] = 'Successfully create menu!';
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function update_group() {
    $response = [];
    
    try {
      $param = [
        'user_group'    => $this->input->post('user_group'),
        'user_role_id'  => $this->input->post('user_role_id')
      ];

      $user_group_id = $this->input->post('id_user_group');

      $this->user_group_model->update_group($param, $user_group_id);

      $modul = array_keys($this->input->post());
      for($i = 3; $i < count($modul); $i++){
        $modul_name = $modul[$i];
        for($j = 0; $j < count($_POST[$modul_name]); $j++){
          $menu_modul = $_POST[$modul_name][$j];
          $menu_modul = explode("_", $menu_modul);
          $menu_id = $menu_modul[0];
          $modul_id = $menu_modul[1];
          $data_insert[] = [
            'user_group_id' => $user_group_id,
            'menu_id'       => $menu_id
          ];
        }
      }

      $this->group_management_model->update_menu_management($data_insert, $user_group_id);
    
      $response['status'] = 'success';
      $response['message'] = 'Successfully update menu!';
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  function toggle_group_status() {
    $response = [];
    
    try {
      $id_user = $this->input->post('id_user');
      $this->user_model->delete_user($id_user);

      $id_user_group = $this->input->post('id_user_group');
      $status = $this->input->post('status');

      $check = $this->group_management_model->check_group_used($id_user_group);
      if($check) {
        $response['status'] = 'error';
        $response['message'] = 'Cannot delete the group, the group is being used!';
      } else {
        $this->user_group_model->update_group(['is_active' => $status], $user_group_id);
        $response['status'] = 'success';
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function ajax_list_users() {
    $users = $this->user_model->get_list_users();
    $records_total = $this->user_model->get_count_list_users();
    $records_filtered = $this->user_model->get_count_list_users_filtered();

    $data = array();
    $no = $_REQUEST['start'];
    foreach ($users as $item) {
      $switch_status = ($item->IS_ACTIVE > 0) ? 'checked' : '';

      $disabled = '';
      if(get_session('id_user') == $item->ID_USER) $disabled = 'disabled';

      $name = get_session('name') == $item->NAME ? $item->NAME . '<span class="is-label is-success" style="margin-left: 8px;">It\'s you</span>' : $item->NAME;

      $no++;
      $list = array();
      $list['no'] = $no;
      $list['name'] = $name;
      $list['username'] = $item->USERNAME;
      $list['user_role'] = $item->USER_ROLE;
      $list['work_center'] = $item->WORK_CENTER;
      $list['switch'] = "<input type='checkbox' class='switch' id-user='$item->ID_USER' name='$item->NAME' $switch_status $disabled>";
      $list['action'] = "<div class='action'>
        <i class='material-icons success edit-user' data-type='modal' modal-target='editUserModal' id-user='$item->ID_USER'>edit</i>
        <i class='material-icons warning edit-password' data-type='modal' modal-target='editPasswordModal' id-user='$item->ID_USER'>lock</i>
        <i class='material-icons danger delete-user $disabled' id-user='$item->ID_USER' name='$item->NAME'>delete</i></div>";

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data
    );

    echo json_encode($results);
  }

  function ajax_list_groups() {
    $groups = $this->user_group_model->get_list_groups();
    $records_total = $this->user_group_model->get_count_list_groups();
    $records_filtered = $this->user_group_model->get_count_list_groups_filtered();

    $data = array();
    $no = $_REQUEST['start'];
    foreach ($groups as $item) {
      $switch_status = ($item->IS_ACTIVE > 0) ? 'checked' : '';

      $no++;
      $list = array();
      $list['no'] = $no;
      $list['group_name'] = $item->USER_GROUP;
      $list['status'] = "<input type='checkbox' class='switch' id-user-group='$item->ID_USER_GROUP' user-group='$item->USER_GROUP' $switch_status>";
      $list['action'] = "<div class='action'><i class='material-icons success edit' data-type='modal' modal-target='groupModal' id-user-group='$item->ID_USER_GROUP'>edit</i></div>";

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data
    );

    echo json_encode($results);
  }

}