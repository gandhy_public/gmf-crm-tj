<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('revision_model');
    $this->load->model('pmorder_model');
    $this->load->model('project_planning_model');
    $this->load->model('area_model');
  }

  public function get_project_timeline() {
    $response = [];

    try {
      $param = $this->input->post();
      $data = $this->revision_model->get_projects($param);
      
      $hangar = $data['hangar'];
      $projects = $data['projects'];
  
      $result = [];
      foreach ($hangar as $key => $value) {
        $result['groups'][] = [
          'id'      => $value->VAWRK,
          'content' => $value->VAWRK
        ];
      }
  
      $id = 0;
      foreach ($projects as $key => $value) {
        $result['items'][] = [
          'id'        => $id,
          'group'     => $value->VAWRK,
          'content'   => "<span class='project'>" . trim($value->REVNR) . "</span>" . trim($value->REVTX),
          'start'     => date_to_timeline_format($value->REVBD).' 00:00:00',
          'end'       => date_to_timeline_format($value->REVED).' 23:59:59',
          'link'      => base_url('index.php/dashboard/detail_project/' . $value->REVNR),
          'className' => strtolower($value->TXT04)
        ];
        $id++;
      }

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_project_planning_detail() {
    $response = [];

    try {
      $result = [];

      $area = $this->area_model->get_only('AREA');

      $result['groups'] = [];
      foreach ($area as $val) {
        $result['groups'][] = [
          'id'      => $val,
          'content' => $val
        ];
      }

      $revision = $this->input->post('revision');
      $planning = $this->project_planning_model->get_project_planning($revision);

      $result['items'] = [];
      $id = 0;

      if (count($planning) > 0) {
        foreach ($planning as $val) {
          if($val->START_DATE != '1900-01-01' && $val->END_DATE != '1900-01-01') {
            $result['items'][] = [
              'id'        => $id,
              'group'     => trim($val->AREA),
              'content'   => $val->REMARKS == '' ? 'Empty' : $val->REMARKS,
              'start'     => $val->START_DATE.' 00:00:00',
              'end'       => $val->END_DATE.' 23:59:59',
              'className' => strtolower($val->PHASE)
            ];
            $id++;
          }
        }
      }

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_project_planning_detail_all() {
    $response = [];

    try {
      $result = [];

      $param_area = $this->input->post('area');
      $area = $this->area_model->get_only('AREA', $param_area);

      $result['groups'] = [];
      foreach ($area as $val) {
        $result['groups'][] = [
          'id'      => $val,
          'content' => $val
        ];
      }

      $param = $this->input->post();
      $planning = $this->project_planning_model->get_project_planning_all($param);

      $result['items'] = [];
      $id = 0;

      if (count($planning) > 0) {
        foreach ($planning as $val) {
          if($val->START_DATE != '1900-01-01' && $val->END_DATE != '1900-01-01') {
            $remarks = $val->REMARKS == '' ? '' : ': '.$val->REMARKS;            
            $content = '<strong>' . trim($val->TPLNR) . '</strong>' . $remarks;

            $result['items'][] = [
              'id'        => $id,
              'group'     => trim($val->AREA),
              'content'   => $content,
              'start'     => $val->START_DATE.' 00:00:00',
              'end'       => $val->END_DATE.' 23:59:59',
              'className' => strtolower($val->PHASE)
            ];
            $id++;
          }
        }
      }

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }
  
  public function get_material_status() {
    $response = [];

    try {
      $revision = $this->input->post('revision');

      $work_center_list = '';
      if (get_session('work_center_group') == 'Shop') {
        $work_center = get_session('work_center');
        if (count($work_center) > 0) {
          $work_center_list = implode("','", $work_center);
        }
      }
  
      $materials_value = $this->pmorder_model->get_materials_value($revision, $work_center_list);
  
      $material_status_color = [
        'Actual Nil Stock'                => '#FF4949',
        'Ordered By Purchasing'           => '#FFA500', 
        'Shipment/Custom Process'         => '#5B60EF', 
        'Provision in Store'              => '#0065FF', 
        'Preloaded in Hangar/Shop Store'  => '#23BF9B', 
        'Delivered to Production'         => '#13CE66'
      ];
  
      foreach ($materials_value as $status => $val) {
        $material_status = [
          'name'  => $status,
          'value' => $val,
          'color' => $material_status_color[$status]
        ];
        
        $result[] = $material_status;
      }

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_status_close_maintenance() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      $work_center_list = '';
      if (get_session('work_center_group') == 'Shop') {
        $work_center = get_session('work_center');
        if (count($work_center) > 0) {
          $work_center_list = implode("','", $work_center);
        }
      }

      $order_type_status = $this->pmorder_model->get_total_progress_status($revision, $work_center_list);

      $area = ['GAH', 'W101', 'W102', 'W103', 'W401', 'W402', 'W501', 'W502'];
      $status = ['Open', 'Progress', 'Close', 'Waiting RO', 'Waiting Material', 'Sent to Other'];
      $type = ['GA01', 'GA02', 'GA05'];

      $status_color = [
        'Open'              => '#ff4949',
        'Progress'          => '#0065FF',
        'Close'             => '#13CE66',
        'Waiting RO'        => '#FFAB00',
        'Waiting Material'  => '#5b60ef',
        'Sent to Other'     => '#23bf9b'
      ];

      $data = [];
      foreach ($status as $status_val) {
        foreach ($area as $area_val) {
          foreach ($type as $type_val) {
            $data[$status_val][$area_val][$type_val] = 0;
          }
        }
      }

      $grafik = $this->pmorder_model->get_progress_status_by_revision($revision, $work_center_list);
      foreach ($grafik as $key => $value) {
        $work_center = (strpos($value['WORK_CENTER'], 'GAH') !== false) ? 'GAH' : $value['WORK_CENTER'];
        $progress_status = (strpos($value['PROGRESS_STATUS'], 'Progress') !== false) ? 'Progress' : $value['PROGRESS_STATUS'];
        $data[$progress_status][$work_center][$value['AUART']] = $value['TOTAL'];
      }

      $val_grafik = [];
      $i = 0;
      foreach ($status as $s) {
        $chart_data = [];
        foreach ($data[$s] as $key => $value) {
          foreach (array_values($data[$s][$key]) as $val) {
            array_push($chart_data, $val);
          }
        }
        $val_grafik[$i] = [
          'name'  => $s,
          'color' => $status_color[$s],
          'data'  => $chart_data,
        ];
        $i++;
      }

      $day_maintenance = $this->revision_model->get_day_maintenance($revision);

      $result = [
        'order_type_status' => $order_type_status,
        'progress_status_chart' => $val_grafik,
        'progress_status_chart_color' => $status_color,
        'maintenance' => [
          'actual'  => $day_maintenance['actual'],
          'plan'    => $day_maintenance['plan']
        ]
      ];

      $response['status'] = "success";
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = "error";
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_manhours() {
    $response = [];

    try {
      $revision = $this->input->post('revision');

      $area = [
        'hangar' => 0, 
        'w101' => 0, 
        'w102' => 0, 
        'w103' => 0, 
        'w401' => 0, 
        'w402' => 0, 
        'w501' => 0, 
        'w502' => 0
      ];
  
      $manhours['plan'] = $area;
      $manhours['actual'] = $area;
  
      $data = $this->pmorder_model->get_total_manhours($revision);
      
      foreach ($data as $value_data) {
        $work_center = strtolower($value_data->ARBPL_BG);

        if($work_center == 'gah1' || $work_center == 'gah3' || $work_center == 'gah4' ) {
          $manhours['plan']['hangar'] += $value_data->PMHRS;
          $manhours['actual']['hangar'] += $value_data->AMHRS;
        }

        foreach ($area as $key => $value_area) {
          if($work_center == $key) {
            $manhours['plan'][$key] += $value_data->PMHRS;
            $manhours['actual'][$key] += $value_data->AMHRS;
          }
        }
      }

      $response['status'] = 'success';
      $response['data'] = $manhours;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

}
