<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
//lets Use the Spout Namespaces
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Import extends MY_Controller {
  
  public function __construct() {
    parent::__construct();  
    is_has_session($this->session->userdata('crm_sess_logged_in'));

    $this->load->model('pmorder_model');
    $this->load->model('revision_model');
    $this->load->model('project_planning_model');
  }

  public function sync_order() {
    $response = [];
    
    try {
      $file = explode(".", $_FILES['file']['name']);
      $file_type = strtolower($file[1]);
      $file_path = $_FILES['file']['tmp_name'];
      
      if($file_type != 'xlsx') {
        $response['status'] = 'failed';
        $response['error_message'] = 'File type is not supported';
      } else {
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($file_path);
        
        $order = [];
        foreach ($reader->getSheetIterator() as $sheet) {
          foreach ($sheet->getRowIterator() as $row) {
            if ($row[0] != '' && strtolower($row[0]) != 'order number') {
              $order[] = [$row[0], $row[1], $row[2]];
            }
          }
        }
        
        $reader->close();

        if (count($order) > 1) { // index 0 is title
          $update = $this->pmorder_model->sync_order($order);
        
          $response['status'] = 'success';
          $response['data'] = $update;
        } else {
          $response['status'] = 'failed';
          $response['error_message'] = "File is empty, please insert data before submit!";
        }
      }
      
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function sync_production_planning() {
    $response = [];

    try {
      $file = explode(".", $_FILES['file']['name']);
      $file_type = strtolower($file[1]);
      $file_path = $_FILES['file']['tmp_name'];
      
      if ($file_type != 'xlsx') {
        $response['status'] = 'failed';
        $response['error_message'] = 'File type is not supported';
      } else {
        $reader = ReaderFactory::create(Type::XLSX);
        $reader->open($file_path);
        
        $project_planning = [];
        $hilite = [];

        foreach ($reader->getSheetIterator() as $sheet) {
          if (strtolower($sheet->getName()) == 'project planning') {
            foreach ($sheet->getRowIterator() as $row) {
              if ($row[0] != '' && strtolower($row[0]) != 'revision') {
                if(!($row[3] instanceof DateTime)){
                  $start_date = '0000-00-00';
                }else{
                  $start_date = explode(" ", $row[3]->format('Y-m-d H:i:s'));
                  $start_date = $start_date[0];
                } 

                if(!($row[4] instanceof DateTime)){
                  $end_date = '0000-00-00';
                }else{
                  $end_date = explode(" ", $row[3]->format('Y-m-d H:i:s'));
                  $end_date = $end_date[0];
                } 

                $project_planning[] = [
                  'REVNR'       => $row[0],
                  'AREA'        => $row[1],
                  'PHASE'       => $row[2],
                  'START_DATE'  => $start_date,
                  'END_DATE'    => $end_date,
                  'REMARKS'     => $row[5]
                ];
              }
            }
          } else {
            foreach ($sheet->getRowIterator() as $row) {
              if ($row[0] != '' && strtolower($row[0]) != 'revision') {
                $hilite[] = [$row[0], $row[1]];
              }
            }
          }
        }
        
        $reader->close();

        if (count($project_planning) > 0 || count($hilite) > 0) { // index 0 is title
          $this->project_planning_model->sync_project_planning($project_planning);
          $this->revision_model->sync_hilite($hilite);

          $hilite_revision = [];
          foreach ($hilite as $val) {
            $hilite_revision[] = $val[0];
          }

          $is_top_project = $this->pmorder_model->is_top_project(array_unique($hilite_revision));
          if (count($is_top_project) > 0) {
            $resp = $this->revision_model->get_hilite(implode("','", $is_top_project), TRUE);
            $this->pusher->trigger('CRM-TJ-development', 'update_hilite', json_encode($resp));
          }
          
          $response['status'] = 'success';
          $response['data'] = true;
        } else {
          $response['status'] = 'failed';
          $response['error_message'] = 'File is empty, please insert data before submit!';
        }
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

}