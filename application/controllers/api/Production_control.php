<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production_control extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('global_model');
    $this->load->model('revision_model');
    $this->load->model('order_model');
    $this->load->model('pmorder_model');
    $this->load->model('order_view_model');
    $this->load->model('plant_model');
    $this->load->model('progress_status_model');
    $this->load->model('work_center_model');
  }

  public function ajax_list_all_order() {
    $results = $this->order_model->get_list_order('all');

    $orders = $results['result'];

    $plant = $this->plant_model->get_all();
    $progress_status = $this->progress_status_model->get_without_progress();
    array_splice($progress_status, 3, 0, ['Progress in']);

    $is_user_hangar = get_session('work_center_group') == 'Hangar' ? true : false;
    $work_center = get_session('work_center');
    
    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {
      $in_work_center = (count($work_center) == 0 || in_array($item->WORK_CENTER_BG, $work_center)) ? true : false;
      // if order has been move and waiting for approval
      // $disabled if not waiting approval, !$disabled is waiting approval
      $disabled = (!$in_work_center || ($item->IS_MOVING == 1 && $item->WORK_CENTER_BG != $item->WORK_CENTER)) ? true : false;

      $checkbox = '';
      if (is_has_feature('move_order') && !$disabled) {
        $checkbox = "<div class='checkbox'><label><input id='order-$item->AUFNR' class='check item' type='checkbox' name='orders[]' work_center='$item->WORK_CENTER' value='$item->AUFNR'/><span></span></label></div>";
      }

      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      // label approval
      $is_moving = ($item->IS_MOVING == 1 && $item->WORK_CENTER_BG != $item->WORK_CENTER) ? true : false;
      $approval = $is_moving ? "<span class='is-label is-success' style='margin-left:5px'>Waiting Approval</span>" : '';

      if (is_has_feature('move_order') && $is_moving) {
        $approval .= "<div style='margin-top:5px'><span class='is-label is-full-danger cancel-move' aufnr='$item->AUFNR' work_center='$item->WORK_CENTER'>Cancel</span></div>";
      }

      // set value progress status dropdown
      $status_dropdown = $item->PROGRESS_STATUS;
      if (is_has_feature('update_order_status') && !$disabled) {
        $status_dropdown = "<select class='select2 select2_progress_status' style='width:170px;' data-identity='$item->AUFNR'>";

        if(in_array($item->WORK_CENTER, $plant)) {
          $progress_status[3] = 'Progress in Hangar';
        } else {
          $progress_status[3] = 'Progress in '.$item->WORK_CENTER;
        }
  
        // if progress status value 'Progress in ...'
        if (strpos($item->PROGRESS_STATUS, 'Progress in') !== false) {
          $progress_status[3] = $item->PROGRESS_STATUS;
        }

        foreach ($progress_status as $status) {
          $selected = $status == $item->PROGRESS_STATUS ? "selected" : "";
          $status_dropdown .= "<option value='$status' data-action='update_status' $selected>$status</option>";
        }    

        $status_dropdown .= '</select>';
      }

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      if (is_has_feature('update_order_form') && !$disabled) {
        $target_date = "<input class='input-date target_date' type='text' value='$target_date_val' style='".target_date_bg($item->TARGET_DATE)."' data-aufnr='$item->AUFNR' data-column='TARGET_DATE' data-alias='Target Date' data-toggle='datepicker'>";
        $part_name = "<input type='text' class='input-editable partname-$item->AUFNR' style='min-width:100px' value='$item->PART_NAME' data-aufnr='$item->AUFNR' data-column='PART_NAME' data-alias='Part Name'>";
        $qty = "<input type='text' class='input-editable qty-$item->AUFNR' style='min-width:50px' value='$item->QTY' data-aufnr='$item->AUFNR' data-column='QTY' data-alias='Qty'>";
      } else {
        $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";
        $part_name = $item->PART_NAME;
        $qty = $item->QTY;
        $location = $item->LOCATION;
      }

      if (is_has_feature('update_location') && !$disabled) {
        $location_blank = $item->LOCATION == '' ? "<option value=''></option>" : "";
        $location = "<select class='select2 select2_location select-editable' data-aufnr='$item->AUFNR' data-column='LOCATION' data-alias='Location'>$location_blank";
        $location .= "<option value='shop' ". ($item->LOCATION == 'shop' ? 'selected' : '') .">Shop</option>";
        $location .= "<option value='insitu' ". ($item->LOCATION == 'insitu' ? 'selected' : '') .">Insitu</option></select>";
      } else {
        $location = $item->LOCATION;
      }

      if (is_has_feature('update_remarks') && !$disabled) {
        $remarks = "<input type='text' class='input-editable select2_remarks remarks-$item->AUFNR' style='min-width:200px' value='$item->REMARKS' data-aufnr='$item->AUFNR' data-column='REMARKS' data-alias='Remarks'>";
      } else {
        $remarks = $item->REMARKS;
      }

      $is_work_center_hangar = in_array($item->WORK_CENTER, $work_center) ? true : false; // 

      if ($is_user_hangar && !$is_work_center_hangar) {
        $checkbox = '';
        $status_dropdown = $item->PROGRESS_STATUS;
        $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";
        $remarks = $item->REMARKS;
        $part_name = $item->PART_NAME;
        $qty = $item->QTY;
        $location = $item->LOCATION;
      } 

      $no++;
      $list = [];
      $list['check'] = $checkbox;
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $aufnr.' '.$approval;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ARBPL;
      $list['moving'] = $item->WORK_CENTER;
      $list['status'] = $status_dropdown;
      $list['remarks'] = $remarks;
      $list['part_name'] = $part_name;
      $list['qty'] = $qty;
      $list['location'] = $location;
      $list['sap_status'] = $item->SAP_STATUS;

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
      'query'           => $results['query']
    );

    echo json_encode($results);
  }

  public function ajax_list_new_order() {
    $new = $this->global_model->get_count_new_order();
    if ($new > 0) {
      $results = $this->order_model->get_list_new_order();

      $orders = $results['result'];

      $records_total = $results['records_total'];
      $records_filtered = $results['records_filtered'];
    } else {
      $orders = [];
      $records_total = 0;
      $records_filtered = 0;
    }

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {     
      $checkbox = '';
      if (is_has_feature('move_order')) {
        $checkbox = "<div class='checkbox'><label><input id='order-$item->AUFNR' class='check item' type='checkbox' name='orders[]' work_center='$item->WORK_CENTER' value='$item->AUFNR'/><span></span></label></div>";
      }

      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['check'] = "<div class='checkbox'><label><input id='order-$item->AUFNR' class='check' type='checkbox' name='orders[]' value='$item->AUFNR' work_center_origin='$item->ARBPL' work_center_moving='$item->WORK_CENTER'/><span></span></label></div>";
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $aufnr;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ARBPL;
      $list['moving'] = $item->WORK_CENTER;
      $list['status'] = $item->PROGRESS_STATUS;
      $list['remarks'] = $item->REMARKS;
      $list['part_name'] = $item->PART_NAME;
      $list['qty'] = $item->QTY;
      $list['location'] = $item->LOCATION;
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data
    );

    echo json_encode($results);
  }

  public function ajax_list_close_order() {
    $results = $this->order_model->get_list_order('close');

    $orders = $results['result'];

    $plant = $this->plant_model->get_all();
    $progress_status = $this->progress_status_model->get_without_progress();

    $work_center = get_session('work_center');

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {
      $in_work_center = (count($work_center) == 0 || in_array($item->WORK_CENTER_BG, $work_center)) ? true : false;

      $checkbox = '';
      if (is_has_feature('update_order_status') && $in_work_center) {
        $checkbox = "<div class='checkbox'><label><input id='order-$item->AUFNR' class='check item' type='checkbox' name='orders[]' work_center='$item->WORK_CENTER' value='$item->AUFNR'/><span></span></label></div>";
      }

      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['check'] = $checkbox;
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $aufnr;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ARBPL;
      $list['moving'] = $item->WORK_CENTER;
      $list['remarks'] = $item->REMARKS;
      $list['part_name'] = $item->PART_NAME;
      $list['qty'] = $item->QTY;
      $list['location'] = $item->LOCATION;
      $list['sap_status'] = $item->SAP_STATUS;
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data
    );

    echo json_encode($results);
  }

  public function ajax_list_incoming_order() {
    $results = $this->order_model->get_list_incoming_order();

    $orders = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {     
      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $aufnr;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ORIGIN;
      $list['moving'] = $item->WORK_CENTER;
      $list['status'] = $item->PROGRESS_STATUS;
      $list['remarks'] = $item->REMARKS;
      $list['part_name'] = $item->PART_NAME;
      $list['qty'] = $item->QTY;
      $list['location'] = $item->LOCATION;
      
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
    );

    echo json_encode($results);
  }

  public function ajax_list_log_order() {
    $results = $this->order_model->get_list_log_order();

    $orders = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {     
      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $aufnr;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ARBPL;
      $list['moving'] = $item->WORK_CENTER;
      $list['status'] = $item->PROGRESS_STATUS;
      $list['remarks'] = $item->REMARKS;
      $list['part_name'] = $item->PART_NAME;
      $list['qty'] = $item->QTY;
      $list['location'] = $item->LOCATION;
      
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data
    );

    echo json_encode($results);
  }

  public function ajax_list_material() {
    $results = $this->order_model->get_list_material();

    $materials = $results['result'];

    $material_fulfillment_items = ['Actual Nil Stock', 'Ordered By Purchasing', 'Shipment/Custom Process', 'Provision in Store', 'Preloaded in Hangar/Shop Store', 'Delivered to Production'];
    $responsibility_items = ['(GAH) Hangar', '(WSCB) Monument Shop', '(WSSS) Seat Shop 1', '(WSSE) Seat Shop 2', '(WSSW) Sewing Shop', '(WSLS) Laundry Shop'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($materials as $item) {
      $checkbox = '';
      if (is_has_feature('update_material_form')) {
        $checkbox = "<div class='checkbox'><label><input class='check item' type='checkbox' name='materials[]' value='$item->REVNR|$item->AUFNR|$item->VORNR|$item->RSPOS|$item->MATNR'/><span></span></label></div>";
      }

      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $aufnr = $item->AUFNR;
      if (is_has_feature('view_detail_order')) {
        $aufnr = "<a href='javascript:void(0)' class='order-number' data-type='modal' modal-target='orderModal' aufnr='$item->AUFNR'>".$item->AUFNR."</a>";
      }

      $identity = "<input type='hidden' class='identity-$no' value='$item->REVNR|$item->AUFNR|$item->VORNR|$item->RSPOS|$item->MATNR'>";

      $no++;
      $list = [];
      $list['check'] = $checkbox;
      $list['no'] = $no;
      $list['revnr'] = $revnr . $identity;
      $list['order'] = $aufnr;
      $list['part_number'] = $item->MATNR;
      $list['reg'] = $item->TPLNR;
      $list['material_desc'] = $item->MAKTX;
      $list['material_type'] = $item->MTART;
      $list['qty'] = $item->BDMNG;
      $list['qty_total'] = $item->QTY_TOTAL;
      $list['uom'] = $item->MEINS_STATUS == 'TRUE' ? $item->MEINS : '';
      $list['uom_status'] = $item->MEINS_STATUS;
      
      if (is_has_feature('update_material_form')) {
        $responsibility_dropdown = "<select class='select2 select2_responsibility input-editable' data-id='$no'>";
        $responsibility_dropdown .= $item->RESPONSIBILITY == "" ? "<option value=''></option>" : "";
        foreach ($responsibility_items as $responsibility) {
          $selected = $responsibility == $item->RESPONSIBILITY ? "selected" : "";
          $responsibility_dropdown .= "<option value='$responsibility' $selected>$responsibility</option>";
        }  
        $responsibility_dropdown .= "</select>";

        $material_fulfillment_dropdown = "<select class='select2 select2_material_fulfillment input-editable' data-id='$no'>";
        $material_fulfillment_dropdown .= $item->MATERIAL_FULFILLMENT_STATUS == "" ? "<option value=''></option>" : "";
        foreach ($material_fulfillment_items as $material_fulfillment) {
          $selected = $material_fulfillment == $item->MATERIAL_FULFILLMENT_STATUS ? "selected" : "";
          $material_fulfillment_dropdown .= "<option value='$material_fulfillment' $selected>$material_fulfillment</option>";
        }  
        $material_fulfillment_dropdown .= "</select>";

        $list['responsibility'] = $responsibility_dropdown;
        $list['material_fulfillment'] = $material_fulfillment_dropdown;

        $list['part_number_alt'] = "<input type='text' class='input-editable' value='$item->MATNR_ALT' data-id='$no' data-column='MATNR_ALT' data-alias='Alternate Part Number'>";
        $list['remarks'] = "<input type='text' class='input-editable' value='$item->MATERIAL_REMARK' data-id='$no' data-column='MATERIAL_REMARK' data-alias='Remarks'>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Actual Nil Stock' ? 'disabled' : '';
        // $list['csp_rfq'] = "<input type='text' class='input-editable mfs csp_rfq' value='$item->CSP_RFQ' data-id='$no' data-column='CSP_RFQ' data-alias='CSP/RFQ' $disabled>";
        $csp_selected = $item->CSP_RFQ == 'CSP' ? 'selected' : '';
        $rfq_selected = $item->CSP_RFQ == 'RFQ' ? 'selected' : '';
        $csp_rfq_blank = $item->CSP_RFQ == '' ? "<option value=''></option>" : "";
        $list['csp_rfq'] = "<select class='select2 select2_csp_rfq input-editable select-editable mfs csp_rfq' data-id='$no' data-column='CSP_RFQ' data-alias='CSP/RFQ' $disabled>$csp_rfq_blank<option value='CSP' $csp_selected>CSP</option><option value='RFQ' $rfq_selected>RFQ</option></select>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Ordered By Purchasing' ? 'disabled' : '';
        $list['po_number'] = "<input type='text' class='input-editable mfs po_number' value='$item->PO_NUMBER' data-id='$no' data-column='PO_NUMBER' data-alias='PO Number' $disabled>";
        $list['lead_time'] = "<input type='text' class='input-editable mfs lead_time' value='$item->LEAD_TIME' data-id='$no' data-column='LEAD_TIME' data-alias='Lead Time' $disabled>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Shipment/Custom Process' ? 'disabled' : '';
        $list['awb'] = "<input type='text' class='input-editable mfs awb' value='$item->AWB' data-id='$no' data-column='AWB' data-alias='AWB' $disabled>";
        $list['inb'] = "<input type='text' class='input-editable mfs inb' value='$item->INB' data-id='$no' data-column='INB' data-alias='INB' $disabled>";
        $list['sp'] = "<input type='text' class='input-editable mfs sp' value='$item->SP' data-id='$no' data-column='SP' data-alias='SP' $disabled>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Provision in Store' ? 'disabled' : '';
        $list['sto'] = "<input type='text' class='input-editable mfs sto' value='$item->STO' data-id='$no' data-column='STO' data-alias='STO' $disabled>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Preloaded in Hangar/Shop Store' ? 'disabled' : '';
        $list['storage_location'] = "<input type='text' class='input-editable mfs storage_location' value='$item->STORAGE_LOCATION' data-id='$no' data-column='STORAGE_LOCATION' data-alias='Storage Location' $disabled>";
        
        $disabled = $item->MATERIAL_FULFILLMENT_STATUS != 'Delivered to Production' ? 'disabled' : '';
        $list['date_delivered'] = "<input type='text' class='input-editable mfs date_delivered' value='$item->DATE_DELIVERED' data-toggle='datepicker' data-id='$no' data-column='DATE_DELIVERED' data-alias='Date Delivered' $disabled>";
        $list['qty_delivered'] = "<input type='number' class='input-editable mfs qty_delivered' value='$item->QTY_DELIVERED' min='0' data-id='$no' data-column='QTY_DELIVERED' data-alias='Qty Delivered' $disabled>";
        $list['receiver_name'] = "<input type='text' class='input-editable mfs receiver_name' value='$item->RECEIVER_NAME' data-id='$no' data-column='RECEIVER_NAME' data-alias='Receiver Name' $disabled>";
      } else {
        $list['responsibility'] = $item->RESPONSIBILITY;
        $list['material_fulfillment'] = $item->MATERIAL_FULFILLMENT_STATUS;
        $list['part_number_alt'] = $item->MATNR_ALT;
        $list['remarks'] = $item->MATERIAL_REMARK;
        $list['csp_rfq'] = $item->CSP_RFQ;
        $list['po_number'] = $item->PO_NUMBER;
        $list['lead_time'] = $item->LEAD_TIME;
        $list['awb'] = $item->AWB;
        $list['inb'] = $item->INB;
        $list['sp'] = $item->SP;
        $list['sto'] = $item->STO;
        $list['storage_location'] = $item->STORAGE_LOCATION;
        $list['date_delivered'] = $item->DATE_DELIVERED;
        $list['qty_delivered'] = $item->QTY_DELIVERED;
        $list['receiver_name'] = $item->RECEIVER_NAME;
      }

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
      'query'           => $results['query']
    );

    echo json_encode($results);
  }

  public function ajax_view_list_order() {
    $results = $this->order_view_model->get_list_order();

    $orders = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {
      // if order has been move and waiting for approval
      $disabled = ($item->IS_MOVING == 1 && $item->WORK_CENTER_BG != $item->WORK_CENTER) ? true : false;

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $item->REVNR;

      // label approval
      $approval = $disabled ? "<span class='is-label is-success' style='margin-left:5px'>Waiting Approval</span>" : '';

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $item->AUFNR.' '.$approval;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['origin'] = $item->ARBPL;
      $list['moving'] = $item->WORK_CENTER;
      $list['status'] = $item->PROGRESS_STATUS;
      $list['remarks'] = $item->REMARKS;
      $list['part_name'] = $item->PART_NAME;
      $list['qty'] = $item->QTY;
      $list['location'] = $item->LOCATION;
      $list['sap_status'] = $item->SAP_STATUS;

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
      'query'           => $results['query']
    );

    echo json_encode($results);
  }

  public function ajax_view_list_material() {
    $results = $this->order_view_model->get_list_material();

    $materials = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($materials as $item) {
      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $item->REVNR;

      $no++;
      $list = [];
      $list['no'] = $no;
      $list['revnr'] = $revnr;
      $list['order'] = $item->AUFNR;
      $list['part_number'] = $item->MATNR;
      $list['reg'] = $item->TPLNR;
      $list['material_desc'] = $item->MAKTX;
      $list['material_type'] = $item->MTART;
      $list['qty'] = $item->BDMNG;
      $list['qty_total'] = $item->QTY_TOTAL;
      $list['uom'] = $item->MEINS_STATUS == 'TRUE' ? $item->MEINS : '';
      $list['uom_status'] = $item->MEINS_STATUS;
      $list['responsibility'] = $item->RESPONSIBILITY;
      $list['material_fulfillment'] = $item->MATERIAL_FULFILLMENT_STATUS;
      $list['part_number_alt'] = $item->MATNR_ALT;
      $list['remarks'] = $item->MATERIAL_REMARK;
      $list['csp_rfq'] = $item->CSP_RFQ;
      $list['po_number'] = $item->PO_NUMBER;
      $list['lead_time'] = $item->LEAD_TIME;
      $list['awb'] = $item->AWB;
      $list['inb'] = $item->INB;
      $list['sp'] = $item->SP;
      $list['sto'] = $item->STO;
      $list['storage_location'] = $item->STORAGE_LOCATION;
      $list['date_delivered'] = $item->DATE_DELIVERED;
      $list['qty_delivered'] = $item->QTY_DELIVERED;
      $list['receiver_name'] = $item->RECEIVER_NAME;

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
      'query'           => $results['query']
    );

    echo json_encode($results);
  }

  public function update_progress_status() {
    $response = [];

    try {
      $aufnr = $this->input->post('aufnr');
      $status = $this->input->post('status');

      if(!empty($aufnr) && !empty($status)){
        $update = $this->pmorder_model->update_progress_status($aufnr, $status);

        $response['status'] = 'success';
        $response['data'] = true;
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function move_order() {
    try {
      $aufnr = $this->input->post('aufnr');
      $work_center_curr = $this->input->post('work_center_curr');
      $work_center_dest = $this->input->post('work_center_dest');
      
      if(!empty($aufnr)) {
        $update = $this->pmorder_model->move_order($aufnr, $work_center_curr, $work_center_dest);

        $resp = array(
          'action'      => 'move',
          'work_center' => [$work_center_dest],
          'total'       => count($aufnr)
        );
        $this->pusher->trigger('CRM-TJ-development', 'update_new_order', json_encode($resp));

        $response['status'] = 'success';
        $response['message'] = 'Successfully update Work Center!';
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function approve_order() {
    $response = [];

    try {
      $param = $this->input->post();

      $submit = array();
      if(!empty($param['aufnr'])){
        $approve = $this->pmorder_model->approve_order($param);

        $resp = array(
          'action'      => 'approve',
          'work_center' => array_unique($param['work_center_moving']),
          'total'       => $this->global_model->get_count_new_order()
        );
        
        $this->pusher->trigger('CRM-TJ-development', 'update_new_order', json_encode($resp));

        $response['status'] = 'success';
        $response['data'] = $approve;
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function cancel_move_order() {
    try {
      $aufnr = $this->input->post('aufnr');
      $work_center_dest = $this->input->post('work_center_dest');
      $act = $this->input->post('act');
      
      if($aufnr) {
        $update = $this->pmorder_model->cancel_move_order($aufnr);

        if ($act == 'cancel') {
          $resp = array(
            'action'      => $act,
            'work_center' => array_unique($work_center_dest),
            'total'       => count($aufnr)
          );
          $this->pusher->trigger('CRM-TJ-development', 'update_new_order', json_encode($resp));

          $message = 'Order has been canceled';
        } else {
          $message = 'Order has been declined';
        }

        $response['status'] = 'success';
        $response['message'] = $message;
      }
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function detail_order() {
    $aufnr = $this->input->post('aufnr');
    
    if(!empty($aufnr)) {
      $detail_order = $this->pmorder_model->view_detail_order($aufnr);
      echo json_encode($detail_order);
    }
  }

  public function update_order_per_column() {
    $response = [];

    try {
      $param = $this->input->post();

      $update = $this->pmorder_model->update_order_per_column($param);    

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function update_material_per_column() {
    $response = [];

    try {
      $submit = array(
        'revnr'   => $this->input->post('revnr'),
        'aufnr'   => $this->input->post('aufnr'),
        'vornr'   => $this->input->post('vornr'),
        'rspos'   => $this->input->post('rspos'),
        'column'  => $this->input->post('column'),
        'value'   => $this->input->post('value')
      );
  
      $update = $this->pmorder_model->update_material_per_column($submit);  
      
      $is_top_project = $this->pmorder_model->is_top_project([$submit['revnr']]);
  
      if ($submit['column'] == 'MATERIAL_FULFILLMENT_STATUS' && count($is_top_project) > 0) {
        $resp = $this->calc_material_status($is_top_project);
        $this->pusher->trigger('CRM-TJ-development', 'update_material_status', json_encode($resp));
      }

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function update_material_multiple() {
    $response = [];

    try {
      $param = $this->input->post();
  
      $update = $this->pmorder_model->update_material_multiple($param);  
      
      $revision = [];
      for ($i = 0; $i < count($param['target']); $i++) { 
        $split = explode('|', $param['target'][$i]);
        $revision[] = $split[0];
      }
  
      $is_top_project = $this->pmorder_model->is_top_project(array_unique($revision));
  
      if ($param['column'] == 'MATERIAL_FULFILLMENT_STATUS' && count($is_top_project) > 0) {
        $resp = $this->calc_material_status($is_top_project);
        $this->pusher->trigger('CRM-TJ-development', 'update_material_status', json_encode($resp));
      }

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_new_order_count() {
    echo $this->global_model->get_count_new_order();
  }

  private function calc_material_status($revision) {
    $revision = implode("','", $revision);
    $materials_value = $this->pmorder_model->get_materials_value($revision, "", TRUE);

    foreach ($materials_value as $revnr => $val) {
      foreach ($materials_value[$revnr] as $status => $val) {
        $material_status = [
          'name'  => $status,
          'value' => $val,
        ];
        
        $result[$revnr][] = $material_status;
      }
    }

    return $result;
  }

  public function get_data_top_project() {
    $response = [];

    try {
      $revision_list = $this->input->post('revision');
      $progress_status = $this->input->post('progress_status');
      
      $param_work_center = "";
      if (get_session('work_center_group') == 'Shop') {
        $work_center = get_session('work_center');
        if (count($work_center) > 0) $param_work_center = implode("','", $work_center);
      }

      $revision = implode("','", $revision_list);

      $result = [];
      foreach ($revision_list as $val) {
        $result[$val] = [];
      }

      // day maintenance
      $day_maintenance = $this->revision_model->get_day_maintenance($revision, TRUE);
      foreach ($day_maintenance as $revnr => $val) {
        $result[$revnr]['maintenance'] = $val;
      }

      // order close percentage
      $order_close_percentage = $this->pmorder_model->get_order_close_percentage($revision, $param_work_center, TRUE);
      foreach ($order_close_percentage as $revnr => $val) {
        $result[$revnr]['order_close_percentage'] = $val;
      }

      // get hilite
      $hilite = $this->revision_model->get_hilite($revision, TRUE);
      foreach ($hilite as $revnr => $val) {
        $result[$revnr]['hilite'] = $val;
      }

      // get material status
      $materials_value = $this->pmorder_model->get_materials_value($revision, $param_work_center, TRUE);

      $material_status_color = [
        'Actual Nil Stock'                => '#FF4949',
        'Ordered By Purchasing'           => '#FFA500', 
        'Shipment/Custom Process'         => '#5B60EF', 
        'Provision in Store'              => '#0065FF', 
        'Preloaded in Hangar/Shop Store'  => '#23BF9B', 
        'Delivered to Production'         => '#13CE66'
      ];

      foreach ($materials_value as $revnr => $val) {
        foreach ($materials_value[$revnr] as $status => $val) {
          $material_status = [
            'name'  => $status,
            'value' => $val,
            'color' => $material_status_color[$status]
          ];
          
          $result[$revnr]['material_status'][] = $material_status;
        }
      }

      // get progress status
      $progress_status = $this->pmorder_model->get_total_progress_status_monitoring($revision, $param_work_center, $progress_status);
      foreach ($progress_status as $revnr => $val) {
        $result[$revnr]['progress_status'] = $val;
      }

      $response['status'] = 'success';
      $response['data'] = $result;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

}