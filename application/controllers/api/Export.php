<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'/third_party/spout/src/Spout/Autoloader/autoload.php';
//lets Use the Spout Namespaces
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Export extends MY_Controller {
  
  public function __construct() {
    parent::__construct();  
    is_has_session($this->session->userdata('crm_sess_logged_in'));
    
    $this->load->model('export_model');

    $this->writer = WriterFactory::create(Type::XLSX);
    $this->headerStyle = (new StyleBuilder())->setFontBold()->setFontName('Arial')->setFontSize(10)->build();
    $this->contentStyle = (new StyleBuilder())->setFontName('Arial')->setFontSize(10)->setShouldWrapText(false)->build();
  }

  public function excel() {
    $page = $this->input->post('page');
    $query = $this->input->post('query');

    if (isset($page) && isset($query)) {
      $this->writer->openToBrowser(date('Ymd') . '_' . $page . '.xlsx');

      $data = $this->export_model->get_data($query);
      
      $header = [];
      $content = [];

      if (count($data) > 0) {
        if ($page == 'List Order') {
          $header = $this->get_data_list_order($data)['header'];
          $content = $this->get_data_list_order($data)['content'];
        } else if ($page == 'List Material') {
          $header = $this->get_data_list_material($data)['header'];
          $content = $this->get_data_list_material($data)['content'];
        } else if ($page == 'Daily Menu') {
          $header = $this->get_data_list_report_detail($data)['header'];
          $content = $this->get_data_list_report_detail($data)['content'];
        }
      }

      $this->writer->addRowWithStyle($header, $this->headerStyle);
      $this->writer->addRowsWithStyle($content, $this->contentStyle);

      $this->writer->close();  
    }
  }

  public function get_data_list_report_detail($data) {
    $header = $headerFix = $content = [];

    $except = ['ID_DAILY_REPORT', 'CREATED_AT', 'ROWNUM'];
    foreach ($data[0] as $key => $val) {
      if (!in_array($key, $except)) {
        $header[] = $key;   
      }
    }

    foreach ($header as $val) {
      $headerFix[] = $this->get_header_name($val);
    }

    foreach ($data as $key => $val) {
      foreach ((array) $val as $keyData => $valData) {
        if (in_array($keyData, $header)) {
          $content[$key][] = $valData;
        }
      }
    }

    return [
      'header'  => $headerFix,
      'content' => $content
    ];
  }

  public function get_data_list_order($data) {
    $header = $headerFix = $content = [];

    $except = ['ROW_ID', 'APPROVED', 'TOP_ORDER', 'TOP_OPERATION', 'VAWRK', 'IS_MOVING', 'WORK_CENTER_BG'];
    foreach ($data[0] as $key => $val) {
      if (!in_array($key, $except)) {
        $header[] = $key;   
      }
    }

    foreach ($header as $val) {
      $headerFix[] = $this->get_header_name($val);
    }

    foreach ($data as $key => $val) {
      foreach ((array) $val as $keyData => $valData) {
        if (in_array($keyData, $header)) {
          $content[$key][] = $valData;
        }
      }
    }

    return [
      'header'  => $headerFix,
      'content' => $content
    ];
  }

  public function get_data_list_material($data) {
    $header = $headerFix = $content = [];

    $except = ['ROW_ID', 'VORNR', 'RSPOS'];
    foreach ($data[0] as $key => $val) {
      if (!in_array($key, $except)) {
        $header[] = $key;   
      }
    }

    foreach ($header as $val) {
      $headerFix[] = $this->get_header_name($val);
    }

    foreach ($data as $key => $val) {
      foreach ((array) $val as $keyData => $valData) {
        if (in_array($keyData, $header)) {
          $content[$key][] = $valData;
        }
      }
    }

    return [
      'header'  => $headerFix,
      'content' => $content
    ];
  }

  private function get_header_name($key) {
    $header = array(
      'TOP_ORDER' => 'TOP_ORDER',
      'TOP_OPERATION' => 'TOP_OPERATION',
      'REVNR' => 'REVISION',
      'AUFNR' => 'ORDER',
      'VORNR' => 'VORNR',
      'RSPOS' => 'RSPOS',
      'AUART' => 'ORDER TYPE',
      'MATNR' => 'PART NUMBER',
      'MATNR_ALT' => 'ALT PART NUMBER',
      'MAKTX' => 'MATERIAL DESC',
      'MTART' => 'MATERIAL TYPE',
      'KTEXT' => 'DESCRIPTION',
      'TPLNR' => 'A/C REG',
      'ARBPL' => 'WORK CENTER ORIGIN',
      'PMHRS' => 'PMHRS (HOURS)',
      'AMHRS' => 'AMHRS (HOURS)',
      'WORK_CENTER' => 'WORK CENTER',
      'WORK_CENTER_BG' => 'WORK CENTER BG',
      'TARGET_DATE' => 'TARGET DATE',
      'PROGRESS_STATUS' => 'PROGRESS STATUS',
      'REMARKS' => 'REMARKS',
      'PART_NAME' => 'PART NAME',
      'QTY' => 'QTY',
      'LOCATION' => 'LOCATION',
      'SAP_STATUS' => 'SAP STATUS',
      'IS_MOVING' => 'IS MOVING',
      'VAWRK' => 'VAWRK',
      'BDMNG' => 'QTY ON THE TASK',
      'QTY_TOTAL' => 'QTY ON ALL TASK',
      'MEINS' => 'UOM',
      'RESPONSIBILITY' => 'RESPONSIBILITY',
      'MATERIAL_FULFILLMENT_STATUS' => 'MATERIAL FULFILLMENT STATUS',
      'MATERIAL_REMARK' => 'MATERIAL REMARK',
      'CSP_RFQ' => 'CSP/RFQ',
      'PO_NUMBER' => 'PO NUMBER',
      'LEAD_TIME' => 'LEAD TIME',
      'AWB' => 'AWB',
      'INB' => 'INB',
      'SP' => 'SP',
      'STO' => 'STO',
      'STORAGE_LOCATION' => 'STORAGE LOCATION',
      'DATE_DELIVERED' => 'DATE DELIVERED',
      'QTY_DELIVERED' => 'QTY DELIVERED',
      'RECEIVER_NAME' => 'RECEIVER NAME',
      'DOCNO' => 'DOCNO',
      'PLAN_TARGET' => 'PLAN_TARGET',
      'HIGHLIGHT' => 'HIGHLIGHT',
    );
    return $header[$key];
  }
}