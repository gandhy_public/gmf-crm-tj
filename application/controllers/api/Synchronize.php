<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Synchronize extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('rfclog_model');
  }

  public function ajax_list_sync() {
    $results = $this->rfclog_model->ajax_list_sync();

    $syncs = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($syncs as $item) {
      $list = [];
      $list['crm'] = $item->CRM;
      $list['project'] = $item->REVISION;
      $list['created_at'] = $item->DTIME;
      $list['created_by'] = $item->EXEC_BY;
      $list['job_status'] = "<span class='is-label ".job_interface_status($item->JOBSTATUS)."'>$item->JOBSTATUS</span>";
      $list['interface_status'] = "<span class='is-label ".job_interface_status($item->RFCSTATUS)."'>$item->RFCSTATUS</span>";

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data
    );

    echo json_encode($results);
  }

}