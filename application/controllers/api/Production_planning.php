<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production_planning extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('revision_model');
    $this->load->model('area_model');
    $this->load->model('project_planning_model');
  }

  public function ajax_list_projects() {
    $projects = $this->revision_model->get_list_projects();
    $records_total = $this->revision_model->get_count_list_projects();
    $records_filtered = $this->revision_model->get_count_list_projects_filtered();

    $data = array();
    $no = $_REQUEST['start'];
    foreach ($projects as $item) {
      $checkbox_status = ($item->TOP_PROJECT > 0) ? 'checked disabled' : '';
      $switch_status = ($item->TOP_PROJECT > 0) ? 'checked' : '';
      $top_project = "<div class='flex-center'><input type='checkbox' class='switch unset' revnr='$item->REVNR' $switch_status></div>";
      
      $revnr = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $sync = '';
      if (is_has_feature('prod_planning_act')) {
        $sync = "<i class='material-icons warning sync' revnr='$item->REVNR' style='margin-left:10px'>sync</i>";
      }

      $status_color = '';
      if ($item->STATUS_PLN == 'NEED REVIEW') {
        $status_color = 'is-danger';
      } else if ($item->STATUS_PLN == 'REVIEWED') {
        $status_color = 'is-success';
      } else if ($item->STATUS_PLN == 'UPDATED') {
        $status_color = 'is-warning';
      }

      $status_label = '';
      if ($item->STATUS_PLN != '') {
        $status_label = "<span class='is-label $status_color status-planning' style='cursor:pointer' data-type='modal' modal-target='logModal' revnr='$item->REVNR' status='$item->STATUS_PLN'>$item->STATUS_PLN</span>";
      }

      $planning = '';
      if($item->STATUS_PLN != '') {
        $planning = "<div class='action'><i class='material-icons primary view-plan' data-type='modal' modal-target='planningModal' act='edit' revnr='$item->REVNR' status='$item->STATUS_PLN'>assignment_turned_in</i></div>";
      } else {
        if (is_has_feature('create_update_planning') && $item->STATUS_PLN == '') {
          $planning = "<div class='action'><i class='material-icons success set-plan' data-type='modal' modal-target='planningModal' act='create' revnr='$item->REVNR'>create</i></div>";
        }
      }

      $hilite = "<div class='action'><i class='material-icons warning hilite' data-type='modal' modal-target='hiliteModal' revnr='$item->REVNR'>notes</i></div>";

      $no++;
      $list = array();
      $list['no'] = $no;
      $list['project'] = "<div class='flex-middle'>".$revnr.$sync."</div>";
      $list['reg'] = $item->TPLNR;
      $list['hangar'] = $item->VAWRK;
      $list['maint'] = $item->REVTX;
      $list['top_project'] = $top_project;
      $list['update_date'] = substr($item->UPDATE_DATE, 0, -4);
      $list['status'] = $status_label;
      $list['planning'] = $planning;
      $list['hilite'] = $hilite;
      $list['start'] = get_date_formatv2($item->REVBD);
      $list['finish'] = get_date_formatv2($item->REVED);

      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data,
    );

    echo json_encode($results);
  }

  public function toggle_top_project() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      $status = $this->input->post('status');

      $update = $this->revision_model->update_top_project($revision, $status);

      $resp['status'] = true;
      $this->pusher->trigger('CRM-TJ-development', 'update_top_project', json_encode($resp));

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function initial_planning() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      
      $project_planning_info = $this->project_planning_model->get_project_planning_info($revision); 
      $get_project_planning = $this->project_planning_model->get_project_planning($revision, 'initial');

      $response['status'] = 'success';
      $response['data'] = [
        'project_planning_info' => $project_planning_info,
        'project_planning'      => $get_project_planning
      ];
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function save_planning() {
    $response = [];
    
    try {
      $id_project_planning = $this->input->post('id_project_planning');
      $act = $this->input->post('act');
      $revision = $this->input->post('revision');
      $area = $this->input->post('area');
      $phase = $this->input->post('phase');
      $start_date = $this->input->post('start_date');
      $end_date = $this->input->post('end_date');
      $remarks = $this->input->post('remarks');
      $delete = $this->input->post('deleted_planning');

      // update status planning
      $this->revision_model->set_status_planning($revision, $act);

      $insert = $update = [];
      foreach ($id_project_planning as $key => $id_planning) {
        if ($id_planning == 0 || $id_planning == "0") {
          $insert[] = array(
            'REVNR'         => $revision,
            'AREA'          => $area[$key],
            'PHASE'         => $phase[$key],
            'START_DATE'    => $start_date[$key],
            'END_DATE'      => $end_date[$key],
            'REMARKS'       => $remarks[$key],
            'INSERT_DATE'   => date('Y-m-d h:i:s')
          );
        } else {
          $update[] = array(
            'ID_PROJECT_PLANNING' => $id_planning,
            'AREA'          => $area[$key],
            'PHASE'         => $phase[$key],
            'START_DATE'    => $start_date[$key],
            'END_DATE'      => $end_date[$key],
            'REMARKS'       => $remarks[$key],
            'UPDATE_DATE'   => date('Y-m-d h:i:s')
          );
        }
      }

      // insert planning
      if (count($insert) > 0) $this->project_planning_model->insert_project_planning($insert);

      // update planning
      if (count($update) > 0) $this->project_planning_model->update_project_planning($update);

      // delete planning
      if (count($delete) > 0) $this->project_planning_model->delete_project_planning($delete);

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }  

  public function confirm_planning() {
    $response = [];
    
    try {
      $revision = $this->input->post('revision');
      $this->revision_model->set_status_planning($revision, 'confirm');

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_hilite() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      $hilite = $this->revision_model->get_hilite($revision);

      $response['status'] = 'success';
      $response['data'] = $hilite;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function save_hilite() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      $hilite = $this->input->post('hilite');

      $this->revision_model->submit_hilite($revision, $hilite);

      $resp['revision'] = $revision;
      $resp['hilite'] = $hilite;
      $this->pusher->trigger('CRM-TJ-development', 'update_hilite', json_encode([$revision => $hilite]));

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function get_log_planning() {
    $response = [];

    try {
      $revision = $this->input->post('revision');
      $log = $this->project_planning_model->get_log_planning($revision);

      $response['status'] = 'success';
      $response['data'] = $log;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }
}