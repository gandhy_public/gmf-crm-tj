<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('order_model');
    $this->load->model('daily_report_model');
  }

  public function ajax_list_orders() {
    $results = $this->order_model->get_list_order('daily');

    $orders = $results['result'];

    $no = $_REQUEST['start'];
    $data = [];
    foreach ($orders as $item) {
      $checkbox = '';
      if (is_has_feature('create_daily_menu')){
        $checkbox = "<div class='checkbox'><label><input id='order-$item->AUFNR' class='check' type='checkbox' name='orders[]' value='$item->AUFNR' revnr='$item->REVNR'/><span></span></label></div>";
      }

      $revnr_val = $item->REVNR;
      if (is_has_feature('view_detail_project')) {
        $revnr_val = "<a href='" . base_url('index.php/dashboard/detail_project/'.$item->REVNR) . "' target='_blank'>$item->REVNR</a>";
      }

      $revnr = $item->REVNR == 'Non Project' ? "<span class='is-label is-dark'>$item->REVNR</span>" : $revnr_val;

      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";

      $no++;
      $list = [];
      $list['check'] = $checkbox;
      $list['no'] = $no;
      $list['project'] = $revnr;
      $list['order'] = $item->AUFNR;
      $list['type'] = "<span class='is-label ".auart_label_color($item->AUART)."'>$item->AUART</span>";
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['work_center'] = $item->WORK_CENTER;
      $list['status'] = $item->PROGRESS_STATUS;
      $list['plan_manhours'] = $item->PMHRS;
      $list['actual_manhours'] = $item->AMHRS;
      $list['location'] = $item->LOCATION;
      
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $results['records_total'],
      'recordsFiltered' => $results['records_filtered'],
      'data'            => $data,
    );

    echo json_encode($results);
  }

  public function ajax_list_daily_report_detail($docno) {
    $reports = $this->daily_report_model->get_list_daily_report_by_docno($docno)['result'];
    $query = $this->daily_report_model->get_list_daily_report_by_docno($docno)['query'];
    $records_total = count($reports);
    $records_filtered = count($reports);

    $no = $_REQUEST['start'];
    $data = array();
    foreach ($reports as $item) {
      $target_date_val = $item->TARGET_DATE == '1900-01-01' ? '' : $item->TARGET_DATE;
      $target_date = "<span class='is-label-lg' style='".target_date_bg($target_date_val)."white-space: nowrap'>$target_date_val</span>";
      
      $no++;
      $list = array();
      $list['no'] = $no;
      $list['order'] = $item->AUFNR;
      $list['type'] = $item->AUART;
      $list['desc'] = $item->KTEXT;
      $list['reg'] = $item->TPLNR;
      $list['target_date'] = $target_date;
      $list['pmhrs'] = $item->PMHRS;
      $list['amhrs'] = $item->AMHRS;
      $list['plan_target'] = $item->PLAN_TARGET;
      $list['highlight'] = $item->HIGHLIGHT;
      $list['status'] = $item->STATUS;
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data,
      'query'           => $query
    );

    echo json_encode($results);
  }

  public function ajax_list_report_history() {
    $data = $this->daily_report_model->get_list_daily_report_history();

    $reports = $data['result'];
    $records_total = $this->daily_report_model->get_count_list_daily_report_history();
    $records_filtered = $data['is_filtered'] == true ? $this->daily_report_model->get_count_list_daily_report_history_filtered() : $records_total;

    $no = $_REQUEST['start'];
    $data = array();
    foreach ($reports as $item) {
      $assignment_date_val = $item->ASSIGNMENT_DATE == '1900-01-01' ? '' : $item->ASSIGNMENT_DATE;

      $disabled = get_session('id_user') != $item->USER_ID ? 'disabled' : '';

      $assignment_date = "<input class='input-date assignment_date' type='text' value='$assignment_date_val' style='text-align:center' data-docno='$item->DOCNO' data-column='ASSIGNMENT_DATE' data-toggle='datepicker' $disabled>";

      $no++;
      $list = array();
      $list['no'] = $no;
      $list['docno'] = $item->DOCNO;
      $list['assignment_date'] = $assignment_date;
      $list['created_by'] = $item->CREATED_BY;
      $list['created_at'] = $item->CREATED_AT;
      $list['action'] = "<div class='action'>
        <i class='material-icons success detail' link='".base_url('index.php/daily/detail_daily_menu/'.$item->DOCNO)."'>visibility</i>
        <i class='material-icons warning edit' link='".base_url('index.php/daily/edit_daily_menu/'.$item->DOCNO)."'>edit</i>
        <i class='material-icons primary print' link='".base_url('index.php/daily/print_to_device/'.$item->DOCNO)."'>print</i></action>";
      $data[] = $list;
    }

    $results = array(
      'draw'            => $_REQUEST['draw'],
      'recordsTotal'    => $records_total,
      'recordsFiltered' => $records_filtered,
      'data'            => $data
    );

    echo json_encode($results);
  }

  public function submit_daily_menu() {
    $response = [];
    
    try {
      $param = $this->input->post();
      $assign_date = $this->input->post('assignment_date');
      $deleted_daily_menu = $this->input->post('deleted_daily_menu');

      $docno = $this->daily_report_model->submit_daily_report($param);

      if (count($deleted_daily_menu) > 0) $this->daily_report_model->delete_daily_report_by_id($deleted_daily_menu);

      $resp = array(
        'user_id'     => get_session('id_user'),
        'assign_date' => $assign_date,
      );
      
      $this->pusher->trigger('CRM-TJ-development', 'update_daily_menu', json_encode($resp));

      $response['status'] = 'success';
      $response['data'] = ['docno' => $docno];
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  function get_daily_menu() {
    $response = [];

    try {
      $docno = $this->input->post('docno');
      $data = $this->daily_report_model->get_list_daily_report_by_docno($docno)['result'];

      $response['status'] = 'success';
      $response['data'] = $data;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }
  
  public function update_daily_report_per_column() {
    $response = [];

    try {
      $param = $this->input->post();

      $update = $this->daily_report_model->update_daily_report_per_column($param);    

      $response['status'] = 'success';
      $response['data'] = true;
    } catch (Exception $e) {
      $response['status'] = 'error';
      $response['error_message'] = $e->getMessage();
    }

    echo json_encode($response);
  }

  public function print_to_device($docno){
    $pdf = $this->pdf();

    $file_name = 'Daily Menu Seatshop ' . date('Y-m-d') . '.pdf';

    $data['daily_menu'] = $this->daily_report_model->get_list_daily_report_by_docno($docno)['result'];

    $html = $this->load->view('daily/report_pdf', $data, true); 
    $pdf->writeHTML($html, true, false, true, false, 'center');
    
    // set javascript
    $pdf->IncludeJS('print(true)');

    $pdf->Output($file_name, 'I');
  }

  private function pdf(){
    $this->load->library('tcpdf/tcpdf');
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('PT SISI');
    $pdf->SetTitle('Daily Menu Seatshop');
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);

    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    $pdf->AddPage('L', 'A4');
    $pdf->SetFont('helvetica', '', 10);
    return $pdf;
  }
}