<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('revision_model');
    $this->load->model('global_model');
    $this->load->model('progress_status_model');

    is_has_access('Dashboard');
  }

  public function index() {
    $data['page'] = 'Dashboard';
    $data['script'] = 'dashboard/index_script';

    $data['top_project'] = $this->revision_model->get_top_project();
    $data['progress_status_label'] = $this->progress_status_model->get_label();

    $this->view('dashboard/index', $data);
  }

  public function detail_project($revision) {
    $data['page'] = 'Dashboard';
    $data['script'] = 'dashboard/detail_project_script';

    $data['detail_project'] = $this->revision_model->get_detail_project($revision);
    
    if (count($data['detail_project']) > 0) {
      $this->view('dashboard/detail_project', $data);
    } else {
      redirect('dashboard', 'refresh');
    }
  }
}