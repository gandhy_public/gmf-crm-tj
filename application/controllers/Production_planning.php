<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production_planning extends MY_Controller {

  public function __construct() {
    parent::__construct();  
    $this->load->model('revision_model');
    $this->load->model('area_model');
  }

  public function index() {
    is_has_access('Production Planning');

    $data['page'] = 'Production Planning';
    $data['script'] = 'production_planning/planner_script';

    $data['area'] =$this->area_model->get_only('AREA');
    $data['phase'] =$this->area_model->get_only('PHASE');
    
    $this->view('production_planning/planner', $data);
  }
}