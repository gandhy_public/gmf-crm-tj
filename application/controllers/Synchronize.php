<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Synchronize extends MY_Controller {

  public function __construct() {
    parent::__construct();
  }

  public function index() {
    is_has_access('Synchronize');

    $data['page'] = 'Synchronize';
    $data['script'] = 'synchronize/index_script';

    $this->view('synchronize/index', $data);
  }

}