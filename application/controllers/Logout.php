<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends CI_Controller {

	public function index() {
		setcookie('crm_tj_menu', null, -1, '/');
		setcookie('crm_tj_remember', null, -1, '/');
		setcookie('crm_tj_monitoring_prod', null, -1, '/');
		setcookie('crm_tj_monitoring_daily', null, -1, '/');
		$this->session->unset_userdata('crm_sess_logged_in');
		$this->session->sess_destroy();
		// $this->session->set_flashdata('alert_logout', 'You are Logout from CRM - TC, Please login for Next :)');
		redirect('/', 'refresh');
	}
}
