<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Production_control extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('revision_model');
    $this->load->model('pmorder_model');
    $this->load->model('plant_model');
    $this->load->model('progress_status_model');
    $this->load->model('work_center_model');
  }
  
  public function index() {
    redirect(base_url('index.php/production_control/all_order'), 'refresh');
  }

  public function all_order() {
    is_has_access('All');

    $plant = $this->plant_model->get_all();
    $work_center_group_select = $this->work_center_model->get_grouping_select();
    $progress_status = $this->progress_status_model->get_without_progress();
    $status_sync_order = $this->pmorder_model->get_status_check('SYNC_ORDER');
    
    $work_center_group = get_session('work_center_group');
    $work_center = get_session('work_center');
    
    if($work_center_group == 'Hangar') {
      array_splice($progress_status, 3, 0, ['Progress in Hangar']);
    } else if($work_center_group == 'Shop'){
      $i = 3;
      foreach ($work_center as $val) {
        array_splice($progress_status, $i, 0, ['Progress in '.$val]);
        $i++;
      }
    } else {
      $progress_status = $this->progress_status_model->get();
    }

    $data['page'] = 'Production Control';
    $data['script'] = 'production_control/index_script';

    $data['progress_status'] = $progress_status;
    $data['work_center_group_select'] = $work_center_group_select;
    $data['material_status'] = ['Actual Nil Stock', 'Ordered By Purchasing', 'Shipment/Custom Process', 'Provision in Store', 'Preloaded in Hangar/Shop Store', 'Delivered to Production'];
    $data['responsibility'] = ['(GAH) Hangar', '(WSCB) Monument Shop', '(WSSS) Seat Shop 1', '(WSSE) Seat Shop 2', '(WSSW) Sewing Shop', '(WSLS) Laundry Shop'];
    $data['status_sync_order'] = $status_sync_order;

    $this->view('production_control/index', $data);
  }

  public function new_order() {
    is_has_access('New');

    $data['page'] = 'Production Control';
    $data['script'] = 'production_control/new_script';

    $this->view('production_control/new', $data);
  }

  public function close_order() {
    is_has_access('Close');

    $plant = $this->plant_model->get_all();
    $progress_status = $this->progress_status_model->get_without_progress();
    
    $work_center_group = get_session('work_center_group');
    $work_center = get_session('work_center');

    if($work_center_group == 'Hangar') {
      array_splice($progress_status, 3, 0, ['Progress in Hangar']);
    } else if($work_center_group == 'Shop'){
      $i = 3;
      foreach ($work_center as $val) {
        array_splice($progress_status, $i, 0, ['Progress in '.$val]);
        $i++;
      }
    } else {
      $progress_status = $this->progress_status_model->get();
    }

    // remove close from list progress status
    unset($progress_status[count($progress_status) - 1]);

    $data['page'] = 'Production Control';
    $data['script'] = 'production_control/close_script';
    $data['progress_status'] = $progress_status;
    $this->view('production_control/close', $data);
  }

  public function view_order() {
    $data['page'] = 'Production Control';
    $data['script'] = 'production_control/view_order_script';

    $this->view('production_control/view_order', $data);
  }

  public function view_material() {
    $data['page'] = 'Production Control';
    $data['script'] = 'production_control/view_material_script';

    $this->view('production_control/view_material', $data);
  }

  public function monitoring() {
    is_has_access('Production Control');

    setcookie('crm_tj_monitoring_prod', true, 2147485657, "/");

    $data['top_project'] = $this->revision_model->get_top_project();
    $data['progress_status_label'] = $this->progress_status_model->get_label();
    
    $this->load->view('production_control/monitoring', $data);
  }
}